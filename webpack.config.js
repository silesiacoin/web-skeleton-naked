const path = require('path');

module.exports = {
    entry: './lib/js/index.js',
    target: 'web',
    module: {
        rules: [
            {
                test: /\.(js|jsx)$/,
                exclude: /^(node_modules)$/,
                loader: 'babel-loader?minimize=false',
                query: {
                    presets: ['@babel/preset-react', '@babel/preset-env']
                }
            },
            {
                test: /\.scss$/,
                use: [
                    {
                        loader: "style-loader" // creates style nodes from JS strings
                    },
                    {
                        loader: "css-loader" // translates CSS into CommonJS
                    },
                    {
                        loader: "sass-loader" // compiles Sass to CSS
                    }
                ]
            }
        ]
    },
    node: {
        fs: 'empty'
    },
    resolve: {
        extensions: ['.js', '.jsx']
    },
    output: {
        path: path.resolve(__dirname, 'public/assets/javascripts/silesiacoin'),
        publicPath: '/',
        filename: 'main.js'
    },
};
