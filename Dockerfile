# This Dockerfile uses the latest version of the silesiacoin web image
FROM silesiacoin/php72:ubuntu

# Copy app's source code to the /app directory
COPY ./.docker/public/etc/nginx/conf.d/default.conf /etc/nginx/sites-available/default
COPY ./.docker/public/etc/nginx/conf.d/default.conf /etc/nginx/sites-enabled/default
COPY ./.docker/public/etc/supervisor/supervisord.conf /etc/supervisor/supervisord.conf

# The application's directory will be the working directory
WORKDIR /var/www/app

EXPOSE 80 8080 443 5000
CMD ["/usr/bin/supervisord", "-c", "/etc/supervisord.conf"]
