[Silesiacoin]

[![js-standard-style](https://img.shields.io/badge/code%20style-standard-brightgreen.svg)](http://standardjs.com)

Requires: `docker and docker-compose` 

1. run `sudo bin/run` within root of project with no arguments => => add to role: `ROLE_SUPER_ADMIN`

WARNING:
Do this if you want to wipe out all data and start within new fresh app

HOSTS
===========
This application routing depends on the hosts. The easiest way is to edit Your 

1. unix/mac `/etc/hosts`
2. windows `c:\windows\system32\drivers\etc\hosts`

into following:

`
127.0.66.1     silesiacoin.local
127.0.66.1     price.silesiacoin.local
127.0.66.1     account.silesiacoin.local
127.0.66.1     api.silesiacoin.local
127.0.66.1     secure.silesiacoin.local
127.0.66.1     docs.silesiacoin.local
127.0.66.1     download.silesiacoin.local
127.0.66.1     news.silesiacoin.local
127.0.66.1     wallet.silesiacoin.local
127.0.66.1     authencticator.silesiacoin.local
`

https://www.howtogeek.com/howto/27350/beginner-geek-how-to-edit-your-hosts-file/

 
 
 
INFRASTRUCTURE
================
`docker-compose.override.yml.dist` file should represent current state of infrastructure. If you want dive in further info check each docker image.
PHP is set up in `ssc-web` image
NODEJS is set up in `nodejs` image
In .dist file nodejs runs in watch mode, so any change that you make is automaticaly transpiled.


 
Roles Management
================

To see roles management run `php bin/console |grep acc` within container


Blockchain Node
==============
Blockchain node now is running after deploying container.
If You have issues with container, best solution is to update image.
Type this on Your machine/cluster:

`docker rmi silesiacoin/php72 && docker pull silesiacoin/php72`

Try to stay updated with main docker hub!
https://hub.docker.com/r/silesiacoin


Start docker containers and open shell
==============
`docker-compose up -d`

`docker-compose exec ssc-web sh`


Update branch
============
pull to Your branch from origin/master
type `composer deploy` to fully deploy changes from remote


Deploy changes
==============
Branch must be compatible with origin/master. See `update branch` in above.
Before making pull request do run within container:
`composer prepare-mr`

If there are still unfixable errors, Your code is not proper. Fix it.


ESLint
==============
Before push your changes, check your files with ESLint.
Usage in shell: `composer your-file-path.js`
Config file: `root/.eslintrc.json`
Documentation: https://eslint.org/docs/rules/


TESTS
==============
In javascript (Integration Tests) we use fetchMock to work on contracts that are mocked

To run JS test that watch for changes in code run in `nodejs` container:
`yarn run test-watch`

to run full test suite from project run in your host (containers must be up and running):
`bin/run test`

FEATURE_FLAGS
==============
Feature flags are used to deliver continous integration. There are two types of features:
- via cookie (you should set cookie that enables feature)
- roles (your account should have certain role in system to gain access to feature)
- ssc_not_finished (you should set a cookie that enables not finished components)

LICENSE
==============
Silesiacoin web project is [MIT licensed](./LICENSE).
