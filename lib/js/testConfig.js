// setup file for react tests
import Adapter from 'enzyme-adapter-react-16';
import { configure } from 'enzyme';

export const resetWindow = (mainContainerName) => {
    if ('string' !== typeof mainContainerName) {
        mainContainerName = 'angband';
    }

    /* setup.js */
    const { JSDOM } = require('jsdom');
    configure({ adapter: new Adapter() });
    const jsdom = new JSDOM(`<!doctype html><html><body><div id="${mainContainerName}"></div></body></html>`, {
        url: 'http://silesiacoin.org'
    });
    const { window } = jsdom;

    function copyProps(src, target) {
        Object.defineProperties(target, {
            ...Object.getOwnPropertyDescriptors(src),
            ...Object.getOwnPropertyDescriptors(target),
        });
    }

    global.window = window;
    global.document = window.document;
    global.navigator = {
        userAgent: 'node.js',
    };
    global.requestAnimationFrame = function (callback) {
        return setTimeout(callback, 0);
    };
    global.cancelAnimationFrame = function (id) {
        clearTimeout(id);
    };
    copyProps(window, global);
};

export const setWindowWithLocation = (hostname, protocol, pathname, hash = '', port = 80) => {
    if ('undefined' === typeof global.window) {
        resetWindow();
    }

    delete window.location;

    window.location = {
        host: hostname,
        hostname: hostname,
        protocol: protocol,
        pathname: pathname,
        port: port,
        hash: hash,
        origin: `${protocol}//${hostname}`,
        href: `${protocol}//${hostname}/${pathname}${hash}`
    }
};

export const withSscLocation = ((pathname = '', hash = '', protocol = 'https:', port = 80) => {
    setWindowWithLocation(
        'silesiacoin.org',
        protocol,
        pathname,
        hash,
        port
    )
});

resetWindow();


