let expect = require('expect.js');

describe('name Of The Test context', function () {
    it('name of simple test', function () {
        const test = 'testString';
        expect(test).to.be.equal('testString');
    });
});
