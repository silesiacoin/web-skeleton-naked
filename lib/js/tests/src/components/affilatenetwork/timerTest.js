import React from 'react';
import {shallow} from "enzyme/build";
import {resetWindow} from "../../../../testConfig";
import {Timer} from "../../../../src/components/affiliatenetwork/timer";
import validationError from "../../../../src/error/validationError";

const expect = require('expect.js');

describe('<Timer/> unit test', () => {
    beforeEach(() => {
        resetWindow();
    });

    after(() => resetWindow());

    it('should not show when revDate is not set', (done) => {
        try {
            new Timer({currentDate: Date.now()});
        } catch (error) {
            expect(error.message).to.be.equal('revDate is invalid');
            done();

        }
    });

    it('Should throw error when revDate is invalid', (done) => {
        window.revDate = "AABBBCCCDDD";

        try {
            new Timer({currentDate: Date.now()});
        } catch (error) {
            expect(error.message).to.be.equal('revDate is invalid');
            done();
        }
    });

    it('Should throw error when revDate is null', (done) => {
        window.revDate = '';

        try {
            new Timer({currentDate: Date.now()});
        } catch (error) {
            expect(error.message).to.be.equal('revDate is invalid');
            done();
        }
    });

    it('Should not throw any errors when revDate is valid ', () => {
        window.revDate = '2020/01/01';

        const timer = new Timer({currentDate: Date.now()});
        const state = timer.state;
        const seconds = state.seconds;
        expect(typeof seconds).to.be.equal('number');
    });

    it('Should render date', () => {
        const futureDateFromNow = setRevDateWithWeeksFromNow();
        const timer = new Timer({currentDate: futureDateFromNow.getTime()});
        //timer.componentDidMount();
        expect(typeof timer.state).to.be.equal('object');
        expect(typeof timer.state.currentDate).to.be.equal('number');
        const dateNowMinusInterval = Date.now() - 20;
        expect(timer.state.currentDate).to.be.greaterThan(dateNowMinusInterval);
        expect(typeof timer.state.seconds).to.be.equal('number');

        const secondsOutcome = Math.floor((futureDateFromNow - timer.state.currentDate) / 1000);
        const secondsAccuracyAssertion = secondsOutcome - timer.state.seconds < 100;
        expect(secondsAccuracyAssertion).to.be.equal(true);
    });

    it('Should do proper countdown', () => {
        const futureDateFromNow = setRevDateWithWeeksFromNow();
        const timer = new Timer({currentDate: futureDateFromNow});
       // timer.componentDidMount();
        const secondsBeforeCountdown = timer.state.seconds;
        const secondsAfterCountdown = timer.countdown();
        const secondsCalc = () => secondsBeforeCountdown - secondsAfterCountdown;
        expect(secondsCalc()).to.be.greaterThan(0);
    });
});

describe('<Timer> react test', () => {
    beforeEach(() => {
        resetWindow();
    });

    after(() => resetWindow());

    it('Should not render anything due to lack of env rev date variable', (done) => {
        try {
            shallow(<Timer/>);
        } catch (error) {
            expect(error).to.be.an(validationError);
            done();
        }
    });

    it('Should not render anything with wrong future date', () => {
        const futureDateFromNow = setRevDateWithWeeksFromNow(-1);
        const timer = shallow(<Timer currentDate={futureDateFromNow}/>);
        const state = timer.state();
        expect(typeof state.seconds).to.be.equal('number');
        expect(state.seconds).to.be.equal(0);
        const timerChildren = timer.children();
        expect(timerChildren.length).to.be.equal(0);
    });

    it('Should not render anything with seconds < 1', () => {
        const futureDateFromNow = setRevDateWithWeeksFromNow(0);
        const timer = shallow(<Timer currentDate={futureDateFromNow} />);
        const state = timer.state('seconds');
        expect(typeof state).to.be.equal('number');
        expect(state).to.be.lessThan(1);
        const timerChildren = timer.children();
        expect(timerChildren.length).to.be.equal(0);
    });
});

function setRevDateWithWeeksFromNow(weeks) {
    if ('undefined' === typeof weeks) {
        weeks = 1;
    }

    const futureDateFromNow = new Date();
    // Set week from now
    futureDateFromNow.setDate(futureDateFromNow.getDate() + weeks * 7);
    window.revDate = futureDateFromNow;

    return futureDateFromNow;
}
