import React from 'react';
import RodoInfo from '../../../../src/components/generic/rodoInfo';
import CookieService from '../../../../src/_redux/services/cookieService';
import CookieStruct from '../../../../src/struct/cookieStruct';
import { shallow } from 'enzyme/build';
import { resetWindow } from '../../../../testConfig';
const expect = require('expect.js');

describe('<RodoInfo/> unit test', () => {
    beforeEach(() => {
        resetWindow();
    });

    it('should not show when cookies are set', () => {
        CookieService.setCookie(new CookieStruct('ssc_rodo_agreement', 'dummy'));
        const rodoInfoComponent = new RodoInfo();
        expect(rodoInfoComponent.shouldRender).to.be.equal(false);
    });

    it('should show when cookies are not set', () => {
        const rodoInfoComponent = new RodoInfo();
        expect(rodoInfoComponent.shouldRender).to.be.equal(true);
    });

    it('should set cookies when handle agreement', () => {
        const rodoInfoComponent = new RodoInfo();
        let rodoAgreement = CookieService.getCookie('ssc_rodo_agreement');
        expect(rodoAgreement).to.be.equal('');
        rodoInfoComponent.handleAgreement();
        rodoAgreement = CookieService.getCookie('ssc_rodo_agreement');
        expect(rodoAgreement).to.be.equal('accepted');
        rodoInfoComponent.handleAgreement();
        expect(rodoAgreement).to.be.equal('accepted');
    });
});

describe('<RodoInfo/> react test', () => {
    beforeEach(() => {
        resetWindow();
    });

    it('should hide when clicked on button and set cookie', () => {
        const rodoInfo = shallow(<RodoInfo/>);
        rodoInfo.find('button').simulate('click');
        let rodoAgreement = CookieService.getCookie('ssc_rodo_agreement');
        expect(rodoAgreement).to.be.equal('accepted');
        expect(rodoInfo.find('#hiddenDiv').hasClass('hidden')).to.be.equal(true);
        expect(rodoInfo.find('#rodoDiv').hasClass('rodo-div')).to.be.equal(true);
        expect(rodoInfo.find('button').hasClass('rodo-button')).to.be.equal(true);
        expect(rodoInfo.find('.disclosure').exists()).to.be.equal(true);
    });
});
