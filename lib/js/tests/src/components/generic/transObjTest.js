import TransObject from '../../../../src/components/generic/transObject';
import React from 'react';
import { shallow } from 'enzyme';
import ValidationError from '../../../../src/error/validationError';
import TranslationStruct from '../../../../src/struct/translationStruct';
import translatorService from "../../../../src/_redux/services/translatorService";
import { translationActions } from '../../../../src/_redux/actions/translationActions';
import { translationConstants } from '../../../../src/_redux/constants/translationConstants';
const expect = require('expect.js');

describe('<TransObject/> component test', () => {
    let transObj;
    let props;
    let state;
    let store;

    beforeEach(() => {
        delete window.translationStore;
        transObj = shallow(<TransObject translationKey={'abcd'}/>);
        props = transObj.props();
        state = transObj.state();
        store = translatorService.getTranslationStore();
    });

    it('should create component, that is not yet translated', () => {
        expect(state.translationKey).to.be.equal('abcd');
        expect(state.translation).to.be.equal('');
        expect(state.translated).to.be.equal(false);
    });

    it('should fail to create component without translationKey', () => {
        expect(() => {
            shallow(<TransObject/>)
        }).to.throwError((e) => {
            expect(e).to.be.an(ValidationError);
        });
    });

    it('should throw error when invalid object provided for translation', () => {
       expect(() => {
           transObj.instance().changeTranslation({});
       }).to.throwError((e) => {
           expect(e).to.be.an(ValidationError);
           expect(e.message).to.be.equal('translationStruct is not instance of TranslationStruct');
       })
    });

    it('should change translation', () => {
        const translationStruct = new TranslationStruct(
            'te',
            state.translationKey,
            'dummyTranslation'
        );
        const instance = transObj.instance();

        instance.changeTranslation(translationStruct);
        state = transObj.state();
        props = transObj.props();
        expect(state.translation).to.be.equal('dummyTranslation');
        expect(state.translated).to.be.equal(true);
        expect(state.translationKey).to.be.equal('abcd');
        expect(state.locale).to.be.equal('te');
    });

    it('should not change translation via store, when keys does not match', (done) => {
        const translationStruct = new TranslationStruct(
            'te',
            'not.matching.key',
            'dummyTranslation'
        );

        store.subscribe(() => {
            const storeState = store.getState();
            state = transObj.state();

            if (translationConstants.EMIT_TRANSLATION === storeState.type) {
                if (state.translationKey !== storeState.translationKey) {
                    expect(state.locale).to.be.equal('en');
                    done();
                }
            }
        });

        store.dispatch(translationActions.emitTranslation(translationStruct));
    });

    it('should change translation only once via store, when keys match', (done) => {
        const translationStruct = new TranslationStruct(
            'te',
            state.translationKey,
            'dummyTranslation'
        );
        const translationStruct1 = new TranslationStruct(
            'hg',
            state.translationKey,
            'dummyTranslation1'
        );
        let counter = 0;

        store.subscribe(() => {
            const storeState = store.getState();
            const isEmitting = () => translationConstants.EMIT_TRANSLATION === storeState.type;
            const isValid = () => state.translationKey === storeState.translationKey;
            const shouldEnd = () => isValid() && counter === 1;
            const expectStructValidStruct = () => {
                expect(state.locale).to.be.equal(translationStruct.locale);
                expect(state.translationKey).to.be.equal(translationStruct.translationKey);
                expect(state.translation).to.be.equal(translationStruct.translation);
            };

            state = transObj.state();

            if (isEmitting()) {
                if (isValid()) {
                    expectStructValidStruct();
                    ++counter;
                }

                if (shouldEnd()) {
                    expectStructValidStruct();
                    done();
                }
            }
        });

        store.dispatch(translationActions.emitTranslation(translationStruct));
        store.dispatch(translationActions.emitTranslation(translationStruct1));
    });
});
