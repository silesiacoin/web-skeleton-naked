import LanguageSwitch from '../../../../src/components/modals/languageSwitch';
import React from 'react';
import { shallow, render } from 'enzyme';
import CookieService from '../../../../src/_redux/services/cookieService';
import CookieStruct from '../../../../src/struct/cookieStruct';
import ValidationError from '../../../../src/error/validationError';
import { resetWindow, withSscLocation } from '../../../../testConfig';
const expect = require('expect.js');

describe('<LanguageSwitch/> component test', () => {
    let languageSwitch;
    let state;

    beforeEach(() => {
        withSscLocation();
        languageSwitch = undefined;
        state = undefined;
    });

    after(() => resetWindow());

    it('should create object properly', () => {
        CookieService.setCookie(new CookieStruct('ssc_language', 'pl'));
        expect(() => shallow(<LanguageSwitch/>)).to.be.ok();
        CookieService.setCookie(new CookieStruct('ssc_language', 'en'));
        languageSwitch = shallow(<LanguageSwitch/>);
        expect(languageSwitch).to.be.ok();
        state = languageSwitch.state();
        expect(state.currentLanguage).to.be.equal('en');
    });

    it('should set default EN when no cookie', () => {
        languageSwitch = shallow(<LanguageSwitch/>);
        state = languageSwitch.state();
        expect(state.currentLanguage).to.be.equal('en');
    });

    it('should return current language', () => {
        CookieService.setCookie(new CookieStruct('ssc_language', 'pl'));
        languageSwitch = shallow(<LanguageSwitch/>);
        state = languageSwitch.state();
        expect(state.currentLanguage).to.be.equal('pl');
        languageSwitch = render(<LanguageSwitch/>);
        const children = languageSwitch.children();
        expect(children[0].name).to.be.equal('form');
    });

    it('should switch language', () => {
        CookieService.setCookie(new CookieStruct('ssc_language', 'pl'));
        languageSwitch = new LanguageSwitch();
        languageSwitch.switchLanguage('en');
        const locationHref = window.location.href;
        expect(locationHref.search('ssc_language=en')).to.be.greaterThan(-1);
    });

    it('should throw error when invalid switch language', () => {
        CookieService.setCookie(new CookieStruct('ssc_language', 'pl_PL'));
        languageSwitch = new LanguageSwitch();
        expect(() => languageSwitch.switchLanguage()).to.throwError((e) => {
            expect(e).to.be.an(ValidationError);
        });
        expect(() => languageSwitch.switchLanguage({})).to.throwError((e) => {
            expect(e).to.be.an(ValidationError);
        });
        expect(() => languageSwitch.switchLanguage([])).to.throwError((e) => {
            expect(e).to.be.an(ValidationError);
        });
        expect(() => languageSwitch.switchLanguage('az')).to.throwError((e) => {
            expect(e).to.be.an(ValidationError);
        });
    });
});
