import React from 'react';
import { shallow } from 'enzyme';
import Authenticator from '../../../../src/components/modals/authenticator';
import { resetWindow, withSscLocation } from '../../../../testConfig';
import AuthenticatorService from '../../../../src/_redux/services/authenticatorService';
import { authenticationActions } from '../../../../src/_redux/actions/authenticationActions';
import TranslationStruct from '../../../../src/struct/translationStruct';
import fetchMock from 'fetch-mock';
import TranslatorService from '../../../../src/_redux/services/translatorService';
import { translationActions } from '../../../../src/_redux/actions/translationActions';
import expect from 'expect.js';
import CookieService from '../../../../src/_redux/services/cookieService';
import CookieStruct from '../../../../src/struct/cookieStruct';
import YourWallets from '../../../../src/components/dashboard/yourWallets';
import Wallets from '../../../../src/components/dashboard/wallets';

describe('<Authenticator/> component test', () => {
    const translationStruct = new TranslationStruct(
        'en',
        'dummy.key',
        'This is key for dummy'
    );
    const translatedStruct = new TranslationStruct(
        'pl',
        'dummy.key',
        'To jest translacja kukly'
    );
    const loginUrls = {
        facebookUrl: 'https://fb.logme',
        googleUrl: 'https://goo.le'
    };
    let authenticator;
    let state;
    let children;

    beforeEach(() => {
        CookieService.setCookie(new CookieStruct('ssc_not_finished', 'true'));
        window.csrf ='DUMMYCSRF';
        withSscLocation();
        fetchMock.reset();
        fetchMock.mock('glob:https://*/i18n/en?*', { body: JSON.stringify([translationStruct]) });
        fetchMock.mock('glob:https://*/i18n/pl?*', { body: JSON.stringify([translatedStruct]) });
        fetchMock.mock('glob:https://*/i18n/dummyPath/*', 404);
        delete window.translationStore;

        const store = TranslatorService.getTranslationStore();
        store.dispatch(translationActions.addToTranslationQueue('dummy.key'));
        let state;

        store.subscribe(() => {
            state = store.getState();
        });
    });

    afterEach(() => {
        state = undefined;
        authenticator = undefined;
        children = undefined;
    });

    after(() => {
        resetWindow();
    });

    it('should be hidden after feature flag', () => {
        resetWindow();
        withSscLocation();
        window.csrf ='DUMMYCSRF';
        const authenticator = shallow(<Authenticator/>);
        const state = authenticator.state();
        expect(state.enabledFeature).to.be.equal(false);
        expect(authenticator.find('ssc_not_finished').exists()).to.be.equal(false);
    });

    it('should create component', () => {
        const authenticator = shallow(<Authenticator/>);
        expect(authenticator).to.be.ok();
    });

    it('should be open', () => {
        const authenticator = shallow(<Authenticator modal={true}/>);
        let state = authenticator.state();
        expect(state.modal).to.be.equal(true);
    });

    it('should be able to open if the user is not logged in', () => {
        let authenticator = shallow(<Authenticator modal={false}/>);
        let yourWallets = shallow(<YourWallets numberOffWallets={1}/>);
        const wallets = shallow(<Wallets/>);
        let state = authenticator.state();
        expect(state.modal).to.be.equal(false);
        yourWallets.find('#show-modal-desktop').simulate('click');
        state = authenticator.state();
        expect(state.modal).to.be.equal(false);
        yourWallets = shallow(<YourWallets numberOffWallets={false}/>);
        yourWallets.find('#show-modal-desktop').simulate('click');
        state = authenticator.state();
        expect(state.modal).to.be.equal(true);
        authenticator = shallow(<Authenticator modal={false}/>);
        state = authenticator.state();
        expect(state.modal).to.be.equal(false);
        wallets.find('#show-authentication-modal').simulate('click');
        state = authenticator.state();
        expect(state.modal).to.be.equal(true);
    });

    it('should have register form', () => {
        const authenticator = shallow(<Authenticator/>);
        authenticator.find('#overlap-register').simulate('click');
        const registerBody = authenticator.find('RegisterBody');
        expect(registerBody.exists()).to.be.equal(true);
    });

    it('should have login form', () => {
        const authenticator = shallow(<Authenticator/>);
        authenticator.find('#overlap-login').simulate('click');
        const loginBody = authenticator.find('LoginBody');
        expect(loginBody.exists()).to.be.equal(true);
    });

    it('should exist', () => {
        const authenticator = shallow(<Authenticator/>);
        children = authenticator.children();
        expect(children.length).to.be.equal(2);
        const modal = authenticator.find('Modal');
        expect(modal.exists()).to.be.equal(true);
        const modalBody = authenticator.find('ModalBody');
        children = modalBody.children();
        expect(children.length).to.be.equal(1);
    });

    it('should dispatch login urls after mounting the component', () => {
        const store = AuthenticatorService.getAuthenticatorStore();
        const jsonLoginUrls = JSON.stringify(loginUrls);
        store.dispatch(authenticationActions.registerLoginUrlsInStore(loginUrls));
        authenticator = shallow(<Authenticator/>);

        const storeState = store.getState();
        const storeLoginUrls = storeState.loginUrls;
        expect(JSON.stringify(storeLoginUrls)).to.be.equal(jsonLoginUrls);

        const fbLoginComponent = authenticator.find('.facebookLoginUrl');
        expect(fbLoginComponent.exists()).to.be.equal(true);

        const googleLoginComponent = authenticator.find('.googleLoginUrl');
        expect(googleLoginComponent.exists()).to.be.equal(true);
    });
});
