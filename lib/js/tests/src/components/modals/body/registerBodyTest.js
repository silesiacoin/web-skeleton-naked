import React from 'react';
import { resetWindow, withSscLocation } from '../../../../../testConfig';
import RegisterBody from '../../../../../src/components/modals/body/registerBody';
import chai from 'chai';
import spies from 'chai-spies';
import { shallow } from 'enzyme';
import expect from 'expect.js';
import TranslationStruct from '../../../../../src/struct/translationStruct';
import fetchMock from 'fetch-mock';
import TranslatorService from '../../../../../src/_redux/services/translatorService';
import { translationActions } from '../../../../../src/_redux/actions/translationActions';
import Dashboard from '../../../../../src/containers/dashboard';
import YourWallets from '../../../../../src/components/dashboard/yourWallets';
import Wallets from '../../../../../src/components/dashboard/wallets';
import PortableWallet from '../../../../../src/components/dashboard/portableWallet';

describe('<RegisterBody/> unit test', () => {
    const translationStruct = new TranslationStruct(
        'en',
        'dummy.key',
        'This is key for dummy'
    );
    const translatedStruct = new TranslationStruct(
        'pl',
        'dummy.key',
        'To jest translacja kukly'
    );

    before(() => chai.use(spies));

    beforeEach(() => {
        withSscLocation();
        fetchMock.reset();
        fetchMock.mock('glob:https://*/i18n/en?*', { body: JSON.stringify([translationStruct]) });
        fetchMock.mock('glob:https://*/i18n/pl?*', { body: JSON.stringify([translatedStruct]) });
        fetchMock.mock('glob:https://*/i18n/dummyPath/*', 404);
        delete window.translationStore;

        const store = TranslatorService.getTranslationStore();
        store.dispatch(translationActions.addToTranslationQueue('dummy.key'));
        let state = {
            available: true
        };

        store.subscribe(() => {
            state = store.getState();
        });
    });

    after(() => resetWindow());

    it('should download account json file', (done) => {
        const htmlAnchor = document.createElement('a');

        if ('function' !== typeof URL.createObjectURL) {
            chai.spy.on(URL, 'createObjectURL', () => {
                return 'http://dummy.com'
            });
        }

        chai.spy.on(htmlAnchor, 'click', () => {
            done();
        });
        chai.spy.on(global.document, 'createElement', (elementName) => {
            if ('a' === elementName) {
                return htmlAnchor;
            }
        });

        const registerBody = new RegisterBody({
            available: true
        });
        registerBody.registerUser();
    });
});

describe('<RegisterBody/> react test', () => {
    const translationStruct = new TranslationStruct(
        'en',
        'dummy.key',
        'This is key for dummy'
    );
    const translatedStruct = new TranslationStruct(
        'pl',
        'dummy.key',
        'To jest translacja kukly'
    );

    beforeEach(() => {
        withSscLocation();
        fetchMock.reset();
        fetchMock.mock('glob:https://*/i18n/en?*', { body: JSON.stringify([translationStruct]) });
        fetchMock.mock('glob:https://*/i18n/pl?*', { body: JSON.stringify([translatedStruct]) });
        fetchMock.mock('glob:https://*/i18n/dummyPath/*', 404);
        delete window.translationStore;

        const store = TranslatorService.getTranslationStore();
        store.dispatch(translationActions.addToTranslationQueue('dummy.key'));
        let state;

        store.subscribe(() => {
            state = store.getState();
        });
    });

    after(() => resetWindow());

    it('Should after register create first wallet address and display it in every place on dashboard', () => {
        let registerBody = shallow(<RegisterBody/>);
        const dashboard = shallow(<Dashboard/>);
        let registerBodyState = registerBody.state();
        expect(registerBodyState.available).to.be.equal(false);
        registerBody.find('#checkbox').simulate('click');
        registerBodyState = registerBody.state();
        expect(registerBodyState.available).to.be.equal(true);
        registerBody.find('#register-button-available').simulate('click');
        const dashboardState = dashboard.state();
        expect(dashboardState.wallet).to.not.be.equal(undefined);
        const yourWallets = shallow(<YourWallets wallet={dashboardState.wallet}/>);
        const yourWalletsState = yourWallets.state();
        expect(yourWalletsState.addresses[0].length > 0).to.be.equal(true);
        expect(yourWalletsState.numberOffWallets).to.be.equal(1);
        const wallets = shallow(<Wallets wallet={dashboardState.wallet}/>);
        const walletsState = wallets.state();
        expect(walletsState.addresses[0].length > 0).to.be.equal(true);
        expect(walletsState.numberOffWallets).to.be.equal(1);
        const portableWallet = shallow(<PortableWallet walletAddress={yourWalletsState.addresses[0]}/>);
        const portableWalletState = portableWallet.state();
        expect(portableWalletState.walletAddress.length > 0).to.be.equal(true);
    });

    it('should register and download account json file', () => {
        chai.use(spies);
        const registerBody = shallow(<RegisterBody/>);
        let state = registerBody.state();
        expect(state.available).to.be.equal(false);
        expect(state.mnemonic).to.not.be.equal('');
        registerBody.find('#checkbox').simulate('click');
        state = registerBody.state();
        expect(state.available).to.be.equal(true);
        registerBody.find('#register-button-available').simulate('click', () => {
            const htmlAnchor = document.createElement('a');
            chai.spy.on(htmlAnchor, 'click', () => {
                done();
            });
            chai.spy.on(global.document, 'createElement', (elementName) => {
                if ('a' === elementName) {
                    return htmlAnchor;
                }
            });
        });
        const walletObj = {
            version: state.version,
            mnemonic: state.mnemonic,
        };
        const json = JSON.stringify(walletObj);
        expect(registerBody.instance().mapWalletToJSON(state.version, state.mnemonic)).to.be.equal(json)
    });

    it('should show or hide validation error', () => {
        const registerBody = shallow(<RegisterBody/>);
        let state = registerBody.state();
        expect(state.formErrors.checkbox).to.be.equal('');
        registerBody.find('#register-button-available').simulate('click');
        state = registerBody.state();
        expect(state.formErrors.checkbox).to.be.equal('If you want to use our service, please do accept.');
        registerBody.find('#checkbox').simulate('click');
        state = registerBody.state();
        expect(state.formErrors.checkbox).to.be.equal('');
        registerBody.find('#checkbox').simulate('click');
        state = registerBody.state();
        expect(state.formErrors.checkbox).to.be.equal('If you want to use our service, please do accept.');
    });

    it('should refresh mnemonic', () => {
        const registerBody = shallow(<RegisterBody/>);
        let state = registerBody.state();
        const oldMnemonic = state.mnemonic;
        expect(state.mnemonic).to.be.equal(oldMnemonic);
        registerBody.find('#refresh-mnemonic').simulate('click');
        state = registerBody.state();
        expect(state.mnemonic).to.not.be.equal(oldMnemonic);
    });
});
