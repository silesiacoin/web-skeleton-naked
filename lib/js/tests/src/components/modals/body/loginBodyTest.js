import React from 'react';
import { resetWindow, withSscLocation } from '../../../../../testConfig';
import LoginBody from '../../../../../src/components/modals/body/loginBody';
import { shallow } from 'enzyme';
import expect from 'expect.js';
import TranslationStruct from '../../../../../src/struct/translationStruct';
import fetchMock from 'fetch-mock';
import TranslatorService from '../../../../../src/_redux/services/translatorService';
import { translationActions } from '../../../../../src/_redux/actions/translationActions';
import PortableWallet from '../../../../../src/components/dashboard/portableWallet';
import Dashboard from '../../../../../src/containers/dashboard';
import YourWallets from '../../../../../src/components/dashboard/yourWallets';
import Wallets from '../../../../../src/components/dashboard/wallets';

describe('<LoginBody/> react test', () => {
    const translationStruct = new TranslationStruct(
        'en',
        'dummy.key',
        'This is key for dummy'
    );
    const translatedStruct = new TranslationStruct(
        'pl',
        'dummy.key',
        'To jest translacja kukly'
    );

    beforeEach(() => {
        withSscLocation();
        fetchMock.reset();
        fetchMock.mock('glob:https://*/i18n/en?*', { body: JSON.stringify([translationStruct]) });
        fetchMock.mock('glob:https://*/i18n/pl?*', { body: JSON.stringify([translatedStruct]) });
        fetchMock.mock('glob:https://*/i18n/dummyPath/*', 404);
        delete window.translationStore;

        const store = TranslatorService.getTranslationStore();
        store.dispatch(translationActions.addToTranslationQueue('dummy.key'));
        let state;

        store.subscribe(() => {
            state = store.getState();
        });
    });

    after(() => resetWindow());

    it('Should after log in create first wallet address and display it in every place on dashboard', () => {
        let loginBody = shallow(<LoginBody/>);
        const dashboard = shallow(<Dashboard/>);
        let loginBodyState = loginBody.state();
        expect(loginBodyState.available).to.be.equal(false);
        loginBody = shallow(<LoginBody available={true}/>);
        loginBodyState = loginBody.state();
        expect(loginBodyState.available).to.be.equal(true);
        loginBody.find('#log-in').simulate('click');
        const dashboardState = dashboard.state();
        expect(dashboardState.wallet).to.not.be.equal(undefined);
        const yourWallets = shallow(<YourWallets wallet={dashboardState.wallet}/>);
        const yourWalletsState = yourWallets.state();
        expect(yourWalletsState.addresses[0].length > 0).to.be.equal(true);
        expect(yourWalletsState.numberOffWallets).to.be.equal(1);
        const wallets = shallow(<Wallets wallet={dashboardState.wallet}/>);
        const walletsState = wallets.state();
        expect(walletsState.addresses[0].length > 0).to.be.equal(true);
        expect(walletsState.numberOffWallets).to.be.equal(1);
        const portableWallet = shallow(<PortableWallet walletAddress={yourWalletsState.addresses[0]}/>);
        const portableWalletState = portableWallet.state();
        expect(portableWalletState.walletAddress.length > 0).to.be.equal(true);
    });

    it('should not log in', () => {
        let loginBody = shallow(<LoginBody/>);
        let state = loginBody.state();
        expect(state.available).to.be.equal(false);
        loginBody.find('#log-in').simulate('click');
        state = loginBody.state();
        expect(state.available).to.be.equal(false);
    });

    it('should show error', () => {
        let loginBody = shallow(<LoginBody/>);
        let state = loginBody.state();
        expect(state.error).to.be.equal('');
        loginBody.find('#log-in').simulate('click');
        state = loginBody.state();
        expect(state.error).to.be.equal('Incorrect mnemonic phrase.');
    });
});
