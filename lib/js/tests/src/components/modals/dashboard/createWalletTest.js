import React from 'react';
import { shallow } from 'enzyme/build';
import { resetWindow, withSscLocation } from '../../../../../testConfig';
import TranslationStruct from '../../../../../src/struct/translationStruct';
import fetchMock from 'fetch-mock';
import TranslatorService from '../../../../../src/_redux/services/translatorService';
import { translationActions } from '../../../../../src/_redux/actions/translationActions';
import YourWallets from '../../../../../src/components/dashboard/yourWallets';
import CreateWallet from '../../../../../src/components/modals/dashboard/createWallet';
const expect = require('expect.js');

describe('<CreateWallet/> react test', () => {
    const translationStruct = new TranslationStruct(
        'en',
        'dummy.key',
        'This is key for dummy'
    );
    const translatedStruct = new TranslationStruct(
        'pl',
        'dummy.key',
        'To jest translacja kukly'
    );

    let state;

    beforeEach(() => {
        withSscLocation();
        fetchMock.reset();
        fetchMock.mock('glob:https://*/i18n/en?*', { body: JSON.stringify([translationStruct]) });
        fetchMock.mock('glob:https://*/i18n/pl?*', { body: JSON.stringify([translatedStruct]) });
        fetchMock.mock('glob:https://*/i18n/dummyPath/*', 404);
        delete window.translationStore;

        const store = TranslatorService.getTranslationStore();
        store.dispatch(translationActions.addToTranslationQueue('dummy.key'));
        let state;

        store.subscribe(() => {
            state = store.getState();
        });
    });

    afterEach(() => {
        state = undefined;
    });

    after(() => resetWindow());

    it('should be able to open if the user is not logged in', () => {
        let createWallet = shallow(<CreateWallet/>);
        let state = createWallet.state();
        expect(state.createWalletModalActive).to.not.be.equal(true);
        let yourWallets = shallow(<YourWallets numberOffWallets={false}/>);
        yourWallets.find('#show-modal-desktop').simulate('click');
        state = createWallet.state();
        expect(state.createWalletModalActive).to.not.be.equal(true);
        yourWallets = shallow(<YourWallets numberOffWallets={1}/>);
        yourWallets.find('#show-modal-desktop').simulate('click');
        state = createWallet.state();
        expect(state.createWalletModalActive).to.be.equal(true);
    });
});
