import React from 'react';
import Header from '../../../../src/components/header/header';
import { shallow } from 'enzyme/build';
import { resetWindow } from '../../../../testConfig';
const expect = require('expect.js');

describe('<Header/> react test', () => {
    let headerComponent;
    let exists;

    beforeEach(() => {
        resetWindow();
        headerComponent = shallow(<Header/>);
    });

    it('should have language switch', () => {
        exists = headerComponent.find('LanguageSwitch').exists();
        expect(exists).to.be.equal(true);
    });
});
