import React from 'react';
import { shallow } from 'enzyme/build';
import Wallet from '../../../../src/components/dashboard/wallet';
import { resetWindow, withSscLocation } from '../../../../testConfig';
import TranslationStruct from '../../../../src/struct/translationStruct';
import fetchMock from 'fetch-mock';
import TranslatorService from '../../../../src/_redux/services/translatorService';
import { translationActions } from '../../../../src/_redux/actions/translationActions';
import CookieService from '../../../../src/_redux/services/cookieService';
import CookieStruct from '../../../../src/struct/cookieStruct';
import ApiUrlParser from '../../../../src/_helpers/urlParser';
const expect = require('expect.js');

describe('<Wallet/> react test', () => {
    const translationStruct = new TranslationStruct(
        'en',
        'dummy.key',
        'This is key for dummy'
    );
    const translatedStruct = new TranslationStruct(
        'pl',
        'dummy.key',
        'To jest translacja kukly'
    );

    beforeEach(() => {
        CookieService.setCookie(new CookieStruct('ssc_payment', 'true'));
        CookieService.setCookie(new CookieStruct('ssc_not_finished', 'true'));
        withSscLocation();
        fetchMock.reset();
        fetchMock.mock('glob:https://*/i18n/en?*', { body: JSON.stringify([translationStruct]) });
        fetchMock.mock('glob:https://*/i18n/pl?*', { body: JSON.stringify([translatedStruct]) });
        fetchMock.mock('glob:https://*/i18n/dummyPath/*', 404);
        const apiUrl = ApiUrlParser.createFromWindowLocation();
        fetchMock.mock('glob:' + ApiUrlParser.parseApiUrlWithDefaultProtocol(apiUrl, 'v1/coinpayments/basic-transaction/LTC/0'), {
            body: {
                "response": {
                    "error": "ok",
                    "result": {
                        "amount": "0.00000000",
                        "txn_id": "abcd1234",
                        "address": "abcd1234",
                        "confirms_needed": "3",
                        "timeout": 7200,
                        "checkout_url": "https:\/\/www.coinpayments.net\/index.php?cmd=checkout\u0026id=abcd1234\u0026key=abcd1234",
                        "status_url": "https:\/\/www.coinpayments.net\/index.php?cmd=status\u0026id=abcd1234\u0026key=abcd1234",
                        "qrcode_url": "https:\/\/www.coinpayments.net\/qrgen.php?id=abcd1234\u0026key=abcd1234"
                    }
                }
            }
        });

        delete window.translationStore;
        const store = TranslatorService.getTranslationStore();
        store.dispatch(translationActions.addToTranslationQueue('dummy.key'));
        let state;

        store.subscribe(() => {
            state = store.getState();
        });
    });

    after(() => resetWindow());

    it('should be able to buy ssc', async () => {
        const wallet = shallow(<Wallet ssc={'2'} canBuy={true}/>);
        let state = wallet.state();
        expect(state.overlapBuySscStepActive).to.be.equal('first');
        expect(state.qrCode).to.be.equal('');
        expect(state.transactionUrl).to.be.equal('');
        wallet.find('#buy-ssc').simulate('click');
        state = wallet.state();
        expect(state.overlapBuySscStepActive).to.be.equal('second');
    });

    it('should payment methods hidden after feature flag', () => {
        resetWindow();
        withSscLocation();
        window.csrf ='DUMMYCSRF';
        const wallet = shallow(<Wallet/>);
        const state = wallet.state();
        expect(state.enabledFeature).to.be.equal(false);
        expect(wallet.find('#ssc_payment').exists()).to.be.equal(false);
    });

    it('should transactions overlap hidden after feature flag', () => {
        resetWindow();
        withSscLocation();
        window.csrf ='DUMMYCSRF';
        const wallet = shallow(<Wallet/>);
        const state = wallet.state();
        expect(state.enabledFeatureNF).to.be.equal(false);
        expect(wallet.find('#overlap-transactions').exists()).to.be.equal(false);
    });

    it('Should check which overlap is active', () => {
        const wallet = shallow(<Wallet/>);
        wallet.find('#overlap-buy-ssc').simulate('click');
        expect(wallet.find('#overlap-buy-ssc').hasClass('overlap--active')).to.be.equal(true);
        expect(wallet.find('#overlap-transactions').hasClass('overlap--active')).to.be.equal(false);

        wallet.find('#overlap-transactions').simulate('click');
        expect(wallet.find('#overlap-buy-ssc').hasClass('overlap--active')).to.be.equal(false);
        expect(wallet.find('#overlap-transactions').hasClass('overlap--active')).to.be.equal(true);
    });
});
