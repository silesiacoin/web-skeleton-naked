import React from 'react';
import { shallow } from 'enzyme/build';
import Navbar from '../../../../src/components/dashboard/navbar';
import Account from '../../../../src/components/dashboard/account';
import YourWallets from '../../../../src/components/dashboard/yourWallets';
import Wallets from '../../../../src/components/dashboard/wallets';
import { resetWindow, withSscLocation } from '../../../../testConfig';
import TranslationStruct from '../../../../src/struct/translationStruct';
import fetchMock from 'fetch-mock';
import TranslatorService from '../../../../src/_redux/services/translatorService';
import { translationActions } from '../../../../src/_redux/actions/translationActions';
import CookieService from "../../../../src/_redux/services/cookieService";
import CookieStruct from "../../../../src/struct/cookieStruct";
const expect = require('expect.js');

describe('<Navbar/> react test', () => {
    const translationStruct = new TranslationStruct(
        'en',
        'dummy.key',
        'This is key for dummy'
    );
    const translatedStruct = new TranslationStruct(
        'pl',
        'dummy.key',
        'To jest translacja kukly'
    );

    beforeEach(() => {
        CookieService.setCookie(new CookieStruct('ssc_not_finished', 'true'));
        withSscLocation();
        fetchMock.reset();
        fetchMock.mock('glob:https://*/i18n/en?*', { body: JSON.stringify([translationStruct]) });
        fetchMock.mock('glob:https://*/i18n/pl?*', { body: JSON.stringify([translatedStruct]) });
        fetchMock.mock('glob:https://*/i18n/dummyPath/*', 404);
        delete window.translationStore;

        const store = TranslatorService.getTranslationStore();
        store.dispatch(translationActions.addToTranslationQueue('dummy.key'));
        let state;

        store.subscribe(() => {
            state = store.getState();
        });
    });

    after(() => resetWindow());

    it('Should have icons active or not', () => {
        let navbar = shallow(<Navbar profilePanelActive={true} mainPageActive={false} walletActive={false} yourWalletsActive={false}/>);
        const account = shallow(<Account/>);

        navbar.find('#show-main-sections-home').simulate('click');
        let state = navbar.state();
        expect(state.mainPageActive).to.be.equal(true);
        expect(state.profilePanelActive).to.be.equal(false);

        navbar.find('#show-main-sections-logo').simulate('click');
        state = navbar.state();
        expect(state.mainPageActive).to.be.equal(true);
        expect(state.profilePanelActive).to.be.equal(false);

        navbar.find('#show-your-wallets').simulate('click');
        state = navbar.state();
        expect(state.yourWalletsActive).to.be.equal(true);
        expect(state.profilePanelActive).to.be.equal(false);

        navbar = shallow(<Navbar profilePanelActive={false} mainPageActive={true} walletActive={false} yourWalletsActive={false}/>);
        navbar.find('#show-your-wallets').simulate('click');
        state = navbar.state();
        expect(state.mainPageActive).to.be.equal(false);
        expect(state.yourWalletsActive).to.be.equal(true);

        account.find('#show-profile-panel').simulate('click');
        state = navbar.state();
        expect(state.mainPageActive).to.be.equal(false);
        expect(state.profilePanelActive).to.be.equal(true);

        navbar.find('#show-main-sections-logo').simulate('click');
        state = navbar.state();
        expect(state.mainPageActive).to.be.equal(true);
        expect(state.yourWalletsActive).to.be.equal(false);

        navbar.find('#show-main-sections-home').simulate('click');
        state = navbar.state();
        expect(state.mainPageActive).to.be.equal(true);
        expect(state.yourWalletsActive).to.be.equal(false);

        account.find('#show-profile-panel').simulate('click');
        state = navbar.state();
        expect(state.profilePanelActive).to.be.equal(true);
        expect(state.yourWalletsActive).to.be.equal(false);

        navbar = shallow(<Navbar profilePanelActive={false} mainPageActive={false} walletActive={true} yourWalletsActive={false}/>);
        account.find('#show-profile-panel').simulate('click');
        state = navbar.state();
        expect(state.profilePanelActive).to.be.equal(true);
        expect(state.walletActive).to.be.equal(false);

        navbar.find('#show-main-sections-logo').simulate('click');
        state = navbar.state();
        expect(state.mainPageActive).to.be.equal(true);
        expect(state.walletActive).to.be.equal(false);

        navbar.find('#show-main-sections-home').simulate('click');
        state = navbar.state();
        expect(state.mainPageActive).to.be.equal(true);
        expect(state.walletActive).to.be.equal(false);
    });

    it('should have portable wallet when number of wallets is grater than 0', () => {
        let navbar = shallow(<Navbar profilePanelActive={true} mainPageActive={false} walletActive={false} yourWalletsActive={false}/>);
        const yourWallets = shallow(<YourWallets numberOffWallets={1}/>);
        const wallets = shallow(<Wallets numberOffWallets={1}/>);

        const walletSelectorAssertion = 'someDummyString';
        wallets.find('#portable-show-wallet-0').simulate('click', walletEvent(walletSelectorAssertion));
        let state = navbar.state();
        expect(state.mainPageActive).to.be.equal(false);
        expect(state.walletActive).to.be.equal(true);

        navbar = shallow(<Navbar profilePanelActive={false} mainPageActive={false} walletActive={false} yourWalletsActive={true}/>);
        yourWallets.find('#portable-show-wallet-0').simulate('click', walletEvent(walletSelectorAssertion));
        state = navbar.state();
        expect(state.yourWalletsActive).to.be.equal(false);
        expect(state.walletActive).to.be.equal(true);
    });

    function walletEvent(walletSelectorAssertion) {
        const eventMock = {
            target: {
                getAttribute: () => walletSelectorAssertion
            }
        };

        return eventMock;
    }
});
