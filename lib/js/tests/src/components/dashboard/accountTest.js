import React from 'react';
import {shallow} from 'enzyme/build';
import Account from '../../../../src/components/dashboard/account';
import { resetWindow, withSscLocation } from '../../../../testConfig';
import TranslationStruct from '../../../../src/struct/translationStruct';
import fetchMock from 'fetch-mock';
import TranslatorService from '../../../../src/_redux/services/translatorService';
import { translationActions } from '../../../../src/_redux/actions/translationActions';
const expect = require('expect.js');

describe('<Account/> react test', () => {
    const translationStruct = new TranslationStruct(
        'en',
        'dummy.key',
        'This is key for dummy'
    );
    const translatedStruct = new TranslationStruct(
        'pl',
        'dummy.key',
        'To jest translacja kukly'
    );

    beforeEach(() => {
        withSscLocation();
        fetchMock.reset();
        fetchMock.mock('glob:https://*/i18n/en?*', { body: JSON.stringify([translationStruct]) });
        fetchMock.mock('glob:https://*/i18n/pl?*', { body: JSON.stringify([translatedStruct]) });
        fetchMock.mock('glob:https://*/i18n/dummyPath/*', 404);
        delete window.translationStore;

        const store = TranslatorService.getTranslationStore();
        store.dispatch(translationActions.addToTranslationQueue('dummy.key'));
        let state;

        store.subscribe(() => {
            state = store.getState();
        });
    });

    after(() => resetWindow());

    it('Should show or hide Profile panel after click', () => {
        const panel = shallow(<Account/>);
        expect(panel.find('#profile-panel').hasClass('hidden')).to.be.equal(true);
        panel.find('#profile-avatar').simulate('click');
        expect(panel.find('#profile-panel').hasClass('hidden')).to.be.equal(false);
        panel.find('#profile-avatar').simulate('click');
        expect(panel.find('#profile-panel').hasClass('hidden')).to.be.equal(true);
    });
});
