import React from 'react';
import { shallow } from 'enzyme/build';
import YourWallets from '../../../../src/components/dashboard/yourWallets';
import { resetWindow, withSscLocation } from '../../../../testConfig';
import TranslationStruct from '../../../../src/struct/translationStruct';
import fetchMock from 'fetch-mock';
import TranslatorService from '../../../../src/_redux/services/translatorService';
import { translationActions } from '../../../../src/_redux/actions/translationActions';
const expect = require('expect.js');

describe('<YourWallets/> react test', () => {
    const translationStruct = new TranslationStruct(
        'en',
        'dummy.key',
        'This is key for dummy'
    );
    const translatedStruct = new TranslationStruct(
        'pl',
        'dummy.key',
        'To jest translacja kukly'
    );

    beforeEach(() => {
        withSscLocation();
        fetchMock.reset();
        fetchMock.mock('glob:https://*/i18n/en?*', { body: JSON.stringify([translationStruct]) });
        fetchMock.mock('glob:https://*/i18n/pl?*', { body: JSON.stringify([translatedStruct]) });
        fetchMock.mock('glob:https://*/i18n/dummyPath/*', 404);
        delete window.translationStore;

        const store = TranslatorService.getTranslationStore();
        store.dispatch(translationActions.addToTranslationQueue('dummy.key'));
        let state;

        store.subscribe(() => {
            state = store.getState();
        });
    });

    after(() => resetWindow());

    it('Should show modal download wallet', () => {
        let yourWallets = shallow(<YourWallets numberOffWallets={1}/>);
        const walletSelectorAssertion = 'someDummyString';
        yourWallets.find('#show-modal-download-wallet-desktop-0').simulate('click', walletEvent(walletSelectorAssertion));
        const state = yourWallets.state();
        expect(state.downloadWalletModalActive).to.be.equal(true);
    });

    function walletEvent(walletSelectorAssertion) {
        const eventMock = {
            target: {
                getAttribute: () => walletSelectorAssertion
            }
        };

        return eventMock;
    }
});
