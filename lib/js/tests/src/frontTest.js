import React from 'react';
import { shallow } from 'enzyme/build';
import { resetWindow, withSscLocation } from '../../testConfig';
import fetchMock from 'fetch-mock';
import Front from '../../src/front';
import AuthenticatorService from '../../src/_redux/services/authenticatorService';
import { authenticationConstants } from '../../src/_redux/constants/authenticationConstants';
const expect = require('expect.js');

describe('<Front/> react test', () => {
    const loginUrls = {
        facebookUrl: 'https://fb.logme',
        googleUrl: 'https://goo.le'
    };

    before(() => {
        fetchMock.get(
            'glob:https://api.silesiacoin.org/v1/auth',
            () => JSON.stringify(loginUrls)
        );
    });

    beforeEach(() => {
        resetWindow();
        withSscLocation();
    });

    it('should fail when angband not found', (done) => {
        resetWindow('dummyModuleName');
        withSscLocation();

        try {
            shallow(<Front/>)
        } catch (error) {
            expect(error).to.be.an(Error);
            expect(error.message).to.be.equal('angband not found');
            done();
        }
    });

    it('should call authenticator endpoint for fetching urls', (done) => {
        window.csrf = 'DUMMYCSRF';
        const authenticationStore = AuthenticatorService.getAuthenticatorStore();
        authenticationStore.subscribe(() => {
            const storeState = authenticationStore.getState();

            if (authenticationConstants.FETCHED_AUTH_LINKS === storeState.type) {
                expect(JSON.stringify(storeState.loginUrls)).to.be.equal(JSON.stringify(loginUrls));
                done();
            }
        });

        shallow(<Front/>);
        expect(window.location.pathname).to.be.equal('');
    });
});
