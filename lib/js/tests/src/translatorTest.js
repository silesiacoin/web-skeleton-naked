import Translator from '../../src/translator';
import ValidationError from '../../src/error/validationError';
import fetchMock from 'fetch-mock';
import TranslationStruct from '../../src/struct/translationStruct';
import TranslationClientError from '../../src/error/translationClientError';
import TranslationCollectionStruct from '../../src/struct/translationCollectionStruct';
let expect = require('expect.js');

describe('Test of Translator.js service', function () {
    let translator;

    beforeEach(() => {
        translator = new Translator('/i18n');
    });

    it('Should throw error if invalid apiUrl', function () {
        expect(() => new Translator(null)).to.throwException((e) => {
            expect(e).to.be.an(ValidationError);
        });
        expect(() => new Translator({})).to.throwException((e) => {
            expect(e).to.be.an(ValidationError);
        });
        expect(() => new Translator('')).to.throwException((e) => {
            expect(e).to.be.an(ValidationError);
        });
    });

    it('Should create object', function() {
        expect(() => new Translator('/i18n')).to.be.ok();
    });

    it('Should throw error if invalid trans key', function (done) {
        translator.trans(null).catch((e) => {
            expect(e).to.be.an(ValidationError);
        });
        translator.trans({}).catch((e) => {
            expect(e).to.be.an(ValidationError);
        });
        translator.trans('').catch((e) => {
            expect(e).to.be.an(ValidationError);
            done();
        });
    });
});

describe('Integration test of Translator', () => {
    const transObj = new TranslationStruct(
        'en',
        'dummy.key',
        'This is key for dummy'
    );
    const transObj1 = new TranslationStruct(
        'pl',
        'dummy.key2',
        'To jest klucz 2'
    );
    const transObj2 = new TranslationStruct(
        'pl',
        'dummy.key3',
        'To jest klucz 3'
    );
    const translationJson = JSON.stringify(transObj);
    const translationJsonCollection = JSON.stringify([transObj1, transObj2]);

    let translator;

    beforeEach(() => {
        fetchMock.reset();
        fetchMock.mock('glob:http://*/i18n/en/*', translationJson);
        fetchMock.mock('glob:http://*/i18n/pl?*', translationJsonCollection);
        fetchMock.mock('glob:http://*/i18n/dummyPath/*', 404);
        translator = new Translator('api.silesiacoin.localhost');
    });

    /**
     * @Deprecated
     * translator.trans is deprecated
     */
    it ('should return valid response for translation', async() => {
        try {
            const translationStruct = await translator.trans(
                'dummy.key',
                'en',
                'http'
            );

            expect(translationStruct).to.be.an(TranslationStruct);
        } catch (error) {
            expect('deprecated').to.be.equal('deprecated');
        }
    });

    it ('should throw error with invalid creation for translation', (done) => {
        translator.trans(
            'dummy.key',
            'dummyPath',
            'http'
        ).catch((e) => {
            expect(e).to.be.an(TranslationClientError);
            done();
        });
    });

    it ('should return valid response for translation collection', async() => {
        const translationCollectionStruct = await translator.transCollection(
            [transObj1.translationKey, transObj2.translationKey],
            transObj1.locale,
            'http'
        );

        expect(translationCollectionStruct).to.be.an(TranslationCollectionStruct);
    });

    it ('should throw error with invalid object parsed to request', () => {
        translator.transCollection()
            .catch((e) => {
            expect(e).to.be.an(ValidationError);
        });
    });
});
