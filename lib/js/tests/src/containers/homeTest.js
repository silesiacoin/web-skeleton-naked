import React from 'react';
import Home from '../../../src/containers/home';
import {shallow} from 'enzyme/build';
import {resetWindow, withSscLocation} from '../../../testConfig';
import TranslationStruct from '../../../src/struct/translationStruct';
import fetchMock from 'fetch-mock';
import TranslatorService from '../../../src/_redux/services/translatorService';
import {translationActions} from '../../../src/_redux/actions/translationActions';
const expect = require('expect.js');

describe('<Home/> react test', () => {
    const translationStruct = new TranslationStruct(
        'en',
        'dummy.key',
        'This is key for dummy'
    );
    const translatedStruct = new TranslationStruct(
        'pl',
        'dummy.key',
        'To jest translacja kukly'
    );

    beforeEach(() => {
        withSscLocation();
        fetchMock.reset();
        fetchMock.mock('glob:https://*/i18n/en?*', { body: JSON.stringify([translationStruct]) });
        fetchMock.mock('glob:https://*/i18n/pl?*', { body: JSON.stringify([translatedStruct]) });
        fetchMock.mock('glob:https://*/i18n/dummyPath/*', 404);
        delete window.translationStore;

        const store = TranslatorService.getTranslationStore();
        store.dispatch(translationActions.addToTranslationQueue('dummy.key'));
        let state;

        store.subscribe(() => {
            state = store.getState();
        });
    });

    after(() => resetWindow());

    it('Should have Header', () => {
        const home = shallow(<Home/>);
        const header = home.find('Header');
        expect(header.exists()).to.be.equal(true);
    });

    it('Should have Container', () => {
        const home = shallow(<Home/>);
        const container = home.find('Container');
        expect(container.exists()).to.be.equal(true);
    });

    it('Should have MainSection which is child Container', () => {
        const home = shallow(<Home/>);
        const mainSection = home.find('Container').children('MainSection');
        expect(mainSection.exists()).to.be.equal(true);
    });

    it('Should have SmallInfoSection which is child Container', () => {
        const home = shallow(<Home/>);
        const smallInfoSection = home.find('Container').children('SmallInfoSection');
        expect(smallInfoSection.exists()).to.be.equal(true);
    });

    it('Should have LargeInfoSection which is child Container', () => {
        const home = shallow(<Home/>);
        const largeInfoSection = home.find('Container').children('LargeInfoSection');
        expect(largeInfoSection.exists()).to.be.equal(true);
    });

    it('Should have Tree which is child Container', () => {
        const home = shallow(<Home/>);
        const tree = home.find('Container').children('Tree');
        expect(tree.exists()).to.be.equal(true);
    });

    it('Should have Footer', () => {
        const home = shallow(<Home/>);
        const footer = home.find('Footer');
        expect(footer.exists()).to.be.equal(true);
    });

    it('Should have RodoInfo', () => {
        const home = shallow(<Home/>);
        const rodoInfo = home.find('RodoInfo');
        expect(rodoInfo.exists()).to.be.equal(true);
    });
});
