import React from 'react';
import { render, shallow } from 'enzyme';
import TranslatedComponentFixture from './fixtures/translatedComponentFixture';
import fetchMock from 'fetch-mock';
import TranslationStruct from '../../../src/struct/translationStruct';
import CookieService from '../../../src/_redux/services/cookieService';
import { cookieConstrants } from '../../../src/_redux/constants/cookieConstants';
import CookieStruct from '../../../src/struct/cookieStruct';
import TranslatorService from '../../../src/_redux/services/translatorService';
import {translationConstants} from '../../../src/_redux/constants/translationConstants';
import {resetWindow, withSscLocation} from '../../../testConfig';

let expect = require('expect.js');

describe('Integration test of TranslatedComponent class', () => {
    const translationStruct = new TranslationStruct(
        'en',
        'dummy.key',
        'This is key for dummy'
    );
    const translatedStruct = new TranslationStruct(
        'pl',
        'dummy.key',
        'To jest translacja kukly'
    );

    let translatedComponent;
    let translatedMockedComponent;

    beforeEach(() => {
        withSscLocation();
        fetchMock.reset();
        fetchMock.mock('glob:https://*/i18n/en?*', { body: JSON.stringify([translationStruct]) });
        fetchMock.mock('glob:https://*/i18n/pl?*', { body: JSON.stringify([translatedStruct]) });
        fetchMock.mock('glob:https://*/i18n/dummyPath/*', 404);
        delete window.translationStore;
    });

    after(() => resetWindow());

    it ('should translate all TransObj elements for EN', (done) => {
        checkProperInstantiateForLocale(done, 'en');
    });

    it ('should translate all TransObj elements for PL', (done) => {
        checkProperInstantiateForLocale(done, 'pl');
    });

    function checkProperInstantiateForLocale(done, locale = 'en') {
        let state;
        const store = TranslatorService.getTranslationStore();
        store.subscribe(() => {
            state = store.getState();

            if (translationConstants.EMIT_TRANSLATION === state.type) {
                expect(state.locale).to.be.equal(locale);
            }

            if (translationConstants.END_EMISSION === state.type) {
                done();
            }
        });

        CookieService.setCookie(new CookieStruct(cookieConstrants.LOCALE_LANG, locale));
        translatedMockedComponent = render(<TranslatedComponentFixture translationKey={'dummy.key'} />);
        translatedComponent = shallow(<TranslatedComponentFixture translationKey={'dummy.key'} />);
    }
});
