import React from 'react';
import TranslatedComponent from '../../../../src/containers/translatedComponent';
import TransObject from '../../../../src/components/generic/transObject';

export default class TranslatedComponentFixture extends TranslatedComponent {
    render() {
        if ('string' === typeof this.props.translationKey) {
            return (
                <TransObject translationKey={this.props.translationKey}/>
            );
        }

        return (<div>Dummy message</div>);
    }
}
