import React from 'react';
import MiningCrew from '../../../src/containers/miningCrew';
import {shallow} from 'enzyme/build';
import {resetWindow, withSscLocation} from '../../../testConfig';
import TranslationStruct from '../../../src/struct/translationStruct';
import fetchMock from 'fetch-mock';
import TranslatorService from '../../../src/_redux/services/translatorService';
import {translationActions} from '../../../src/_redux/actions/translationActions';
const expect = require('expect.js');

describe('<MiningCrew/> react test', () => {
    const translationStruct = new TranslationStruct(
        'en',
        'dummy.key',
        'This is key for dummy'
    );
    const translatedStruct = new TranslationStruct(
        'pl',
        'dummy.key',
        'To jest translacja kukly'
    );

    beforeEach(() => {
        withSscLocation();
        fetchMock.reset();
        fetchMock.mock('glob:https://*/i18n/en?*', { body: JSON.stringify([translationStruct]) });
        fetchMock.mock('glob:https://*/i18n/pl?*', { body: JSON.stringify([translatedStruct]) });
        fetchMock.mock('glob:https://*/i18n/dummyPath/*', 404);
        delete window.translationStore;

        const store = TranslatorService.getTranslationStore();
        store.dispatch(translationActions.addToTranslationQueue('dummy.key'));
        let state;

        store.subscribe(() => {
            state = store.getState();
        });
    });

    after(() => resetWindow());

    it('Should have Header', () => {
        const miningCrew = shallow(<MiningCrew/>);
        const header = miningCrew.find('Header');
        expect(header.exists()).to.be.equal(true);
    });

    it('Should have Container', () => {
        const miningCrew = shallow(<MiningCrew/>);
        const container = miningCrew.find('Container');
        expect(container.exists()).to.be.equal(true);
    });

    it('Should have MainSection which is child Container', () => {
        const miningCrew = shallow(<MiningCrew/>);
        const mainSection = miningCrew.find('Container').children('MainSection');
        expect(mainSection.exists()).to.be.equal(true);
    });

    it('Should have MiningSystemSection which is child Container', () => {
        const miningCrew = shallow(<MiningCrew/>);
        const miningSystemSection = miningCrew.find('Container').children('MiningSystemSection');
        expect(miningSystemSection.exists()).to.be.equal(true);
    });

    it('Should have LargeInfoSection which is child Container', () => {
        const miningCrew = shallow(<MiningCrew/>);
        const largeInfoSection = miningCrew.find('Container').children('LargeInfoSection');
        expect(largeInfoSection.exists()).to.be.equal(true);
    });

    it('Should have ProofOfWorkSection which is child Container', () => {
        const miningCrew = shallow(<MiningCrew/>);
        const proofOfWorkSection = miningCrew.find('Container').children('ProofOfWorkSection');
        expect(proofOfWorkSection.exists()).to.be.equal(true);
    });

    it('Should have BlocksAreValidatedSection which is child Container', () => {
        const miningCrew = shallow(<MiningCrew/>);
        const blocksAreValidatedSection = miningCrew.find('Container').children('BlocksAreValidatedSection');
        expect(blocksAreValidatedSection.exists()).to.be.equal(true);
    });

    it('Should have SmallInfoSection which is child Container', () => {
        const miningCrew = shallow(<MiningCrew/>);
        const smallInfoSection = miningCrew.find('Container').children('SmallInfoSection');
        expect(smallInfoSection.exists()).to.be.equal(true);
    });

    it('Should have ProcessSection which is child Container', () => {
        const miningCrew = shallow(<MiningCrew/>);
        const processSection = miningCrew.find('Container').children('ProcessSection');
        expect(processSection.exists()).to.be.equal(true);
    });

    it('Should have ValuableSection which is child Container', () => {
        const miningCrew = shallow(<MiningCrew/>);
        const valuableSection = miningCrew.find('Container').children('ValuableSection');
        expect(valuableSection.exists()).to.be.equal(true);
    });

    it('Should have Join which is child Container', () => {
        const miningCrew = shallow(<MiningCrew/>);
        const join = miningCrew.find('Container').children('Join');
        expect(join.exists()).to.be.equal(true);
    });

    it('Should have Footer', () => {
        const miningCrew = shallow(<MiningCrew/>);
        const footer = miningCrew.find('Footer');
        expect(footer.exists()).to.be.equal(true);
    });
});
