import React from 'react';
import { shallow } from 'enzyme/build';
import Dashboard from '../../../src/containers/dashboard';
import Profile from '../../../src/components/dashboard/account';
import Navbar from '../../../src/components/dashboard/navbar';
import Wallet from '../../../src/components/dashboard/wallet';
import Wallets from '../../../src/components/dashboard/wallets';
import YourWallets from '../../../src/components/dashboard/yourWallets';
import { resetWindow, withSscLocation } from '../../../testConfig';
import TranslationStruct from '../../../src/struct/translationStruct';
import fetchMock from 'fetch-mock';
import TranslatorService from '../../../src/_redux/services/translatorService';
import { translationActions } from '../../../src/_redux/actions/translationActions';
import expect from 'expect.js';
import CookieService from '../../../src/_redux/services/cookieService';
import CookieStruct from '../../../src/struct/cookieStruct';

describe('<Dashboard/> react test', () => {
    const translationStruct = new TranslationStruct(
        'en',
        'dummy.key',
        'This is key for dummy'
    );
    const translatedStruct = new TranslationStruct(
        'pl',
        'dummy.key',
        'To jest translacja kukly'
    );

    beforeEach(() => {
        CookieService.setCookie(new CookieStruct('ssc_not_finished', 'true'));
        window.csrf ='DUMMYCSRF';
        withSscLocation();
        fetchMock.reset();
        fetchMock.mock('glob:https://*/i18n/en?*', { body: JSON.stringify([translationStruct]) });
        fetchMock.mock('glob:https://*/i18n/pl?*', { body: JSON.stringify([translatedStruct]) });
        fetchMock.mock('glob:https://*/i18n/dummyPath/*', 404);
        delete window.translationStore;

        const store = TranslatorService.getTranslationStore();
        store.dispatch(translationActions.addToTranslationQueue('dummy.key'));
        let state;

        store.subscribe(() => {
            state = store.getState();
        });
    });

    after(() => {
        resetWindow();
    });

    it('Should have active main sections', () => {
        const dashboard = shallow(<Dashboard/>);
        const navbar = shallow(<Navbar/>);

        navbar.find('#show-main-sections-logo').simulate('click');
        let state = dashboard.state();
        expect(state.mainPageActive).to.be.equal(true);

        navbar.find('#show-main-sections-home').simulate('click');
        state = dashboard.state();
        expect(state.mainPageActive).to.be.equal(true);
    });

    it('Should have hidden main sections panel', () => {
        const dashboard = shallow(<Dashboard/>);
        const navbar = shallow(<Navbar/>);
        const profile = shallow(<Profile/>);
        const wallets = shallow(<Wallets numberOffWallets={1}/>);

        profile.find('#show-profile-panel').simulate('click');
        let state = dashboard.state();
        expect(state.mainPageActive).to.be.equal(false);

        const walletSelectorAssertion = 'someDummyString';
        wallets.find('#portable-show-wallet-0').simulate('click', walletEvent(walletSelectorAssertion));
        state = dashboard.state();
        expect(state.mainPageActive).to.be.equal(false);
        expect(state.walletSelector).to.be.equal(walletSelectorAssertion);

        navbar.find('#show-your-wallets').simulate('click');
        state = dashboard.state();
        expect(state.mainPageActive).to.be.equal(false);

        wallets.find('#go-to-your-wallets').simulate('click');
        state = dashboard.state();
        expect(state.mainPageActive).to.be.equal(false);
    });

    it('Should have active profile panel', () => {
        const dashboard = shallow(<Dashboard/>);
        const profile = shallow(<Profile/>);

        profile.find('#show-profile-panel').simulate('click');
        let state = dashboard.state();
        expect(state.profilePanelActive).to.be.equal(true);
    });

    it('Should have hidden profile panel', () => {
        const dashboard = shallow(<Dashboard/>);
        const navbar = shallow(<Navbar/>);

        navbar.find('#show-main-sections-logo').simulate('click');
        let state = dashboard.state();
        expect(state.profilePanelActive).to.be.equal(false);

        navbar.find('#show-main-sections-home').simulate('click');
        state = dashboard.state();
        expect(state.profilePanelActive).to.be.equal(false);

        navbar.find('#show-your-wallets').simulate('click');
        state = dashboard.state();
        expect(state.profilePanelActive).to.be.equal(false);
    });

    it('Should have active your wallets panel', () => {
        const dashboard = shallow(<Dashboard/>);
        const wallet = shallow(<Wallet/>);
        const navbar = shallow(<Navbar/>);
        const wallets = shallow(<Wallets numberOffWallets={1}/>);

        navbar.find('#show-your-wallets').simulate('click');
        let state = dashboard.state();
        expect(state.yourWalletsActive).to.be.equal(true);

        wallet.find('#return-your-wallets').simulate('click');
        state = dashboard.state();
        expect(state.yourWalletsActive).to.be.equal(true);

        wallets.find('#go-to-your-wallets').simulate('click');
        state = dashboard.state();
        expect(state.yourWalletsActive).to.be.equal(true);
    });

    it('Should have hidden your wallets panel', () => {
        const dashboard = shallow(<Dashboard/>);
        const profile = shallow(<Profile/>);
        const navbar = shallow(<Navbar/>);
        const yourWallets = shallow(<YourWallets numberOffWallets={1}/>);

        navbar.find('#show-main-sections-logo').simulate('click');
        let state = dashboard.state();
        expect(state.yourWalletsActive).to.be.equal(false);

        navbar.find('#show-main-sections-home').simulate('click');
        state = dashboard.state();
        expect(state.yourWalletsActive).to.be.equal(false);

        profile.find('#show-profile-panel').simulate('click');
        state = dashboard.state();
        expect(state.yourWalletsActive).to.be.equal(false);

        const walletSelectorAssertion = 'someDummyString';
        yourWallets.find('#portable-show-wallet-0').simulate('click', walletEvent(walletSelectorAssertion));

        state = dashboard.state();
        expect(state.yourWalletsActive).to.be.equal(false);
        expect(state.walletSelector).to.be.equal(walletSelectorAssertion);
    });

    it('Should have hidden wallet panel', () => {
        const dashboard = shallow(<Dashboard/>);
        const navbar = shallow(<Navbar/>);
        const profile = shallow(<Profile/>);
        const wallet = shallow(<Wallet/>);

        navbar.find('#show-main-sections-logo').simulate('click');
        let state = dashboard.state();
        expect(state.walletActive).to.be.equal(false);

        navbar.find('#show-main-sections-home').simulate('click');
        state = dashboard.state();
        expect(state.walletActive).to.be.equal(false);

        navbar.find('#show-your-wallets').simulate('click');
        state = dashboard.state();
        expect(state.walletActive).to.be.equal(false);

        profile.find('#show-profile-panel').simulate('click');
        state = dashboard.state();
        expect(state.walletActive).to.be.equal(false);

        wallet.find('#return-your-wallets').simulate('click');
        state = dashboard.state();
        expect(state.walletActive).to.be.equal(false);
    });

    it('Should have active wallet panel', () => {
        const dashboard = shallow(<Dashboard yourWalletsActive={true}/>);
        const yourWallets = shallow(<YourWallets numberOffWallets={1}/>);
        const wallets = shallow(<Wallets numberOffWallets={1}/>);

        const walletSelectorAssertion = 'someDummyString';
        yourWallets.find('#portable-show-wallet-0').simulate('click', walletEvent(walletSelectorAssertion));

        let state = dashboard.state();
        expect(state.walletActive).to.be.equal(true);
        expect(state.walletSelector).to.be.equal(walletSelectorAssertion);

        wallets.find('#portable-show-wallet-0').simulate('click', walletEvent(walletSelectorAssertion));
        state = dashboard.state();
        expect(state.walletActive).to.be.equal(true);
        expect(state.walletSelector).to.be.equal(walletSelectorAssertion);
    });

    it('Should have Navbar', () => {
        const dashboard = shallow(<Dashboard/>);
        const navbar = dashboard.find('Navbar');
        expect(navbar.exists()).to.be.equal(true);
    });

    it('Should have Profile', () => {
        const dashboard = shallow(<Dashboard mainPageActive={true}/>);
        const profile = dashboard.find('Profile');
        expect(profile.exists()).to.be.equal(true);
    });

    it('Should have Container', () => {
        const dashboard = shallow(<Dashboard/>);
        const container = dashboard.find('Container');
        expect(container.exists()).to.be.equal(true);
    });

    it('Should have Wallets', () => {
        const dashboard = shallow(<Dashboard mainPageActive={true}/>);
        const wallets = dashboard.find('Wallets');
        expect(wallets.exists()).to.be.equal(true);
    });

    it('Should have Account', () => {
        const dashboard = shallow(<Dashboard/>);
        const account = dashboard.find('Account');
        expect(account.exists()).to.be.equal(true);
    });

    it('Should have Ssc', () => {
        const dashboard = shallow(<Dashboard mainPageActive={true}/>);
        const ssc = dashboard.find('Ssc');
        expect(ssc.exists()).to.be.equal(true);
    });

    it('Should have Mining', () => {
        const dashboard = shallow(<Dashboard mainPageActive={true}/>);
        const mining = dashboard.find('Mining');
        expect(mining.exists()).to.be.equal(true);
    });

    it('Should have FullProfile', () => {
        const dashboard = shallow(<Dashboard profilePanelActive={true}/>);
        const fullProfile = dashboard.find('FullProfile');
        expect(fullProfile.exists()).to.be.equal(true);
    });

    it('Should have Wallet', () => {
        const dashboard = shallow(<Dashboard walletActive={true}/>);
        const wallet = dashboard.find('Wallet');
        expect(wallet.exists()).to.be.equal(true);
    });

    it('Should have YourWallets', () => {
        const dashboard = shallow(<Dashboard yourWalletsActive={true}/>);
        const yourWallets = dashboard.find('YourWallets');
        expect(yourWallets.exists()).to.be.equal(true);
    });

    it('Should have CreateWallet', () => {
        const dashboard = shallow(<Dashboard/>);
        const createWallet = dashboard.find('CreateWallet');
        expect(createWallet.exists()).to.be.equal(true);
    });

    it('Should have Authenticator', () => {
        const dashboard = shallow(<Dashboard/>);
        const authenticator = dashboard.find('Authenticator');
        expect(authenticator.exists()).to.be.equal(true);
    });

    function walletEvent(walletSelectorAssertion) {
        const eventMock = {
            target: {
                getAttribute: () => walletSelectorAssertion
            }
        };

        return eventMock;
    }
});
