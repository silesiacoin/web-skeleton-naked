import React from 'react';
import BusinessEarning from '../../../src/containers/businessEarning';
import {shallow} from 'enzyme/build';
import {resetWindow, withSscLocation} from '../../../testConfig';
import TranslationStruct from '../../../src/struct/translationStruct';
import fetchMock from 'fetch-mock';
import TranslatorService from '../../../src/_redux/services/translatorService';
import {translationActions} from '../../../src/_redux/actions/translationActions';
const expect = require('expect.js');

describe('<BusinessEarning/> react test', () => {
    const translationStruct = new TranslationStruct(
        'en',
        'dummy.key',
        'This is key for dummy'
    );
    const translatedStruct = new TranslationStruct(
        'pl',
        'dummy.key',
        'To jest translacja kukly'
    );

    beforeEach(() => {
        withSscLocation();
        fetchMock.reset();
        fetchMock.mock('glob:https://*/i18n/en?*', { body: JSON.stringify([translationStruct]) });
        fetchMock.mock('glob:https://*/i18n/pl?*', { body: JSON.stringify([translatedStruct]) });
        fetchMock.mock('glob:https://*/i18n/dummyPath/*', 404);
        delete window.translationStore;

        const store = TranslatorService.getTranslationStore();
        store.dispatch(translationActions.addToTranslationQueue('dummy.key'));
        let state;

        store.subscribe(() => {
            state = store.getState();
        });
    });

    after(() => resetWindow());

    it('Should have Header', () => {
        const businessEarning = shallow(<BusinessEarning/>);
        const header = businessEarning.find('Header');
        expect(header.exists()).to.be.equal(true);
    });

    it('Should have Container', () => {
        const businessEarning = shallow(<BusinessEarning/>);
        const container = businessEarning.find('Container');
        expect(container.exists()).to.be.equal(true);
    });

    it('Should have MainSection which is child Container', () => {
        const businessEarning = shallow(<BusinessEarning/>);
        const mainSection = businessEarning.find('Container').children('MainSection');
        expect(mainSection.exists()).to.be.equal(true);
    });

    it('Should have HighChance which is child Container', () => {
        const businessEarning = shallow(<BusinessEarning/>);
        const highChance = businessEarning.find('Container').children('HighChance');
        expect(highChance.exists()).to.be.equal(true);
    });

    it('Should have LargeInfoSection which is child Container', () => {
        const businessEarning = shallow(<BusinessEarning/>);
        const largeInfoSection = businessEarning.find('Container').children('LargeInfoSection');
        expect(largeInfoSection.exists()).to.be.equal(true);
    });

    it('Should have WhyTheFeatureIsSSC which is child Container', () => {
        const businessEarning = shallow(<BusinessEarning/>);
        const whyTheFeatureIsSSC = businessEarning.find('Container').children('WhyTheFeatureIsSSC');
        expect(whyTheFeatureIsSSC.exists()).to.be.equal(true);
    });

    it('Should have LookWhyCrypto which is child Container', (done) => {
        const businessEarning = shallow(<BusinessEarning/>);
        const lookWhyCrypto = businessEarning.find('Container').children('LookWhyCrypto');
        expect(lookWhyCrypto.exists()).to.be.equal(true);
        done();
    });

    it('Should have EthereumAddressGrowthChart which is child Container', () => {
        const businessEarning = shallow(<BusinessEarning/>);
        const ethereumAddressGrowthChart = businessEarning.find('Container').children('EthereumAddressGrowthChart');
        expect(ethereumAddressGrowthChart.exists()).to.be.equal(true);
    });

    it('Should have ValuableSection which is child Container', () => {
        const businessEarning = shallow(<BusinessEarning/>);
        const valuableSection = businessEarning.find('Container').children('ValuableSection');
        expect(valuableSection.exists()).to.be.equal(true);
    });

    it('Should have EthereumAddressGrowth which is child Container', () => {
        //This element was tested differently because even if the element exists, the test stated that the element does not exist
        const businessEarning = shallow(<BusinessEarning/>);
        const businessEarningContainer = businessEarning.find('Container');
        const businessEarningContainerHtml = businessEarningContainer.html();
        const seekClass = businessEarningContainerHtml.indexOf('ethereum-adress-growth-section');
        expect(seekClass).to.be.greaterThan(-1);
        expect(businessEarningContainer.children().length).to.be.equal(16);
    });

    it('Should have Join which is child Container', () => {
        const businessEarning = shallow(<BusinessEarning/>);
        const join = businessEarning.find('Container').children('Join');
        expect(join.exists()).to.be.equal(true);
    });

    it('Should have EthereumActiveAddressesChart which is child Container', () => {
        const businessEarning = shallow(<BusinessEarning/>);
        const ethereumActiveAddressesChart = businessEarning.find('Container').children('EthereumActiveAddressesChart');
        expect(ethereumActiveAddressesChart.exists()).to.be.equal(true);
    });

    it('Should have CryptoUserGrowthChart which is child Container', () => {
        const businessEarning = shallow(<BusinessEarning/>);
        const cryptoUserGrowthChart = businessEarning.find('Container').children('CryptoUserGrowthChart');
        expect(cryptoUserGrowthChart.exists()).to.be.equal(true);
    });

    it('Should have CryptoExchangeUserGrowthChart which is child Container', () => {
        const businessEarning = shallow(<BusinessEarning/>);
        const cryptoExchangeUserGrowthChart = businessEarning.find('Container').children('CryptoExchangeUserGrowthChart');
        expect(cryptoExchangeUserGrowthChart.exists()).to.be.equal(true);
    });

    it('Should have BitcoinActiveAddressesChart which is child Container', () => {
        const businessEarning = shallow(<BusinessEarning/>);
        const bitcoinActiveAddressesChart = businessEarning.find('Container').children('BitcoinActiveAddressesChart');
        expect(bitcoinActiveAddressesChart.exists()).to.be.equal(true);
    });

    it('Should have CryptoExchangeVolumeChart which is child Container', () => {
        const businessEarning = shallow(<BusinessEarning/>);
        const cryptoExchangeVolumeChart = businessEarning.find('Container').children('CryptoExchangeVolumeChart');
        expect(cryptoExchangeVolumeChart.exists()).to.be.equal(true);
    });

    it('Should have WebsiteGrowthChart which is child Container', () => {
        const businessEarning = shallow(<BusinessEarning/>);
        const websiteGrowthChart = businessEarning.find('Container').children('WebsiteGrowthChart');
        expect(websiteGrowthChart.exists()).to.be.equal(true);
    });

    it('Should have BitcoinWalletAddressGrowthChart which is child Container', () => {
        const businessEarning = shallow(<BusinessEarning/>);
        const bitcoinWalletAddressGrowthChart = businessEarning.find('Container').children('BitcoinWalletAddressGrowthChart');
        expect(bitcoinWalletAddressGrowthChart.exists()).to.be.equal(true);
    });

    it('Should have Footer', () => {
        const businessEarning = shallow(<BusinessEarning/>);
        const footer = businessEarning.find('Footer');
        expect(footer.exists()).to.be.equal(true);
    });
});
