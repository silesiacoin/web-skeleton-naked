import React from 'react';
import AffiliateNetwork from '../../../src/containers/affiliateNetwork';
import {shallow} from 'enzyme/build';
import {resetWindow, withSscLocation} from '../../../testConfig';
import TranslationStruct from '../../../src/struct/translationStruct';
import fetchMock from 'fetch-mock';
import TranslatorService from '../../../src/_redux/services/translatorService';
import {translationActions} from '../../../src/_redux/actions/translationActions';
const expect = require('expect.js');

describe('<AffiliateNetwork/> react test', () => {
    const translationStruct = new TranslationStruct(
        'en',
        'dummy.key',
        'This is key for dummy'
    );
    const translatedStruct = new TranslationStruct(
        'pl',
        'dummy.key',
        'To jest translacja kukly'
    );

    beforeEach(() => {
        withSscLocation();
        fetchMock.reset();
        fetchMock.mock('glob:https://*/i18n/en?*', { body: JSON.stringify([translationStruct]) });
        fetchMock.mock('glob:https://*/i18n/pl?*', { body: JSON.stringify([translatedStruct]) });
        fetchMock.mock('glob:https://*/i18n/dummyPath/*', 404);
        delete window.translationStore;

        const store = TranslatorService.getTranslationStore();
        store.dispatch(translationActions.addToTranslationQueue('dummy.key'));
        let state;

        store.subscribe(() => {
            state = store.getState();
        });
    });

    after(() => resetWindow());

    it('Should have Header', () => {
        const affiliateNetwork = shallow(<AffiliateNetwork/>);
        const header = affiliateNetwork.find('Header');
        expect(header.exists()).to.be.equal(true);
    });

    it('Should have Container', () => {
        const affiliateNetwork = shallow(<AffiliateNetwork/>);
        const container = affiliateNetwork.find('Container');
        expect(container.exists()).to.be.equal(true);
    });

    it('Should have MainSection which is child Container', () => {
        const affiliateNetwork = shallow(<AffiliateNetwork/>);
        const mainSection = affiliateNetwork.find('Container').children('MainSection');
        expect(mainSection.exists()).to.be.equal(true);
    });

    it('Should have TimeRevolution which is child Container', () => {
        const affiliateNetwork = shallow(<AffiliateNetwork/>);
        const timeRevolution = affiliateNetwork.find('Container').children('TimeRevolution');
        expect(timeRevolution.exists()).to.be.equal(true);
    });

    it('Should have LargeInfoSection which is child Container', () => {
        const affiliateNetwork = shallow(<AffiliateNetwork/>);
        const largeInfoSection = affiliateNetwork.find('Container').children('LargeInfoSection');
        expect(largeInfoSection.exists()).to.be.equal(true);
    });

    it('Should have Timer which is child Container', () => {
        const affiliateNetwork = shallow(<AffiliateNetwork/>);
        const timer = affiliateNetwork.find('Container').children('Timer');
        expect(timer.exists()).to.be.equal(true);
    });

    it('Should have Team which is child Container', () => {
        const affiliateNetwork = shallow(<AffiliateNetwork/>);
        const team = affiliateNetwork.find('Container').children('Team');
        expect(team.exists()).to.be.equal(true);
    });

    it('Should have People which is child Container', () => {
        const affiliateNetwork = shallow(<AffiliateNetwork/>);
        const people = affiliateNetwork.find('Container').children('People');
        expect(people.exists()).to.be.equal(true);
    });

    it('Should have Join which is child Container', () => {
        const affiliateNetwork = shallow(<AffiliateNetwork/>);
        const join = affiliateNetwork.find('Container').children('Join');
        expect(join.exists()).to.be.equal(true);
    });

    it('Should have Footer', () => {
        const affiliateNetwork = shallow(<AffiliateNetwork/>);
        const footer = affiliateNetwork.find('Footer');
        expect(footer.exists()).to.be.equal(true);
    });
});
