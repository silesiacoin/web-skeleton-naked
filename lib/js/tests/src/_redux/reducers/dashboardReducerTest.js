import defaultStore from '../../../../src/_redux/store/defaultStore';
import { describe } from 'mocha';
import { dashboard } from '../../../../src/_redux/reducers/dashboardReducer';
import { resetWindow, withSscLocation } from '../../../../testConfig';
import { dashboardConstants } from "../../../../src/_redux/constants/dashboardConstants";
import { dashboardActions } from "../../../../src/_redux/actions/dashboardActions";

let expect = require('expect.js');

describe('Test of dashboard reducer', () => {
    let storeClass;
    let storeToTest;
    let storeState;

    beforeEach(() => {
        withSscLocation();
    });

    afterEach(() => {
        storeClass = undefined;
        storeToTest = undefined;
        storeState = undefined;
    });

    after(() => resetWindow());

    function getDefaultStore(initialState = {}) {
        storeClass = new defaultStore(dashboard);

        return storeClass.registerStore(initialState);
    }

    it('Should dispatch default action', () => {
        storeToTest = getDefaultStore();
        storeState = storeToTest.getState();
        expect(Object.keys(storeState).length).to.be.equal(0);
    });

    it('Should dispatch show profile', () => {
        storeToTest = getDefaultStore();
        storeToTest.dispatch(dashboardActions.showProfile());
        storeState = storeToTest.getState();
        expect(storeState.type).to.be.equal(dashboardConstants.SHOW_PROFILE);
    });

    it('Should dispatch show mains sections', () => {
        storeToTest = getDefaultStore();
        storeToTest.dispatch(dashboardActions.showMainSections());
        storeState = storeToTest.getState();
        expect(storeState.type).to.be.equal(dashboardConstants.SHOW_MAIN_SECTIONS);
    });

    it('Should dispatch show your wallets', () => {
        storeToTest = getDefaultStore();
        storeToTest.dispatch(dashboardActions.showYourWallets());
        storeState = storeToTest.getState();
        expect(storeState.type).to.be.equal(dashboardConstants.SHOW_YOUR_WALLETS);
    });

    it('Should dispatch show wallet', () => {
        storeToTest = getDefaultStore();
        storeToTest.dispatch(dashboardActions.showWallet());
        storeState = storeToTest.getState();
        expect(storeState.type).to.be.equal(dashboardConstants.SHOW_WALLET);
    });

    it('Should dispatch create wallet', () => {
        storeToTest = getDefaultStore();
        storeToTest.dispatch(dashboardActions.createWallet());
        storeState = storeToTest.getState();
        expect(storeState.type).to.be.equal(dashboardConstants.CREATE_WALLET);
    });
});
