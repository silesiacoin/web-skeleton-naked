import defaultStore from '../../../../src/_redux/store/defaultStore';
import { authenticationActions } from '../../../../src/_redux/actions/authenticationActions';
import { describe } from 'mocha';
import { authentication } from '../../../../src/_redux/reducers/authenticationReducer';
import ValidationError from '../../../../src/error/validationError';
import { resetWindow, withSscLocation } from '../../../../testConfig';
import { authenticationConstants } from '../../../../src/_redux/constants/authenticationConstants';
import AuthenticatorError from '../../../../src/error/authenticatorError';

let expect = require('expect.js');

describe('Test of authentication reducer', () => {
    let storeClass;
    let storeToTest;
    let storeState;
    let state;

    beforeEach(() => {
        withSscLocation();
    });

    afterEach(() => {
        storeClass = undefined;
        storeToTest = undefined;
        storeState = undefined;
        state = undefined;
    });

    after(() => resetWindow());

    it('Should throw exception', () => {
        storeToTest = getDefaultStore();

        expect(() => storeToTest.dispatch(authentication({})))
            .to.throwError((e) => {
                expect(e).to.be.an(ValidationError);
        });
    });

    it('should throw error when no csrf token found', () => {
        storeToTest = getDefaultStore();
        expect(() => {
            storeToTest.dispatch(authenticationActions.initial());
        }).to.throwError((e) => {
            expect(e).to.be.an(ValidationError);
            expect(e.message).to.be.equal('CSRF token not found');
        });
    });

    it('should fetch csrf token', () => {
        storeToTest = getDefaultStore();
        window.csrf = 'DUMMYCSRF';
        storeToTest.dispatch(authenticationActions.initial());
        storeState = storeToTest.getState();
        expect(storeState.csrf).to.be.equal(window.csrf);
        expect(storeState.type).to.be.equal(authenticationConstants.INITIAL);
    });

    it('should throw error on front register validation', () => {
        storeToTest = getDefaultStore({});
        expect(() => {
            storeToTest.dispatch(authenticationActions.validateRegisterForm());
        }).to.throwError((e) => {
            expect(e).to.be.an(ValidationError);
            expect(e.message).to.be.equal('Form not found within action');
        });

        expect(() => {
            storeToTest.dispatch(authenticationActions.validateRegisterForm(
                {}, 'KKWWWXXX'
            ));
        }).to.throwError((e) => {
            expect(e).to.be.an(ValidationError);
            expect(e.message).to.be.equal('CSRF not found within state');
        });
    });

    it('should throw error when no response is present in authentication action', () => {
        storeToTest = getDefaultStore();

        expect(() => {
            storeToTest.dispatch(authenticationActions.handleRegisterResponse());
        }).to.throwError((e) => {
            expect(e).to.be.an(AuthenticatorError);
            expect(e.message).to.be.equal('Could not reduce action without response');
        });
    });

    it('should dispatch backend error when response has invalid status code', () => {
        storeToTest = getDefaultStore();
        storeToTest.dispatch(authenticationActions.handleRegisterResponse({}));
        state = storeToTest.getState();
        expect(state.type).to.be.equal(authenticationConstants.REGISTER_USER_BACKEND_ERROR);

        storeToTest.dispatch(authenticationActions.handleRegisterResponse({ status: 500 }));
        state = storeToTest.getState();
        expect(state.type).to.be.equal(authenticationConstants.REGISTER_USER_BACKEND_ERROR);

        storeToTest.dispatch(authenticationActions.handleRegisterResponse({ status: 301 }));
        state = storeToTest.getState();
        expect(state.type).to.be.equal(authenticationConstants.REGISTER_USER_BACKEND_ERROR);
    });

    it('should dispatch user succes when response has 201 status code', () => {
        storeToTest = getDefaultStore();
        storeToTest.dispatch(authenticationActions.handleRegisterResponse({ status: 201 }));
        state = storeToTest.getState();
        expect(state.type).to.be.equal(authenticationConstants.REGISTER_USER_SUCCESS);
    });

    it('should throw error when no login urls after fetching links', (done) => {
        storeToTest = getDefaultStore();
        expect(() => {
            storeToTest.dispatch(authenticationActions.registerLoginUrlsInStore());
        }).to.throwError((err) => {
            expect(err).to.be.an(ValidationError);
            expect(err.message).to.be.equal('login urls are not present in response');
            done();
        });
    });

    it('should dispach registering login urls', () => {
        const loginUrls = {
            facebookUrl: 'http://dummy.com',
            googleUrl: 'https://noogle.com'
        };
        storeToTest = getDefaultStore();
        storeToTest.dispatch(authenticationActions.registerLoginUrlsInStore(JSON.stringify(loginUrls)));
        storeState = storeToTest.getState();
        expect(storeState.type).to.be.equal(authenticationConstants.FETCHED_AUTH_LINKS);
        expect(storeState.loginUrls).to.be.equal(JSON.stringify(loginUrls));
    });

    function getDefaultStore(initialState = {}) {
        storeClass = new defaultStore(authentication);

        return storeClass.registerStore(initialState);
    }
});
