import defaultStore from '../../../../src/_redux/store/defaultStore';
import { translationActions } from '../../../../src/_redux/actions/translationActions';
import { describe } from 'mocha';
import { translation } from '../../../../src/_redux/reducers/translationReducer';
import ValidationError from '../../../../src/error/validationError';

let expect = require('expect.js');

describe('Test of translation reducer', () => {
    const defaultAction = (message = '') => { return message };
    let reducer;
    let storeClass;
    let storeToTest;
    let storeState;
    let action;

    afterEach(() => {
        storeClass = undefined;
        reducer = undefined;
        storeToTest = undefined;
        storeState = undefined;
        action = undefined;
    });

    it('Should throw exception', () => {
        storeClass = new defaultStore(defaultAction);
        storeToTest = storeClass.registerStore();

        expect(() => storeToTest.dispatch(translation({})))
            .to.throwError((e) => {
                expect(e).to.be.an(ValidationError);
        });
    });

    it('Should dispatch default action', () => {
        storeToTest = getDefaultStore();
        storeState = storeToTest.getState();
        expect(Object.keys(storeState).length).to.be.equal(1);
    });

    it('Should add few elements to initial state', () => {
        storeToTest = (new defaultStore(translation)).registerStore();
        storeToTest.dispatch(translationActions.addToTranslationQueue('dummy.key'));
        storeState = storeToTest.getState();
        expect(storeState.toTranslate.length).to.be.equal(1);

        storeToTest = getDefaultStore();
        storeToTest.dispatch(translationActions.addToTranslationQueue('dummy.key'));
        storeState = storeToTest.getState();
        expect(storeState.toTranslate.length).to.be.equal(1);
        storeToTest.dispatch(translationActions.addToTranslationQueue('dummy.value'));
        storeState = storeToTest.getState();
        expect(storeState.toTranslate.length).to.be.equal(2);
        storeToTest.dispatch(translationActions.addToTranslationQueue('dummy.value'));
        storeState = storeToTest.getState();
        expect(storeState.toTranslate.length).to.be.equal(2);
    });

    function getDefaultStore() {
        storeClass = new defaultStore(translation);
        return storeClass.registerStore({
            toTranslate: []
        });
    }
});
