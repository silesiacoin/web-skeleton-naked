import TranslatorService from './../../../../src/_redux/services/translatorService';
import React from 'react';
import fetchMock from "fetch-mock";
import TranslationStruct from '../../../../src/struct/translationStruct';
import { shallow } from 'enzyme';
import TransObject from '../../../../src/components/generic/transObject';
import ValidationError from '../../../../src/error/validationError';
import CookieService from '../../../../src/_redux/services/cookieService';
import CookieStruct from '../../../../src/struct/cookieStruct';
import { cookieConstrants } from '../../../../src/_redux/constants/cookieConstants';
import TranslationClientError from '../../../../src/error/translationClientError';
import { resetWindow, withSscLocation } from '../../../../testConfig';

let expect = require('expect.js');

describe('Integration test of redux translation service', () => {
    const translationStruct = new TranslationStruct(
        'en',
        'dummy.key',
        'This is key for dummy'
    );
    const translatedStruct = new TranslationStruct(
        'pl',
        'dummy.key',
        'To jest translacja kukly'
    );
    let translatorService;

    beforeEach(() => {
        fetchMock.reset();
        fetchMock.mock('glob:https://*/i18n/en?*', { body: JSON.stringify([translationStruct]) });
        fetchMock.mock('glob:https://*/i18n/pl?*', { body: JSON.stringify([translatedStruct]) });
        fetchMock.mock('glob:https://*/i18n/dummyPath/*', 404);
        withSscLocation();
        translatorService = new TranslatorService();
    });

    after(() => resetWindow());

    it ('should translate elements, when there are translations provided for default en locale', async () => {
        const transObj = shallow(<TransObject translationKey={translationStruct.translationKey}/>);
        let state = transObj.state();

        expect(state.translated).to.be.equal(false);
        expect(state.translation).to.be.equal('');
        expect(state.locale).to.be.equal('en');

        // Check for valid parse
        const parsedArray = TranslatorService.parseSimpleTransactionArray();
        expect(parsedArray.length).to.be.equal(1);

        await translatorService.translateWholeWindow();
        state = transObj.state();
        expect(state.translated).to.be.equal(true);
        expect(state.translation).to.be.equal(translationStruct.translation);
        expect(state.translationKey).to.be.equal(translationStruct.translationKey);
        expect(state.locale).to.be.equal(translationStruct.locale);
    });

    it ('should translate elements, when there are translations provided for pl locale', async () => {
        const transObj = shallow(<TransObject translationKey={translationStruct.translationKey}/>);

        CookieService.setCookie(new CookieStruct(cookieConstrants.LOCALE_LANG, 'pl'));
        translatorService = new TranslatorService();
        expect(CookieService.getCookie(cookieConstrants.LOCALE_LANG)).to.be.equal('pl');
        let state = transObj.state();
        expect(state.translated).to.be.equal(false);
        expect(state.translation).to.be.equal('');
        expect(state.locale).to.be.equal('en');

        await translatorService.translateWholeWindow();
        state = transObj.state();
        expect(state.translated).to.be.equal(true);
        expect(state.translation).to.be.equal(translatedStruct.translation);
        expect(state.translationKey).to.be.equal(translatedStruct.translationKey);
        expect(state.locale).to.be.equal(translatedStruct.locale);
    });

    it ('should pass validation on valid objects', () => {
        const transObj = shallow(<TransObject translationKey={translationStruct.translationKey}/>);
        const instance = transObj.instance();

        expect(() => {
            TranslatorService.validateTranslationObjects(instance, translatedStruct);
        }).to.not.throwError();
    });

    it ('should throw error for validation of elements', () => {
        const transObj = shallow(<TransObject translationKey={'aaa'}/>);
        const instance = transObj.instance();

        expect(() => {
            TranslatorService.validateTranslationObjects({}, {});
        }).to.throwError((e) => {
            expect(e).to.be.an(ValidationError);
            expect(e.message).to.be.equal('Object provided for translation is not instance of TransObj');
        });

        expect(() => {
            TranslatorService.validateTranslationObjects(instance, {});
        }).to.throwError((e) => {
            expect(e).to.be.an(ValidationError);
            expect(e.message).to.be.equal('Translated object is not instance of translatedStruct');
        });

        expect(() => {
            TranslatorService.validateTranslationObjects(instance, translatedStruct);
        }).to.throwError((e) => {
            expect(e).to.be.an(ValidationError);
            expect(e.message).to.be.equal('Keys does not match in translation: aaa !== dummy.key');
        });
    });

    it ('should not translate elements, when there are no translations', () => {
        translatorService.translateWholeWindow()
            .catch((e) => {
                expect(e).to.be.an(TranslationClientError);
            });
    });

    it ('should return valid store', () => {
        const storeToTest = TranslatorService.getTranslationStore();
        expect(window.translationStore).to.be.equal(storeToTest);
        expect(storeToTest.dispatch).to.be.an('function');
        expect(storeToTest.getState).to.be.an('function');
        expect(storeToTest.subscribe).to.be.an('function');
        expect(storeToTest.replaceReducer).to.be.an('function');
    });
});
