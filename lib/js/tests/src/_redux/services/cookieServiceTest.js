import CookieService from './../../../../src/_redux/services/cookieService';
import ValidationError from '../../../../src/error/validationError';
import CookieStruct from '../../../../src/struct/cookieStruct';
import { resetWindow } from '../../../../testConfig';
let expect = require('expect.js');

describe('Cookie service test', () => {
    beforeEach(() => {
        resetWindow();
    });

    after(() => {
        resetWindow();
    });

    it ('should not set cookie', () => {
        expect(() => {
            CookieService.setCookie();
        }).to.throwError((e) => {
            expect(e).to.be.an(ValidationError);
            expect(e.message).to.be.equal('argument provided is not instance of CookieStruct');
        });
    });

    it ('should get cookie', () => {
        document.cookie = 'john=doe';
        expect(CookieService.getCookie('john')).to.be.equal('doe');
    });

    it ('should return nothing', () => {
        document.cookie = undefined;
        expect(CookieService.getCookie('dummy-blabla')).to.be.equal('');
    });

    it ('should set cookie', () => {
        CookieService.setCookie(new CookieStruct('key', 'value'));
        expect(CookieService.getCookie('key')).to.be.equal('value');
    });
});
