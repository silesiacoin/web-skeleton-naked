import React from 'react';
import TransObject from "../../../../../src/components/generic/transObject";

export default class EmptyTranslationMock extends React.Component {
    render () {
        return (
            <div><span>Dummy content</span></div>
        );
    }
}
