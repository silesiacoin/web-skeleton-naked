import React from 'react';
import TransObject from "../../../../../src/components/generic/transObject";

export default class TranslationMock extends React.Component {
    render () {
        return (
            <div><TransObject
                translationkey={this.props.translationKey}
                translation={this.props.translation}
            /></div>
        );
    }
}
