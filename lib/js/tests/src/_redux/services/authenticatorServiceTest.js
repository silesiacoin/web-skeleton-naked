import fetchMock from "fetch-mock";
import ValidationError from '../../../../src/error/validationError';
import { resetWindow, withSscLocation } from '../../../../testConfig';
import AuthenticatorService from '../../../../src/_redux/services/authenticatorService';
import AuthenticatorError from '../../../../src/error/authenticatorError';
import defaultStore from '../../../../src/_redux/store/defaultStore';
import { authentication } from '../../../../src/_redux/reducers/authenticationReducer';
import {
    authenticationConstants,
    authenticationResponseConstants
} from '../../../../src/_redux/constants/authenticationConstants';
import RegistrationUser from '../../../../src/struct/registrationUser';

let expect = require('expect.js');

describe('Integration test of redux authenticator service', () => {
    const loginUrls = {
        fbLoginUrl: 'https://fb.logme',
        googleLoginUrl: 'https://goo.le'
    };
    let authenticatorService;
    let storeClass;

    beforeEach(() => {
        fetchMock.reset();
        fetchMock.get(
            'glob:https://api.silesiacoin.org/v1/auth',
            () => JSON.stringify(loginUrls)
        );
        fetchMock.put('glob:https://api.silesiacoin.org/v1/users', (url, { body }) => {
            const payload = JSON.parse(body);
            let registrationUser;

            const { email, plainPassword, errorsOccured } = payload;
            registrationUser = new RegistrationUser(email, plainPassword, plainPassword, errorsOccured ? false : 'on');

            if (registrationUser.errorsOccured) {
                return {
                    status: authenticationResponseConstants.REGISTRATION_ERROR_CODE,
                    body: JSON.stringify( { message: 'Invalid payload' })
                }
            }

            return {
                status: authenticationResponseConstants.REGISTRATION_SUCCESS_CODE,
                body: JSON.stringify({ message: `User ${email.email} registered` })
            };
        });

        withSscLocation();
        storeClass = undefined;
        window.csrf = 'DUMMYCSRF';
        authenticatorService = new AuthenticatorService();
    });

    after(() => resetWindow());

    it('should not register when no csrf found within window', () => {
        delete window.csrf;
        expect(() => new AuthenticatorService())
            .to.throwError((error) => {
                expect(error).to.be.an(ValidationError);
        })
    });

    it('should instantiate when csrf found', () => {
        expect(authenticatorService).to.be.an(AuthenticatorService);
    });

    it('should throw error when registerUser store has invalid state', (done) => {
        authenticatorService.registerUser()
            .catch((error) => {
                expect(error).to.be.an(AuthenticatorError);
                expect(error.message).to.be.equal('Invalid state, could not register user');
                done();
        });
    });

    it('should send api request to fetch login urls', async () => {
        authenticatorService.store = getDefaultStore({
            type: authenticationConstants.INITIAL,
        });

        await authenticatorService.fetchLoginUrls();

        const state = authenticatorService.store.getState();
        expect(state.type).to.be.equal(authenticationConstants.FETCHED_AUTH_LINKS);
    });

    function getDefaultStore(initialState = {}) {
        storeClass = new defaultStore(authentication);
        window.authenticatorStore = storeClass.registerStore(initialState);

        return window.authenticatorStore;
    }
});
