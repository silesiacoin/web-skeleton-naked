import defaultStore from '../../../../src/_redux/store/defaultStore';
import ValidationError from "../../../../src/error/validationError";
import { translationActions } from '../../../../src/_redux/actions/translationActions';
import { describe } from 'mocha';
import { translationConstants } from '../../../../src/_redux/constants/translationConstants';
import { translation } from "../../../../src/_redux/reducers/translationReducer";
let expect = require('expect.js');

describe('Test of defaultStore class', () => {
    const defaultAction = () => { return {
        type: 'dummy',
        message: 'elo'
    }};
    let reducer;
    let storeClass;
    let storeToTest;
    let storeState;

    afterEach(() => {
        storeClass = undefined;
        reducer = undefined;
        storeToTest = undefined;
        storeState = undefined;
    });

    it('should throw error when trying to register non-object type', () => {
        reducer = {};
        expect(() => {
            new defaultStore([]);
        }).to.throwError((e) => {
            expect(e).to.be.an(ValidationError);
        });

        expect(() => {
            new defaultStore('');
        }).to.throwError((e) => {
            expect(e).to.be.an(ValidationError);
        });

        expect(() => {
            new defaultStore({});
        }).to.throwError((e) => {
            expect(e).to.be.an(ValidationError);
        });
    });

    it('should create reducer', () => {
        storeClass = new defaultStore();
        expect(storeClass).to.be.ok();
    });

    // it('should register store', () => {
    //     storeClass = new defaultStore(defaultAction);
    //     storeToTest = storeClass.registerStore();
    //     storeState = storeToTest.getState();
    //     expect(storeClass).to.be.ok();
    //     expect(storeToTest.dispatch).to.be.an('function');
    //     expect(Object.keys(storeState.form).length).to.be.equal(0);
    //     expect(Object.keys(storeState.translation).length).to.be.equal(2);
    // });

    // it('should update store', () => {
    //     storeClass = new defaultStore();
    //     storeToTest = storeClass.registerStore();
    //     storeToTest.dispatch(translationActions.successSingle('hello'));
    //     storeState = storeToTest.getState();
    //     const translationState = storeState.translation;
    //     expect(translationState.type).to.be.equal(translationConstants.INITIAL);
    // });
});
