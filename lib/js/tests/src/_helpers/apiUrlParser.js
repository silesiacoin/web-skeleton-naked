import ApiUrlParser from './../../../src/_helpers/urlParser';
import ValidationError from "../../../src/error/validationError";
import { resetWindow } from "../../../testConfig";
let expect = require('expect.js');

describe('Test of validator helper', function () {
    after(() => {
        resetWindow();
    });

    it('Should throw error on invalid string', function () {
        expect(() => ApiUrlParser.parseGetUrl(
            '',
            'i18n/en'
        )).to.throwException((e) => {
            expect(e).to.be.an(ValidationError);
        });
        expect(() => ApiUrlParser.parseGetUrl(
            'api.silesiacoin.localhost',
            'i18n/en',
            '',
            ''
        )).to.throwException((e) => {
            expect(e).to.be.an(ValidationError);
        });
        expect(() => ApiUrlParser.parseGetUrl(
            'api.silesiacoin.localhost',
            'i18n/en',
            'fake.key',
            ''
        )).to.throwException((e) => {
            expect(e).to.be.an(ValidationError);
        });
        expect(() => ApiUrlParser.parseGetUrl(
            'api.silesiacoin.localhost',
            'i18n/en',
            '',
            'as'
        )).to.throwException((e) => {
            expect(e).to.be.an(ValidationError);
        });
    });

    it('Should not throw error on parseGetUrl', function() {
        global.window = {
            location: {
                hostname: 'silesiacoin.localhost',
                protocol: 'http'
            }
        };

        const createdFromWindowLocation = ApiUrlParser.createFromWindowLocation();

        const parsedUrl = ApiUrlParser.parseGetUrl(
            'api.silesiacoin.localhost',
            'i18n/en',
            'fake.key'
        );

        const parsedUrlWithoutArgs = ApiUrlParser.parseGetUrl(
            'api.silesiacoin.localhost',
            'i18n/en'
        );

        const parsedUrlWithHttp = ApiUrlParser.parseGetUrl(
            'api.silesiacoin.localhost',
            'i18n/en',
            '',
            'http'
        );

        const parsedUrlFromWindowLocation = ApiUrlParser.parseGetUrl(
            createdFromWindowLocation,
            'i18n/en',
            '',
            'https'
        );

        const parsedWithInjectionOfProtocl = ApiUrlParser.parseGetUrl(
            'api.silesiacoin.localhost/http://',
            'i18n/en',
            ''
        );

        expect(parsedUrl).to.be.equal('https://api.silesiacoin.localhost/i18n/en/fake.key');
        expect(parsedUrlWithoutArgs).to.be.equal('https://api.silesiacoin.localhost/i18n/en');
        expect(parsedUrlWithHttp).to.be.equal('http://api.silesiacoin.localhost/i18n/en');
        expect(parsedUrlFromWindowLocation).to.be.equal('https://api.silesiacoin.localhost/i18n/en');
        expect(parsedWithInjectionOfProtocl).to.be.equal('https://api.silesiacoin.localhost/http:///i18n/en');
    });

    it('should encode data properly', () => {
        const parsedFromObj = ApiUrlParser.encodeData({
            data: 'something',
            dummy: 'payload'
        });

        const parsedFromArray = ApiUrlParser.encodeData([
            'a', 'b', 'c'
        ]);

        // There is no support for this kind of operation in parsing url
        const parsedFromObjectsInArray = ApiUrlParser.encodeData([
            {something: 'a'},
            {something: 'b'},
        ]);

        expect(parsedFromObj).to.be.equal('data=something&dummy=payload');
        expect(parsedFromArray).to.be.equal('0=a&1=b&2=c');
        expect(parsedFromObjectsInArray).to.be.ok();
    });

    it('should throw, when provided no arguments', () => {
        expect(() => {
            ApiUrlParser.encodeData();
        }).to.throwException((e) => {
            expect(e).to.be.an(ValidationError);
        });
    });

    it('should create proper link to api from array params', () => {
        const linkForApiFromArray = ApiUrlParser.parseGetParameters(
            ApiUrlParser.parseGetUrl('dummyurl.com', 'elo'),
            ['key1', 'key2']
        );

        expect(linkForApiFromArray).to.be.equal('https://dummyurl.com/elo?0=key1&1=key2');
    });

    it('should not create link to api from invalid array params', () => {
        expect(() => {
            ApiUrlParser.parseGetParameters();
        }).to.throwException((e) => {
            expect(e).to.be.an(ValidationError);
        });

        expect(() => {
            ApiUrlParser.parseGetParameters('ssss');
        }).to.throwException((e) => {
            expect(e).to.be.an(ValidationError);
        });
    });

    it('should create link from window location', () => {
        global.window = {
            location: {
                hostname: 'silesiacoin.localhost',
                protocol: 'https'
            }
        };

        expect(ApiUrlParser.createFromWindowLocation()).to.be.equal('api.silesiacoin.localhost');
    });

    it('should create link from window location on port 8080', () => {
        global.window = {
            location: {
                hostname: 'silesiacoin.localhost',
                protocol: 'http',
                port: '8080'
            }
        };

        expect(ApiUrlParser.createFromWindowLocation()).to.be.equal('api.silesiacoin.localhost:8080');
    });

    it('should create link from window location on default port even if other is specified', () => {
        global.window = {
            location: {
                hostname: 'silesiacoin.localhost',
                protocol: 'http',
                port: '8081'
            }
        };

        expect(ApiUrlParser.createFromWindowLocation()).to.be.equal('api.silesiacoin.localhost');
    });

    it('should throw error that window is not defined', () => {
        window = undefined;

        expect(() => {
            ApiUrlParser.createFromWindowLocation();
        }).to.throwError((e) => {
            expect(e).to.be.an(ValidationError);
        });

        global.window = { location: {} };

        expect(() => {
            ApiUrlParser.createFromWindowLocation();
        }).to.throwError((e) => {
            expect(e).to.be.an(ValidationError);
        });

        global.window.location.hostname = 'silesiacoin.localhost';

        expect(() => {
            ApiUrlParser.createFromWindowLocation();
        }).to.throwError((e) => {
            expect(e).to.be.an(ValidationError);
        });

        global.window.location.protocol = 'https';

        expect(ApiUrlParser.createFromWindowLocation()).to.be.equal('api.silesiacoin.localhost');
    });

    it('should throw error on ApiParseUrl without valid arguments', () => {
        expect(() => {
            ApiUrlParser.parseApiUrl();
        }).to.throwError((error) => {
            expect(error).to.be.an(ValidationError);
            expect(error.message).to.be.equal('Invalid string');
        });

        expect(() => {
            ApiUrlParser.parseApiUrl('dummy.host');
        }).to.throwError((error) => {
            expect(error).to.be.an(ValidationError);
            expect(error.message).to.be.equal('Invalid string');
        });

        expect(() => {
            ApiUrlParser.parseApiUrl('dummy.host', 'dummyPath', '');
        }).to.throwError((error) => {
            expect(error).to.be.an(ValidationError);
            expect(error.message).to.be.equal('Invalid string');
        });

        expect(() => {
            ApiUrlParser.parseApiUrl('dummy.host', 'dummyPath', []);
        }).to.throwError((error) => {
            expect(error).to.be.an(ValidationError);
            expect(error.message).to.be.equal('Invalid string');
        });

        expect(() => {
            ApiUrlParser.parseApiUrl('dummy.host', 'dummyPath', {});
        }).to.throwError((error) => {
            expect(error).to.be.an(ValidationError);
            expect(error.message).to.be.equal('Invalid string');
        });
    });

    it('should return api url', () => {
        let apiUrl = ApiUrlParser.parseApiUrl('dummy.host', 'dummyPath');
        expect(apiUrl).to.be.equal('https://dummy.host/dummyPath');
        apiUrl = ApiUrlParser.parseApiUrl('dummy.host', 'dummyPath', 'http');
        expect(apiUrl).to.be.equal('http://dummy.host/dummyPath');
        apiUrl = ApiUrlParser.parseApiUrl('dummy.host', 'dummyPath', 'dummy');
        expect(apiUrl).to.be.equal('dummy://dummy.host/dummyPath');
    });
});
