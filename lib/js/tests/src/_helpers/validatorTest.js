import Validator from './../../../src/_helpers/validator';
import ValidationError from "../../../src/error/validationError";
let expect = require('expect.js');

describe('Test of validator helper', function () {
    it('Should throw error on invalid string', function () {
        expect(() => Validator.validateString()).to.throwException((e) => {
            expect(e).to.be.an(ValidationError);
        });
        expect(() => Validator.validateString('')).to.throwException((e) => {
            expect(e).to.be.an(ValidationError);
        });
        expect(() => Validator.validateString({})).to.throwException((e) => {
            expect(e).to.be.an(ValidationError);
        });
        expect(() => Validator.validateString('a', 2)).to.throwException((e) => {
            expect(e).to.be.an(ValidationError);
        });
    });

    it('Should not throw error on valid string', function() {
        expect(() => Validator.validateString('', 0)).to.be.ok();
        expect(() => Validator.validateString('a', 1)).to.be.ok();
        expect(() => Validator.validateString('aa', 2)).to.be.ok();
    });

    it('Should throw error on invalid string list', function () {
        expect(() => Validator.validateStrings(1, [
            'aaa',
            ''
        ])).to.throwException((e) => {
            expect(e).to.be.an(ValidationError);
        });
    });

    it('Should not throw error on valid string list', function () {
        expect(() => Validator.validateStrings(2, [
            'aaa',
            'hitler',
            'a'
        ])).to.be.ok();
    });

    it('Should not throw error when array is proper', function () {
        expect( () => Validator.validateArray([], 0)).to.be.ok();
        expect( () => Validator.validateArray([''], 1)).to.be.ok();
        expect( () => Validator.validateArray(['', ''], 1)).to.be.ok();
    });

    it('Should thorw error when array is not proper', function() {
        expect(() => Validator.validateArray('')).to.throwException((e) => {
            expect(e).to.be.an(ValidationError);
        });
        expect(() => Validator.validateArray([])).to.throwException((e) => {
            expect(e).to.be.an(ValidationError);
        });
        expect(() => Validator.validateArray([''], 3)).to.throwException((e) => {
            expect(e).to.be.an(ValidationError);
        });
    });
});
