import Email from './../../../src/struct/email';
import EmailError, { emailErrors } from '../../../src/error/emailError';
let expect = require('expect.js');

describe('Unit test of email struct', () => {
    it ('should throw error for invalid creation', () => {
        expect(() => {
            new Email({});
        }).to.throwError((e) => {
            expect(e).to.be.an(EmailError);
            expect(e.message).to.be.equal(emailErrors.INVALID_EMAIL);
        });

        expect(() => {
            new Email('');
        }).to.throwError((e) => {
            expect(e).to.be.an(EmailError);
            expect(e.message).to.be.equal(emailErrors.INVALID_EMAIL);
        });

        expect(() => {
            new Email('email@email@email.com');
        }).to.throwError((e) => {
            expect(e).to.be.an(EmailError);
            expect(e.message).to.be.equal(emailErrors.INVALID_EMAIL);
        });

        expect(() => {
            new Email('email@kk');
        }).to.throwError((e) => {
            expect(e).to.be.an(EmailError);
            expect(e.message).to.be.equal(emailErrors.INVALID_EMAIL);
        });

        expect(() => {
            new Email('e@m.p');
        }).to.throwError((e) => {
            expect(e).to.be.an(EmailError);
            expect(e.message).to.be.equal(emailErrors.INVALID_EMAIL);
        });

        expect(() => {
            new Email('em@pl.pl.');
        }).to.throwError((e) => {
            expect(e).to.be.an(EmailError);
            expect(e.message).to.be.equal(emailErrors.INVALID_EMAIL);
        });

        expect(() => {
            new Email('ew@"kr.p');
        }).to.throwError((e) => {
            expect(e).to.be.an(EmailError);
            expect(e.message).to.be.equal(emailErrors.INVALID_EMAIL);
        });

        expect(() => {
            new Email("dummy@kr.pl'");
        }).to.throwError((e) => {
            expect(e).to.be.an(EmailError);
            expect(e.message).to.be.equal(emailErrors.INVALID_EMAIL);
        });

        expect(() => {
            new Email([]);
        }).to.throwError((e) => {
            expect(e).to.be.an(EmailError);
            expect(e.message).to.be.equal(emailErrors.INVALID_EMAIL);
        });
    });

    it ('should create struct', () => {
        let email = new Email('ed@silesiacoin.org');
        expect(email).to.be.ok();
        expect(email.toString()).to.be.equal('ed@silesiacoin.org');
        email = new Email('ed@ssc.pl');
        expect(email).to.be.ok();
        expect(email.toString()).to.be.equal('ed@ssc.pl');
        email = new Email('eduardo@ssc.co');
        expect(email).to.be.ok();
        expect(email.toString()).to.be.equal('eduardo@ssc.co');
    });
});
