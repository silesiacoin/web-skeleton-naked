import CookieStruct from './../../../src/struct/cookieStruct';
import ValidationError from "../../../src/error/validationError";
let expect = require('expect.js');

describe('Unit test of cookie struct', () => {
    it ('should throw error for invalid creation', () => {
        expect(() => {
            new CookieStruct();
        }).to.throwError((e) => {
            expect(e).to.be.an(ValidationError);
        });

        expect(() => {
            new CookieStruct('key');
        }).to.throwError((e) => {
            expect(e).to.be.an(ValidationError);
        });

        expect(() => {
            new CookieStruct('', '');
        }).to.throwError((e) => {
            expect(e).to.be.an(ValidationError);
        });
    });

    it ('should create struct', () => {
        expect(new CookieStruct('key', 'value')).to.be.ok();
    });
});
