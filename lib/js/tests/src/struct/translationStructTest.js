import TranslationStruct from './../../../src/struct/translationStruct';
import ValidationError from "../../../src/error/validationError";
let expect = require('expect.js');

describe('Test creation of transtlation struct', function () {
    const emptyJsonObj = '{}';
    const invalidJsonStruct = '{"key": "value"}';
    const invalidJsonStruct1 = '{"key": ["value"]}';
    const invalidJsonStruct2 = '{"key": {"subkey": "valueKey"}}';
    const validJsonStruct = '{"locale": "en", "translationKey": "dummy.key", "translation": "ooo"}';

    it('should be ok, when valid values provided', () => {
        const translationStruct = new TranslationStruct(
            'locale',
            'trans.key',
            'translation value'
        );

        expect(() => translationStruct).to.be.ok();
        expect(translationStruct.locale).to.be.equal('locale');
        expect(translationStruct.translationKey).to.be.equal('trans.key');
        expect(translationStruct.translation).to.be.equal('translation value');
    });

    it('should throw error, when invalid values provided', () => {
        expect(() => new TranslationStruct()).to.throwException((e) => {
            expect(e).to.be.an(ValidationError);
        });
    });

    it('should be ok, when valid json provided', () => {
        const translationStruct = TranslationStruct.fromJson(validJsonStruct);

        expect(() => translationStruct).to.be.ok();
        expect(translationStruct.locale).to.be.equal('en');
        expect(translationStruct.translationKey).to.be.equal('dummy.key');
        expect(translationStruct.translation).to.be.equal('ooo');
    });

    it('should throw error, when invalid json provided', () => {
        expect(() => TranslationStruct.fromJson(emptyJsonObj)).to.throwException((e) => {
            expect(e).to.be.an(ValidationError);
        });
        expect(() => TranslationStruct.fromJson(invalidJsonStruct)).to.throwException((e) => {
            expect(e).to.be.an(ValidationError);
        });
        expect(() => TranslationStruct.fromJson(invalidJsonStruct1)).to.throwException((e) => {
            expect(e).to.be.an(ValidationError);
        });
        expect(() => TranslationStruct.fromJson(invalidJsonStruct2)).to.throwException((e) => {
            expect(e).to.be.an(ValidationError);
        });
    });

    it('should be ok, when valid object provided', () => {
        const translationStruct = TranslationStruct.fromObj(JSON.parse(validJsonStruct));

        expect(() => translationStruct).to.be.ok();
        expect(translationStruct.locale).to.be.equal('en');
        expect(translationStruct.translationKey).to.be.equal('dummy.key');
        expect(translationStruct.translation).to.be.equal('ooo');
    });

    it('should throw error, when invalid object provided', () => {
        expect(() => TranslationStruct.fromObj(JSON.parse(emptyJsonObj))).to.throwException((e) => {
            expect(e).to.be.an(ValidationError);
        });
        expect(() => TranslationStruct.fromObj(JSON.parse(invalidJsonStruct)).to.throwException((e) => {
            expect(e).to.be.an(ValidationError);
        }));
        expect(() => TranslationStruct.fromObj(JSON.parse(invalidJsonStruct1))).to.throwException((e) => {
            expect(e).to.be.an(ValidationError);
        });
        expect(() => TranslationStruct.fromObj(JSON.parse(invalidJsonStruct2))).to.throwException((e) => {
            expect(e).to.be.an(ValidationError);
        });
        expect(() => TranslationStruct.fromObj('')).to.throwException((e) => {
            expect(e).to.be.an(ValidationError);
        });
        expect(() => TranslationStruct.fromObj([])).to.throwException((e) => {
            expect(e).to.be.an(ValidationError);
        });
    });
});
