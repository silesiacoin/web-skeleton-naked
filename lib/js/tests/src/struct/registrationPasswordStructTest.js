import RegistrationPassword from './../../../src/struct/registrationPassword';
import PasswordError, { passwordErrors } from '../../../src/error/passwordError';
let expect = require('expect.js');

describe('Unit test of registrationPassword struct', () => {
    it ('should throw error for invalid creation', () => {
        expect(() => {
            new RegistrationPassword();
        }).to.throwError((e) => {
            expect(e).to.be.an(PasswordError);
            expect(e.message).to.be.equal(passwordErrors.INVALID_PASSWORD);
        });

        expect(() => {
            new RegistrationPassword('key');
        }).to.throwError((e) => {
            expect(e).to.be.an(PasswordError);
            expect(e.message).to.be.equal(passwordErrors.INVALID_PASSWORD);
        });

        expect(() => {
            new RegistrationPassword('eightle', 'eightlen');
        }).to.throwError((e) => {
            expect(e).to.be.an(PasswordError);
            expect(e.message).to.be.equal(passwordErrors.DOES_NOT_MATCH);
        });

        expect(() => {
            new RegistrationPassword('eightle', 'eightle');
        }).to.throwError((e) => {
            expect(e).to.be.an(PasswordError);
            expect(e.message).to.be.equal(passwordErrors.INVALID_LENGTH);
        });

        expect(() => {
            new RegistrationPassword('eightlen', 'eightlen');
        }).to.throwError((e) => {
            expect(e).to.be.an(PasswordError);
            expect(e.message).to.be.equal(passwordErrors.NO_CAPITAL_LETTER);
        });

        expect(() => {
            new RegistrationPassword('EIGHTLEN', 'EIGHTLEN');
        }).to.throwError((e) => {
            expect(e).to.be.an(PasswordError);
            expect(e.message).to.be.equal(passwordErrors.NO_LOWERCASE_LETTER);
        });

        expect(() => {
            new RegistrationPassword('EightleN', 'EightleN');
        }).to.throwError((e) => {
            expect(e).to.be.an(PasswordError);
            expect(e.message).to.be.equal(passwordErrors.NO_DIGIT);
        });

        expect(() => {
            new RegistrationPassword('EightleN1', 'EightleN1');
        }).to.throwError((e) => {
            expect(e).to.be.an(PasswordError);
            expect(e.message).to.be.equal(passwordErrors.NO_SPECIAL_SIGN);
        });

        expect(() => {
            new RegistrationPassword({}, 'EightleN1');
        }).to.throwError((e) => {
            expect(e).to.be.an(PasswordError);
            expect(e.message).to.be.equal(passwordErrors.INVALID_PASSWORD);
        });

        expect(() => {
            new RegistrationPassword([]);
        }).to.throwError((e) => {
            expect(e).to.be.an(PasswordError);
            expect(e.message).to.be.equal(passwordErrors.INVALID_PASSWORD);
        });

        expect(() => {
            new RegistrationPassword('a', []);
        }).to.throwError((e) => {
            expect(e).to.be.an(PasswordError);
            expect(e.message).to.be.equal(passwordErrors.INVALID_PASSWORD);
        });
    });

    it ('should create struct', () => {
        const plainPassword = 'vAlidPasswo#rd1!xD';
        const password = new RegistrationPassword(plainPassword, plainPassword);
        expect(password).to.be.ok();
        expect(password.toString()).to.be.equal(plainPassword);
    });
});
