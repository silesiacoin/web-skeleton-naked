import RegistrationUser from './../../../src/struct/registrationUser';
import { emailErrors } from '../../../src/error/emailError';
import { passwordErrors } from '../../../src/error/passwordError';
let expect = require('expect.js');

describe('Unit test of registrationUser struct', () => {
    let registrationUser;

    it('should create user with errors', () => {
        registrationUser = new RegistrationUser('wrong');
        expect(registrationUser.errorsOccured).to.be.equal(true);
        expect(registrationUser.errors.email).to.be.equal(emailErrors.INVALID_EMAIL);
        expect(registrationUser.errors.password).to.be.equal(passwordErrors.INVALID_PASSWORD);
        expect(Object.keys(registrationUser.errors).length).to.be.equal(3);

        registrationUser = new RegistrationUser({}, []);
        expect(registrationUser.errorsOccured).to.be.equal(true);
        expect(registrationUser.errors.email).to.be.equal(emailErrors.INVALID_EMAIL);
        expect(registrationUser.errors.password).to.be.equal(passwordErrors.INVALID_PASSWORD);
        expect(Object.keys(registrationUser.errors).length).to.be.equal(3);

        registrationUser = new RegistrationUser('email@email@email', 'dummy', 'krummy');
        expect(registrationUser.errorsOccured).to.be.equal(true);
        expect(registrationUser.errors.email).to.be.equal(emailErrors.INVALID_EMAIL);
        expect(registrationUser.errors.password).to.be.equal(passwordErrors.DOES_NOT_MATCH);
        expect(Object.keys(registrationUser.errors).length).to.be.equal(3);

        registrationUser = new RegistrationUser('email@com', 'dummy', 'dummy');
        expect(registrationUser.errorsOccured).to.be.equal(true);
        expect(registrationUser.errors.email).to.be.equal(emailErrors.INVALID_EMAIL);
        expect(registrationUser.errors.password).to.be.equal(passwordErrors.INVALID_LENGTH);
        expect(Object.keys(registrationUser.errors).length).to.be.equal(3);

        registrationUser = new RegistrationUser('email@dummy.com', 'dummydummy', 'dummydummy');
        expect(registrationUser.errorsOccured).to.be.equal(true);
        expect(registrationUser.errors.password).to.be.equal(passwordErrors.NO_CAPITAL_LETTER);
        expect(Object.keys(registrationUser.errors).length).to.be.equal(2);

        registrationUser = new RegistrationUser('email@dummy.com', 'DUMMYDUMMY', 'DUMMYDUMMY');
        expect(registrationUser.errorsOccured).to.be.equal(true);
        expect(registrationUser.errors.password).to.be.equal(passwordErrors.NO_LOWERCASE_LETTER);
        expect(Object.keys(registrationUser.errors).length).to.be.equal(2);

        registrationUser = new RegistrationUser('email@dummy.com', 'dummydummY', 'dummydummY');
        expect(registrationUser.errorsOccured).to.be.equal(true);
        expect(registrationUser.errors.password).to.be.equal(passwordErrors.NO_DIGIT);
        expect(Object.keys(registrationUser.errors).length).to.be.equal(2);

        registrationUser = new RegistrationUser('email@dummy.com', 'dummydummY1', 'dummydummY1');
        expect(registrationUser.errorsOccured).to.be.equal(true);
        expect(registrationUser.errors.password).to.be.equal(passwordErrors.NO_SPECIAL_SIGN);
        expect(Object.keys(registrationUser.errors).length).to.be.equal(2);

        registrationUser = new RegistrationUser('email@dummy.com', 'dummydummY1', 'dummydummY1', 'on');
        expect(registrationUser.errorsOccured).to.be.equal(true);
        expect(registrationUser.errors.password).to.be.equal(passwordErrors.NO_SPECIAL_SIGN);
        expect(Object.keys(registrationUser.errors).length).to.be.equal(1);
    });

    it('should create user without errors', () => {
        registrationUser = new RegistrationUser('email@dummy.com', 'dummydummY1!', 'dummydummY1!', 'on');
        expect(registrationUser.errorsOccured).to.be.equal(false);
        expect(Object.keys(registrationUser.errors).length).to.be.equal(0);
        const json = registrationUser.toJson();
        expect(json).to.be.equal('{"email":"email@dummy.com","password":"dummydummY1!"}');
    });
});
