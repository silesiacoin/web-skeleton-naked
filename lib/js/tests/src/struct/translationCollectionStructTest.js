import TranslationCollectionStruct from './../../../src/struct/translationCollectionStruct';
import ValidationError from "../../../src/error/validationError";
import TranslationStruct from "../../../src/struct/translationStruct";
let expect = require('expect.js');

describe('Test creation of transtlation collection struct', function () {
    const emptyJsonObj = '{}';
    const invalidJsonStruct = '{"key": "value"}';
    const invalidJsonStruct1 = '{"key": ["value"]}';
    const invalidJsonStruct2 = '{"key": {"subkey": "valueKey"}}';

    it('should throw error, when invalid json provided', () => {
        expect(() => new TranslationCollectionStruct()).to.throwException((e) => {
            expect(e).to.be.an(ValidationError);
        });
        expect(() => new TranslationCollectionStruct(emptyJsonObj)).to.throwException((e) => {
            expect(e).to.be.an(ValidationError);
        });
        expect(() => new TranslationCollectionStruct(emptyJsonObj)).to.throwException((e) => {
            expect(e).to.be.an(ValidationError);
        });
        expect(() => new TranslationCollectionStruct(invalidJsonStruct)).to.throwException((e) => {
            expect(e).to.be.an(ValidationError);
        });
        expect(() => new TranslationCollectionStruct(invalidJsonStruct1)).to.throwException((e) => {
            expect(e).to.be.an(ValidationError);
        });
        expect(() => new TranslationCollectionStruct(invalidJsonStruct2)).to.throwException((e) => {
            expect(e).to.be.an(ValidationError);
        });
    });

    it ('should create proper translation struct', () => {
        const validStructObj = new TranslationStruct(
            'en',
            'dummy.key',
            'dummy'
        );
        const validStructObj2 = new TranslationStruct(
            'en',
            'dummy.key2',
            'dummy'
        );

        const translationCollectionStruct = new TranslationCollectionStruct(
            JSON.stringify([validStructObj, validStructObj2])
        );
        const translations = translationCollectionStruct.translations;
        expect(translations[0].translationKey).to.be.equal(validStructObj.translationKey);
        expect(translations[0].locale).to.be.equal(validStructObj.locale);
        expect(translations[0].translation).to.be.equal(validStructObj.translation);
        expect(translations[1].translationKey).to.be.equal(validStructObj2.translationKey);
        expect(translations[1].locale).to.be.equal(validStructObj2.locale);
        expect(translations[1].translation).to.be.equal(validStructObj2.translation);

        const simpleArrayTranslations = translationCollectionStruct.toSimpleArray();
        expect(simpleArrayTranslations[0]).to.be.equal(validStructObj.translationKey);
        expect(simpleArrayTranslations[1]).to.be.equal(validStructObj2.translationKey);
    });
});
