import ValidationError from './validationError';

export const emailErrors = {
    INVALID_EMAIL: 'invalid.email'
};

export default class EmailError extends ValidationError {
    constructor(message) {
        super(message);
        this.name = this.constructor.name;
        this.code = 500;
    }
}
