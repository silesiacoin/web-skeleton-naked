import ValidationError from './validationError';

export const passwordErrors = {
    INVALID_PASSWORD: 'passwords.invalid',
    DOES_NOT_MATCH: 'passwords.does.not.match',
    INVALID_LENGTH: 'passwords.invalid.length',
    NO_CAPITAL_LETTER: 'passwords.no.capital',
    NO_LOWERCASE_LETTER: 'passwords.no.lowercase',
    NO_DIGIT: 'passwords.no.digit',
    NO_SPECIAL_SIGN: 'passwords.no.special.sign'
};

export default class PasswordError extends ValidationError {
    constructor(message) {
        super(message);
        this.name = this.constructor.name;
        this.code = 500;
    }
}
