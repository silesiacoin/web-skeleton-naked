export default class TranslationClientError extends Error {
    constructor(message = 'route not found') {
        super(message);
        this.name = this.constructor.name;
        this.code = 404;
    }
}
