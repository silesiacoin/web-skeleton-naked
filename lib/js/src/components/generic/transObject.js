import React from 'react';
import ValidationError from '../../error/validationError';
import Validator from '../../_helpers/validator';
import TranslationStruct from '../../struct/translationStruct';
import { translationActions } from '../../_redux/actions/translationActions';
import TranslatorService from '../../_redux/services/translatorService';
import { translationConstants } from '../../_redux/constants/translationConstants';
import PropTypes from 'prop-types';

export default class TransObject extends React.Component {
    constructor(props) {
        super(props);

        if ('undefined' === typeof this.props.translationKey) {
            throw new ValidationError('TransKey not found in TransObj');
        }

        Validator.validateString(this.props.translationKey);

        this.state = {
            translated: false,
            locale: 'en',
            translationKey: this.props.translationKey,
            translation: ''
        };

        this.store = TranslatorService.getTranslationStore();
        this.handleStore();
    }

    changeTranslation(translationStruct, callback) {
        if (translationStruct instanceof TranslationStruct) {
            this.setState({
                translated: true,
                translationKey: translationStruct.translationKey,
                translation: translationStruct.translation,
                locale: translationStruct.locale
            }, callback);

            this.unsubscribe();

            return;
        }

        throw new ValidationError('translationStruct is not instance of TranslationStruct');
    }

    handleStore() {
        this.store.dispatch(translationActions.addToTranslationQueue(this.props.translationKey));

        this.unsubscribe = this.store.subscribe(() => {
            const state = this.store.getState();
            const isEmitting = () => state.type === translationConstants.EMIT_TRANSLATION;
            const isSameKey = () => state.translationKey === this.state.translationKey;

            if (isEmitting() && isSameKey()) {
                this.changeTranslation(
                    new TranslationStruct(
                        state.locale,
                        state.translationKey,
                        state.translation
                    )
                );
            }
        });
    }

    render () {
        return (this.state.translation);
    }
}

TransObject.propTypes = {
    translationKey: PropTypes.string
};