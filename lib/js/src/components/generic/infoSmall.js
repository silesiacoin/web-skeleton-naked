import React from 'react';
import PropTypes from 'prop-types';

class InfoSmall extends React.Component {
  render () {
    const divStyle = {
      width: '100%',
      display: 'flex',
      justifyContent: 'start',
      alignItems: 'center',
      flexDirection: 'column'
    };
    const pStyle = {
      fontFamily: 'HKGrotesk, sans-serif',
      fontSize: '24px',
      fontWeight: '500',
      fontStyle: 'normal',
      fontStretch: 'normal',
      lineHeight: 'normal',
      letterSpacing: 'normal',
      textAlign: 'center',
      color: '#3d3d3d',
      marginTop: '1.4rem',
      whiteSpace: 'pre-line'
    };
    return (
      <div style={divStyle}>
        <img src={this.props.imgSrc} width={'100%'} />
        <p style={pStyle}>{ this.props.title }</p>
      </div>
    );
  }
}

InfoSmall.propTypes = {
  title: PropTypes.oneOfType([
      PropTypes.string,
      PropTypes.object
  ]),
  imgSrc: PropTypes.string
};

export default InfoSmall;
