import React from 'react';
import PropTypes from 'prop-types';
import { Col, Row } from 'reactstrap';

class InfoLarge extends React.Component {
  constructor (props) {
    super(props);
    this.img = this.img.bind(this);
    this.text = this.text.bind(this);
  }
  img () {
    return (
      <Col sm={12} md={6}>
        <img src={this.props.imgSrc} width={'100%'} />
      </Col>);
  }
  text () {
    return (
      <Col sm={12} md={6} className='order-text-last'>
        <h1 className='box-container-title'>
          { this.props.title }
        </h1>
        <div className='line' />
        <p className='box-container-desc'>
          { this.props.desc }
        </p>
      </Col>);
  }
  render () {
    const imgSideEnum = Object.freeze({ 'imgLeftSide': 1, 'imgRightSide': 2 });
    let first = null;
    let second = null;

    switch (this.props.imgSide) {
      case imgSideEnum.imgLeftSide:
        first = this.img();
        second = this.text();
        break;
      case imgSideEnum.imgRightSide:
        first = this.text();
        second = this.img();
        break;
      default:
        first = this.img();
        second = this.text();
    }

    return (
      <Row className='row-eq-height box-container'>
        {first}
        {second}
      </Row>
    );
  }
}

InfoLarge.propTypes = {
  title: PropTypes.oneOfType([
      PropTypes.string,
      PropTypes.object
  ]),
  desc: PropTypes.oneOfType([
      PropTypes.string,
      PropTypes.object
  ]),
  imgSrc: PropTypes.string,
  imgSide: PropTypes.number
};

export default InfoLarge;
