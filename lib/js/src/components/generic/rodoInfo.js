import React from 'react';
import CookieService from '../../_redux/services/cookieService';
import CookieStruct from '../../struct/cookieStruct';
import TransObject from './transObject';

class RodoInfo extends React.Component {
    constructor() {
        super();
        const agreementFromCookie = CookieService.getCookie('ssc_rodo_agreement');
        this.shouldRender = !agreementFromCookie;
        this.state = {
            hiddenClass: ""
        };
        this.handleAgreement = this.handleAgreement.bind(this);
    }

    handleAgreement() {
        CookieService.setCookie(new CookieStruct('ssc_rodo_agreement', 'accepted'));
        this.setState({
            hiddenClass: 'hidden'
        });
    }

    render() {
        if (this.shouldRender === true) {
            return (
                <div id={'hiddenDiv'} className={this.state.hiddenClass}>
                    <div id={'rodoDiv'} className={'rodo-div'}>
                        <div className={'rodo-info'}>
                            <TransObject className={'disclosure'} translationKey={'rodo.agreement.text'}/>
                        </div>
                        <button onClick={this.handleAgreement} className={'rodo-button mt-2'}>
                            <TransObject translationKey={'rodo.agreement.button'}/>
                        </button>
                    </div>
                </div>
            );
        }

        return ('')
    }
}

export default RodoInfo;
