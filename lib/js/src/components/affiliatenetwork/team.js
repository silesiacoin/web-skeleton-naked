import React, { Component } from 'react';
import { Row, Col } from 'reactstrap';
import TransObject from '../generic/transObject';

export class Team extends Component {
  render () {
    return (
      <Row className='mb-5 pb-5'>
        <Col xs={12}>
          <Row className='blue_section pb-5'>
            <Col xs={12}>
              <h2 className='map-title'><TransObject translationKey={'affiliatenetwork.similar.people'}/></h2>
            </Col>
          </Row>
          <Row className='bg-map  pb-5 mb-5 pt-xs-5 pt-md-0 mt-xs-5 mt-md-0'>
            <Col xs={{ size: 8, offset: 2 }} md={{ size: 4, offset: 4 }} className='p-xs-1 pb-xs-5 p-md-5'>
              <img
                  src='../assets/images/illustrations/img3-map.png'
                  width='100%'
                  alt={'map'}
              />
            </Col>
            <Col xs={4} />
          </Row>
        </Col>
      </Row>
    );
  }
}
