import React, { Component } from 'react';
import { Row, Col } from 'reactstrap';
import TransObject from '../generic/transObject';

export class Join extends Component {
  render () {
    return (
      <Row>
        <Col xs={12}>
          <Row className='violet_section pb-5'>
            <Col xs={12} className='pb-5 pr-2 pt-2'>
              <h2 className='Join-team-title pt-2 pb-2'> { <TransObject translationKey={'affiliatenetwork.join.team'}/> } </h2>
              <p className='Join-team-desc mb-4 p-1'> { <TransObject translationKey={'affiliatenetwork.we.will.teach'}/> } </p>
              <a href='/dashboard'>
                <button className="tree-button">
                  <TransObject translationKey={'become.a.pro'}/>
                </button>
              </a>
            </Col>
          </Row>
        </Col>
      </Row>
    );
  }
}
