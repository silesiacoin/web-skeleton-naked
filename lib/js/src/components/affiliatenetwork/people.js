import React, { Component } from 'react';
import { Row, Col } from 'reactstrap';
import TransObject from '../generic/transObject';

export class People extends Component {
  render () {
    return (
      <Row className='row box-container pb-5 pt-5'>
          <Col sm={{ size: 12 }} md={{ size: 8, offset: 2 }}>
              <Row className={'m-0'}>
                  <Col sm={12} md={6} className={'p-0 pl-md-5 pr-md-5 pb-md-5'}>
                      <img src={'https://storage.googleapis.com/ssc-assets/Developers_Pack/Example/affilate_ill_3.png'} width={'100%'} className={'mt-0'}/>
                  </Col>
                  <Col sm={12} md={6} className='order-text-last pb-5'>
                      <h1 className='box-container-title'>
                          <TransObject translationKey={'affiliatenetwork.create.team'}/>
                      </h1>
                      <p className='sub-title-info-large mt-2 mb-2'>
                          <TransObject translationKey={'affiliatenetwork.united'}/><br />
                          <TransObject translationKey={'affiliatenetwork.divided'}/>
                      </p>
                      <div className='line' />
                      <div className='box-container-desc mb-5'>
                          <p>+ <TransObject translationKey={'affiliatenetwork.secure.currency'}/></p>
                          <p>+ <TransObject translationKey={'affiliatenetwork.common.initiatives'}/></p>
                          <p>+ <TransObject translationKey={'affiliatenetwork.information.channel'}/></p>
                          <p>+ <TransObject translationKey={'affiliatenetwork.before.everything'}/></p>
                      </div>
                  </Col>
              </Row>
          </Col>
      </Row>
    );
  }
}
