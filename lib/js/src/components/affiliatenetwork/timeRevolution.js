import React, { Component } from 'react';
import { Row, Col } from 'reactstrap';
import TransObject from '../generic/transObject';

export class TimeRevolution extends Component {
  render () {
    return (
      <Row noGutters>
        <Col xs={12} >
          <Row noGutters className='margin-full violet_section pb-5 pr-2 pt-2'>
            <Col xs={12}>
              <h2 className='Its-time-to-rev mt-5 p-2'>{<TransObject translationKey={'affiliatenetwork.revolution.time'}/>}</h2>
              <p className='Its-time-to-rev-desc  mt-5 mb-5 p-5 align-content-center'>
                {<TransObject translationKey={'affiliatenetwork.revolution.hearts'}/>} <br />
                {<TransObject translationKey={'affiliatenetwork.revolution.safe'}/>} <br />
                {<TransObject translationKey={'affiliatenetwork.revolution.open.eyes'}/>} <br />
              </p>
            </Col>
          </Row>
        </Col>
      </Row>
    );
  }
}
