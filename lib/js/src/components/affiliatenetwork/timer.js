import React, {Component} from 'react';
import {Row, Col} from 'reactstrap';
import ValidationError from '../../error/validationError';
import PropTypes from 'prop-types';
import TransObject from '../generic/transObject';

export class Timer extends Component {
    constructor(props)
    {
        super(props);
        const revDateObject = this.guard();
        const secondsLeft = Math.floor((revDateObject - this.props.currentDate) / 1000);
        this.state = {
            currentDate: this.props.currentDate,
            time: {},
            seconds: secondsLeft
        };
    }
    guard()
    {
        const revDate = window.revDate;
        const revDateObject = new Date(revDate);
        const revDateCheck = revDateObject instanceof Date && isFinite(revDateObject);
        if (false === revDateCheck) {
            throw new ValidationError('revDate is invalid');
        }

        if ('undefined' === typeof this.props) {
            throw new Error('Wrong currentDate')
        }

        this.timer = 0;
        this.countdown = this.countdown.bind(this);

        return revDateObject;
    }
    static secondsConversion(secs)
    {
        const totalSeconds = secs;
        const days = Math.floor(secs / (60 * 60 * 24));
        const divisionForHours = secs % (60 * 60 * 24);
        const hours = Math.floor(divisionForHours / (60 * 60));
        const divisorForMinutes = secs % (60 * 60);
        const minutes = Math.floor(divisorForMinutes / 60);
        const divisorForSeconds = divisorForMinutes % 60;
        const seconds = Math.ceil(divisorForSeconds);

        return {
            'd': days,
            'h': hours,
            'm': minutes,
            's': seconds,
            't': totalSeconds
        };
    }

    componentDidMount()
    {
        let timeLeftVar = Timer.secondsConversion(this.state.seconds);
        this.setState({time: timeLeftVar});
        if (this.timer === 0) {
            this.timer = setInterval(this.countdown, 1000);
        }
    }

    countdown()
    {
        let seconds = this.state.seconds - 1;
        this.setState({
            time: Timer.secondsConversion(seconds),
            seconds: seconds
        });

        if (seconds < 1) {
            clearInterval(this.timer);
        }

        return seconds;
    }

    render()
    {
        if (this.state.seconds < 1) {
            return ('');
        }

        return (
            <Row noGutters>
                <Col xs={12}>
                    <Row className='large_violet_section pt-0'>
                        <Col xs={12} md={{size: 8}}>
                            <Row>
                                <Col xs={{size: 10, offset: 1}} md={{size: 8, offset: 2}}
                                     className='p-5 d-flex flex-column justify-content-center align-items-xs-center align-items-md-start'>
                                    <div className='timer-header mt-3 mb-5'>
                                        <TransObject translationKey={'affiliatenetwork.information.revolution.1'}/><br />
                                        <TransObject translationKey={'affiliatenetwork.information.revolution.2'}/>
                                    </div>
                                    {(() => {
                                        if (this.state.time.t <= 0) {
                                            return (
                                                <div className='mt-5 d-flex timer-flex-direct align-items-center justify-content-center'>
                                                <span className='timer-sec'>
                                            MickeyMouse: Im Batman.
                                            EdwardTubbs: No, you are Bruce Wayne.
                                                 </span>
                                                </div>
                                            )
                                        } else {
                                            return (
                                                <div>
                                                    <div className='mt-5 d-flex timer-flex-direct align-items-center justify-content-center'>
                                            <span className='timer-sec'>
                                             {this.state.time.t}
                                            </span>
                                                        <span className='timer-desc pl-xs-0 pl-md-2'>
                                                                                                          <TransObject translationKey={'affiliatenetwork.seconds'}/>
                                            </span>
                                                    </div>
                                                    <div className='mt-5 mb-5 d-flex timer-flex-direct align-items-center justify-content-center'>
                                            <span>
                                            <span className='timer-number'> {this.state.time.d} </span>
                                            <span className='timer-desc'><TransObject translationKey={'affiliatenetwork.days'}/></span>
                                            </span>
                                                        <span className='pl-xs-0 pl-md-2'>
                                            <span className='timer-number'> {this.state.time.h} </span>
                                            <span className='timer-desc'><TransObject translationKey={'affiliatenetwork.hours'}/></span>
                                            </span>
                                                        <span className='pl-xs-0 pl-md-2'>
                                            <span className='timer-number'> {this.state.time.m} </span>
                                            <span className='timer-desc'><TransObject translationKey={'affiliatenetwork.minutes'}/></span>
                                            </span>
                                                    </div>
                                                </div>
                                            )
                                        }
                                    })()}
                                </Col>
                            </Row>
                        </Col>
                        <Col xs={12} md={{size: 4}} className='pr-0 pl-0'>
                            <img
                                src={'https://storage.googleapis.com/ssc-assets/Developers_Pack/Photography/AffiliateNetwork/img2.png'}
                                width={'100%'}
                                alt={'time is money'}
                            />
                        </Col>
                    </Row>
                </Col>
            </Row>
        );
    }
}

Timer.propTypes = {
    currentDate: PropTypes.number
};
