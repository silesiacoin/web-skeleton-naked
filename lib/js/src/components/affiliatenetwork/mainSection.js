import React, { Component } from 'react';
import { Row, Col } from 'reactstrap';
import TransObject from '../generic/transObject';

export class MainSection extends Component {
  render () {
    return (
      <Row noGutters>
        <Col xs={12}>
          <Row noGutters className='margin-full'>
            <Col xs={12} md={{ size: 4 }} >
              <img
                  src={'https://storage.googleapis.com/ssc-assets/Developers_Pack/Photography/AffiliateNetwork/img1.png'}
                  width={'100%'}
                  alt={'train to the future'}
              />
            </Col>
            <Col xs={12} md={{ size: 6 }} className='p-3'>
              <Row noGutters>
                <Col xs={{ size: 10, offset: 1 }} md={{ size: 8, offset: 2 }}>
                  <h1 className='main-selection-till-title mt-4'>{<TransObject translationKey={'affiliatenetwork.looking.for'}/>}</h1>
                  <div className='ornament' />
                  <p className='main-selection-till-desc mb-5 '>{<TransObject translationKey={'affiliatenetwork.become.part.investors'}/>}</p>
                </Col>
              </Row>
              <Row noGutters className='mt-2'>
                <Col xs={{ size: 6, offset: 0 }} md={{ size: 4, offset: 2 }} className='d-flex flex-column justify-content-md-start justify-content-xs-center align-content-center pr-xs-0 pr-md-2 p-1'>
                  <img
                      src={'https://storage.googleapis.com/ssc-assets/Developers_Pack/content/learning.jpg'}
                      width={'100%'}
                      alt={'learning'}
                  />
                  <p className='main-selection-till mt-xs-3 mt-md-1'>{<TransObject translationKey={'affiliatenetwork.possibility.to.learn'}/>}</p>
                </Col>
                <Col xs={{ size: 6, offset: 0 }} md={{ size: 4, offset: 2 }} className='d-flex  flex-column justify-content-md-start justify-content-xs-center align-content-center pr-xs-0 pr-md-2 p-1 mb-xs-2 mb-md-0'>
                  <img
                      src={'https://storage.googleapis.com/ssc-assets/Developers_Pack/content/support.jpg'}
                      width={'100%'}
                      alt={'support'}
                  />
                  <p className='main-selection-till  mt-xs-3 mt-md-1'>{<TransObject translationKey={'affiliatenetwork.supporting.the.team'}/>}</p>
                </Col>
                <Col xs={{ size: 6, offset: 0 }} md={{ size: 4, offset: 2 }} className='d-flex flex-column  justify-content-md-start justify-content-xs-center align-content-center pr-xs-0 pr-md-2 p-xs-1 p-1'>
                  <img
                      src={'https://storage.googleapis.com/ssc-assets/Developers_Pack/content/beapart.jpg'}
                      width={'100%'}
                      alt={'be a part of something vast'}
                  />
                  <p className='main-selection-till mt-xs-3 mt-md-1'>{<TransObject translationKey={'affiliatenetwork.be.a.part'}/>}</p>
                </Col>
                <Col xs={{ size: 6, offset: 0 }} md={{ size: 4, offset: 2 }} className='d-flex flex-column  justify-content-md-start justify-content-xs-center align-content-center pr-xs-0 pr-md-2 p-xs-1 p-1 mb-xs-2 mb-md-0'>
                  <img
                      src={'https://storage.googleapis.com/ssc-assets/Developers_Pack/content/earn.jpg'}
                      width={'100%'}
                      alt={'earn with us'}
                  />
                  <p className='main-selection-till mt-xs-3 mt-md-1'>{<TransObject translationKey={'affiliatenetwork.opportunity.to.earn'}/>}</p>
                </Col>
              </Row>
            </Col>
          </Row>
        </Col>
      </Row>
    );
  }
}
