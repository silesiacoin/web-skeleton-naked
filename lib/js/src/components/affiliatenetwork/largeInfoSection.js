import React from 'react';
import { Row, Col } from 'reactstrap';
import RowGap from '../home/rowGap';
import InfoLarge from '../generic/infoLarge';
import TransObject from '../generic/transObject';

export default class LargeInfoSection extends React.Component {
  render () {
    return (
      <Row className='bg-white mt-5 mb-5'>
        <Col sm={{ size: 12 }} md={{ size: 8, offset: 2 }}>
          <RowGap />
            <InfoLarge title={<TransObject translationKey={'affiliatenetwork.blockchain.technology'}/>}
                       desc={<TransObject translationKey={'affiliatenetwork.ethereum.decentralized.platform'}/>}
                       imgSrc={'https://s3-eu-west-1.amazonaws.com/images.ssc/website/front/Illustration/AffiliateNetworkOne.svg'}
                       imgSide={1}
            />
          <RowGap />
            <InfoLarge title={<TransObject translationKey={'affiliatenetwork.become.developer'}/>}
                       desc={<TransObject translationKey={'affiliatenetwork.collectively.created'}/>}
                       imgSrc={'https://s3-eu-west-1.amazonaws.com/images.ssc/website/front/Illustration/AffiliateNetworkTwo.svg'}
                       imgSide={2}
            />
          <RowGap />
        </Col>
      </Row>
    );
  }
}
