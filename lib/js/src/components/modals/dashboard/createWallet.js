import React from 'react';
import { Modal, ModalHeader, ModalBody, Row, Col } from 'reactstrap';
import TranslatedComponent from '../../../containers/translatedComponent';
import DashboardService from '../../../_redux/services/dashboardService';
import CreateWalletBody from '../body/createWalletBody';
import { dashboardConstants } from '../../../_redux/constants/dashboardConstants';
import CookieService from '../../../_redux/services/cookieService';
import { authenticationActions } from '../../../_redux/actions/authenticationActions';
import authenticatorService from '../../../_redux/services/authenticatorService';

const walletSteps = {
    first:  'first',
    second: 'second',
    third:  'third'
};

export default class CreateWallet extends TranslatedComponent {
    constructor(props) {
        super(props);
        this.props = props;

        if ('undefined' === typeof props) {
            this.props = {};
        }

        const enabledFeature = CookieService.getCookie('ssc_not_finished');

        this.state = {
            enabledFeature: enabledFeature.length > 0,
            createWalletModalActive: false,
            activeStep: walletSteps.first,
            walletAddress: '',
            count: 0,
            walletId: 1
        };

        {'undefined' !== typeof this.props.wallet ? (
            this.state = {
                wallet: !!this.props.wallet
            }
        ) : (
            this.state = {
                wallet: ''
            }
        )}

        this.toggle = this.toggle.bind(this);
        this.showStepSecond = this.showStepSecond.bind(this);
        this.showStepThird = this.showStepThird.bind(this);

        this.autStore = authenticatorService.getAuthenticatorStore();

        this.store = DashboardService.getDashboardStore();
        this.unsubscribe = this.store.subscribe(() => {
            const state = this.store.getState();

            if (state.type === dashboardConstants.SHOW_CREATE_WALLET_MODAL) {
                return(
                    this.toggle()
                );
            }

            return state
        });
    }

    componentWillUnmount() {
        if ('function' === typeof this.unsubscribe) {
            this.unsubscribe();
        }
    }

    toggle() {
        const self = this;
        clearInterval(self.incrementer);

        this.setState({
            createWalletModalActive: !this.state.createWalletModalActive,
            activeStep: walletSteps.first,
            count: 0,
        });
    }

    showStepSecond() {
        const self = this;

        self.setState({
            activeStep: walletSteps.second,
        });

        self.incrementer = setInterval(function() {
            let count = self.state.count + 1;

            self.setState({
                count: count,
            });

            if(self.state.count === 4){
                self.showStepThird();
                clearInterval(self.incrementer)
            }
        }, 400);
    }

    showStepThird() {
        const wallet = this.props.wallet;
        const address = wallet.generateAddresses(1);

        this.setState({
            activeStep: walletSteps.third,
            walletAddress: address,
            walletId: this.state.walletId + 1,
        });

        this.autStore.dispatch(authenticationActions.walletObject(this.props.wallet));

        window.localStorage.setItem('numberOffWallets', this.state.walletId);
    }

    render() {
        return (
            <Modal isOpen={this.state.createWalletModalActive} toggle={this.toggle}>
                <ModalHeader toggle={this.toggle} className={'modal-header pt-4 pl-4 pl-md-5 pr-4 pr-md-5 pb-4'}>
                    <Row className={'p-0 m-0'}>
                        <Col xs="12" className={'p-0 m-0'}>
                            <h1 className={'modal-header__title modal-header__title--font-sm mb-5'}>
                                Creating address
                            </h1>
                        </Col>
                    </Row>
                    <div className={'modal-header__bar'}>
                        <div className={'modal-header__bar__progress ' + (this.state.activeStep === 'first' ? 'modal-header__bar__progress--first' : ' ') + (this.state.activeStep === 'second' ? 'modal-header__bar__progress--second' : ' ') + (this.state.activeStep === 'third' ? 'modal-header__bar__progress--third' : ' ')} role={'progressbar'} aria-valuenow={'25'} aria-valuemin={'0'} aria-valuemax={'100'}>
                        </div>
                    </div>
                </ModalHeader>
                {this.state.activeStep === 'first' &&
                <ModalBody className={'p-4 p-md-5 pb-5 pr-5 pr-sm-4 pr-md-4 modal-height'}>
                    {this.state.enabledFeature &&
                    <div>
                        <p className={'modal-alert modal-alert--red mb-4'}>
                            Be sure to remember your password. It’s not possible to recover it.<br/>
                            We recommend you take a pen and a card and save.
                        </p>
                        <Row className={'p-0 m-0'}>
                            <Col xs={"12"} md={'6'} className={'m-0 pl-0 pr-0 pr-sm-5'}>
                                <div className={'pr-5 pr-md-0 mr-0 mr-sm-5 mr-md-0'}>
                                    <CreateWalletBody/>
                                </div>
                            </Col>
                        </Row>
                    </div>
                    }
                    <Row className={'p-0 m-0'}>
                        <Col xs={"12"} className={'pl-0 pr-5 pr-sm-0 m-0'}>
                            <button id={'show-step-second'} onClick={this.showStepSecond}
                                    className={'modal-button modal-button--blue modal-button--margin'}>
                                Create your unique address
                            </button>
                        </Col>
                    </Row>
                    {this.state.enabledFeature &&
                    <div className={'mt-4'}>
                        <img src={'../assets/images/icons/button_keylock_icon.png'} className={'float-left mr-3'}
                             alt={'button keylock'}/>
                        <p className={'modal-info modal-info--sm mb-0 pt-2'}>
                            This information is private Your access to real financial resources
                        </p>
                    </div>
                    }
                </ModalBody>
                }
                {this.state.activeStep === 'second' &&
                <ModalBody className={'text-center modal-height'}>
                    <div className="lds-default lds-default--modal">
                        <div></div>
                        <div></div>
                        <div></div>
                        <div></div>
                        <div></div>
                        <div></div>
                        <div></div>
                        <div></div>
                        <div></div>
                        <div></div>
                        <div></div>
                        <div></div>
                    </div>
                    <p className={'modal-info modal-info--bg mb-0 mt-4'}>
                        Operation in progress
                    </p>
                </ModalBody>
                }
                {this.state.activeStep === 'third' &&
                <ModalBody className={'p-4 p-md-5 pb-5 pr-5 pr-sm-4 pr-md-4 modal-height'}>
                    <p className={'modal-text modal-text--bg'}>
                        Your public address
                    </p>
                    <div className={'pr-5 pr-sm-0'}>
                        <Row className={'m-0 pr-5'}>
                            <Col className={'m-0 pl-0 pr-0 pr-sm-0 pr-md-4 pr-lg-0 pr-xl-4'}>
                                <img src={'../assets/images/icons/wallet_squares.png'} className={'modal-wallet-squares float-right'} alt={'wallet squares'}/>
                                <div className={'overlay-hidden'}>
                                    <p className={'modal-key float-left mb-2'}>
                                        { this.state.walletAddress }
                                    </p>
                                </div>
                            </Col>
                        </Row>
                    </div>
                    <p className={'modal-alert modal-alert--green pt-5 mb-0'}>
                        Your address created succesfull
                    </p>
                    <Row className={'pt-4 m-0'}>
                        <Col xs={"12"} className={'pl-0 pr-5 pr-sm-0 mt-2'}>
                            <button id={'hide-modal'}
                                    onClick={this.toggle}
                                    className={'modal-button modal-button--blue mt-5'}>
                                OK
                            </button>
                        </Col>
                    </Row>
                </ModalBody>
                }
            </Modal>
        );
    }
}
