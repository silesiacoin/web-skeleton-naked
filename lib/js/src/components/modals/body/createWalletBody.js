import React from 'react';
import TranslatedComponent from '../../../containers/translatedComponent';
import { FormGroup, Input } from 'reactstrap';
import DashboardService from '../../../_redux/services/dashboardService';

export default class CreateWalletBody extends TranslatedComponent {
    constructor(props) {
        super(props);

        this.state = {
            formValues: {
                password1: '',
                password2: '',
            },
            formErrors: {
                password1: '',
                password2: '',
            }
        };

        this.store = DashboardService.getDashboardStore();
        this.handleChange = this.handleChange.bind(this);
    }

    handleChange(event) {
        const { name, value } = event.target;
        const { formValues } = this.state;
        const { formErrors } = this.state;

        this.setState({
            formValues: {
                ...formValues,
                [name]: value,
            }
        });

        switch (name) {
            case 'password1':
                value.length < 1 ?
                    (formErrors.password1 = 'za mało znaków') : (formErrors.password1 = '');

                break;
            case 'password2':
                this.state.formValues.password2 === this.state.formValues.password1 ?
                    (formErrors.password2 = 'GITARA') : (formErrors.password2 = 'NIE');
                break;
            default:
                break;
        }
    }

    render() {
        return (
            <form>
                <div className={'form-group'}>
                    <FormGroup>
                        <Input placeholder="password"
                               name="password1"
                               className={'form-control modal-input modal-input--width'}
                               onChange={this.handleChange}
                        />
                        <p className={'p-0 m-0'}>
                            {this.state.formErrors.password1}
                        </p>
                    </FormGroup>
                </div>
                <div className={'form-group mt-4'}>
                    <FormGroup>
                        <Input placeholder="repeat password"
                               name="password2"
                               className={'form-control modal-input modal-input--width'}
                               onChange={this.handleChange}
                        />
                        <p className={'p-0 m-0'}>
                            {this.state.formErrors.password2}
                        </p>
                    </FormGroup>
                </div>
            </form>
        );
    }
}
