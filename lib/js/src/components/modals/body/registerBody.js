import React from 'react';
import {
    Button,
    Input,
    FormGroup, FormText, Col, Row
} from 'reactstrap';
import authenticatorService from '../../../_redux/services/authenticatorService';
import TransObject from '../../generic/transObject';
import TranslatedComponent from '../../../containers/translatedComponent';
import { generateMnemonic, EthHdWallet } from 'eth-hd-wallet';
import { authenticationActions } from '../../../_redux/actions/authenticationActions'

export default class RegisterBody extends TranslatedComponent {
    constructor(props) {
        super(props);
        this.props = props;

        if ('undefined' === typeof props) {
            this.props = {};
        }

        this.state = {
            version: 0.1,
            formErrors: {
                checkbox: '',
            },
            available: !!this.props.available,
            checkboxValue: true,
            mnemonic: generateMnemonic().toString(),
        };

        this.store = authenticatorService.getAuthenticatorStore();

        this.refreshMnemonic = this.refreshMnemonic.bind(this);
        this.registerUser = this.registerUser.bind(this);
        this.checked = this.checked.bind(this);
        this.mapWalletToJSON = this.mapWalletToJSON.bind(this);
        this.handleChange = this.handleChange.bind(this);
    }

    componentWillUnmount() {
        if ('function' === typeof this.unsubscribe) {
            this.unsubscribe();
        }
    }

    refreshMnemonic() {
        this.setState({
            mnemonic: generateMnemonic().toString(),
        });
    }

    checked() {
        this.handleChange();

        this.setState({
            checkboxValue: !this.state.checkboxValue,
        });
    }

    registerUser() {
        if (this.state.available) {
            const wallet = EthHdWallet.fromMnemonic(this.state.mnemonic);
            const address = wallet.generateAddresses(1);
            this.store.dispatch(authenticationActions.walletObject(wallet));

            const jsonFile = this.mapWalletToJSON(this.state.version, this.state.mnemonic);

            const element = document.createElement("a");
            const file = new Blob([jsonFile], {type: 'application/json'});
            element.href = URL.createObjectURL(file);
            element.download = address;
            document.body.appendChild(element);
            element.click();

            window.localStorage.setItem('numberOffWallets', '1');

            this.store.dispatch(authenticationActions.authenticationModal());
        }

        if (!this.state.available) {
            this.setState({
                formErrors: {
                    checkbox: 'If you want to use our service, please do accept.'
                }
            })
        }
    }

    mapWalletToJSON(version, mnemonic){
        const walletObj = {
            version: version,
            mnemonic: mnemonic,
        };
        return JSON.stringify(walletObj);
    }

    handleChange() {
        const { formErrors } = this.state;
        let checkboxError = '';

        if (this.state.checkboxValue === false) {
            checkboxError = 'If you want to use our service, please do accept.';
            this.setState({
                available: false
            })
        }

        if (this.state.checkboxValue === true) {
            checkboxError = '';
            this.setState({
                available: true
            })
        }

        this.setState({
            formErrors: {
                ...formErrors,
                checkbox: checkboxError
            }
        });
    }

    render() {
        const mnemonicTable = this.state.mnemonic.match(/("[^"]+"|[^"\s]+)/g);

        return (
            <form className={'registerForm'}>
                <img id={'refresh-mnemonic'} onClick={this.refreshMnemonic}
                     src={'../assets/images/icons/refresh.svg'}
                     className={'modal-button-action mt-4'}
                />
                <div className={'form-group mt-3'}>
                    <FormGroup>
                        <Row className={'p-0 m-0'}>
                            <Col xs={'4'} className={'pl-0'}>
                                <Input placeholder="1." name="word-one" value={'1. ' + mnemonicTable[0]} className={'modal-input'} disabled/>
                            </Col>
                            <Col xs={'4'} className={'pl-0'}>
                                <Input placeholder="2." name="word-two" value={'2. ' + mnemonicTable[1]} className={'modal-input'} disabled/>
                            </Col>
                            <Col xs={'4'} className={'pl-0'}>
                                <Input placeholder="3." name="word-three" value={'3. ' + mnemonicTable[2]} className={'modal-input'} disabled/>
                            </Col>
                            <Col xs={'4'} className={'pl-0'}>
                                <Input placeholder="4." name="word-four" value={'4. ' + mnemonicTable[3]} className={'modal-input'} disabled/>
                            </Col>
                            <Col xs={'4'} className={'pl-0'}>
                                <Input placeholder="5." name="word-five" value={'5. ' + mnemonicTable[4]} className={'modal-input'} disabled/>
                            </Col>
                            <Col xs={'4'} className={'pl-0'}>
                                <Input placeholder="6." name="word-six" value={'6. ' + mnemonicTable[5]} className={'modal-input'} disabled/>
                            </Col>
                            <Col xs={'4'} className={'pl-0'}>
                                <Input placeholder="7." name="word-seven" value={'7. ' + mnemonicTable[6]} className={'modal-input'} disabled/>
                            </Col>
                            <Col xs={'4'} className={'pl-0'}>
                                <Input placeholder="8." name="word-eight" value={'8. ' + mnemonicTable[7]} className={'modal-input'} disabled/>
                            </Col>
                            <Col xs={'4'} className={'pl-0'}>
                                <Input placeholder="9." name="word-nine" value={'9. ' + mnemonicTable[8]} className={'modal-input'} disabled/>
                            </Col>
                            <Col xs={'4'} className={'pl-0'}>
                                <Input placeholder="10." name="word-ten" value={'10. ' + mnemonicTable[9]} className={'modal-input'} disabled/>
                            </Col>
                            <Col xs={'4'} className={'pl-0'}>
                                <Input placeholder="11." name="word-eleven" value={'11. ' + mnemonicTable[10]} className={'modal-input'} disabled/>
                            </Col>
                            <Col xs={'4'} className={'pl-0'}>
                                <Input placeholder="12." name="word-twelve" value={'12. ' + mnemonicTable[11]} className={'modal-input'} disabled/>
                            </Col>
                        </Row>
                    </FormGroup>
                </div>
                <p className={'modal-alert modal-alert--red'}>
                    <TransObject translationKey={'register.modal.alert'}/>
                </p>
                <div className="form-group">
                    <FormGroup>
                        <label className="container-checkbox">
                            <FormText className={'mt-2'}>
                                <TransObject translationKey={'accept.terms.and.policy'}/>
                            </FormText>
                            <Input id="checkbox" type="checkbox" name="checked" className={'ml-0 mr-0'} onClick={this.checked}/>
                            <span className="checkmark"></span>
                            <p className={'p-0 m-0 modal-alert modal-alert--red-sm'}>
                                {this.state.formErrors.checkbox}
                            </p>
                        </label>
                    </FormGroup>
                </div>
                <Button id={'register-button-available'}
                        className={'modal-button modal-button--authenticator pl-4 pr-4'}
                        onClick={this.registerUser}>
                    <TransObject translationKey={'become.a.pro'}/>
                </Button>
            </form>
        );
    }
}
