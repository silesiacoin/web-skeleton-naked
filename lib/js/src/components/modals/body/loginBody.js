import React from 'react';
import {
    Input,
    FormGroup, Col, Row, Button
} from 'reactstrap';
import authenticatorService from '../../../_redux/services/authenticatorService';
import TranslatedComponent from '../../../containers/translatedComponent';
import TransObject from '../../generic/transObject';
import { EthHdWallet } from 'eth-hd-wallet';
import { authenticationActions } from '../../../_redux/actions/authenticationActions';

export default class LoginBody extends TranslatedComponent {
    constructor(props) {
        super(props);
        this.props = props;

        if ('undefined' === typeof props) {
            this.props = {};
        }

        this.state = {
            available: !!this.props.available,
            mnemonic: '',
            error: '',
            one: '',
            two: '',
            three: '',
            four: '',
            five: '',
            six: '',
            seven: '',
            eight: '',
            nine: '',
            ten: '',
            eleven: '',
            twelve: '',
        };

        this.store = authenticatorService.getAuthenticatorStore();

        this.handleChange = this.handleChange.bind(this);
        this.loginUser = this.loginUser.bind(this);
        this.importFile = this.importFile.bind(this);
        this.handleChangeFile = this.handleChangeFile.bind(this);
    }

    componentWillUnmount() {
        if ('function' === typeof this.unsubscribe) {
            this.unsubscribe();
        }
    }

    loginUser() {
        if (this.state.available) {
            let wallet;
            try {
                wallet = EthHdWallet.fromMnemonic(this.state.mnemonic);
                wallet.generateAddresses(1);
            } catch(error) {
                this.setState({
                    error: 'Incorrect mnemonic phrase.'
                });
            }

            this.store.dispatch(authenticationActions.walletObject(wallet));
            this.store.dispatch(authenticationActions.authenticationModal());
        }

        if (!this.state.available) {
            this.setState({
                error: 'Incorrect mnemonic phrase.'
            })
        }
    }

    handleChangeFile(event)
    {
        const file = event.target.files[0];
        const fr = new FileReader();

        fr.onload = (event) => {
            const jsonFile = JSON.parse(event.target.result);
            const mnemonic = jsonFile.mnemonic;

            if (jsonFile.mnemonic) {
                const mnemonicTable = mnemonic.match(/("[^"]+"|[^"\s]+)/g);

                this.setState({
                    one: mnemonicTable[0],
                    two: mnemonicTable[1],
                    three: mnemonicTable[2],
                    four: mnemonicTable[3],
                    five: mnemonicTable[4],
                    six: mnemonicTable[5],
                    seven: mnemonicTable[6],
                    eight: mnemonicTable[7],
                    nine: mnemonicTable[8],
                    ten: mnemonicTable[9],
                    eleven: mnemonicTable[10],
                    twelve: mnemonicTable[11]
                }, () => {
                    const formValueTable = [
                        this.state.one,
                        this.state.two,
                        this.state.three,
                        this.state.four,
                        this.state.five,
                        this.state.six,
                        this.state.seven,
                        this.state.eight,
                        this.state.nine,
                        this.state.ten,
                        this.state.eleven,
                        this.state.twelve
                    ];

                    for(let j = 0; j < 12; j++){
                        if (formValueTable[j] === '') {
                            this.setState({
                                available: false
                            })
                        }

                        if (formValueTable[j] !== '') {
                            this.setState({
                                available: true,
                                error: ''
                            })
                        }
                    }

                    this.setState({
                        mnemonic: mnemonic
                    });
                });
            }

            if (!jsonFile.mnemonic) {
                this.setState({
                    error: 'Failed to load file.'
                })
            }
        };
        fr.readAsText(file);
        event.target.value = ''
    }

    importFile() {
        document.getElementById('input-file').click();
    }

    handleChange(event) {
        const { name, value } = event.target;

        this.setState({
            [name]: value,
        }, () => {
            const formValueTable = [
                this.state.one,
                this.state.two,
                this.state.three,
                this.state.four,
                this.state.five,
                this.state.six,
                this.state.seven,
                this.state.eight,
                this.state.nine,
                this.state.ten,
                this.state.eleven,
                this.state.twelve
            ];

            let mnemonicString = this.state.one;

            if (mnemonicString === '') {
                this.setState({
                    available: false
                })
            }

            if (mnemonicString !== '') {
                this.setState({
                    available: true
                })
            }

            for(let j = 1; j < 12; j++){
                mnemonicString += ' ' + formValueTable[j];

                if (formValueTable[j] === '') {
                    this.setState({
                        available: false
                    })
                }

                if (formValueTable[j] !== '') {
                    this.setState({
                        available: true
                    })
                }
            }

            this.setState({
                mnemonic: mnemonicString.toString()
            });
        });
    }

    render() {
        return (
            <form className={'registerForm'}>
                <img id={'import-file'}
                     alt={'Download icon'}
                     src={'../assets/images/icons/download.svg'}
                     onClick={this.importFile}
                     className={'modal-button-action cursor-pointer mt-4'}
                />
                <input id={'input-file'} type="file" name="files" accept=".json" onChange={this.handleChangeFile} hidden={'hidden'}/>
                <div className={'form-group mt-3 mb-0'}>
                    <FormGroup onChange={this.handleChange}>
                        <Row className={'p-0 m-0'}>
                            <Col xs={'4'} className={'pl-0'}>
                                <Input id="one" placeholder="1." name="one" className={'modal-input'} value={this.state.one}/>
                            </Col>
                            <Col xs={'4'} className={'pl-0'}>
                                <Input placeholder="2." name="two" className={'modal-input'} value={this.state.two}/>
                            </Col>
                            <Col xs={'4'} className={'pl-0'}>
                                <Input placeholder="3." name="three" className={'modal-input'} value={this.state.three}/>
                            </Col>
                            <Col xs={'4'} className={'pl-0'}>
                                <Input placeholder="4." name="four" className={'modal-input'} value={this.state.four}/>
                            </Col>
                            <Col xs={'4'} className={'pl-0'}>
                                <Input placeholder="5." name="five" className={'modal-input'} value={this.state.five}/>
                            </Col>
                            <Col xs={'4'} className={'pl-0'}>
                                <Input placeholder="6." name="six" className={'modal-input'} value={this.state.six}/>
                            </Col>
                            <Col xs={'4'} className={'pl-0'}>
                                <Input placeholder="7." name="seven" className={'modal-input'} value={this.state.seven}/>
                            </Col>
                            <Col xs={'4'} className={'pl-0'}>
                                <Input placeholder="8." name="eight" className={'modal-input'} value={this.state.eight}/>
                            </Col>
                            <Col xs={'4'} className={'pl-0'}>
                                <Input placeholder="9." name="nine" className={'modal-input'} value={this.state.nine}/>
                            </Col>
                            <Col xs={'4'} className={'pl-0'}>
                                <Input placeholder="10." name="ten" className={'modal-input'} value={this.state.ten}/>
                            </Col>
                            <Col xs={'4'} className={'pl-0'}>
                                <Input placeholder="11." name="eleven" className={'modal-input'} value={this.state.eleven}/>
                            </Col>
                            <Col xs={'4'} className={'pl-0'}>
                                <Input placeholder="12." name="twelve" className={'modal-input'} value={this.state.twelve}/>
                            </Col>
                        </Row>
                    </FormGroup>
                    <p className={'p-0 m-0 modal-alert modal-alert--red-sm'}>
                        {this.state.error}
                    </p>
                    <Button id={'log-in'}
                            onClick={this.loginUser}
                            className={'modal-button modal-button--authenticator mt-5 pl-4 pr-4'}>
                        <TransObject translationKey={'modal.log.in'}/>
                    </Button>
                </div>
            </form>
        );
    }
}
