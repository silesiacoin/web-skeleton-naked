import React from 'react';
import Parser from 'html-react-parser';
import ValidationError from '../../error/validationError';
import CookieService from '../../_redux/services/cookieService';
import Validator from '../../_helpers/validator';
import { Form, FormGroup, Input, Label } from 'reactstrap';
import TransObject from '../generic/transObject';
import {cookieConstrants} from "../../_redux/constants/cookieConstants";

export default class LanguageSwitch extends React.Component {
    constructor(props) {
        super(props);

        let currentLanguage = CookieService.getCookie(cookieConstrants.LOCALE_LANG);

        if (currentLanguage.length < 1) {
            currentLanguage = 'en';
        }

        // Move it to api call
        this.supportedLanguages = ['pl', 'en'];

        this.state = {
            currentLanguage: currentLanguage
        };

        this.handleSwitchEvent = this.handleSwitchEvent.bind(this);
    }

    handleSwitchEvent(event) {
        if ('undefined' === typeof event || event.type !== 'change' || 'string' !== typeof event.target.value) {
            throw new ValidationError('Translation Event is invalid');
        }

        this.switchLanguage(event.target.value);
    }

    switchLanguage(lang) {
        Validator.validateString(lang);

        const findElement = (element) => {
            return element === lang;
        };

        if (!this.supportedLanguages.find(findElement)) {
            throw new ValidationError('Language is not supported');
        }

        this.setState({
            currentLanguage: lang
        });

        let oldUrl = window.location.href;
        let currentUrl = new URL('?ssc_language='+lang,oldUrl);
        window.location.href = currentUrl.toString();

    }

    insertOptions() {
        const optionParser = (option) => {
            return `<option value='${option}'>${option.toUpperCase()}</option>`
        };
        let options = [];

        options.push(optionParser(this.state.currentLanguage));

        for (const option of this.supportedLanguages) {
            if (option.toUpperCase() !== this.state.currentLanguage.toUpperCase()) {
                options.push(optionParser(option));
            }
        }

        return options.toString();
    }

    render () {
        const options = this.insertOptions();

        return (
            <div>
                <Form>
                    <FormGroup>
                        <Label for={'languageSelect'}><TransObject translationKey={'change.language'}/></Label>
                        <Input
                            onChange={this.handleSwitchEvent}
                            type={'select'}
                            name={'languageSelect'}
                            id={'languageSelect'}
                        >{Parser(options)}</Input>
                    </FormGroup>
                </Form>
            </div>
        );
    }
}