import React, { Component } from 'react';
import { Row, Col, Modal, ModalHeader, ModalBody } from 'reactstrap';
import RegisterBody from './body/registerBody';
import LoginBody from './body/loginBody';
import authenticatorService from '../../_redux/services/authenticatorService';
import TransObject from '../generic/transObject';
import { authenticationConstants } from '../../_redux/constants/authenticationConstants';
import CookieService from '../../_redux/services/cookieService';
import PropTypes from 'prop-types';

export default class Authenticator extends Component {
    constructor(props) {
        super(props);
        this.props = props;

        if ('undefined' === typeof props) {
            this.props = {};
        }

        this.allowedTypes = ['register', 'login'];

        const enabledFeature = CookieService.getCookie('ssc_not_finished');

        this.state = {
            enabledFeature: enabledFeature.length > 0,
            modal: !!this.props.modal,
            facebookUrl: 'https://facebook.com',
            googleUrl: 'https://google.com',
            fetchedLoginUrls: false,
            overlapActive: this.allowedTypes[0],
        };

        this.toggle = this.toggle.bind(this);
        this.showLoginForm = this.showLoginForm.bind(this);
        this.showRegisterForm = this.showRegisterForm.bind(this);

        this.store = authenticatorService.getAuthenticatorStore();
        this.unsubscribe = this.store.subscribe(() => {
            const state = this.store.getState();

            if ('undefined' !== typeof state.loginUrls && false === this.state.fetchedLoginUrls) {
                this.setState({
                    facebookUrl: state.loginUrls.facebookUrl,
                    googleUrl: state.loginUrls.googleUrl,
                    fetchedLoginUrls: true
                });
            }

            if (state.type === authenticationConstants.AUTHENTICATION_MODAL) {
                return(
                    this.toggle()
                );
            }

            return state
        });
    }

    componentWillUnmount() {
        if ('function' === typeof this.unsubscribe) {
            this.unsubscribe();
        }
    }

    toggle() {
        this.setState({
            modal: !this.state.modal
        });
    }

    showRegisterForm() {
        this.setState({
            overlapActive: this.allowedTypes[0]
        })
    }

    showLoginForm() {
        this.setState({
            overlapActive: this.allowedTypes[1]
        })
    }

    render() {
        return (
            <Modal isOpen={this.state.modal} toggle={this.toggle}>
                <ModalHeader toggle={this.toggle} className={'modal-header pt-4 pl-4 pl-md-5 pr-4 pr-md-5 pb-3'}>
                    <Row className={'p-0 m-0'}>
                        {this.state.overlapActive === 'register' &&
                        <Col xs="12" md={{size: 12}} className={'p-0 m-0'}>
                            <h1 className={'modal-header__title modal-header__title--font-bg modal-header__title--padding mb-2'}>
                                <TransObject translationKey={'register.modal.title'}/>
                            </h1>
                            <p className={'modal-header__text mb-2'}>
                                <TransObject translationKey={'register.modal.text'}/>
                            </p>
                        </Col>
                        }
                        {this.state.overlapActive === 'login' &&
                        <Col xs="12" md={{size: 12}} className={'p-0 m-0'}>
                            <h1 className={'modal-header__title modal-header__title--font-bg modal-header__title--padding mb-2'}>
                                <TransObject translationKey={'login.modal.title'}/>
                            </h1>
                            <p className={'modal-header__text mb-2'}>
                                <TransObject translationKey={'login.modal.text'}/>
                            </p>
                        </Col>
                        }
                        {this.state.enabledFeature &&
                        <Col xs="12" md={{size: 3}} className={'p-0 pr-2 m-0'}>
                            <div className={'modal-header__sign-up-using'}>
                                {this.state.overlapActive === 'register' &&
                                <TransObject translationKey={'register.modal.sign.up.using'}/>
                                }
                                {this.state.overlapActive === 'login' &&
                                <TransObject translationKey={'login.modal.log.in.using'}/>
                                }
                            </div>
                            <a href={`${this.state.facebookUrl}`} className={'facebookLoginUrl'}><img
                                className={'modal-icons mt-3'} src={'../assets/images/icons/fb-icon.png'}
                                alt={'fb logo'}/></a>
                            <a href={`${this.state.googleUrl}`} className={'googleLoginUrl'}><img
                                className={'modal-icons mt-3 ml-3'} src={'../assets/images/icons/google-icon.png'}
                                alt={'google logo'}/></a>
                        </Col>
                        }
                    </Row>
                </ModalHeader>
                <ModalBody className={'p-0'}>
                    <Row className={'p-0 m-0'}>
                        <Col xs="12" md={{size: 5}} className={'p-0 m-0'}>
                            <img
                                src={'../assets/images/modal/' + (this.state.overlapActive === 'register' ? 'register_modal.jpg' : 'login_modal.jpg')}
                                alt={'become a pro!'}
                                className={'modal-image img-fluid'}/>
                        </Col>
                        <Col xs="12" md={{size: 7}} className={'pt-4 pt-md-5 pl-4 pl-md-5 pr-5 pb-5'}>
                            <Row className={'p-0 m-0'}>
                                <Col xs={'6'}
                                     id={'overlap-login'}
                                     className={'overlap cursor-pointer pl-0 pr-0 pb-3 ' + (this.state.overlapActive === 'login' ? 'overlap--active' : 'overlap--not-active')}
                                     onClick={this.showLoginForm}>
                                    <TransObject translationKey={'login.overlap'}/>
                                </Col>
                                <Col xs={'6'}
                                     id={'overlap-register'}
                                     className={'overlap cursor-pointer pl-0 pr-0 pb-3 ' + (this.state.overlapActive === 'register' ? 'overlap--active' : 'overlap--not-active')}
                                     onClick={this.showRegisterForm}>
                                    <TransObject translationKey={'register.overlap'}/>
                                </Col>
                            </Row>
                            {this.state.overlapActive === 'register' &&
                            <div className={'pr-0'}>
                                <RegisterBody/>
                            </div>
                            }
                            {this.state.overlapActive === 'login' &&
                            <div className={'pr-0'}>
                                <LoginBody/>
                            </div>
                            }
                        </Col>
                    </Row>
                </ModalBody>
            </Modal>
        );
    }
}

Authenticator.propTypes = {
    modal: PropTypes.bool
};
