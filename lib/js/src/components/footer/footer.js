import React from 'react';
import { Col, Container, Row } from 'reactstrap';

export default class Footer extends React.Component {
  render () {
    const year = new Date().getFullYear();
    return (
      <footer className='footer'>
        <Container fluid className='paddingZero'>
          <Row>
            <Col xs={6}>
              <img src={'https://storage.googleapis.com/ssc-assets/Developers_Pack/Logo/silesiacoin_small.png'} />
            </Col>
            <Col xs={6}>
              <p className='All-right-reserved'>{ `All rights reserved ${year}` }</p>
            </Col>
          </Row>
        </Container>
      </footer>
    );
  }
}
