import React from 'react';
import { Row, Col } from 'reactstrap';
import RowGap from '../home/rowGap';
import InfoLarge from '../generic/infoLarge';
import TransObject from '../generic/transObject';

export default class LargeInfoSection extends React.Component {
  render () {
    return (
      <Row className='bg-white mt-5 mb-5 ml-0 mr-0'>
        <Col sm={{ size: 12 }} md={{ size: 8, offset: 2 }}>
          <RowGap />
          <InfoLarge title={<TransObject translationKey={'businessEarning.based.on.ethereum'}/>}
                     desc={<TransObject translationKey={'businessEarning.ethereum.decentralized.platform'}/>}
                     imgSrc={'https://storage.googleapis.com/ssc-assets/Developers_Pack/Illustration/BussinessEarningOne.svg'}
                     imgSide={1}
          />
          <RowGap />
          <InfoLarge title={<TransObject translationKey={'businessEarning.smart.contracts'}/>}
                     desc={<TransObject translationKey={'businessEarning.new.generation.of.transactions'}/>}
                     imgSrc={'https://storage.googleapis.com/ssc-assets/Developers_Pack/Illustration/BussinessEarningTwo.svg'}
            imgSide={2} />
          <RowGap />
        </Col>
      </Row>
    );
  }
}
