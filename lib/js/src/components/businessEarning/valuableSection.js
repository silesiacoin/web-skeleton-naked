import React from "react";
import {Row, Col} from "reactstrap";
import TransObject from '../generic/transObject';

export default class ValuableSection extends React.Component {
    render() {
        return (
            <div className='miningContainer'>
                <Row>
                    <Col sm={ 2 } lg={ 2 } xl={ 2 } md={ 2 }>
                    </Col>
                    <Col sm={ 8 } lg={ 8 } xl={ 8 } md={ 8 }>
                        <div className='title_process_mining pb-4 mb-3'>
                            <TransObject translationKey={'businessEarning.ssc.stable'}/>
                        </div>
                        <div className='desc_process_mining  pb-4'>
                            <TransObject translationKey={'businessEarning.2019.change.first'}/><br />
                            <TransObject translationKey={'businessEarning.2019.change.second'}/>
                        </div>
                        <div className='miningContainer__rectangle-yellow'>
                        </div>
                        <Row>
                            <Col sm={ 12 } lg={ 6 } xl={ 6 } md={ 6 }>
                                <div
                                    className='desc_valuableSec miningContainer__text pt-4'>
                                    <TransObject translationKey={'businessEarning.real.surface'}/>
                                </div>
                                <div
                                    className='desc_valuableSec miningContainer__text pt-4'>
                                    <TransObject translationKey={'businessEarning.involved.programmers'}/>
                                </div>
                                <div
                                    className='desc_valuableSec miningContainer__text pt-4'>
                                    <TransObject translationKey={'businessEarning.anonymous.communication'}/>
                                </div>
                            </Col>
                            <Col sm={ 12 } lg={ 6 } xl={ 6 } md={ 6 }>
                                <div
                                    className='desc_valuableSec miningContainer__text pt-4'>
                                    <TransObject translationKey={'businessEarning.business.plan'}/>
                                </div>
                                <div
                                    className='desc_valuableSec miningContainer__text pt-4'>
                                    <TransObject translationKey={'businessEarning.years.of.experience'}/>
                                </div>
                                <div
                                    className='desc_valuableSec miningContainer__text pt-4 pb-5 mb-5'>
                                    <TransObject translationKey={'businessEarning.tutorials.and.support'}/>
                                </div>
                            </Col>
                        </Row>
                    </Col>
                </Row>
            </div>
        );
    }
}
