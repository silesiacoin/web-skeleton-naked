import React, { Component } from 'react';
import { Row, Col } from 'reactstrap';

export class People extends Component {
  render () {
    return (
      <Row className='row box-container pb-5 pt-5'>
        <Col sm={12} md={6}>
          <img
              src={'https://storage.googleapis.com/ssc-assets/Developers_Pack/Example/affilate_ill_3.png'}
              width={'100%'}
              alt={'people illustration'}
          />
        </Col>
        <Col sm={12} md={6} className='order-text-last pb-5'>
          <h1 className='box-container-title'>
                        You creating a team
          </h1>
          <p className='sub-title-info-large mt-2 mb-2'>
                        We are staying united<br />
                        divided we are falling down
          </p>
          <div className='line' />
          <div className='box-container-desc mb-5'>
            <p>+ Secure independent currency</p>
            <p>+ People in a virtual & real with common initiatives</p>
            <p>+ Anonymous/private information channel</p>
            <p>+ You are before everything. We start in 01.01.2019</p>
          </div>
        </Col>
      </Row>
    );
  }
}
