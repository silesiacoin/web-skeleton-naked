import React, { Component } from 'react';
import { Row, Col } from 'reactstrap';
import TransObject from '../generic/transObject';

export default class MainSection extends Component {
  render () {
    return (
        <Row noGutters className={'m-0'}>
          <Col xs={12}>
            <Row noGutters className='margin-full m-0'>
              <Col xs={12} md={{ size: 4 }} >
                <img
                    src={'https://storage.googleapis.com/ssc-assets/Developers_Pack/Photography/BussinessEarning/intro-info-graphics.png'}
                    width={'100%'}
                    alt={'intro info graphics'}
                />
              </Col>
              <Col xs={12} md={{ size: 6 }} className='p-3'>
                <Row noGutters className={'m-0'}>
                  <Col xs={{ size: 10, offset: 1 }} md={{ size: 8, offset: 2 }}>
                    <h1 className='main-selection-till-title mt-4'><TransObject translationKey={'businessEarning.invest.in.ssc'}/></h1>
                    <div className='ornament' />
                    <p className='main-selection-till-desc mb-5 '><TransObject translationKey={'businessEarning.become.part.investors'}/></p>
                  </Col>
                </Row>
                <Row noGutters className='mt-2 ml-0 mr-0'>
                  <Col xs={{ size: 6, offset: 0 }} md={{ size: 4, offset: 2 }} className='d-flex flex-column justify-content-md-start justify-content-xs-center align-content-center pr-xs-0 pr-md-2 p-1'>
                    <img
                        src={'https://storage.googleapis.com/ssc-assets/Developers_Pack/content/increse.jpg'}
                        width={'100%'}
                        alt={'increase'}
                    />
                    <p className='main-selection-till mt-xs-3 mt-md-1'><TransObject translationKey={'businessEarning.value.will.increase'}/></p>
                  </Col>
                  <Col xs={{ size: 6, offset: 0 }} md={{ size: 4, offset: 2 }} className='d-flex  flex-column justify-content-md-start justify-content-xs-center align-content-center pr-xs-0 pr-md-2 p-1 mb-xs-2 mb-md-0'>
                    <img
                        src={'https://storage.googleapis.com/ssc-assets/Developers_Pack/content/choice.jpg'}
                        width={'100%'}
                        alt={'choice'}
                    />
                    <p className='main-selection-till  mt-xs-3 mt-md-1'><TransObject translationKey={'businessEarning.invest.as.you.want'}/></p>
                  </Col>
                  <Col xs={{ size: 6, offset: 0 }} md={{ size: 4, offset: 2 }} className='d-flex flex-column  justify-content-md-start justify-content-xs-center align-content-center pr-xs-0 pr-md-2 p-xs-1 p-1'>
                    <img
                        src={'https://storage.googleapis.com/ssc-assets/Developers_Pack/content/integration.jpg'}
                        width={'100%'}
                        alt={'integration'}
                    />
                    <p className='main-selection-till mt-xs-3 mt-md-1'><TransObject translationKey={'businessEarning.crypto.integration'}/></p>
                  </Col>
                  <Col xs={{ size: 6, offset: 0 }} md={{ size: 4, offset: 2 }} className='d-flex flex-column  justify-content-md-start justify-content-xs-center align-content-center pr-xs-0 pr-md-2 p-xs-1 p-1 mb-xs-2 mb-md-0'>
                    <img
                        src={'https://storage.googleapis.com/ssc-assets/Developers_Pack/content/strategy.jpg'}
                        width={'100%'}
                        alt={'strategy'}
                    />
                    <p className='main-selection-till mt-xs-3 mt-md-1'><TransObject translationKey={'businessEarning.strategy.development'}/></p>
                  </Col>
                </Row>
              </Col>
            </Row>
          </Col>
        </Row>
    );
  }
}
