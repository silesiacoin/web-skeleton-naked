import React from 'react';
import { Col, Container, Row } from 'reactstrap';

const CryptoExchangeVolumeChart = () => {
    return (
            <Container id="crypto-exchange-volume-chart" className="mt-5">
                <Row>
					<Col xs={{size:10, offset:1}}>
                        <img
                            className="img-fluid"
                            src="https://storage.googleapis.com/ssc-assets/Developers_Pack/Photography/BussinessEarning/BussinessEarningGraph/BussinessEarningGraph4.png"
                            alt="total crypto exchange volume chart"
                        />
                    </Col>
                </Row>
            </Container>
    );
};

export default CryptoExchangeVolumeChart;
