import React from 'react';
import { Col, Container, Row } from 'reactstrap';

const WebsiteGrowthChart = () => {
    return (

            <Container id="website-growth-chart" className="mt-5">
                <Row>
					<Col xs={{size:10, offset:1}}>
                        <img
                            className="img-fluid"
                            src="https://storage.googleapis.com/ssc-assets/Developers_Pack/Photography/BussinessEarning/BussinessEarningGraph/BussinessEarningGraph1.png"
                            alt="website growth chart"
                        />
                    </Col>
                </Row>
            </Container>
    );
};

export default WebsiteGrowthChart;
