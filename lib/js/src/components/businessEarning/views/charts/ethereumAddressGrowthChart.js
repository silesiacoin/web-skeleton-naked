import React from 'react';
import { Col, Row, Container } from 'reactstrap';

const EthereumAddressGrowthChart = () => {
    return (
        <Container id="ethereum-address-growth-chart" className="pb-5">
            <Row noGutters>
                    <Col xs={{size:10, offset:1}}>
                        <img
                            className="img-fluid"
                            src="https://storage.googleapis.com/ssc-assets/Developers_Pack/Photography/BussinessEarning/BussinessEarningGraph/BussinessEarningGraph7.png"
                            alt="ethereum address growth chart"
                        />
                    </Col>
            </Row>
        </Container>
    );
};

export default EthereumAddressGrowthChart;
