import React from 'react';
import { Col, Container, Row } from 'reactstrap';

const BitcoinWalletAddressGrowthChart = () => {
    return (
            <Container id="bitcoin-wallet-address-growth-chart" className="mt-5 mb-5">
                <Row>
					<Col xs={{size:10, offset:1}}>
                        <img
                            className="img-fluid"
                            src="https://storage.googleapis.com/ssc-assets/Developers_Pack/Photography/BussinessEarning/BussinessEarningGraph/BussinessEarningGraph3.png"
                            alt="bitcoin wallet address growth chart"
                        />
                    </Col>
                </Row>
            </Container>
    );
};

export default BitcoinWalletAddressGrowthChart;
