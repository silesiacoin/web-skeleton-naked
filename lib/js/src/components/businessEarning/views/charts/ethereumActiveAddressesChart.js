import React from 'react';
import { Col, Container, Row } from 'reactstrap';

const EthereumActiveAddressesChart = () => {
    return (

            <Container id="ethereum-active-addresses-chart" className="mt-5">
                <Row>
					<Col xs={{size:10, offset:1}}>
                        <img
                            className="img-fluid"
                            src="https://storage.googleapis.com/ssc-assets/Developers_Pack/Photography/BussinessEarning/BussinessEarningGraph/BussinessEarningGraph6.png"
                            alt="ethereum active addresses per day chart"
                        />
                    </Col>
                </Row>
            </Container>

    );
};

export default EthereumActiveAddressesChart;
