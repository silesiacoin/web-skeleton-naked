import React from 'react';
import { Col, Container, Row } from 'reactstrap';

const CryptoUserGrowthChart = () => {
    return (
        <Container id="crypto-user-growth-chart" className="mt-5">
			<Row noGutters>
				<Col xs={{size:10, offset:1}}>
					<img className="img-fluid"
                        src="https://storage.googleapis.com/ssc-assets/Developers_Pack/Photography/BussinessEarning/BussinessEarningGraph/BussinessEarningGraph5.png"
                        alt="estimated crypto user growth chart" />
				</Col>
			</Row>
		</Container>
    );
};

export default CryptoUserGrowthChart;
