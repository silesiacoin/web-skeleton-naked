import React from 'react';
import { Col, Container, Row } from 'reactstrap';

const BitcoinActiveAddressesChart = () => {
    return (
            <Container id="bitcoin-active-addresses-chart" className="mt-5">
                <Row>
					<Col xs={{size:10, offset:1}}>
                        <img
                            className="img-fluid"
                            src="https://storage.googleapis.com/ssc-assets/Developers_Pack/Photography/BussinessEarning/BussinessEarningGraph/BussinessEarningGraph2.png"
                            alt="bitcoin active addresses per day chart"
                        />
                    </Col>
                </Row>
            </Container>
    );
};

export default BitcoinActiveAddressesChart;
