import React from 'react';
import { Col, Container, Row } from 'reactstrap';

const CryptoExchangeUserGrowthChart = () => {
    return (
            <Container id="crypto-exchange-user-growth-chart" className="mt-5">
                <Row>
					<Col xs={{size:10, offset:1}}>
                        <img
                            className="img-fluid"
                            src="https://storage.googleapis.com/ssc-assets/Developers_Pack/Photography/BussinessEarning/BussinessEarningGraph/BussinessEarningGraph8.png"
                            alt="crypto exchange user growth chart"
                        />
                    </Col>
                </Row>
            </Container>
    );
};

export default CryptoExchangeUserGrowthChart;
