import React from 'react';
import { Row, Col, Container } from 'reactstrap';

const EthereumBased = () => {
    return (
        <div className='ethereum-based-section'>
            <Container >
                <Row>
                    <div className="white-empty-space" >&nbsp;</div>
                    <Col xs={12} sm={6}>
                        <img src ="https://storage.googleapis.com/ssc-assets/Developers_Pack/Illustration/BussinessEarningOne.svg" alt="a diamond and two cube shapes"/>
                    </Col>
                    <Col xs={12} sm={6}>
                        <h2 className="h2purple">Based on Ethereum</h2>
                        <div className="ornament">&nbsp;</div>
                        <p2>Ethereum is a decentralized platform that runs smart contracts:<br/>
                            Without any possibility of downtime, censorship, fraud or third-party interference.</p2>
                    </Col>
                </Row>
            </Container>
        </div>
    )
};

export default EthereumBased;
