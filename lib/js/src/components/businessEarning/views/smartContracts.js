import React from 'react';
import { Row, Col, Container } from 'reactstrap';

const SmartContracts = () => {
    return (
        <div className="smart-contracts-section">
            <Container>
                <Row>
                    <div className="white-empty-space" >&nbsp;</div>
                    <Col xs={12} sm={6}>
                        <h2 className="h2purple">Smart Contracts</h2>
                        <div className="ornament">&nbsp;</div>
                        <p2>Ethereum is a decentralized platform that runs smart contracts:<br/>
                            applications that run exactly as programmed without any possibility of downtime,
                            censorship, fraud or third-party interference.</p2>
                    </Col>
                    <Col xs={12} sm={6}>
                        <img
                            src="https://storage.googleapis.com/ssc-assets/Developers_Pack/Illustration/BussinessEarningTwo.svg"
                            alt={'Business earning'}
                        />
                    </Col>
                </Row>
            </Container>
        </div>
    )
};

export default SmartContracts;
