import React from 'react';
import { Row, Col, Container } from 'reactstrap';
import scrollToChart from '../scrollToChart';
import TransObject from '../../generic/transObject';

const EthereumAdressGrowth = () => {
    const firstParagraphStyle = {
        color : '#ddcf08'
    };

    return (
        <Container className="ethereum-adress-growth-section pt-5">
                <Row noGutters>
                    <Col xs={12} sm={{size:4, offset:2}}>
                        <div className="ethereum-adress-growth-section__paragraph">
                            <a href="#ethereum-address-growth-chart"
                                    onClick={(e) => {e.preventDefault(); scrollToChart()}}
                                    style={firstParagraphStyle}><TransObject translationKey={'businessEarning.ethereum.adress.growth'}/>
                            </a>
                            </div>
                            <div className="ethereum-adress-growth-section__paragraph">
                                <a
                                    href="#crypto-exchange-user-growth-chart"
                                    onClick={(e) => {e.preventDefault(); scrollToChart()}}><TransObject translationKey={'businessEarning.crypto.user.growth'}/>
                                </a>
                            </div>
                            <div className="ethereum-adress-growth-section__paragraph">
                                <a
                                    href="#ethereum-active-addresses-chart"
                                    onClick={(e) => {e.preventDefault(); scrollToChart()}}><TransObject translationKey={'businessEarning.ethereum.active.addresses'}/>
                                </a>
                            </div>
                            <div className="ethereum-adress-growth-section__paragraph">
                                <a
                                    href="#crypto-user-growth-chart"
                                    onClick={(e) => {e.preventDefault(); scrollToChart()}}><TransObject translationKey={'businessEarning.estimated.crypto.user.growth'}/>
                                </a>
                            </div>
                        </Col>
                        <Col xs={12} sm={4} >
                            <div className="ethereum-adress-growth-section__paragraph">
                                <a
                                    href="#bitcoin-wallet-address-growth-chart"
                                    onClick={(e) => {e.preventDefault(); scrollToChart()}}><TransObject translationKey={'businessEarning.bitcoin.wallet.address.growth'}/>
                                </a>
                            </div>
                            <div className="ethereum-adress-growth-section__paragraph">
                                <a
                                    href="#bitcoin-active-addresses-chart"
                                    onClick={(e) => {e.preventDefault(); scrollToChart()}}><TransObject translationKey={'businessEarning.active.addresses.per.day'}/>
                                </a>
                            </div>
                            <div className="ethereum-adress-growth-section__paragraph">
                                <a
                                    href="#crypto-exchange-volume-chart"
                                    onClick={(e) => {e.preventDefault(); scrollToChart()}}><TransObject translationKey={'businessEarning.total.crypto.exchange'}/>
                                </a>
                            </div>
                            <div className="ethereum-adress-growth-section__paragraph">
                                <a
                                    href="#website-growth-chart"
                                    onClick={(e) => {e.preventDefault(); scrollToChart()}}><TransObject translationKey={'businessEarning.website.growth'}/>
                                </a>
                            </div>
                        </Col>
                    </Row>
        </Container>
    );
};

export default EthereumAdressGrowth;
