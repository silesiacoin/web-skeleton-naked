import React from 'react';
import { Row, Col, Container } from 'reactstrap';
import Translator from '../../../translator';

const MainSection = () => {
    const translator = new Translator();

    const rectangleYellowStyle = {
        marginTop: '14%'
    };
    const w100Style = {
        height: '50px'
    };
    const paddingZeroStyle = {
        padding: '0 !important'
    };
    const containerStyle = {
        maxWidth: '100%'
    };

    return (
        <div className="main-section">
            <Container style={containerStyle}>
                <Row>
                    <Col xs={12} md={4} className="main-section__graphics" style={paddingZeroStyle}>
                        <img className="img-fluid"
                             src="https://storage.googleapis.com/ssc-assets/Developers_Pack/Photography/BussinessEarning/intro-info-graphics.png"
                             alt="an opened notebook"/>
                    </Col>
                    <Col xs={12} md={8} className="main-section__details">
                        <div className="main-section__key-points">
                            <div className="main-section__key-points-header">{translator.trans('business.invest.in.ssc')}</div>
                            <div className="main-section__key-points-underscore__rectangle-yellow" style={rectangleYellowStyle}></div>
                            <div className="main-section__key-points-cta">Become a part of our investors & people interested <br/> to develop idea with us</div>
                        </div>
                        <div className="main-section__key-points-rectangle-area row">
                            <Col sm={12} md={6} lg={4} className="main-section__key-points-rectangle-area-col-one">
                                <div className="main-section__key-points-rectangle"></div>
                                <div className="main-section__key-points-text">A value of coin<br/>will increase</div>
                            </Col>
                            <Col sm={12} md={6} lg={4} className="main-section__key-points-rectangle-area-col-two">
                                <div className="main-section__key-points-rectangle"></div>
                                <div className="main-section__key-points-text">You can invest<br/>as much as you want</div>
                            </Col>
                            <div className="w-100" style={w100Style}></div>
                            <Col sm={12} md={6} lg={4} className="main-section__key-points-rectangle-area-col-three">
                                <div className="main-section__key-points-rectangle"></div>
                                <div className="main-section__key-points-text">Integration with<br/>ETC, BCT, LiteCoin</div>
                            </Col>
                            <Col sm={12} md={6} lg={4} className="main-section__key-points-rectangle-area-col-four">
                                <div className="main-section__key-points-rectangle"></div>
                                <div className="main-section__key-points-text">The business strategy<br/>development is defined</div>
                            </Col>
                        </div>
                    </Col>
                </Row>
            </Container>
        </div>
    );
};

export default MainSection;
