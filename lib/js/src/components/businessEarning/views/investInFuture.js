import React from 'react';
import { Row, Col, Jumbotron, Container, Button } from 'reactstrap';

const InvestInFuture = () => {
    return (
        <div className = "invest-in-future-section">
            <Jumbotron>
                <Container>
                    <Row>
                        <Col xs="12" sm={{size:12}}>
                            <h2 className = "invest-in-future-section__content-header">Invest in future</h2>
                            <p2 className ="invest-in-future-section__content-paragraph">Join the community & earn money</p2>
                            <Button className="invest-in-future-section__button btn btn-outline-warning">Became a pro</Button>
                        </Col>
                    </Row>
                </Container>
            </Jumbotron>
        </div>
    );
};

export default InvestInFuture;
