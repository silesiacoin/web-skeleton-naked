import React from 'react';
import { Col, Container, Jumbotron, Row } from 'reactstrap';
import TransObject from '../../generic/transObject';

const StableCurrency = () => {
    const ornamentStyle = {
        margin: '7% 0 7% 0'
    };

    return (
        <div className="stable-currency-section">
            <Container>
                <Jumbotron>
                    <Col xs={12}>
                        <h2 className="h2purple"><TransObject translationKey={'businessEarning.ssc.stable'}/></h2>
                        <p2 className="stable-currency-section__paragraph"><TransObject translationKey={'businessEarning.2019.change'}/></p2>
                        <div className ="ornament" style={ornamentStyle}>&nbsp;</div>
                    </Col>
                    <Row>
                        <Col xs={12} sm={6}>
                            <p2 className="stable-currency-section__point" >+ <TransObject translationKey={'businessEarning.real.surface'}/></p2>
                            <p2 className="stable-currency-section__point">+ <TransObject translationKey={'businessEarning.involved.programmers'}/></p2>
                            <p2 className="stable-currency-section__point">+ <TransObject translationKey={'businessEarning.anonymous.communication'}/></p2>
                        </Col>
                        <Col xs={12} sm={6}>
                            <p2 className="stable-currency-section__point">+ <TransObject translationKey={'businessEarning.business.plan'}/></p2>
                            <p2 className="stable-currency-section__point">+ <TransObject translationKey={'businessEarning.years.of.experience'}/></p2>
                            <p2 className="stable-currency-section__point">+ <TransObject translationKey={'businessEarning.tutorials.and.support'}/></p2>
                    </Col>
                    </Row>
                </Jumbotron>
            </Container>
        </div>
    );
};

export default StableCurrency;
