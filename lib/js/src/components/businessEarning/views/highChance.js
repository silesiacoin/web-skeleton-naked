import React from 'react';
import { Row, Col, Jumbotron, Container } from 'reactstrap';

const HighChance = () => {
    return (
        <div className = "high-chance-section">
            <Jumbotron>
                <Container>
                    <Row>
                        <Col xs="12" sm={{size:12}}>
                            <h2 className = "high-chance-section__content-header">High chance of earning investment</h2>
                            <p className ="high-chance-section__content-paragraph">We recommend investing part of the capital, the project has enormous potential,<br/>
                                in addition it is fresh
                            </p>
                        </Col>
                    </Row>
                </Container>
            </Jumbotron>
        </div>
        );
}

export default HighChance;
