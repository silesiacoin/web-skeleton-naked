import React from 'react';
import { Row, Col, Jumbotron, Container } from 'reactstrap';

const MarketPotential = () => {
    return (
        <div className = "market-potential-section blue_section">
            <Jumbotron>
                <Container>
                    <Row>
                        <Col xs="12" sm={{size:12}}>
                            <h2 className = "market-potential-section-header">Look why cryptocurrency market have fantastic potential</h2>
                            <p className ="market-potential-section-paragraph">SilesiaCoin is based on the ethereum source code</p>
                        </Col>
                    </Row>
                </Container>
            </Jumbotron>
        </div>
    );
}

export default MarketPotential;
