import React from 'react';
import { Row, Col, Jumbotron, Container, Button } from 'reactstrap';

const FutureIsSsc = () => {
    return (
        <div className="future-is-ssc-section">
            <Jumbotron>
                <Container>
                    <Row>
                        <Col className="future-is-ssc-section-text-column" xs={12} sm={8}>
                            <div className="future-is-ssc-section__header">Why the future is in SSC ?</div>
                            <div className="future-is-ssc-section__description">We have created our own programming infrastructure from scratch,<br/>
                                we have a strong affiliation not only virtual but also<br/>
                                reflected in reality in specific regions located in Poland
                            </div>
                            <Button className="future-is-ssc-section__button btn btn-outline-warning">Learn more</Button>
                        </Col>
                        <Col xs={12} sm={4}>
                            {/*no asset is available yet*/}
                            <img src="" alt={'no asset'}/>
                        </Col>
                    </Row>
                </Container>
            </Jumbotron>
        </div>
    )
};

export default FutureIsSsc;
