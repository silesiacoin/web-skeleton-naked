const scrollToChart = () => {
    $("a[href^='#']").click(function() {
        $('html, body').animate({
            scrollTop: $($(this).attr('href')).offset().top
        }, 500, 'linear');
    });
};

export default scrollToChart;
