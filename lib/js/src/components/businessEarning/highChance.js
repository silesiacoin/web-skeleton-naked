import React, { Component } from 'react';
import { Row, Col } from 'reactstrap';
import TransObject from '../generic/transObject';

export default class HighChance extends Component {
  render () {
    return (
      <Row noGutters className={'m-0'}>
        <Col xs={12} >
          <Row noGutters className='margin-full violet_section pb-5 pr-2 pt-2 m-0'>
            <Col xs={12}>
              <h2 className='Its-time-to-rev mt-5 p-2'><TransObject translationKey={'businessEarning.high.chance.investment'}/></h2>
              <p className='Its-time-to-rev-desc  mt-5 mb-5 p-5 align-content-center'>
                <TransObject translationKey={'businessEarning.recommend.investing.part'}/>
              </p>
            </Col>
          </Row>
        </Col>
      </Row>
    );
  }
}
