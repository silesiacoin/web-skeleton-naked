import React from 'react';
import TranslatedComponent from '../../containers/translatedComponent';

export default class Search extends TranslatedComponent {
    render() {
        return (
            <div>
                <div className={'search-item desktop-display'}>
                    <form className={'m-0'}>
                        <input type="text" id="search-input" name="search-input" className={'m-0'}/>
                    </form>
                </div>
            </div>
        );
    }
}
