import React from 'react';
import { Row, Col } from 'reactstrap';
import TranslatedComponent from '../../containers/translatedComponent';
import CookieService from '../../_redux/services/cookieService';

export default class ProfileHead extends TranslatedComponent {
    constructor(props) {
        super(props);

        const enabledFeature = CookieService.getCookie('ssc_not_finished');

        this.state = {
            enabledFeature: enabledFeature.length > 0,
            mainPageActive: !!this.props.mainPageActive,
        };
    }

    render() {
        return (
            <Row className={'p-0 m-0'}>
                <Col className={'p-0 m-0'}>
                    <div className={'dashboard-avatar float-left mr-4 ' + (this.state.mainPageActive ? 'desktop-display' : ' ')}>
                        <img src={'../assets/images/symbol/silesiacoin_small.png'}
                             alt={'Silesia coin logo'}
                             className={'dashboard-avatar__image'}/>
                    </div>
                    <div>
                        <h2 className={'dashboard-name ' + (this.state.mainPageActive ? 'dashboard-name--white' : 'dashboard-name--purple')}>
                            Silesiacoin
                        </h2>
                        <p className={'dashboard-ssc m-0 ' + (this.state.mainPageActive ? 'dashboard-ssc--main m-0' : 'dashboard-ssc--full m-0 pb-0 pb-sm-0 pb-md-5')}>
                            {this.state.enabledFeature &&
                            <img src={'../assets/images/symbol/silesiacoin_small.png'}
                                 alt={'Silesia coin logo'}
                                 className={'dashboard-ssc__logo mr-2'}/>
                            }
                            Network
                        </p>
                    </div>
                </Col>
            </Row>
        );
    }
}
