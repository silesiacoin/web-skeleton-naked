import React from 'react';
import TranslatedComponent from '../../containers/translatedComponent';
import DashboardService from '../../_redux/services/dashboardService';
import { dashboardActions } from '../../_redux/actions/dashboardActions';
import { dashboardConstants } from '../../_redux/constants/dashboardConstants';
import CookieService from '../../_redux/services/cookieService';

export default class Navbar extends TranslatedComponent {
    constructor(props) {
        super(props);
        this.props = props;

        if ('undefined' === typeof props) {
            this.props = {};
        }

        const enabledFeature = CookieService.getCookie('ssc_not_finished');

        this.state = {
            mainPageActive: !!this.props.mainPageActive,
            profilePanelActive: !!this.props.profilePanelActive,
            yourWalletsActive: !!this.props.yourWalletsActive,
            walletActive: !!this.props.walletActive,

            enabledFeature: enabledFeature.length > 0
        };

        this.store = DashboardService.getDashboardStore();
        this.showMainSections = this.showMainSections.bind(this);
        this.showYourWallets = this.showYourWallets.bind(this);
        this.unsubscribe = this.store.subscribe(() => {
            const state = this.store.getState();

            switch (state.type) {
                case dashboardConstants.SHOW_PROFILE:
                    return(
                        this.setState({
                            mainPageActive: false,
                            profilePanelActive: true,
                            yourWalletsActive: false,
                            walletActive: false,
                        })
                    );
                case dashboardConstants.SHOW_WALLET:
                    return(
                        this.setState({
                            mainPageActive: false,
                            profilePanelActive: false,
                            yourWalletsActive: false,
                            walletActive: true,
                        })
                    );
                case dashboardConstants.SHOW_YOUR_WALLETS:
                    return(
                        this.setState({
                            mainPageActive: false,
                            profilePanelActive: false,
                            yourWalletsActive: true,
                            walletActive: false,
                        })
                    );
                default:
                    return state
            }
        });
    }

    componentWillUnmount() {
        if ('function' === typeof this.unsubscribe) {
            this.unsubscribe();
        }
    }

    showMainSections() {
        this.store.dispatch(dashboardActions.showMainSections());
        this.setState({
            mainPageActive: true,
            profilePanelActive: false,
            yourWalletsActive: false,
            walletActive: false,
        })
    }

    showYourWallets() {
        this.store.dispatch(dashboardActions.showYourWallets());
    }

    render() {
        return (
            <div className={'p-2 m-0 navbar-after-login text-center'}>
                <img id={'show-main-sections-logo'}
                     alt={'Silesia coin logo'}
                     src={'../assets/images/symbol/silesiacoin_small.png'}
                     className={'cursor-pointer navbar-after-login__logo mt-3 mb-5'}
                     onClick={this.showMainSections}/>
                {this.state.mainPageActive ? (
                    <img id={'show-main-sections-home'}
                         alt={'Home icon active'}
                         src={'../assets/images/icons/home-icon-active.png'}
                         className={'cursor-pointer mr-4 mr-sm-4 mr-md-4 mr-xl-0 mr-lg-0 mt-0 mt-sm-0 mt-md-0 mt-lg-5 mt-xl-5 mb-0 mb-sm-0 mb-md-0 mb-lg-4 mb-xl-4'}
                         onClick={this.showMainSections}/>
                ) : (
                    <img id={'show-main-sections-home'}
                         alt={'Home icon not active'}
                         src={'../assets/images/icons/home-icon.png'}
                         className={'cursor-pointer mr-4 mr-sm-4 mr-md-4 mr-xl-0 mr-lg-0 mt-0 mt-sm-0 mt-md-0 mt-lg-5 mt-xl-5 mb-0 mb-sm-0 mb-md-0 mb-lg-4 mb-xl-4'}
                         onClick={this.showMainSections}/>
                )}
                {this.state.yourWalletsActive || this.state.walletActive ? (
                    <img id={'show-your-wallets'}
                         alt={'Wallet icon active'}
                         src={'../assets/images/icons/wallet-icon-active.png'}
                         className={'cursor-pointer mr-4 mr-sm-4 mr-md-4 mr-xl-0 mr-lg-0 ml-4 ml-sm-4 ml-md-4 ml-xl-0 ml-lg-0 mt-0 mt-sm-0 mt-md-0 mt-lg-4 mt-xl-4 mb-0 mb-sm-0 mb-md-0 mb-lg-4 mb-xl-4'}
                         onClick={this.showYourWallets}/>
                ) : (
                    <img id={'show-your-wallets'}
                         alt={'Wallet icon not active'}
                         src={'../assets/images/icons/wallet-icon.png'}
                         className={'cursor-pointer mr-4 mr-sm-4 mr-md-4 mr-xl-0 mr-lg-0 ml-4 ml-sm-4 ml-md-4 ml-xl-0 ml-lg-0 mt-0 mt-sm-0 mt-md-0 mt-lg-4 mt-xl-4 mb-0 mb-sm-0 mb-md-0 mb-lg-4 mb-xl-4'}
                         onClick={this.showYourWallets}/>
                )}
                {this.state.enabledFeature ? (
                    <div>
                        <img src={'../assets/images/icons/documents-icon.png'} alt={'News icon not active'} className={'cursor-pointer mr-4 mr-sm-4 mr-md-4 mr-xl-0 mr-lg-0 ml-4 ml-sm-4 ml-md-4 ml-xl-0 ml-lg-0 mt-0 mt-sm-0 mt-md-0 mt-lg-4 mt-xl-4 mb-0 mb-sm-0 mb-md-0 mb-lg-4 mb-xl-4'}/>
                        <img src={'../assets/images/icons/diament-icon.png'} alt={'Mining icon not active'} className={'cursor-pointer ml-4 ml-sm-4 ml-md-4 ml-xl-0 ml-lg-0 mt-0 mt-sm-0 mt-md-0 mt-lg-4 mt-xl-4 mb-0 mb-sm-0 mb-md-0 mb-lg-4 mb-xl-4'}/>
                    </div>
                ) : (
                    <div>
                    </div>
                )}
            </div>
        );
    }
}
