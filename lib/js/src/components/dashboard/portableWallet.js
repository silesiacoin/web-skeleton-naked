import React from 'react';
import { Row, Col } from 'reactstrap';
import TranslatedComponent from '../../containers/translatedComponent';
import web3Service from '../../_redux/services/web3Service';

export default class PortableWallet extends TranslatedComponent {
    constructor(props) {
        super(props);
        this.props = props;

        if ('undefined' === typeof props) {
            this.props = {};
        }

        this.state = {
            mainPageActive: !!this.props.mainPageActive,
            walletAddress: this.props.walletAddress,
            ssc: null,
            eth: null,
            dollar: null,
            clipboardAlert: false
        };

        this.componentDidMount = this.componentDidMount.bind(this);
        this.copyToClip = this.copyToClip.bind(this);
        this.disableAlert = this.disableAlert.bind(this);
    }

    componentDidMount() {
        web3Service.getBalanceSsc(this.props.walletAddress)
            .then(result => {
                this.setState({
                    ssc: result,
                })
            });
    }

    copyToClip() {
        const el = document.createElement('textarea');
        el.value = this.props.walletAddress;
        el.text = this.props.walletAddress;
        el.setAttribute('id', 'copyText');
        el.setAttribute('readonly', '');
        el.style.position = 'absolute';
        el.style.left = '-9999px';
        document.body.appendChild(el);
        const coptTextArea = document.getElementById('copyText');
        coptTextArea.select();
        document.execCommand('copy');
        document.body.removeChild(el);

        this.setState({
            clipboardAlert: true
        });

        setTimeout(this.disableAlert, 4000)
    }

    disableAlert(){
        this.setState({
            clipboardAlert: false
        });
    }

    render() {
        return (
            <div>
                <Row className={'m-0 p-0'}>
                    <Col xs={'12'} className={'m-0 p-0'}>
                        <div className={'float-left ' + (this.props.mainPageActive ? 'pr-4 pr-sm-4 pr-md-5 pr-lg-4 pr-xl-5 pb-0 pb-sm-0 pb-md-5 pb-xl-5 pb-lg-4' : 'pr-4 pr-sm-4 pr-md-5 pr-lg-5 pr-xl-5 pb-0 pb-sm-0 pb-md-5 pb-xl-5 pb-lg-5')}>
                            <img src={'../assets/images/icons/' + (this.props.mainPageActive ? 'wallet_blue.png' : 'wallet_blue_bg.png')}
                                 alt={'Wallet icon'}
                                 className={'dashboard-wallet'}/>
                        </div>
                        <Row className={'m-0 pr-5'}>
                            <Col className={'m-0 pl-0 pr-0 pr-sm-0 pr-md-4 pr-lg-0 pr-xl-4'}>
                                <div className={(this.state.clipboardAlert ? 'dashboard-copy__clicked dashboard-copy__fadeInOut' : 'dashboard-copy__alert')}>
                                    Address copied to clipboard
                                </div>
                                <img src={'../assets/images/icons/wallet_squares.png'}
                                     alt={'Squares icon'}
                                     className={'dashboard-squares float-right'}
                                     id={'dashboard-squares__id'}
                                     onClick={this.copyToClip}/>
                                <div className={'overlay-hidden'}>
                                    <p className={'dashboard-key float-left mb-2'}
                                        id={'dashboard-key-id'}>
                                        { this.props.walletAddress }
                                    </p>
                                </div>
                            </Col>
                        </Row>
                        <Row className={'m-0 p-0'}>
                            <Col xs={'12'} className={'m-0 p-0'}>
                                {this.props.mainPageActive === true ? (
                                    <div className={'mt-2 mt-sm-2 mt-md-0'}>
                                        <div className={'mr-5 mt-3 mt-sm-3 mt-md-0 float-left'}>
                                            <div className={'dashboard-icon float-left'}>
                                                <img src={'../assets/images/symbol/silesiacoin_small.png'}
                                                     alt={'Silesia coin logo'}
                                                     className={'dashboard-icon__image dashboard-icon__image--sc float-center'}/>
                                            </div>
                                            <div className={'dashboard-item float-left mb-0 pt-1'}>
                                                {this.state.ssc}
                                            </div>
                                        </div>
                                    </div>
                                ) : (
                                    <div className={'mt-2 mt-sm-2 mt-md-0'}>
                                        <div className={'mr-5 mt-3 mt-md-2 float-left'}>
                                            <div className={'dashboard-icon float-left'}>
                                                <img src={'../assets/images/symbol/silesiacoin_small.png'}
                                                     alt={'Silesia coin logo'}
                                                     className={'dashboard-icon__image dashboard-icon__image--sc float-center'}/>
                                            </div>
                                            <p className={'dashboard-item float-left mb-0 pt-1'}>
                                                {this.state.ssc}
                                            </p>
                                        </div>
                                        <div className={'mr-5 mt-3 mt-md-2 float-left'}>
                                            <div className={'dashboard-icon float-left'}>
                                                <img src={'../assets/images/symbol/eth-icon.png'}
                                                     alt={'Eth coin logo'}
                                                     className={'dashboard-icon__image dashboard-icon__image--eth'}/>
                                            </div>
                                            <p className={'dashboard-item float-left mb-0 pt-1'}>
                                                {this.state.eth}
                                            </p>
                                        </div>
                                        <div className={'mt-3 mt-md-2 float-left'}>
                                            <div className={'dashboard-icon float-left'}>
                                                <img src={'../assets/images/symbol/dolar.png'}
                                                     alt={'Dollar coin logo'}
                                                     className={'dashboard-icon__image'}/>
                                            </div>
                                            <p className={'dashboard-item float-left mb-0 pt-1'}>
                                                {this.state.dollar}
                                            </p>
                                        </div>
                                    </div>
                                )}
                            </Col>
                        </Row>
                    </Col>
                </Row>
            </div>
        );
    }
}
