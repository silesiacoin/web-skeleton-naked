import React from 'react';
import { Row, Col } from 'reactstrap';
import TransObject from '../generic/transObject';
import TranslatedComponent from '../../containers/translatedComponent';
import { dashboardActions } from '../../_redux/actions/dashboardActions';
import CookieService from '../../_redux/services/cookieService';
import { authenticationActions } from '../../_redux/actions/authenticationActions';
import AuthenticatorService from '../../_redux/services/authenticatorService';
import { authenticationConstants } from '../../_redux/constants/authenticationConstants';
import DashboardService from "../../_redux/services/dashboardService";

export default class Account extends TranslatedComponent {
    constructor(props) {
        super(props);
        this.props = props;
        const enabledFeature = CookieService.getCookie('ssc_not_finished');

        if ('undefined' === typeof props) {
            this.props = {};
        }

        this.state = {
            enabledFeature: enabledFeature.length > 0,
            hiddenClass: 'hidden',
            arrowRotate: false,
            login: false
        };

        this.togglePanel = this.togglePanel.bind(this);
        this.showProfilePanel = this.showProfilePanel.bind(this);
        this.showAuthenticationModal = this.showAuthenticationModal.bind(this);
        this.logout = this.logout.bind(this);

        this.store = DashboardService.getDashboardStore();

        this.autStore = AuthenticatorService.getAuthenticatorStore();
        this.unsubscribe = this.autStore.subscribe(() => {
            const state = this.autStore.getState();

            if (state.type === authenticationConstants.WALLET_OBJECT) {
                return(
                    this.setState({
                        login: true
                    })
                );
            }

            return state
        });
    }

    togglePanel() {
        let hiddenClass = 'hidden';
        let arrowRotate = false;

        if(this.state.hiddenClass === 'hidden'){
            hiddenClass = '';
            arrowRotate = true;
        }

        this.setState({
            hiddenClass: hiddenClass,
            arrowRotate: arrowRotate
        });
    }

    showProfilePanel() {
        this.store.dispatch(dashboardActions.showProfile());
        this.togglePanel();
    }

    showAuthenticationModal() {
        this.autStore.dispatch(authenticationActions.authenticationModal());
        this.togglePanel();
    }

    logout() {
        window.location.reload();
    }

    render() {
        return (
            <div>
                <button id={'profile-avatar'} className={'profile-avatar'} onClick={this.togglePanel}>
                    <div className={'profile-avatar__avatar'}>
                        <img src={'../assets/images/symbol/silesiacoin_small.png'}
                             alt={'Account avatar'}
                             className={'profile-avatar__avatar__image'}/>
                    </div>
                    <p className={'desktop-display profile-avatar__name pl-2 mb-0'}>

                    </p>
                    <img id={'profile-avatar__arrow'}
                         alt={'Account arrow'}
                         src={'../assets/images/symbol/profile-arrow.svg'}
                         className={this.state.arrowRotate ? 'profile-avatar__arrow-on pl-2 desktop-display' : 'profile-avatar__arrow-off pl-2 desktop-display'}/>
                </button>
                <div id={'profile-panel'} className={this.state.hiddenClass}>
                    <div className={'profile-panel'}>
                        {this.state.enabledFeature &&
                            <Row className={'border-bottom m-0 p-0'}>
                                <button id={'show-profile-panel'} className={'cursor-pointer p-0 m-0'}
                                        onClick={this.showProfilePanel}>
                                    <Col xs={"12"} className={'m-0 p-0'}>
                                        <img src={'../assets/images/icons/noun_avatar.png'}
                                             alt={'Account icon'}
                                             className={'m-3 float-left'}/>
                                        <div className={'pr-3 text-left'}>
                                            <p className={'profile-panel__text profile-panel__text--padding profile-panel__text--title m-0'}>
                                                <TransObject translationKey={'dashboard.my.profile'}/>
                                            </p>
                                            <p className={'profile-panel__text profile-panel__text--padding profile-panel__text--subtitle m-0 pr-3 pt-1'}>
                                                <TransObject translationKey={'dashboard.check.your.account'}/>
                                            </p>
                                        </div>
                                    </Col>
                                </button>
                            </Row>
                        }
                        <Row className={'m-0 p-0'}>
                            <button id={this.state.login ? 'log-out' : 'log-in'} className={'cursor-pointer p-0 m-0'} onClick={this.state.login ? this.logout : this.showAuthenticationModal}>
                                <Col xs={"12"} className={'m-0 p-0'}>
                                    <img src={'../assets/images/icons/noun_logout.png'}
                                         alt={'Account log out'}
                                         className={'m-3 float-left'}/>
                                    <div className={'pr-3'}>
                                        <p className={'profile-panel__text profile-panel__text--title-sm m-0'}>
                                            {this.state.login ? <TransObject translationKey={'dashboard.log.out'}/> : <TransObject translationKey={'dashboard.log.in'}/>}
                                        </p>
                                    </div>
                                </Col>
                            </button>
                        </Row>
                    </div>
                </div>
            </div>
        );
    }
}
