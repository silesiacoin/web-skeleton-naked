import React from 'react';
import { Row, Col } from 'reactstrap';
import TransObject from '../generic/transObject';
import TranslatedComponent from '../../containers/translatedComponent';
import CookieService from '../../_redux/services/cookieService';

export default class Mining extends TranslatedComponent {
    constructor(props) {
        super(props);
        this.props = props;

        if ('undefined' === typeof props) {
            this.props = {};
        }

        const enabledFeature = CookieService.getCookie('ssc_not_finished');

        this.state = {
            enabledFeature: enabledFeature.length > 0,
        };
    }

    render() {
        return (
            <Row>
                <Col xs={"12"} sm={"12"} className={'pb-5 pb-sm-5 pb-md-5 pb-xl-5 pb-lg-5 pt-4 pt-sm-5 pt-md-3 pt-xl-3 pt-lg-3 pl-4 pl-sm-5 pl-md-3 pl-xl-3 pl-lg-3 pr-4 pr-sm-5 pr-md-3 pr-xl-0 pr-lg-0'}>
                    <div className={'user-panels'}>
                        <div className={'user-panels__head pl-3 pr-3 pt-4 pt-sm-4 pt-md-4 pt-lg-1 pt-xl-4 pb-4 pb-sm-4 pb-md-4 pb-lg-1 pb-xl-4 text-center'}>
                            <img src={'../assets/images/illustrations/AffiliateNetworkOne.svg'}
                                 className={'user-panels__head__image'}
                                 alt="Responsive image"/>
                        </div>
                        <p className={'user-panels__title user-panels__title--purple pt-4 pt-sm-4 pt-md-5 pt-lg-4 pt-xl-5 pl-4 pr-sm-4 pr-md-5 pr-lg-4 pr-xl-5 pl-4 pl-sm-4 pl-md-5 pl-lg-4 pl-xl-5'}>
                            <TransObject translationKey={'dashboard.mining'}/>
                        </p>
                        <Row className={'p-0 m-0'}>
                            <Col xs="12" className={'p-0'}>
                                <div className={'user-panels__body-sm text-left pb-4 pl-4 pr-sm-4 pr-md-5 pr-lg-4 pr-xl-5 pl-4 pl-sm-4 pl-md-5 pl-lg-4 pl-xl-5 m-0'}>
                                    <div>
                                        <img src={'../assets/images/icons/mining_ico.png'}
                                             alt={'Mining image'}
                                             className={'float-left margin-minus'}/>
                                        <p className={'user-panels__body-sm__item pt-1 mb-0'}>1 block / 15 sec</p>
                                    </div>
                                    <div className={'save-space'}>
                                        <img src={'../assets/images/symbol/yellow-arrow.svg'}
                                             alt={'Yellow arrow'}
                                             className={'p-0 m-0 user-panels__body-sm__arrow'}/>
                                    </div>
                                </div>
                            </Col>
                        </Row>
                    </div>
                </Col>
            </Row>
        );
    }
}
