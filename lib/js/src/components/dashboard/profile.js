import React from 'react';
import { Row, Col } from 'reactstrap';
import ProfileHead from './profileHead';
import ProfileBody from './profileBody';
import TranslatedComponent from '../../containers/translatedComponent';
import CookieService from '../../_redux/services/cookieService';

export default class Profile extends TranslatedComponent {
    constructor(props) {
        super(props);

        const enabledFeature = CookieService.getCookie('ssc_not_finished');

        this.state = {
            enabledFeature: enabledFeature.length > 0,
            mainPageActive: !!this.props.mainPageActive,
        };
    }

    render() {
        return (
            <Row className={'m-0 p-0'}>
                <Col xs={"12"}
                     className={'p-0 pb-0 pb-sm-0 pb-md-3 pb-xl-3 pb-lg-3 pt-4 pt-sm-5 pt-md-3 pt-xl-5 pt-lg-5 pl-4 pr-4 pl-sm-5 pr-sm-5 pl-md-3 pr-md-3 pl-lg-0 pl-xl-0 pr-lg-0 pr-xl-0'}>
                    <div className={'user-panels overlay-hidden'}>
                        <div className={'user-panels__head user-panels__head--email pt-lg-4 pb-lg-4 pr-lg-4 pl-lg-4 pb-md-5 pr-md-4 pl-md-5 pt-md-5 pb-4 pb-xl-5 pr-4 pr-xl-5 pt-4 pt-xl-5 pl-4 pl-xl-5'}>
                            <ProfileHead mainPageActive={true}/>
                        </div>
                        {this.state.enabledFeature ? (
                            <div className={'pr-4 pr-sm-4 pr-xl-5 pr-lg-4 pb-5 pr-md-5 pl-4 pl-sm-4 pl-md-5 pl-xl-5 pl-lg-4'}>
                                <ProfileBody mainPageActive={true}/>
                            </div>
                        ) : (
                            <div>
                            </div>
                        )}
                    </div>
                </Col>
            </Row>
        );
    }
}
