import React from 'react';
import { Col, Row } from 'reactstrap';
import PortableWallet from '../../components/dashboard/portableWallet';
import TranslatedComponent from '../../containers/translatedComponent';
import DashboardService from '../../_redux/services/dashboardService';
import { dashboardActions } from '../../_redux/actions/dashboardActions';
import TransObject from '../generic/transObject';
import CookieService from '../../_redux/services/cookieService';
import ApiUrlParser from '../../_helpers/urlParser';

export default class Wallet extends TranslatedComponent {
    constructor(props) {
        super(props);
        this.props = props;
        this.apiUrl = ApiUrlParser.createFromWindowLocation();

        const enabledFeatureNF = CookieService.getCookie('ssc_not_finished');
        const enabledFeature = CookieService.getCookie('ssc_payment');

        if ('undefined' === typeof props) {
            this.props = {};
        }

        this.state = {
            enabledFeatureNF: enabledFeatureNF.length > 0,
            enabledFeature: enabledFeature.length > 0,
            overlapActive: 'BuySsc',
            overlapBuySscStepActive: 'first',
            walletSelector: !!this.props.walletSelector,
            currency: 'LTC',
            ssc: !!this.props.ssc,
            ltc: 0,
            btc: 0,
            eth: 0,
            lsk: 0,
            ltcRound: 0,
            btcRound: 0,
            ethRound: 0,
            error: '',
            count: 0,
            qrCodeLoaded: false,
            qrCode: '',
            transactionUrl: '',
            canBuy: !!this.props.canBuy,
            reloadCurrency: false
        };

        this.store = DashboardService.getDashboardStore();
        this.showYourWallets = this.showYourWallets.bind(this);
        this.showBuySsc = this.showBuySsc.bind(this);
        this.showTransactions = this.showTransactions.bind(this);
        this.buySsc = this.buySsc.bind(this);
        this.showTransactionStatus = this.showTransactionStatus.bind(this);
        this.setInputValue = this.setInputValue.bind(this);
        this.selectLTC = this.selectLTC.bind(this);
        this.selectETH = this.selectETH.bind(this);
        this.selectBTC = this.selectBTC.bind(this);
        this.selectLSK = this.selectLSK.bind(this);
        this.showLoading = this.showLoading.bind(this);
        this.reloadCurrencyValue = this.reloadCurrencyValue.bind(this);
    }

    reloadCurrencyValue(event) {
        const { value } = event.target;

        if (!isNaN(value) && value !== '' && value > 0 && value <= 20000) {
            this.setState({
                reloadCurrency: true
            });

            fetch(ApiUrlParser.parseApiUrlWithDefaultProtocol(this.apiUrl, 'v1/get-price'), {
                method: 'GET',
            }).then((response) => {
                return response.json();
            }).then((myJson) => {
                const eth_eur = myJson.response.eth;
                const btc_eur = myJson.response.btc;
                const ltc_eur = myJson.response.ltc;

                fetch(ApiUrlParser.parseApiUrlWithDefaultProtocol(this.apiUrl, 'v1/currency-converter'), {
                    method: 'GET',
                }).then((response) => {
                    return response.json();
                }).then((myJson) => {
                    const eth_pln = eth_eur * myJson.response.rates.PLN;
                    const btc_pln = btc_eur * myJson.response.rates.PLN;
                    const ltc_pln = ltc_eur * myJson.response.rates.PLN;

                    this.setState({
                        ltc: value * 5 / ltc_pln,
                        btc: value * 5 / btc_pln,
                        eth: value * 5 / eth_pln,
                        lsk: value / 1.4,
                        canBuy: true,
                        reloadCurrency: false,
                        error: 'Ready.'
                    });

                    if (window.innerWidth < 768) {
                        this.setState({
                            ltcRound: value * (5 / ltc_pln).toFixed(2),
                            btcRound: value * (5 / btc_pln).toFixed(2),
                            ethRound: value * (5 / eth_pln).toFixed(2),
                        })
                    }

                    if (window.innerWidth > 767) {
                        this.setState({
                            ltcRound: value * (5 / ltc_pln).toFixed(5),
                            btcRound: value * (5 / btc_pln).toFixed(5),
                            ethRound: value * (5 / eth_pln).toFixed(5),
                        })
                    }
                });
            });
        }

        if (isNaN(value) && value !== '') {
            this.setState({
                ssc: 0,
                ltc: 0,
                btc: 0,
                eth: 0,
                lsk: 0,
                ltcRound: 0,
                btcRound: 0,
                ethRound: 0,
                error: 'Value must be a number.'
            })
        }

        if (!isNaN(value) && value !== '' && value > 20000) {
            this.setState({
                ssc: 0,
                ltc: 0,
                btc: 0,
                eth: 0,
                lsk: 0,
                ltcRound: 0,
                btcRound: 0,
                ethRound: 0,
                error: 'The maximum value is 20,000 SSC.'
            })
        }

        if (value === '') {
            this.setState({
                ssc: 0,
                ltc: 0,
                btc: 0,
                eth: 0,
                lsk: 0,
                ltcRound: 0,
                btcRound: 0,
                ethRound: 0,
                error: 'Complete the value.'
            })
        }

        if (value <= 0 && value !== '') {
            this.setState({
                ssc: 0,
                ltc: 0,
                btc: 0,
                eth: 0,
                lsk: 0,
                ltcRound: 0,
                btcRound: 0,
                ethRound: 0,
                error: 'Value must be greater than 0.'
            })
        }
    }

    setInputValue(event) {
        const { value } = event.target;

        if (isNaN(value) && value !== '') {
            this.setState({
                ssc: 0,
                ltc: 0,
                btc: 0,
                eth: 0,
                lsk: 0,
                ltcRound: 0,
                btcRound: 0,
                ethRound: 0,
                error: 'Value must be a number.'
            })
        }

        if (!isNaN(value) && value !== '' && value > 20000) {
            this.setState({
                ssc: 0,
                ltc: 0,
                btc: 0,
                eth: 0,
                lsk: 0,
                ltcRound: 0,
                btcRound: 0,
                ethRound: 0,
                error: 'The maximum value is 20,000 SSC.'
            })
        }

        if (value === '') {
            this.setState({
                ssc: 0,
                ltc: 0,
                btc: 0,
                eth: 0,
                lsk: 0,
                ltcRound: 0,
                btcRound: 0,
                ethRound: 0,
                error: 'Complete the value.'
            })
        }

        if (value <= 0 && value !== '') {
            this.setState({
                ssc: 0,
                ltc: 0,
                btc: 0,
                eth: 0,
                lsk: 0,
                ltcRound: 0,
                btcRound: 0,
                ethRound: 0,
                error: 'Value must be greater than 0.'
            })
        }

        if (!isNaN(value) && value !== '' && value > 0 && value <= 20000) {
            this.setState({
                ssc: value,
                canBuy: false,
                error: 'Click outside the input.'
            })
        }
    }

    showYourWallets() {
        this.store.dispatch(dashboardActions.showYourWallets());
    }

    showBuySsc() {
        this.setState({
            overlapActive: 'BuySsc'
        })
    }

    showTransactions() {
        this.setState({
            overlapActive: 'Transactions'
        })
    }

    showLoading() {
        const self = this;
        self.incrementer = setInterval(function() {
            let count = self.state.count + 1;

            self.setState({
                count: count,
            });

            if(self.state.count === 4){
                self.setState({
                    qrCodeLoaded: true
                });
                clearInterval(self.incrementer)
            }
        }, 600)
    }

    buySsc() {
        if (this.state.ssc === false && this.state.error === '') {
            this.setState({
                error: 'Complete the value.'
            })
        }

        let currencyValue = '';

        if (this.state.currency === 'ETH') {
            currencyValue = this.state.eth
        }

        if (this.state.currency === 'LTC') {
            currencyValue = this.state.ltc
        }

        if (this.state.currency === 'BTC') {
            currencyValue = this.state.btc
        }

        if (this.state.currency === 'LSK') {
            currencyValue = this.state.lsk
        }

        if (this.state.ssc > 0 && this.state.canBuy) {
            fetch(ApiUrlParser.parseApiUrlWithDefaultProtocol(this.apiUrl, 'v1/coinpayments/basic-transaction/' + this.state.currency + '/' + currencyValue), {
                method: 'POST',
                body: JSON.stringify({
                    uuid: this.props.walletSelector
                })
            }).then((response) => {
                return response.json();
            }).then((myJson) => {
                const transactionUrl = myJson.response.result.checkout_url;
                const qrCode = myJson.response.result.qrcode_url;

                this.setState({
                    qrCode: qrCode,
                    transactionUrl: transactionUrl
                });
            });

            this.setState({
                overlapBuySscStepActive: 'second',
            });

            this.showLoading()
        }
    }

    showTransactionStatus() {
        this.setState({
            overlapBuySscStepActive: 'third'
        })
    }

    selectLTC() {
        this.setState({
            currency: 'LTC'
        })
    }

    selectBTC() {
        this.setState({
            currency: 'BTC'
        })
    }

    selectETH() {
        this.setState({
            currency: 'ETH'
        })
    }

    selectLSK() {
        this.setState({
            currency: 'LSK'
        })
    }

    render() {
        return (
            <Row className="m-0 pb-5 container-padding">
                <Col xs={"12"} sm={'12'} md={'12'} lg={'12'} xl={'12'} className={'pr-0 pr-sm-0 pr-md-3 mr-xl-0 mr-lg-0 pb-0 pb-sm-0 pb-md-3 pb-xl-3 pb-lg-3 pt-5 pt-sm-5 pt-md-5 pt-xl-5 pt-lg-5 pl-0 pl-sm-0 pl-md-3 pl-xl-3 pl-lg-3'}>
                    <div className={'pl-5 pr-5 pl-md-0 pr-md-0'}>
                        <button id={'return-your-wallets'}
                                className={'yellow-button yellow-button--size cursor-pointer mb-5'}
                                onClick={this.showYourWallets}>
                            <img src={'../assets/images/symbol/yellow-arrow.svg'}
                                 alt={'Yellow arrow'}
                                 className={'yellow-button__icon yellow-button__icon--object mr-4'}/>
                            <TransObject translationKey={'dashboard.return.to.wallets'}/>
                        </button>
                        <PortableWallet mainPageActive={true} walletAddress={this.props.walletSelector}/>
                    </div>
                    <Row className={'p-0 m-0'}>
                        <Col xs={'6'} sm={'6'} md={'6'} xl={'3'} lg={'3'}
                             id={'overlap-buy-ssc'}
                             className={'overlap cursor-pointer pl-0 pr-0 pb-3 pt-5 ' + (this.state.overlapActive === 'BuySsc' ? 'overlap--active' : 'overlap--not-active')}
                             onClick={this.showBuySsc}>
                            <TransObject translationKey={'dashboard.buy.ssc'}/>
                        </Col>
                        {this.state.enabledFeatureNF &&
                            <Col xs={'6'} sm={'6'} md={'6'} xl={'3'} lg={'3'}
                                 id={'overlap-transactions'}
                                 className={'overlap cursor-pointer pl-0 pr-0 pb-3 pt-5 ' + (this.state.overlapActive === 'Transactions' ? 'overlap--active' : 'overlap--not-active')}
                                 onClick={this.showTransactions}>
                                <TransObject translationKey={'dashboard.transactions'}/>
                            </Col>
                        }
                    </Row>
                    <div className={'pl-5 pr-5 pl-md-0 pr-md-0 ' + (this.state.overlapActive === 'BuySsc' && this.state.overlapBuySscStepActive === 'first' ? ' ' : 'hidden')}>
                        <form className={'pt-5'}>
                            <div className="form-group" onBlur={this.reloadCurrencyValue} onChange={this.setInputValue}>
                                <input className="form-control dashboard-input" id="exampleInputBuySsc" placeholder="How many SSC?" type={'number'}/>
                            </div>
                        </form>
                        {this.state.reloadCurrency &&
                            <div className={'lds-wallet'}>
                                <div className={'lds-default'}>
                                    <div></div>
                                    <div></div>
                                    <div></div>
                                    <div></div>
                                    <div></div>
                                    <div></div>
                                    <div></div>
                                    <div></div>
                                    <div></div>
                                    <div></div>
                                    <div></div>
                                    <div></div>
                                </div>
                            </div>
                        }
                        <p className={'p-0 m-0 modal-alert ' + (this.state.ssc > 0 && this.state.error !== 'Ready.' ? 'modal-alert--yellow-sm' : '') + (this.state.ssc <= 0 && this.state.error !== 'Ready.' ? 'modal-alert--red-sm' : '') + (this.state.ssc > 0 && this.state.error === 'Ready.' ? 'modal-alert--green-sm' : '')}>
                            {this.state.error}
                        </p>
                        <p className={'dashboard-text dashboard-text--md dashboard-text--purple dashboard-text--mt mb-0'}>
                            <TransObject translationKey={'dashboard.choose.the.payment'}/>
                        </p>
                        <Row className={'m-0'}>
                            <Col xs={'12'} className={'mb-5 p-0'}>
                                <div onClick={this.selectLTC}
                                     className={'coin-panel text-center pb-2 pb-md-3 mt-3 mt-md-5 mr-3 mr-md-5 float-left ' + (this.state.currency === 'LTC' ? 'coin-panel--active' : '' )}>
                                    <img src={'../assets/images/symbol/lc.png'}
                                         alt={'Lc icon'}
                                         className={'coin-panel__image'}/>
                                    <p className={'coin-panel__text mb-0 ml-2 mr-2 ml-md-3 mr-md-3'}>
                                        {this.state.ltcRound}
                                    </p>
                                </div>
                                <div onClick={this.selectETH}
                                     className={'coin-panel text-center pb-2 pb-md-3 mt-3 mt-md-5 mr-3 mr-md-5 float-left ' + (this.state.currency === 'ETH' ? 'coin-panel--active' : '' )}>
                                    <img src={'../assets/images/symbol/eth-icon.png'}
                                         alt={'Eth icon'}
                                         className={'coin-panel__image'}/>
                                    <p className={'coin-panel__text mb-0 ml-2 mr-2 ml-md-3 mr-md-3'}>
                                        {this.state.ethRound}
                                    </p>
                                </div>
                                <div onClick={this.selectBTC}
                                     className={'coin-panel text-center pb-2 pb-md-3 mt-3 mt-md-5 mr-3 mr-md-5 float-left ' + (this.state.currency === 'BTC' ? 'coin-panel--active' : '' )}>
                                    <img src={'../assets/images/symbol/bitcoin-logo.png'}
                                         alt={'Btc icon'}
                                         className={'coin-panel__image'}/>
                                    <p className={'coin-panel__text mb-0 ml-2 mr-2 ml-md-3 mr-md-3'}>
                                        {this.state.btcRound}
                                    </p>
                                </div>
                                {this.state.enabledFeature &&
                                    <div id={'ssc_payment'}
                                         onClick={this.selectLSK}
                                         className={'coin-panel text-center pb-2 pb-md-3 mt-3 mt-md-5 float-left ' + (this.state.currency === 'LSK' ? 'coin-panel--active' : '')}>
                                        <img src={'../assets/images/symbol/lisk.png'}
                                             alt={'Lsk icon'}
                                             className={'coin-panel__image'}/>
                                        <p className={'coin-panel__text mb-0 ml-2 mr-2 ml-md-3 mr-md-3'}>
                                            {this.state.lsk}
                                        </p>
                                    </div>
                                }
                            </Col>
                        </Row>
                        <button id={'buy-ssc'}
                                onClick={this.buySsc}
                                className={'dashboard-button mb-5 mb-md-3 pl-5 pr-5'}>
                            <TransObject translationKey={'dashboard.buy'}/>
                        </button>
                    </div>
                    <div className={'pl-5 pr-5 pl-md-0 pr-md-0 ' + (this.state.overlapActive === 'BuySsc' && this.state.overlapBuySscStepActive === 'second' ? ' ' : 'hidden')}>
                        <div className={'mt-5 pb-5'}>
                            <div className={'lds-space float-none float-sm-left mr-0 mr-sm-3 mb-3'}>
                                {!this.state.qrCodeLoaded ? (
                                    <div className="lds-default lds-default--dashboard">
                                        <div></div>
                                        <div></div>
                                        <div></div>
                                        <div></div>
                                        <div></div>
                                        <div></div>
                                        <div></div>
                                        <div></div>
                                        <div></div>
                                        <div></div>
                                        <div></div>
                                        <div></div>
                                    </div>
                                ) : (
                                    <img id={'qr-image'} src={this.state.qrCode}
                                         alt={'QR code'}
                                         className={'img-fluid'}/>
                                )}
                            </div>
                            <p className={'dashboard-text dashboard-text--bg dashboard-text--purple'}>
                                <TransObject translationKey={'dashboard.transaction.in.progress'}/>
                            </p>
                            <p className={'dashboard-text dashboard-text--sm dashboard-text--purple'}>
                                <TransObject translationKey={'dashboard.transaction.implementer'}/>
                                <span>Coinpayment</span>
                            </p>
                            <p className={'dashboard-text dashboard-text--sm-desktop dashboard-text--yellow mb-0'}>
                                <TransObject translationKey={'dashboard.view.status'}/>
                            </p>
                            <p className={'dashboard-text dashboard-text--sm-normal dashboard-text--blue mb-5'}>
                                <TransObject translationKey={'dashboard.we.are.waiting'}/>
                            </p>
                            {this.state.qrCodeLoaded &&
                                <a href={this.state.transactionUrl} rel="noopener noreferrer" target={'_blank'}>
                                    <button className={'dashboard-button mb-5 pl-5 pr-5'}>
                                        Check transaction
                                    </button>
                                </a>
                            }
                        </div>
                    </div>
                    <div className={'pl-5 pr-5 pl-md-0 pr-md-0 ' + (this.state.overlapActive === 'BuySsc' && this.state.overlapBuySscStepActive === 'third' ? ' ' : 'hidden')}>
                        <div className={'mt-0 mt-md-5 text-center text-md-left'}>
                            <p className={'dashboard-text dashboard-text--bg dashboard-text--green'}>
                                <TransObject translationKey={'dashboard.transaction.succesfull'}/>
                            </p>
                            <p className={'dashboard-text dashboard-text--sm dashboard-text--purple'}>
                                <TransObject translationKey={'dashboard.you.buyed'}/>
                                    {this.state.ssc}
                                <span> SSC </span>
                                <TransObject translationKey={'dashboard.for'}/>
                                {this.state.currency === 'LTC' &&
                                    <span>
                                        {this.state.ltc}
                                    </span>
                                }
                                {this.state.currency === 'ETH' &&
                                    <span>
                                        {this.state.eth}
                                    </span>
                                }
                                {this.state.currency === 'BTC' &&
                                    <span>
                                        {this.state.btc}
                                    </span>
                                }
                                <span> {this.state.currency}</span>
                            </p>
                        </div>
                    </div>
                    <Col xs={"12"} className={'p-0 m-0 ' + (this.state.overlapActive === 'Transactions' ? ' ' : 'hidden')}>
                        <Row className={'m-0 mt-0 mt-md-5 pl-5 pr-5 pl-md-0 pr-md-0'}>
                            <Col xs={"12"} className={'ml-0 mr-3 p-4 mt-5 mt-md-0 user-data'}>
                                <Row className={'p-0 m-0'}>
                                    <Col xs={"12"} sm={'6'} md={'3'} className={'p-0'}>
                                        <p className={'mb-0 user-data__title user-data__title--pt'}>
                                            14.11.2018
                                        </p>
                                    </Col>
                                    <Col xs={"12"} sm={'6'} md={'3'} className={'p-0'}>
                                        <p className={'mb-0 user-data__title user-data__title--pt text-left text-sm-right text-md-left'}>
                                            Transaction #1
                                        </p>
                                    </Col>
                                    <Col xs={"12"} sm={'6'} md={'3'} className={'p-0'}>
                                        <Row className={'m-0 pr-5'}>
                                            <Col xs={"12"} className={'m-0 pl-0 pr-0 pb-0 user-data__title user-data__title--pt'}>
                                                <img src={'../assets/images/icons/wallet_squares.png'}
                                                     alt={'Squares icon'}
                                                     className={'user-data__title__squares float-right'}/>
                                                <div className={'overlay-hidden'}>
                                                    <p className={'m-0 float-left'}>
                                                        0xb6d64d4beff4e4877eb36beaea911ad473e7686d
                                                    </p>
                                                </div>
                                            </Col>
                                        </Row>
                                    </Col>
                                    <Col xs={"12"} sm={'6'} md={'3'} className={'p-0'}>
                                        <div className={'text-left text-sm-right'}>
                                            <img src={'../assets/images/symbol/silesiacoin_small.png'}
                                                 alt={'Silesia coin icon'}
                                                 className={'float-sm-right'} height={'29px'}/>
                                            <p className={'mb-0 user-data__title user-data__title--pt float-left float-sm-right mr-3'}>
                                                -500
                                            </p>
                                        </div>
                                    </Col>
                                </Row>
                            </Col>
                        </Row>
                    </Col>
                </Col>
            </Row>
        );
    }
}
