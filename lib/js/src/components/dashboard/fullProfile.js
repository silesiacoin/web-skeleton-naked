import React from 'react';
import { Row, Col } from 'reactstrap';
import TranslatedComponent from '../../containers/translatedComponent';
import ProfileHead from './profileHead';
import ProfileBody from './profileBody';
import CookieService from '../../_redux/services/cookieService';

export default class FullProfile extends TranslatedComponent {
    constructor(props) {
        super(props);

        const enabledFeature = CookieService.getCookie('ssc_not_finished');

        this.state = {
            enabledFeature: enabledFeature.length > 0
        };
    }

    render() {
        return (
            <Row className="m-0 pb-5 container-padding">
                <Col xs={'12'} className={'p-0'}>
                    <Row className={'m-0 p-0'}>
                        <Col xs={"12"} className={'pr-5 pr-sm-5 pr-md-3 mr-xl-0 mr-lg-0 pb-0 pb-sm-0 pb-md-3 pb-xl-3 pb-lg-3 pt-5 pt-sm-5 pt-md-5 pt-xl-5 pt-lg-5 pl-5 pl-sm-5 pl-md-3 pl-xl-3 pl-lg-3'}>
                            <ProfileHead/>
                            {this.state.enabledFeature ? (
                                <Row className={'p-0 m-0'}>
                                    <Col xs={'12'} sm={'12'} md={'12'} lg={'6'} xl={'6'} className={'p-0 mt-4 mt-md-0'}>
                                        <ProfileBody/>
                                    </Col>
                                </Row>
                            ) : (
                                <div>
                                </div>
                            )}
                            {this.state.enabledFeature ? (
                                <Row className={'m-0 p-0 desktop-display'}>
                                    <Col xs={"12"} className={'ml-0 mr-3 p-3 pl-4 pr-4 user-data'}>
                                        <Row>
                                            <Col xs={"4"} >
                                                <p className={'mb-2 user-data__title'}>
                                                    Krzysztof Ibisz
                                                </p>
                                                <p className={'m-0 user-data__text'}>
                                                    Expires 08/2022
                                                </p>
                                            </Col>
                                            <Col xs={"4"}>
                                                <p className={'mb-0 text-center user-data__text user-data__text--margin'}>
                                                    xx		xxxx		xxxx		xxxx		2314
                                                </p>
                                            </Col>
                                            <Col xs={"4"}>
                                                <p className={'mb-0 text-right user-data__text--margin'}>
                                                    Krzysztof Ibisz
                                                </p>
                                            </Col>
                                        </Row>
                                    </Col>
                                </Row>
                            ) : (
                                <div>
                                </div>
                            )}
                        </Col>
                    </Row>
                </Col>
            </Row>
        );
    }
}
