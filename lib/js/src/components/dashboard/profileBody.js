import React from 'react';
import { Row, Col } from 'reactstrap';
import TransObject from '../generic/transObject';
import TranslatedComponent from '../../containers/translatedComponent';

export default class ProfileBody extends TranslatedComponent {
    constructor(props) {
        super(props);

        this.state = {
            mainPageActive: !!this.props.mainPageActive,
        };
    }

    render() {
        return (
            <div>
                <div className={'pt-4 pt-sm-4 pt-md-5 pt-lg-4 pt-xl-5 pb-4 mb-2'}>
                    <div className={'dashboard-profile dashboard-profile--bg'}>
                        <strong><TransObject translationKey={'dashboard.level'}/>  3</strong>
                        <span> Practicant</span>
                    </div>
                    <div className={'dashboard-progress mt-3'}>
                        <div className={'dashboard-progress__bar'} role={'progressbar'} aria-valuenow={'25'} aria-valuemin={'0'} aria-valuemax={'100'}>
                        </div>
                    </div>
                </div>
                <div className="form-check pt-0 pl-0 mb-2">
                    <Row className={'p-0 m-0'}>
                        <Col xs={"8"} sm={'8'} md={'6'} lg={'6'} xl={'6'} className={'p-0 m-0'}>
                            <label className="container-checkbox container-checkbox--width m-0" htmlFor="defaultCheck1">
                                <input className="form-check-input" type="checkbox" value="" id="defaultCheck1"/>
                                <span className="checkmark">
                                </span>
                                <p className={'dashboard-profile dashboard-profile--sm pt-1'}>
                                    <TransObject translationKey={'dashboard.upload.avatar'}/>
                                </p>
                            </label>
                        </Col>
                        <Col xs={"4"} sm={'4'} md={'6'} lg={'6'} xl={'6'} className={'p-0 m-0'}>
                            <img src={'../assets/images/symbol/yellow-arrow.svg'}
                                 alt={'Yellow arrow'}
                                 className={'yellow-button__icon yellow-button__icon--margin float-right cursor-pointer ml-4 ' + (this.state.mainPageActive ? ' ' : 'mobile-display')}/>
                            <p className={'dashboard-complete desktop-display float-right cursor-pointer mb-0 '}>
                                <TransObject translationKey={'dashboard.complete'}/>
                            </p>
                        </Col>
                    </Row>
                </div>
                <div className="form-check pt-0 pl-0">
                    <Row className={'p-0 m-0'}>
                        <Col xs={"8"} sm={'8'} md={'6'} lg={'6'} xl={'6'} className={'p-0 m-0'}>
                            <label className="container-checkbox container-checkbox--width m-0" htmlFor="defaultCheck2">
                                <input className="form-check-input" type="checkbox" value="" id="defaultCheck2"/>
                                <span className="checkmark">
                                </span>
                                <p className={'dashboard-profile dashboard-profile--sm pt-1 mb-0'}>
                                    <TransObject translationKey={'dashboard.add.your.name'}/>
                                </p>
                            </label>
                        </Col>
                        <Col xs={"4"} sm={'4'} md={'6'} lg={'6'} xl={'6'} className={'p-0 m-0'}>
                            <img src={'../assets/images/symbol/yellow-arrow.svg'}
                                 alt={'Yellow arrow'}
                                 className={'yellow-button__icon yellow-button__icon--margin float-right cursor-pointer ml-4 ' + (this.state.mainPageActive ? ' ' : 'mobile-display')}/>
                            <p className={'dashboard-complete desktop-display float-right cursor-pointer mb-0'}>
                                <TransObject translationKey={'dashboard.complete'}/>
                            </p>
                        </Col>
                    </Row>
                </div>
            </div>
        );
    }
}
