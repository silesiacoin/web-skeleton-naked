import React from 'react';
import { Row, Col, ModalHeader, ModalBody, Modal } from 'reactstrap';
import TransObject from '../generic/transObject';
import TranslatedComponent from '../../containers/translatedComponent';
import PortableWallet from '../../components/dashboard/portableWallet';
import { dashboardActions } from '../../_redux/actions/dashboardActions';
import { authenticationActions } from '../../_redux/actions/authenticationActions';
import AuthenticatorService from '../../_redux/services/authenticatorService';
import { authenticationConstants } from '../../_redux/constants/authenticationConstants';
import DashboardService from '../../_redux/services/dashboardService';

export default class YourWallets extends TranslatedComponent {
    constructor(props) {
        super(props);
        this.props = props;

        if ('undefined' === typeof props) {
            this.props = {};
        }

        this.state = {
            downloadWalletModalActive: !!this.props.downloadWalletModalActive,
            wallet: !!this.props.wallet,
            addresses: [],
            numberOffWallets: !!this.props.numberOffWallets,
            addressForDownload: this.props.addressForDownload
        };

        {'undefined' !== typeof this.props.wallet ? (
            this.state = {
                addresses: this.props.wallet.getAddresses(),
                numberOffWallets: this.props.wallet.getAddressCount()
            }
        ) : (
            this.state = {
                addresses: [],
                numberOffWallets: !!this.props.numberOffWallets,
            }
        )}

        this.showCreateWalletModal = this.showCreateWalletModal.bind(this);
        this.showWallet = this.showWallet.bind(this);
        this.downloadFile = this.downloadFile.bind(this);
        this.toggle = this.toggle.bind(this);
        this.mapWalletToJSON = this.mapWalletToJSON.bind(this);
        this.showDownloadWalletModal = this.showDownloadWalletModal.bind(this);

        this.store = DashboardService.getDashboardStore();

        this.autStore = AuthenticatorService.getAuthenticatorStore();
        this.unsubscribe = this.autStore.subscribe(() => {
            const state = this.autStore.getState();

            if (state.type === authenticationConstants.WALLET_OBJECT) {
                this.setState({
                    addresses: state.wallet.getAddresses(),
                    numberOffWallets: state.wallet.getAddressCount()
                });
            }

            return state
        });
    }

    showCreateWalletModal() {
        if (this.state.numberOffWallets === false) {
            this.autStore.dispatch(authenticationActions.authenticationModal());
        }

        if (this.state.numberOffWallets > 0) {
            this.store.dispatch(dashboardActions.showCreateWalletModal());
        }
    }

    showWallet(event) {
        if ('undefined' === typeof event) {
            throw new Error(' Event is undefined');
        }

        const target = event.target;
        const walletSelector = target.getAttribute('walletSelector');
        this.store.dispatch(dashboardActions.showWallet(walletSelector));
    }

    showDownloadWalletModal(event) {
        this.toggle();

        if ('undefined' === typeof event) {
            throw new Error(' Event is undefined');
        }

        const target = event.target;
        const walletSelector = target.getAttribute('walletSelector');

        this.setState({
            addressForDownload: walletSelector
        })
    }

    toggle() {
        this.setState({
            downloadWalletModalActive: !this.state.downloadWalletModalActive,
        });
    }

    downloadFile() {
        const wallet = this.props.wallet;
        const privateKey = wallet.getPrivateKey(this.state.addressForDownload);

        const jsonFile = this.mapWalletToJSON(privateKey.toString('hex'));

        const element = document.createElement("a");
        const file = new Blob([jsonFile], {type: 'application/json'});
        element.href = URL.createObjectURL(file);
        element.download = this.state.addressForDownload;
        document.body.appendChild(element);
        element.click();

        this.toggle();
    }

    mapWalletToJSON(privateKey){
        const walletObj = {
            privateKey: privateKey
        };
        return JSON.stringify(walletObj);
    }

    render() {
        let wallets = [];
        for (let i = 0; i < this.state.numberOffWallets; i++) {
            wallets.push(<tr>
                <Col xs={'12'} sm={'12'} md={'12'} xl={'12'} lg={'12'} className={'user-panels user-panels--rounded pl-5 pr-5 pt-4 pt-sm-4 pt-md-5 pb-4 pb-sm-4 pb-md-5 mb-5'}>
                    <PortableWallet mainPageActive={true} walletAddress={this.state.addresses[i]}/>
                    <Row className={'m-0 padding-left-bg'}>
                        <Col xs={'12'} className={'m-0 p-0 text-sm-center text-md-left'}>
                            <button id={'portable-show-wallet-' + i}
                                    walletSelector={this.state.addresses[i]}
                                    className={'user-panels__button user-panels__button--blue pl-3 pl-sm-5 pl-md-5 pl-lg-5 pl-xl-5 pr-3 pr-sm-5 pr-md-5 pr-lg-5 pr-xl-5 mr-4 mr-sm-5 mt-4 mt-sm-4 mt-md-0 ml-0 ml-md-5 ml-lg-4 ml-xl-5'}
                                    onClick={this.showWallet}>
                                <TransObject translationKey={'dashboard.manage.resources'}/>
                            </button>
                            <button id={'show-modal-download-wallet-desktop-' + i}
                                    walletSelector={this.state.addresses[i]}
                                    onClick={this.showDownloadWalletModal}
                                    className={'user-panels__button user-panels__button--standard desktop-md-display pl-0 pl-sm-5 pl-md-5 pl-lg-5 pl-xl-5 pr-0 pr-sm-5 pr-md-5 pr-lg-5 pr-xl-5'}>
                                <TransObject translationKey={'dashboard.download.wallet'}/>
                            </button>
                            <img id={'show-modal-download-wallet-mobile-' + i}
                                 walletSelector={this.state.addresses[i]}
                                 alt={'Download icon'}
                                 onClick={this.showDownloadWalletModal}
                                 src={'../assets/images/icons/download.svg'}
                                 className={'download-button mobile-md-display cursor-pointer mt-4 mt-sm-0 float-right float-sm-none'}/>
                        </Col>
                    </Row>
                </Col>
            </tr>)
        }

        return (
            <div>
                <Modal isOpen={this.state.downloadWalletModalActive} toggle={this.toggle}>
                    <ModalHeader toggle={this.toggle} className={'modal-header pt-4 pl-4 pl-md-5 pr-4 pr-md-5 pb-4'}>
                        <Row className={'p-0 m-0'}>
                            <Col xs="12" className={'p-0 m-0'}>
                                <h1 className={'modal-header__title modal-header__title--font-sm mb-4'}>
                                    Downloading address
                                </h1>
                                <p className={'modal-header__subtitle'}>
                                    Neccessary instruction. Read it carefully.
                                </p>
                            </Col>
                        </Row>
                    </ModalHeader>
                    <ModalBody className={'p-4 p-md-5 pr-5 pr-sm-4 pr-md-4'}>
                        <p className={'modal-text modal-text--small'}>
                            Do not share this file with anyone.
                            <br/><br/>
                            The file contains a private address.
                            <br/><br/>
                            We do not take any responsibility for portfolios held
                            on silesiacoin.org.
                        </p>
                        <Row className={'p-0 m-0'}>
                            <Col xs={"12"} className={'pl-0 pr-5 pr-sm-0 m-0'}>
                                <button id={'download-wallet'}
                                        onClick={this.downloadFile}
                                        className={'modal-button modal-button--blue mt-5'}>
                                    Download
                                </button>
                            </Col>
                        </Row>
                        <div className={'mt-4'}>
                            <img src={'../assets/images/icons/button_keylock_icon.png'}
                                 alt={'Keylock icon'}
                                 className={'float-left mr-3'}/>
                            <p className={'modal-info modal-info--sm mb-0 pt-2'}>
                                This information is private  They are your access to real financial resources.
                            </p>
                        </div>
                    </ModalBody>
                </Modal>
                <Row className="m-0 pb-5 container-padding">
                    <Col xs={"12"} sm={'12'} md={'12'} lg={'12'} xl={'12'} className={'pr-0 pr-sm-0 pr-md-3 mr-xl-0 mr-lg-0 pb-0 pb-sm-0 pb-md-3 pb-xl-3 pb-lg-3 pt-5 pt-sm-5 pt-md-5 pt-xl-5 pt-lg-5 pl-0 pl-sm-0 pl-md-3 pl-xl-3 pl-lg-3'}>
                        <button id={'show-modal-mobile'}
                                className={'yellow-button cursor-pointer mobile-display ml-5 mb-5'}
                                onClick={this.showCreateWalletModal}>
                            <img src={'../assets/images/icons/add_wallet_icon.png'}
                                 alt={'Add wallet icon'}
                                 className={'yellow-button__icon mr-4'}/>
                            <TransObject translationKey={'dashboard.add.new.wallet'}/>
                        </button>
                        <h1 className={'main-title pb-5 pl-5 pl-sm-5 pl-md-0 pr-5 pr-sm-5 pr-md-0'}>
                            <TransObject translationKey={'dashboard.your.wallets'}/>
                        </h1>
                        <Row className="m-0 p-0">
                            {wallets}
                        </Row>
                        <button onClick={this.showCreateWalletModal}
                                id={'show-modal-desktop'}
                                className={'yellow-button cursor-pointer desktop-display'}>
                            <img src={'../assets/images/icons/add_wallet_icon.png'}
                                 alt={'Add wallet icon'}
                                 className={'yellow-button__wallet mr-4'}/>
                            <TransObject translationKey={'dashboard.add.new.wallet'}/>
                        </button>
                    </Col>
                </Row>
            </div>
        );
    }
}
