import React from 'react';
import { Row, Col } from 'reactstrap';
import TransObject from '../generic/transObject';
import TranslatedComponent from '../../containers/translatedComponent';
import PortableWallet from '../../components/dashboard/portableWallet';
import { dashboardActions } from '../../_redux/actions/dashboardActions';
import DashboardService from '../../_redux/services/dashboardService';
import { authenticationActions } from '../../_redux/actions/authenticationActions';
import AuthenticatorService from '../../_redux/services/authenticatorService';
import { authenticationConstants } from '../../_redux/constants/authenticationConstants';

export default class Wallets extends TranslatedComponent {
    constructor(props) {
        super(props);
        this.props = props;

        if ('undefined' === typeof props) {
            this.props = {};
        }

        this.state = {
            downloadWalletModalActive: !!this.props.downloadWalletModalActive,
            wallet: !!this.props.wallet,
            addresses: [],
            numberOffWallets: !!this.props.numberOffWallets
        };

        {'undefined' !== typeof this.props.wallet ? (
            this.state = {
                addresses: this.props.wallet.getAddresses(),
                numberOffWallets: this.props.wallet.getAddressCount()
            }
        ) : (
            this.state = {
                addresses: [],
                numberOffWallets: !!this.props.numberOffWallets
            }
        )}

        this.showYourWallets = this.showYourWallets.bind(this);
        this.showAuthenticationModal = this.showAuthenticationModal.bind(this);
        this.showWallet = this.showWallet.bind(this);

        this.store = DashboardService.getDashboardStore();

        this.autStore = AuthenticatorService.getAuthenticatorStore();
        this.unsubscribe = this.autStore.subscribe(() => {
            const state = this.autStore.getState();

            if (state.type === authenticationConstants.WALLET_OBJECT) {
                this.setState({
                    addresses: state.wallet.getAddresses(),
                    numberOffWallets: state.wallet.getAddressCount()
                });
            }

            return state
        });
    }

    showAuthenticationModal() {
        this.autStore.dispatch(authenticationActions.authenticationModal());
    }

    showYourWallets() {
        this.store.dispatch(dashboardActions.showYourWallets());
    }

    showWallet(event) {
        if ('undefined' === typeof event) {
            throw new Error(' Event is undefined');
        }

        const target = event.target;
        const walletSelector = target.getAttribute('walletSelector');
        this.store.dispatch(dashboardActions.showWallet(walletSelector));
    }

    render() {
        let wallets = [];
        for (let i = 0; i < this.state.numberOffWallets && i < 2; i++) {
            wallets.push(<tr>
                <Col xs={'12'} sm={'12'} md={'12'} xl={'12'} lg={'12'} className={'user-panels__wallet-body p-4 p-sm-4 p-md-5 p-lg-4 p-xl-5'}>
                    <PortableWallet mainPageActive={true} walletAddress={this.state.addresses[i]}/>
                    <Row className={'m-0 padding-left-sm'}>
                        <Col xs={'12'} className={'p-0 ml-0 ml-sm-0 ml-md-5 ml-lg-4 ml-xl-5 text-sm-center text-md-left'}>
                            <button id={'portable-show-wallet-' + i}
                                    walletSelector={this.state.addresses[i]}
                                    className={'user-panels__wallet-body__button pl-0 pl-sm-5 pl-md-5 pl-lg-5 pl-xl-5 pr-0 pr-sm-5 pr-md-5 pr-lg-5 pr-xl-5 mt-4 mt-sm-4 mt-md-0'}
                                    onClick={this.showWallet}>
                                <TransObject translationKey={'dashboard.manage.resources'}/>
                            </button>
                        </Col>
                    </Row>
                </Col>
            </tr>)
        }

        return (
            <Row className={'p-0 m-0'}>
                <Col xs={"12"} sm={"12"} className={'pb-0 pb-sm-0 pb-md-3 pb-xl-3 pb-lg-3 pt-5 pl-4 pl-sm-5 pl-md-3 pl-xl-3 pl-lg-3 pr-4 pr-sm-5 pr-md-3 pr-xl-3 pr-lg-3'}>
                    <div className={'user-panels overlay-hidden'}>
                        <div className={'user-panels__head pt-4 pt-md-5 pt-lg-4 pr-md-5 pr-lg-4 pl-md-5 pt-xl-5 pr-4 pr-xl-5 pl-4 pl-lg-4 pl-xl-5 pb-xl-5 pb-lg-4 pb-md-5 pb-sm-4 pb-4'}>
                            <h2 className={'user-panels__head__title'}>
                                <TransObject translationKey={'dashboard.wallets'}/>
                            </h2>
                            <div className={'user-panels__head__subtitle desktop-display m-0'}>
                                <p className={'m-0 p-0'}>
                                    <TransObject translationKey={'dashboard.wallets.info.key'}/>
                                </p>
                                <p className={'mb-0'}>
                                    <TransObject translationKey={'dashboard.wallets.info.safety'}/>
                                </p>
                            </div>
                        </div>
                        <Row className={'p-0 m-0'}>
                            {this.state.numberOffWallets > 0 ? (
                                <Col xs={'12'} className={'m-0 p-0'}>
                                    <Row className="m-0 p-0">
                                        {wallets}
                                    </Row>
                                    <Row className={'m-0 p-0'}>
                                        <Col xs={'12'} className={'m-0 p-0 text-right'}>
                                            <button id={'go-to-your-wallets'}
                                                    onClick={this.showYourWallets}
                                                    className={'yellow-button yellow-button--size cursor-pointer mb-4 mt-4 mr-4 mr-md-5 mr-lg-4 mr-xl-5'}>
                                                <TransObject translationKey={'dashboard.go.to.wallets'}/>
                                                <img src={'../assets/images/symbol/yellow-arrow.svg'}
                                                     alt={'Yellow arrow'}
                                                     className={'yellow-button__icon ml-4'}/>
                                            </button>
                                        </Col>
                                    </Row>
                                </Col>
                            ) : (
                                <Col xs={"12"} className={'pb-5 pt-4 pt-sm-4 pt-md-5 pr-4 pr-xl-5 pl-4 pl-xl-5 m-0'}>
                                    <p className={'user-panels__text user-panels__text--gray user-panels__text--desktop pt-0 pt-md-5 pb-4'}>
                                        <TransObject translationKey={'dashboard.wallets.no.wallets'}/>
                                    </p>
                                    <Row className={'p-0 m-0'}>
                                        <Col xs="12" className={'pb-0 pb-md-5 pl-0 pr-0 m-0 text-center'}>
                                            <button id={'show-authentication-modal'}
                                                    onClick={this.showAuthenticationModal}
                                                    className={'user-panels__button user-panels__button--standard pl-0 pl-sm-5 pl-md-5 pl-lg-5 pl-xl-5 pr-0 pr-sm-5 pr-md-5 pr-lg-5 pr-xl-5'}>
                                                <TransObject translationKey={'dashboard.create.wallet'}/>
                                            </button>
                                        </Col>
                                    </Row>
                                </Col>
                            )}
                        </Row>
                    </div>
                </Col>
            </Row>
        );
    }
}
