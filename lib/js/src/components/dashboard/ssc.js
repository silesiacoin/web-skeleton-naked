import React from 'react';
import { Row, Col } from 'reactstrap';
import TranslatedComponent from '../../containers/translatedComponent';
import CookieService from '../../_redux/services/cookieService';
import ApiUrlParser from '../../_helpers/urlParser';

export default class Ssc extends TranslatedComponent {
    constructor(props) {
        super(props);
        this.props = props;
        this.apiUrl = ApiUrlParser.createFromWindowLocation();

        const enabledFeature = CookieService.getCookie('ssc_not_finished');

        if ('undefined' === typeof props) {
            this.props = {};
        }

        this.state = {
            enabledFeature: enabledFeature.length > 0,
            ltc: null,
            btc: null,
            eth: null,
            ltcRound: null,
            btcRound: null,
            ethRound: null,
            currencyLoaded: false
        };

        this.componentDidMount = this.componentDidMount.bind(this)
    }

    componentDidMount() {
        fetch(ApiUrlParser.parseApiUrlWithDefaultProtocol(this.apiUrl, 'v1/get-price'), {
            method: 'GET',
        }).then((response) => {
            return response.json();
        }).then((myJson) => {
            const eth_eur = myJson.response.eth;
            const btc_eur = myJson.response.btc;
            const ltc_eur = myJson.response.ltc;

            fetch(ApiUrlParser.parseApiUrlWithDefaultProtocol(this.apiUrl, 'v1/currency-converter'), {
                method: 'GET',
            }).then((response) => {
                return response.json();
            }).then((myJson) => {
                const eth_pln = eth_eur * myJson.response.rates.PLN;
                const btc_pln = btc_eur * myJson.response.rates.PLN;
                const ltc_pln = ltc_eur * myJson.response.rates.PLN;

                this.setState({
                    ltc: 5 / ltc_pln,
                    btc: 5 / btc_pln,
                    eth: 5 / eth_pln,
                    currencyLoaded: true
                });

                if (window.innerWidth > 1400) {
                    this.setState({
                        ltcRound: (5 / ltc_pln).toFixed(7),
                        btcRound: (5 / btc_pln).toFixed(7),
                        ethRound: (5 / eth_pln).toFixed(7)
                    })
                }

                if (window.innerWidth > 1199 && window.innerWidth < 1401) {
                    this.setState({
                        ltcRound: (5 / ltc_pln).toFixed(3),
                        btcRound: (5 / btc_pln).toFixed(3),
                        ethRound: (5 / eth_pln).toFixed(3)
                    })
                }

                if (window.innerWidth > 991 && window.innerWidth < 1200) {
                    this.setState({
                        ltcRound: (5 / ltc_pln).toFixed(2),
                        btcRound: (5 / btc_pln).toFixed(2),
                        ethRound: (5 / eth_pln).toFixed(2)
                    })
                }

                if (window.innerWidth > 767 && window.innerWidth < 992) {
                    this.setState({
                        ltcRound: (5 / ltc_pln).toFixed(12),
                        btcRound: (5 / btc_pln).toFixed(12),
                        ethRound: (5 / eth_pln).toFixed(12)
                    })
                }

                if (window.innerWidth > 575 && window.innerWidth < 768) {
                    this.setState({
                        ltcRound: 5 / ltc_pln,
                        btcRound: 5 / btc_pln,
                        ethRound: 5 / eth_pln,
                    })
                }

                if (window.innerWidth < 576) {
                    this.setState({
                        ltcRound: (5 / ltc_pln).toFixed(12),
                        btcRound: (5 / btc_pln).toFixed(12),
                        ethRound: (5 / eth_pln).toFixed(12)
                    })
                }
            });
        });
    }

    render() {
        return (
            <Row>
                <Col xs={"12"} sm={"12"} className={'pb-0 pb-sm-0 pb-md-5 pb-xl-5 pb-lg-5 pt-4 pt-sm-5 pt-md-3 pt-xl-3 pt-lg-3 pl-4 pl-sm-5 pl-md-3 pl-xl-0 pl-lg-0 pr-4 pr-sm-5 pr-md-3 pr-xl-3 pr-lg-3'}>
                    <div className={'user-panels'}>
                        <div className={'user-panels__head pl-3 pr-3 pt-4 pt-sm-4 pt-md-4 pt-lg-1 pt-xl-4 pb-4 pb-sm-4 pb-md-4 pb-lg-1 pb-xl-4 text-center'}>
                            <img src={'../assets/images/illustrations/MinningCrewOne.svg'} className={'user-panels__head__image'} alt="Responsive image"/>
                        </div>
                        <p className={'user-panels__title user-panels__title--purple pt-4 pt-sm-4 pt-md-5 pt-lg-4 pt-xl-5 pl-4 pr-sm-4 pr-md-5 pr-lg-4 pr-xl-5 pl-4 pl-sm-4 pl-md-5 pl-lg-4 pl-xl-5'}>
                            1 SSC is
                        </p>
                        <Row className={'p-0 m-0'}>
                            <Col xs="12" className={'user-panels__body-sm text-left pr-4 pl-4 pl-sm-4 pl-md-5 pl-lg-4 pl-xl-5 pb-5 m-0'}>
                                {this.state.currencyLoaded ? (
                                    <div>
                                        <div>
                                            <img src={'../assets/images/symbol/lc.png'}
                                                 alt={'Dollar icon'}
                                                 className={'user-panels__body-sm__icons'}/>
                                            <div className={'currency-value'}>
                                                <div className={'currency-value__alert currency-value__alert--ltc p-2'}>
                                                    {this.state.ltc}
                                                </div>
                                                <img src={'../assets/images/icons/i.png'}
                                                     alt={'Dollar icon'}
                                                     className={'user-panels__body-sm__i ml-3'}/>
                                            </div>
                                            <p className={'user-panels__body-sm__item'}>
                                                {this.state.ltcRound}
                                            </p>
                                        </div>
                                        <div>
                                            <img src={'../assets/images/symbol/eth-icon.png'}
                                                 alt={'Eth icon'}
                                                 className={'user-panels__body-sm__icons user-panels__body-sm__icons--eth'}/>
                                            <div className={'currency-value'}>
                                                <div className={'currency-value__alert currency-value__alert--eth p-2'}>
                                                    {this.state.eth}
                                                </div>
                                                <img src={'../assets/images/icons/i.png'}
                                                     alt={'Dollar icon'}
                                                     className={'user-panels__body-sm__i ml-3'}/>
                                            </div>
                                            <p className={'user-panels__body-sm__item'}>
                                                {this.state.ethRound}
                                            </p>
                                        </div>
                                        <div>
                                            <img src={'../assets/images/symbol/bitcoin-logo.png'}
                                                 alt={'Dollar icon'}
                                                 className={'user-panels__body-sm__icons'}/>
                                            <div className={'currency-value'}>
                                                <div className={'currency-value__alert currency-value__alert--btc p-2'}>
                                                    {this.state.btc}
                                                </div>
                                                <img src={'../assets/images/icons/i.png'}
                                                     alt={'Dollar icon'}
                                                     className={'user-panels__body-sm__i ml-3'}/>
                                            </div>
                                            <p className={'user-panels__body-sm__item mb-0'}>
                                                {this.state.btcRound}
                                            </p>
                                        </div>
                                    </div>
                                ) : (
                                    <div className={'lds-ssc text-center'}>
                                        <div className={'lds-default lds-default--mt'}>
                                            <div></div>
                                            <div></div>
                                            <div></div>
                                            <div></div>
                                            <div></div>
                                            <div></div>
                                            <div></div>
                                            <div></div>
                                            <div></div>
                                            <div></div>
                                            <div></div>
                                            <div></div>
                                        </div>
                                    </div>
                                )}
                                {this.state.enabledFeature &&
                                    <div className={'save-space'}>
                                        <img src={'../assets/images/symbol/yellow-arrow.svg'}
                                             alt={'Yellow arrow'}
                                             className={'p-0 m-0 user-panels__body-sm__arrow'}/>
                                    </div>
                                }
                            </Col>
                        </Row>
                    </div>
                </Col>
            </Row>
        );
    }
}
