import React from 'react';
import {Row, Col, Container} from 'reactstrap';
import RowGap from './rowGap';
import TransObject from '../generic/transObject';

export default class Tree extends React.Component {
    render() {
        const treeIconStyle = {
            objectFit: 'contain',
            padding: '0rem 2rem'
        };
        return (
            <Row>
                <Col xs={12}>
                    <Row className='bg-purple d-none d-md-block'>
                        <Col md={{size: 10, offset: 1}} lg={{size: 8, offset: 2}}>
                            <Container>
                                <Row noGutters>
                                    <Col xs={12}>
                                        <h2 className='Choose-your-path'>{ <TransObject translationKey={'home.your.path'}/> }</h2>
                                        <p className='Our-organization-is'>{ <TransObject translationKey={'home.developing.organization'}/> }</p>
                                    </Col>
                                </Row>
                                <Row noGutters>
                                    <Col xs={{size: 4, offset: 4}}>
                                        <svg viewBox='0 0 100 75' className='marginZeroWidthFull'>
                                            <line x1='50' y1='0' x2='50' y2='75' stroke='#e7b41d' strokeWidth='1.5'/>
                                        </svg>
                                    </Col>
                                </Row>
                                <Row noGutters>
                                    <Col xs={12}>
                                        <svg viewBox='0 0 300 50' className='marginZeroWidthFull'>
                                            <line x1='150' y1='0' x2='50' y2='50' stroke='#e7b41d' strokeWidth='1.5'/>
                                            <line x1='150' y1='0' x2='150' y2='50' stroke='#e7b41d' strokeWidth='1.5'/>
                                            <line x1='150' y1='0' x2='250' y2='50' stroke='#e7b41d' strokeWidth='1.5'/>
                                        </svg>
                                    </Col>
                                </Row>
                                <Row noGutters>
                                    <Col xs={4}>
                                        <svg viewBox='0 0 100 50' className='marginZeroWidthFull'>
                                            <line x1='50' y1='0' x2='50' y2='50' stroke='#e7b41d' strokeWidth='1.5'/>
                                        </svg>
                                    </Col>
                                    <Col xs={4}>
                                        <svg viewBox='0 0 100 50' className='marginZeroWidthFull'>
                                            <line x1='50' y1='0' x2='50' y2='50' stroke='#e7b41d' strokeWidth='1.5'/>
                                        </svg>
                                    </Col>
                                    <Col xs={4}>
                                        <svg viewBox='0 0 100 50' className='marginZeroWidthFull'>
                                            <line x1='50' y1='0' x2='50' y2='50' stroke='#e7b41d' strokeWidth='1.5'/>
                                        </svg>
                                    </Col>
                                </Row>
                                <Row noGutters className='tree-padding'>
                                    <Col xs={4}>
                                        <h1 className='tree-title'>{ <TransObject translationKey={'home.miner'}/> }</h1>
                                    </Col>
                                    <Col xs={4}>
                                        <h1 className='tree-title'>{ <TransObject translationKey={'home.affiliate'}/> }</h1>
                                    </Col>
                                    <Col xs={4}>
                                        <h1 className='tree-title'>{ <TransObject translationKey={'home.investor'}/> }</h1>
                                    </Col>
                                </Row>
                                <Row noGutters>
                                    <Col xs={4}>
                                        <svg viewBox='0 0 100 25' className='marginZeroWidthFull'>
                                            <line x1='50%' y1='0' x2='50' y2='25' stroke='#e7b41d' strokeWidth='1.5'/>
                                        </svg>
                                    </Col>
                                    <Col xs={4}>
                                        <svg viewBox='0 0 100 25' className='marginZeroWidthFull'>
                                            <line x1='50%' y1='0' x2='50' y2='25' stroke='#e7b41d' strokeWidth='1.5'/>
                                        </svg>
                                    </Col>
                                    <Col xs={4}>
                                        <svg viewBox='0 0 100 25' className='marginZeroWidthFull'>
                                            <line x1='50%' y1='0' x2='50' y2='25' stroke='#e7b41d' strokeWidth='1.5'/>
                                        </svg>
                                    </Col>
                                </Row>
                                <Row noGutters className='tree-padding'>
                                    <Col xs={4}>
                                        <Row noGutters>
                                            <Col xs={{size: 6, offset: 3}}>
                                                <img
                                                    src={'https://storage.googleapis.com/ssc-assets/Developers_Pack/Icons/crystal.svg'}
                                                    style={treeIconStyle} width='100%'/>
                                            </Col>
                                        </Row>
                                    </Col>
                                    <Col xs={4}>
                                        <Row noGutters>
                                            <Col xs={{size: 6, offset: 3}}>
                                                <img
                                                    src={'https://storage.googleapis.com/ssc-assets/Developers_Pack/Icons/affiliate.svg'}
                                                    style={treeIconStyle} width='100%'/>
                                            </Col>
                                        </Row>
                                    </Col>
                                    <Col xs={4}>
                                        <Row noGutters>
                                            <Col xs={{size: 6, offset: 3}}>
                                                <img
                                                    src={'https://storage.googleapis.com/ssc-assets/Developers_Pack/Icons/investment.svg'}
                                                    style={treeIconStyle} width='100%'/>
                                            </Col>
                                        </Row>
                                    </Col>
                                </Row>
                                <Row noGutters>
                                    <Col xs={4}>
                                        <svg viewBox='0 0 100 50' className='marginZeroWidthFull'>
                                            <line x1='50' y1='0' x2='50' y2='50' stroke='#e7b41d' strokeWidth='1.5'/>
                                        </svg>
                                    </Col>
                                    <Col xs={4}>
                                        <svg viewBox='0 0 100 50' className='marginZeroWidthFull'>
                                            <line x1='50' y1='0' x2='50' y2='50' stroke='#e7b41d' strokeWidth='1.5'/>
                                        </svg>
                                    </Col>
                                    <Col xs={4}>
                                        <svg viewBox='0 0 100 50' className='marginZeroWidthFull'>
                                            <line x1='50' y1='0' x2='50' y2='50' stroke='#e7b41d' strokeWidth='1.5'/>
                                        </svg>
                                    </Col>
                                </Row>
                                <Row noGutters className='tree-padding'>
                                    <Col xs={4}>
                                        <p className='tree-desc'>{ <TransObject translationKey={'home.dig.efficiently'}/>}</p>
                                    </Col>
                                    <Col xs={4}>
                                        <p className='tree-desc'>{ <TransObject translationKey={'home.reality.impact'}/> }</p>
                                    </Col>
                                    <Col xs={4}>
                                        <p className='tree-desc'>{ <TransObject translationKey={'home.growth.influence'}/> }</p>
                                    </Col>
                                </Row>
                                <Row noGutters>
                                    <Col xs={4}>
                                        <svg viewBox='0 0 100 50' className='marginZeroWidthFull'>
                                            <line x1='50' y1='0' x2='50' y2='50' stroke='#e7b41d' strokeWidth='1.5'/>
                                        </svg>
                                    </Col>
                                    <Col xs={4}>
                                        <svg viewBox='0 0 100 50' className='marginZeroWidthFull'>
                                            <line x1='50' y1='0' x2='50' y2='50' stroke='#e7b41d' strokeWidth='1.5'/>
                                        </svg>
                                    </Col>
                                    <Col xs={4}>
                                        <svg viewBox='0 0 100 50' className='marginZeroWidthFull'>
                                            <line x1='50' y1='0' x2='50' y2='50' stroke='#e7b41d' strokeWidth='1.5'/>
                                        </svg>
                                    </Col>
                                </Row>
                                <Row noGutters className='tree-padding'>
                                    <Col xs={4}>
                                        <p className='tree-desc'>{ <TransObject translationKey={'home.currency-premiere'}/> }</p>
                                    </Col>
                                    <Col xs={4}>
                                        <p className='tree-desc'>{ <TransObject translationKey={'home.similar.people'}/> }</p>
                                    </Col>
                                    <Col xs={4}>
                                        <p className='tree-desc'>{ <TransObject translationKey={'home.moment.to.buy'}/> }</p>
                                    </Col>
                                </Row>
                                <Row noGutters>
                                    <Col xs={4}>
                                        <svg viewBox='0 0 100 50' className='marginZeroWidthFull'>
                                            <line x1='50' y1='0' x2='50' y2='50' stroke='#e7b41d' strokeWidth='1.5'/>
                                        </svg>
                                    </Col>
                                    <Col xs={4}>
                                        <svg viewBox='0 0 100 50' className='marginZeroWidthFull'>
                                            <line x1='50' y1='0' x2='50' y2='50' stroke='#e7b41d' strokeWidth='1.5'/>
                                        </svg>
                                    </Col>
                                    <Col xs={4}>
                                        <svg viewBox='0 0 100 50' className='marginZeroWidthFull'>
                                            <line x1='50' y1='0' x2='50' y2='50' stroke='#e7b41d' strokeWidth='1.5'/>
                                        </svg>
                                    </Col>
                                </Row>
                                <Row noGutters className='tree-padding'>
                                    <Col xs={4}>
                                        <p className='tree-desc'>{ <TransObject translationKey={'home.percentage.back'}/> }</p>
                                    </Col>
                                    <Col xs={4}>
                                        <p className='tree-desc'>{ <TransObject translationKey={'home.develop.in.fields'}/> }</p>
                                    </Col>
                                    <Col xs={4}>
                                        <p className='tree-desc'>{ <TransObject translationKey={'home.monitor.currency'}/> }</p>
                                    </Col>
                                </Row>
                                <Row noGutters>
                                    <Col xs={4}>
                                        <svg viewBox='0 0 100 50' className='marginZeroWidthFull'>
                                            <line x1='50' y1='0' x2='50' y2='50' stroke='#e7b41d' strokeWidth='1.5'/>
                                        </svg>
                                    </Col>
                                    <Col xs={4}>
                                        <svg viewBox='0 0 100 50' className='marginZeroWidthFull'>
                                            <line x1='50' y1='0' x2='50' y2='50' stroke='#e7b41d' strokeWidth='1.5'/>
                                        </svg>
                                    </Col>
                                    <Col xs={4}>
                                        <svg viewBox='0 0 100 50' className='marginZeroWidthFull'>
                                            <line x1='50' y1='0' x2='50' y2='50' stroke='#e7b41d' strokeWidth='1.5'/>
                                        </svg>
                                    </Col>
                                </Row>
                                <Row noGutters className='tree-padding'>
                                    <Col xs={4} className='col-4' align='center'>
                                        <div className='tree-button'>
                                            { <TransObject translationKey={'home.show.more'}/> }
                                        </div>
                                    </Col>
                                    <Col xs={4} align='center'>
                                        <div className='tree-button'>
                                            { <TransObject translationKey={'home.show.more'}/> }
                                        </div>
                                    </Col>
                                    <Col xs={4} align='center'>
                                        <div className='tree-button'>
                                            { <TransObject translationKey={'home.show.more'}/> }
                                        </div>
                                    </Col>
                                </Row>
                                <Row noGutters>
                                    <Col xs={4}>
                                        <svg viewBox='0 0 100 50' className='marginZeroWidthFull'>
                                            <line x1='50' y1='0' x2='50' y2='50' stroke='#e7b41d' strokeWidth='1.5'/>
                                        </svg>
                                    </Col>
                                    <Col>
                                        <svg viewBox='0 0 100 50' className='marginZeroWidthFull'>
                                            <line x1='50' y1='0' x2='50' y2='50' stroke='#e7b41d' strokeWidth='1.5'/>
                                        </svg>
                                    </Col>
                                    <Col>
                                        <svg viewBox='0 0 100 50' className='marginZeroWidthFull'>
                                            <line x1='50' y1='0' x2='50' y2='50' stroke='#e7b41d' strokeWidth='1.5'/>
                                        </svg>
                                    </Col>
                                </Row>
                                <Row noGutters>
                                    <Col xs={12}>
                                        <svg viewBox='0 0 300 50' className='marginZeroWidthFull'>
                                            <line x2='150' y1='0' x1='50' y2='50' stroke='#e7b41d' strokeWidth='1.5'/>
                                            <line x2='150' y1='0' x1='150' y2='50' stroke='#e7b41d' strokeWidth='1.5'/>
                                            <line x2='150' y1='0' x1='250' y2='50' stroke='#e7b41d' strokeWidth='1.5'/>
                                        </svg>
                                    </Col>
                                </Row>
                                <Row noGutters>
                                    <Col xs={{size: 4, offset: 4}}>
                                        <svg viewBox='0 0 100 75' className='marginZeroWidthFull'>
                                            <line x1='50' y1='0' x2='50' y2='75' stroke='#e7b41d' strokeWidth='1.5'/>
                                        </svg>
                                    </Col>
                                </Row>
                                <Row noGutters className='tree-padding'>
                                    <Col xs={12} align='center'>
                                        <a href='/dashboard'>
                                            <button className="tree-button">
                                                <TransObject translationKey={'become.a.pro'}/>
                                            </button>
                                        </a>
                                    </Col>
                                </Row>
                                <Row noGutters className='tree-end'>
                                    <Col xs={12} align='center'>
                                        <p className='tree-desc'>{ <TransObject translationKey={'home.free.registration'}/> }</p>
                                    </Col>
                                </Row>
                            </Container>
                        </Col>
                    </Row>
                    <Row className='d-block d-md-none bg-white'>
                        <Col xs={12}>
                            <Row>
                                <Col xs={12}>
                                    <h2 className='Choose-your-path'>Choose your path:</h2>
                                </Col>
                            </Row>
                            <Row className='bg-blue-tree tree-till'>
                                <Col xs={12}>
                                    <Row className='p-4 mt-2 pb-0'>
                                        <Col xs={4}>
                                            <img
                                                src={'https://storage.googleapis.com/ssc-assets/Developers_Pack/Icons/investment.svg'}
                                                width={'100%'}/>
                                        </Col>
                                        <Col xs={8} className='pl-3 tree-title d-flex align-items-start'>
                                            <p>Investor</p>
                                        </Col>
                                    </Row>
                                    <RowGap/>
                                    <Row>
                                        <Col xs={12} className='tree-desc p-4'>
                                            <p>By investing in the currency
                                                you can influence its growth<br/><br/>
                                                The best moment to buy is the
                                                beginning, the supply will appear
                                                in the future<br/><br/><br/>
                                                Monitor currency trends by buying and selling at the right
                                                moments<br/><br/><br/>
                                            </p>
                                            <a href='/dashboard'>
                                                <button className="tree-button">
                                                    <TransObject translationKey={'become.a.pro'}/>
                                                </button>
                                            </a>
                                            <br/><br/><br/>
                                        </Col>
                                    </Row>
                                </Col>
                            </Row>
                            <Row className='bg-purple tree-till'>
                                <Col xs={12}>
                                    <Row className='p-4 mt-2 pb-0'>
                                        <Col xs={4}>
                                            <img
                                                src={'https://storage.googleapis.com/ssc-assets/Developers_Pack/Icons/crystal.svg'}
                                                width={'100%'}/>
                                        </Col>
                                        <Col xs={8} className='pl-3 tree-title d-flex align-items-start'>
                                            <p>Miner</p>
                                        </Col>
                                    </Row>
                                    <RowGap/>
                                    <Row>
                                        <Col xs={12} className='tree-desc p-4'>
                                            <p>We will show you how
                                                to dig efficiently<br/><br/>
                                                The premiere of the currency is
                                                the best moment to start <br/><br/><br/>
                                                How much energy did you put into digging, you get that percentage
                                                back. <br/><br/><br/>
                                            </p>
                                            <a href='/dashboard'>
                                                <button className="tree-button">
                                                    <TransObject translationKey={'become.a.pro'}/>
                                                </button>
                                            </a>
                                            <br/><br/><br/>
                                        </Col>
                                    </Row>
                                </Col>
                            </Row>
                            <Row className='bg-yellow-tree tree-till'>
                                <Col xs={12}>
                                    <Row className='p-4 mt-2 pb-0'>
                                        <Col xs={4}>
                                            <img
                                                src={'https://storage.googleapis.com/ssc-assets/Developers_Pack/Icons/affiliate.svg'}
                                                width={'100%'}/>
                                        </Col>
                                        <Col xs={8} className='pl-3 tree-title d-flex align-items-start'>
                                            <p>
                                                Affiliate
                                            </p>
                                        </Col>
                                    </Row>
                                    <RowGap/>
                                    <Row>
                                        <Col xs={12} className='tree-desc p-4'>
                                            <p>Have a real impact on
                                                reality in the region<br/><br/>
                                                Meet people with a similar way of thinking and aiming <br/><br/><br/>
                                                Develop in the field of programming, promotion,
                                                operations<br/><br/><br/>
                                            </p>
                                            <a href='/dashboard'>
                                                <button className="tree-button">
                                                    <TransObject translationKey={'become.a.pro'}/>
                                                </button>
                                            </a>
                                            <br/><br/><br/>
                                        </Col>
                                    </Row>
                                </Col>
                            </Row>
                        </Col>
                    </Row>
                </Col>
            </Row>
        );
    }
}
