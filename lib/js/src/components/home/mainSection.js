import React from 'react';
import { Row, Col } from 'reactstrap';
import TransObject from '../generic/transObject';

export default class MainSection extends React.Component {
  constructor (props) {
    super(props);
    this.scrollToBottom = this.scrollToBottom.bind(this);
  }
  scrollToBottom () {
    $([document.documentElement, document.body]).animate({
      scrollTop: $('#smallInfoSection').offset().top
    }, 1000);
  }
  render () {
    return (
      <Row className='bg'>
        <Col xs={12} className='bgContainer'>
          <Row>
            <Col sm={{ size: 12 }} md={{ size: 10, offset: 1 }} lg={{ size: 7, offset: 2 }} className='animated fadeIn'>
              <p className='Time-is-go-on-get-a d-none d-md-block animated fadeIn delay-1s'>{ <TransObject translationKey={'home.time.goes.on'}/> }</p>
              <p className='Number-text-banner d-none d-md-block  animated fadeIn delay-1s'>75 000 / 500 000</p>
              <p className='Welcome-in-revolution  animated fadeIn delay-1s'>{ <TransObject translationKey={'home.welcome.to.revolution'}/> }</p>
              <p className='We-are-a-network-wit  animated fadeIn delay-1s'>{ <TransObject translationKey={'home.network.with.goal'}/> }<br />
                <br className='block d-md-none  animated fadeIn delay-1s' />
                  { <TransObject translationKey={'home.have.fun'}/> }</p>
              <div className='scrollIconContainer  animated fadeIn delay-1s' onClick={ this.scrollToBottom }>
                <span>
                  <img src='https://storage.googleapis.com/ssc-assets/Developers_Pack/Icons/MouseScroll.svg' />
                </span>
                <span className='textByIconScroll  animated fadeIn delay-1s'>{ <TransObject translationKey={'home.insight.scroll'}/> }</span>
              </div>
            </Col>
          </Row>
        </Col>
      </Row>
    );
  }
}
