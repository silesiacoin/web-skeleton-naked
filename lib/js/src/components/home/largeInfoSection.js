import React from 'react';
import { Row, Col } from 'reactstrap';
import RowGap from './rowGap';
import InfoLarge from '../generic/infoLarge';
import TransObject from '../generic/transObject';

export default class LargeInfoSection extends React.Component {
  render () {
    return (
      <Row className='bg-white'>
        <Col sm={{ size: 12 }} md={{ size: 8, offset: 2 }}>
          <RowGap />
          <InfoLarge title={ <TransObject translationKey={'home.transaction.revolution'}/> }
                     desc={ <TransObject translationKey={'home.secure.terminals'}/>}
                     imgSrc={'https://storage.googleapis.com/ssc-assets/Developers_Pack/Illustration/LandingPageOne.svg'}
                     imgSide={1}
          />
          <RowGap />
          <InfoLarge title={<TransObject translationKey={'home.virtual.network'}/>}
                     desc={ <TransObject translationKey={'home.social.network'}/> }
                     imgSrc={'https://storage.googleapis.com/ssc-assets/Developers_Pack/Illustration/LandingPageTwo.svg'}
                     imgSide={2}
          />
          <RowGap />
        </Col>
      </Row>
    );
  }
}
