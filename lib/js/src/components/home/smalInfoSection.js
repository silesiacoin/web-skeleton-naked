import React from 'react';
import { Row, Col } from 'reactstrap';
import InfoSmall from '../generic/infoSmall';
import RowGap from './rowGap';
import TransObject from '../generic/transObject';

export default class SmallInfoSection extends React.Component {
  render () {
    return (
      <Row className='bg-white' id='smallInfoSection'>
        <Col xs={12}>
          <RowGap />
          <Row>
            <Col sm={{ size: 12 }} md={{ size: 8, offset: 2 }}>
              <Row>
                <Col xs={6} sm={6} md={3} className='padding-right-mobile d-flex justify-content-md-between justify-content-sm-center align-content-start'>
                  <InfoSmall title='Blockchain' imgSrc='https://storage.googleapis.com/ssc-assets/Developers_Pack/content/blockchain.jpg' />
                </Col>
                <Col xs={6} sm={6} md={3} className='padding-left-mobile d-flex justify-content-md-between justify-content-sm-center align-content-start'>
                  <InfoSmall title={ <TransObject translationKey={'home.ethereum.technology'}/> } imgSrc='https://storage.googleapis.com/ssc-assets/Developers_Pack/content/ethereum.png' />
                </Col>
                <Col xs={6} sm={6} md={3} className='padding-right-mobile d-flex justify-content-md-between justify-content-sm-center align-content-start'>
                  <InfoSmall title={ <TransObject translationKey={'home.anonymity.support'}/> } imgSrc='https://storage.googleapis.com/ssc-assets/Developers_Pack/content/security.jpg' />
                </Col>
                <Col xs={6} sm={6} md={3} className='padding-left-mobile d-flex justify-content-md-between justify-content-sm-center align-content-start'>
                  <InfoSmall title={ <TransObject translationKey={'home.integrated.network'}/> } imgSrc='https://storage.googleapis.com/ssc-assets/Developers_Pack/content/networking.jpg' />
                </Col>
              </Row>
            </Col>
          </Row>
        </Col>
      </Row>
    );
  }
}
