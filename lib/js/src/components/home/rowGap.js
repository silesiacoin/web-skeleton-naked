import React from 'react';

export default class RowGap extends React.Component {
  render () {
    return (
      <div className='row'>
        <div className='col-12 gap' />
      </div>
    );
  }
}
