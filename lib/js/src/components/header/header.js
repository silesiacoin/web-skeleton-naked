import React from 'react';
import { Collapse, Nav, NavbarToggler, NavItem, NavLink } from 'reactstrap';
import TransObject from '../generic/transObject';
import LanguageSwitch from '../modals/languageSwitch';

export default class Header extends React.Component {
    constructor(props) {
        super(props);

        this.toggleNavbar = this.toggleNavbar.bind(this);
        this.state = {
            collapsed: true
        };
    }

    toggleNavbar() {
        this.setState({
            collapsed: !this.state.collapsed
        });
    }

    render() {
        return (
            <nav id='navBar'>
                <div className='desktop'>
                    <a href='/'><img
                        src='https://storage.googleapis.com/ssc-assets/Developers_Pack/Logo/silesiacoin_small.png'
                        height='100%' className='nav-logo' alt={'silesiacoin logo small'}/></a>
                    <a href='#' className={'language-switch'}><LanguageSwitch/></a>
                    <a href='#' className='item-nav'
                    ><TransObject translationKey={'fresh.journal'}/></a>
                    <a href='/affiliatenetwork' className='item-nav'
                    ><TransObject translationKey={'affiliate.network'}/></a>
                    <a href='/miners' className='item-nav'
                    ><TransObject translationKey={'mining.crew'}/></a>
                    <a href='/business' className='item-nav'
                    ><TransObject translationKey={'business.earning'}/></a>
                    <a href='#' className='item-nav-last'>FAQ</a>
                    <a href='/dashboard'>
                        <button className="tree-button">
                            <TransObject translationKey={'become.a.pro'}/>
                        </button>
                    </a>
                </div>
                <div className='mobile'>
                    <a href='/'
                    ><img
                        src='https://storage.googleapis.com/ssc-assets/Developers_Pack/Logo/silesiacoin_small.png'
                        alt='ssc'
                        height='100%'
                        className='nav-logo'
                    /></a>
                    <NavbarToggler onClick={this.toggleNavbar} className='mr-2'/>
                    <Collapse isOpen={!this.state.collapsed}>
                        <Nav>
                            <NavItem>
                                <NavLink href={'#'}>
                                    <LanguageSwitch/>
                                </NavLink>
                            </NavItem>
                            <NavItem>
                                <NavLink href='#'><TransObject translationKey={'fresh.journal'}/></NavLink>
                            </NavItem>
                            <NavItem>
                                <NavLink href='/affiliatenetwork'><TransObject translationKey={'affiliate.network'}/></NavLink>
                            </NavItem>
                            <NavItem>
                                <NavLink href='/miners'><TransObject translationKey={'mining.crew'}/></NavLink>
                            </NavItem>
                            <NavItem>
                                <NavLink href='/business'><TransObject translationKey={'business.earning'}/></NavLink>
                            </NavItem>
                            <NavItem>
                                <NavLink href='#'>FAQ</NavLink>
                            </NavItem>
                            <NavItem>
                                <NavLink href='/dashboard'>
                                    <button className="tree-button">
                                        <TransObject translationKey={'become.a.pro'}/>
                                    </button>
                                </NavLink>
                            </NavItem>
                        </Nav>
                    </Collapse>
                </div>
            </nav>
        );
    }
}
