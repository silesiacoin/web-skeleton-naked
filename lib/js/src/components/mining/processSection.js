import React from 'react';
import { Row, Col } from 'reactstrap';
import TransObject from '../generic/transObject';

export default class ProcessSection extends React.Component {
    render() {
        return (
            <div className='miningContainer p-0'>
                <Row className='pt-5 mt-5 pb-4'>
                    <Col sm={ 12 } lg={ 12 } xl={ 12 } md={ 12 }>
                        <div className='title_process_mining miningContainer__text miningContainer__text--bold miningContainer__text--font-HKGrotesk-Bold miningContainer__text--purple miningContainer__text--size-middle miningContainer__text--align pt-5 mt-5'>
                            { <TransObject translationKey={'mining.digging.process'}/> }
                        </div>
                    </Col>
                </Row>
                <Row className='pb-5 mb-5'>
                    <Col sm={ 12 } lg={ 12 } xl={ 12 } md={ 12 }>
                        <div className='desc_process_mining miningContainer__text miningContainer__text--align miningContainer__text--font-HKGrotesk-Medium miningContainer__text--size-very-small miningContainer__text--weight-middle miningContainer__text--gray'>
                            { <TransObject translationKey={'mining.your.computing.power'}/> }
                        </div>
                    </Col>
                </Row>
                <Row className='pb-5 mb-5'>
                    <Col xs={12} md={{ size:8, offset:2 }}>
                        <img
                            src='https://storage.googleapis.com/ssc-assets/Developers_Pack/Photography/MiningCrew/img3.png'
                            width={'100%'}
                            alt={'mining crew'}
                        />
                    </Col>
                </Row>
            </div>
        );
    }
}
