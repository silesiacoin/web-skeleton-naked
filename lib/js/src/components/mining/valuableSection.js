import React from "react";
import {Row, Col} from "reactstrap";
import TransObject from '../generic/transObject';

export default class ValuableSection extends React.Component {
    render() {
        return (
            <div className='miningContainer'>
                <Row>
                    <Col sm={ 2 } lg={ 2 } xl={ 2 } md={ 2 }>
                    </Col>
                    <Col sm={ 8 } lg={ 8 } xl={ 8 } md={ 8 }>
                        <div className='title_process_mining pb-4 mb-3'>
                            { <TransObject translationKey={'mining.valuable'}/> }
                        </div>
                        <div className='desc_process_mining  pb-4'>
                            { <TransObject translationKey={'mining.skipping.what.you.earn'}/> }<br/>{ <TransObject translationKey={'perspectives.of.SSC'}/> }
                        </div>
                        <div className='miningContainer__rectangle-yellow'>
                        </div>
                        <Row>
                            <Col sm={ 12 } lg={ 6 } xl={ 6 } md={ 6 }>
                                <div
                                    className='desc_valuableSec miningContainer__text pt-4'>
                                    { <TransObject translationKey={'mining.support.the.region'}/> }
                                </div>
                                <div
                                    className='desc_valuableSec miningContainer__text pt-4'>
                                    { <TransObject translationKey={'mining.little.competition'}/> }
                                </div>
                                <div
                                    className='desc_valuableSec miningContainer__text pt-4'>
                                    { <TransObject translationKey={'mining.convert.easily'}/> }
                                </div>
                            </Col>
                            <Col sm={ 12 } lg={ 6 } xl={ 6 } md={ 6 }>
                                <div
                                    className='desc_valuableSec miningContainer__text pt-4'>
                                    { <TransObject translationKey={'mining.fair.distribution'}/> }
                                </div>
                                <div
                                    className='desc_valuableSec miningContainer__text pt-4'>
                                    { <TransObject translationKey={'mining.energy.input'}/> }
                                </div>
                                <div
                                    className='desc_valuableSec miningContainer__text pt-4 pb-5 mb-5'>
                                    { <TransObject translationKey={'mining.digging.adventure'}/> }
                                </div>
                            </Col>
                        </Row>
                    </Col>
                </Row>
            </div>
        );
    }
}
