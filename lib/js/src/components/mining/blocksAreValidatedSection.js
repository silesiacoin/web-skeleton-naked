import React from "react";
import {Row, Col} from "reactstrap";
import TransObject from '../generic/transObject';

export default class BlocksAreValidatedSection extends React.Component {
    render() {
        return (
            <div className='miningContainer miningContainer--purple p-0'>
                <Row className='m-0'>
                    <Col sm={ 12 } lg={ 12 } xl={ 8 } md={ 12 }>
                        <Row>
                            <Col sm={ 2 } lg={ 2 } xl={ 2 } md={ 2 }>
                            </Col>
                            <Col sm={ 8 } lg={ 6 } xl={ 6 } md={ 6 } className='pt-5'>
                                <div className='title_blocksAreValidated mt-5 pt-5 pb-5 mt-5'>
                                    {<TransObject translationKey={'mining.blocks.are.validated'}/>}
                                </div>
                                <div className='desc_blocksAreValidated pt-5 pb-5'>
                                    {<TransObject translationKey={'mining.dig.for.gnu'}/>}
                                    <br/>
                                    <br/>
                                    {<TransObject translationKey={'mining.kick.on.cpu'}/>}
                                </div>
                                <Row>
                                    <Col sm={ 2 } lg={ 2 } xl={ 2 } md={ 2 } className='pt-5 pb-5 mb-5 mb-sm-5 mb-lg-0 mb-xl-0 mb-md-0'>
                                        <div className='button m-0'>
                                            <div className='button__text'>
                                                {<TransObject translationKey={'mining.learn.more'}/>}
                                            </div>
                                        </div>
                                    </Col>
                                </Row>
                            </Col>
                            <Col sm={ 2 } lg={ 4 } xl={ 4 } md={ 4 }>
                            </Col>
                        </Row>
                    </Col>
                    <Col sm={ 12 } lg={ 12 } xl={ 4 } md={ 12 } className='m-0 p-0'>
                        <img
                            src='https://storage.googleapis.com/ssc-assets/Developers_Pack/Photography/MiningCrew/img2.png'
                            width={'100%'}
                            alt={'mining crew'}
                        />
                    </Col>
                </Row>
            </div>
        );
    }
}
