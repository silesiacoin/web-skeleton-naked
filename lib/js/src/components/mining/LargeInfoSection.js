import React from "react";
import {Row, Col} from "reactstrap";
import RowGap from "../home/rowGap";
import InfoLarge from "../generic/infoLarge";
import TransObject from '../generic/transObject';

export default class LargeInfoSection extends React.Component {
    render() {
        return (
            <Row className="bg-white m-0">
                <Col sm={{ size:12 }} md={{ size:8, offset:2 }}>
                    <RowGap/>
                    <InfoLarge title={ <TransObject translationKey={'mining.starting.assumptions'}/> }
                               desc={ <TransObject translationKey={'mining.block.mining.system'}/> }
                               imgSrc={ 'https://storage.googleapis.com/ssc-assets/Developers_Pack/Illustration/MinningCrewOne.svg' }
                               imgSide={ 1 }/>
                    <RowGap/>
                    <InfoLarge title={ <TransObject translationKey={'mining.necessary.tutorial'}/> }
                               desc={ <TransObject translationKey={'mining.specially.written.instruction'}/> }
                               imgSrc={ 'https://storage.googleapis.com/ssc-assets/Developers_Pack/Illustration/MinningCrewTwo.svg' }
                               imgSide={ 2 }/>
                    <RowGap/>
                </Col>
            </Row>
        );
    }
}
