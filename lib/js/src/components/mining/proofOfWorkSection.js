import React from "react";
import {Row, Col} from "reactstrap";
import TransObject from '../generic/transObject';

export default class ProofOfWorkSection extends React.Component {
    render() {
        return (
            <div className='miningContainer miningContainer--blue p-0'>
                <Row className='m-0 pt-5 mb-4'>
                    <Col sm={ 12 } lg={ 12 } xl={ 12 } md={ 12 }>
                        <div className='title_miningContainer pt-5 pb-5'>
                            {<TransObject translationKey={'mining.proof.of.work'}/>} <br/>
                            {<TransObject translationKey={'mining.in.policy'}/>}
                        </div>
                    </Col>
                </Row>
                <Row className='pb-4 mb-4'>
                    <Col sm={ 12 } lg={ 12 } xl={ 12 } md={ 12 }>
                        <div className='desc_miningContainer pt-4 mt-5'>
                            {<TransObject translationKey={'mining.api.market.xamarin'}/>}
                        </div>
                    </Col>
                </Row>
            </div>
        );
    }
}
