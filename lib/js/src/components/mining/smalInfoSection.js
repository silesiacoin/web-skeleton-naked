import React from "react";
import {Row, Col} from "reactstrap";
import InfoSmall from "../generic/infoSmall";
import RowGap from "../home/rowGap";
import TransObject from "../generic/transObject";

export default class SmallInfoSection extends React.Component {
    render() {
        return (
            <Row className="bg-white m-0" id="smallInfoSection">
                <Col xs={ 12 }>
                    <RowGap/>
                    <Row>
                        <Col sm={{ size:12 }} md={{ size:8, offset:2 }}>
                            <Row>
                                <Col xs={ 6 } sm={ 6 } md={ 3 } className='padding-right-mobile d-flex justify-content-md-between justify-content-sm-center align-content-start'>
                                    <InfoSmall title={ <TransObject translationKey={'mining.atm.machines'}/> } imgSrc="https://storage.googleapis.com/ssc-assets/Developers_Pack/content/atm.png"/>
                                </Col>
                                <Col xs={ 6 } sm={ 6 } md={ 3 } className='padding-left-mobile d-flex justify-content-md-between justify-content-sm-center align-content-start'>
                                    <InfoSmall title={ <TransObject translationKey={'mining.exchange.shops'}/> } imgSrc="https://storage.googleapis.com/ssc-assets/Developers_Pack/content/exchange.png"/>
                                </Col>
                                <Col xs={ 6 } sm={ 6 } md={ 3 } className='padding-right-mobile d-flex justify-content-md-between justify-content-sm-center align-content-start'>
                                    <InfoSmall title={ <TransObject translationKey={'mining.voting.alternative'}/> } imgSrc="https://storage.googleapis.com/ssc-assets/Developers_Pack/content/difrient.png"/>
                                </Col>
                                <Col xs={ 6 } sm={ 6 } md={ 3 } className='padding-left-mobile d-flex justify-content-md-between justify-content-sm-center align-content-start'>
                                    <InfoSmall title={ <TransObject translationKey={'mining.local.market'}/> } imgSrc="https://storage.googleapis.com/ssc-assets/Developers_Pack/content/market.png"/>
                                </Col>
                            </Row>
                        </Col>
                    </Row>
                </Col>
            </Row>
        );
    }
}
