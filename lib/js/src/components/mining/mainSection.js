import React, { Component } from 'react';
import { Row, Col } from 'reactstrap';
import TransObject from '../generic/transObject';

export class MainSection extends Component {
    render() {
        return (
            <Row noGutters={ true } className={'m-0'}>
                <Col xs={ 12 }>
                    <Row noGutters={ true } className='margin-full background__white m-0'>
                        <Col xs={ 12 } md={{ size:4 }} >
                            <img
                                src ={ 'https://storage.googleapis.com/ssc-assets/Developers_Pack/Photography/MiningCrew/img1.png' }
                                width={ '100%' }
                                alt={'mining crew'}
                            />
                        </Col>
                        <Col xs={ 12 } md={{ size:6 }} className='p-3'>
                            <Row noGutters={ true } className={'m-0'}>
                                <Col xs={{ size:10, offset:1 }} md={{ size:8, offset:2 }}>
                                    <h1 className='main-selection-till-title mt-4'>{<TransObject translationKey={'mining.power.earn'}/>}<br />
                                    {<TransObject translationKey={'mining.regardless.you.do'}/>}</h1>
                                    <div className='ornament'>&nbsp;</div>
                                    <p className='main-selection-till-desc mb-5'>
                                        {<TransObject translationKey={'mining.have.experience'}/>}<br />
                                        {<TransObject translationKey={'mining.teach.you.everything'}/>}
                                    </p>
                                </Col>
                            </Row>
                            <Row noGutters={ true } className='mt-2 ml-0 mr-0'>
                                <Col xs={{ size:6, offset:0 }} md={{ size:4, offset:2 }}  className='d-flex flex-column justify-content-md-start justify-content-xs-center align-content-center pr-xs-0 pr-md-2 p-1'>
                                    <img
                                        src={ 'https://storage.googleapis.com/ssc-assets/Developers_Pack/content/contribution.jpg' }
                                        width={ '100%' }
                                        alt={'contribution'}
                                    />
                                    <p className='main-selection-till mt-xs-3 mt-md-1'>{<TransObject translationKey={'mining.contribution.defines.earnings'}/>}</p>
                                </Col>
                                <Col xs={{ size:6, offset:0 }} md={{ size:4, offset:2 }}  className='d-flex  flex-column justify-content-md-start justify-content-xs-center align-content-center pr-xs-0 pr-md-2 p-1 mb-xs-2 mb-md-0'>
                                    <img
                                        src={'https://storage.googleapis.com/ssc-assets/Developers_Pack/content/teamwork.jpg'}
                                        width={'100%'}
                                        alt={'teamwork'}
                                    />
                                    <p className='main-selection-till  mt-xs-3 mt-md-1'>{<TransObject translationKey={'mining.support.from.experienced'}/>}</p>
                                </Col>
                                <Col xs={{ size:6, offset:0 }} md={{ size:4, offset:2 }}  className='d-flex flex-column  justify-content-md-start justify-content-xs-center align-content-center pr-xs-0 pr-md-2 p-xs-1 p-1'>
                                    <img
                                        src={'https://storage.googleapis.com/ssc-assets/Developers_Pack/content/power.jpg'}
                                        width={'100%'}
                                        alt={'power'}
                                    />
                                    <p className='main-selection-till mt-xs-3 mt-md-1'>{<TransObject translationKey={'mining.significant.computing.power'}/>}</p>
                                </Col>
                                <Col xs={{ size:6, offset:0 }} md={{ size:4, offset:2 }}  className='d-flex flex-column  justify-content-md-start justify-content-xs-center align-content-center pr-xs-0 pr-md-2 p-xs-1 p-1 mb-xs-2 mb-md-0'>
                                    <img
                                        src={'https://storage.googleapis.com/ssc-assets/Developers_Pack/content/mining.jpg'}
                                        width={'100%'}
                                        alt={'mining'}
                                    />
                                    <p className='main-selection-till mt-xs-3 mt-md-1'>{<TransObject translationKey={'mining.opportunity.to.earn'}/>}</p>
                                </Col>
                            </Row>
                        </Col>
                    </Row>
                </Col>
            </Row>
        );
    }
}
