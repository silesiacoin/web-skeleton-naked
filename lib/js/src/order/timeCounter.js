export default function timeCounter(seconds) {
    Object.defineProperty(this, "seconds", {
        get: function () {
            if (seconds > 0) {
                return seconds;
            }

            throw new Error('Illegal seconds offset');
        },
        enumerable: false,
        configurable: false
    });

    this.compose = function () {
        if (this.seconds) {
            return {
                created: new Date()
            }
        }
    }
}