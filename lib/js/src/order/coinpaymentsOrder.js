export default function coinPaymentsOrder(amount, currency) {
    const supportedCurrencies = ["btc", "eth", "ltc"];

    Object.defineProperty(this, "amount", {
        get: function () {
            if (amount > 0) {
                return amount;
            }

            throw new Error('Amount is not proper. You have done something wrong.');
        },
        enumerable: false,
        configurable: false
    });

    Object.defineProperty(this, "curency", {
        get: function () {
            if (typeof currency === "string" && supportedCurrencies.includes(currency.toLowerCase())) {
                return currency.toLowerCase();
            }

            throw new Error('Curency is not supported.');
        },
        enumerable: false,
        configurable: false
    });

    this.compose = function () {
        if (this.amount && this.curency) {
            return {
                amount: this.amount,
                currency: this.currency
            }
        }
    }
}