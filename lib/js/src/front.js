import React, {Component} from 'react'
import Home from './containers/home'
import AffiliateNetwork from './containers/affiliateNetwork'
import MiningCrew from './containers/miningCrew'
import BusinessEarning from './containers/businessEarning'
import Dashboard from './containers/dashboard';
import authenticatorService from './_redux/services/authenticatorService';

function chooseComponent() {
    let elementToRender = null;

    switch (window.location.pathname) {
        case '/':
            elementToRender = (<Home/>);
            break;
        case '/affiliatenetwork':
            elementToRender = (<AffiliateNetwork/>);
            break;
        case '/miners':
            elementToRender = (<MiningCrew/>);
            break;
        case '/business':
            elementToRender = (<BusinessEarning/>);
            break;
        case '/dashboard':
            elementToRender = (<Dashboard mainPageActive={true}/>);
            break;
    }

    return elementToRender
}

export default class Front extends Component {
    constructor(props) {
        super(props);

        if (document.getElementById('angband') === null) {
            throw new Error('angband not found');
        }

        this.authenticationService = new authenticatorService();
    }

    // Collect api calls to one root element
    async componentDidMount() {
        await this.authenticationService.fetchLoginUrls();
    }

    render() {
        return (<div>{chooseComponent()}</div>)
    }
}
