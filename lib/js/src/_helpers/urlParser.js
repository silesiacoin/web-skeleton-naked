import Validator from "./validator";
import ValidationError from "../error/validationError";

export default class ApiUrlParser {
    static parseGetUrl(apiUrl, path, argument = '', protocol = 'https') {
        let toValidate = [apiUrl, path];

        ApiUrlParser.pushValidations(argument, protocol, toValidate);
        Validator.validateStrings(1, ...toValidate);

        if (argument.length < 1) {
            return `${protocol}://${apiUrl}/${path}`;
        }

        return `${protocol}://${apiUrl}/${path}/${argument}`;
    }

    static parseApiUrl(apiUrl, path, protocol = 'https') {
        Validator.validateStrings(1, apiUrl, path, protocol);
        protocol = protocol.replace(':', '');

        return `${protocol}://${apiUrl}/${path}`;
    }

    static parseApiUrlWithDefaultProtocol(apiUrl, path) {
        return ApiUrlParser.parseApiUrl(apiUrl, path, window.location.protocol);
    }

    static parseGetParameters(endpoint, getParameters) {
        Validator.validateString(endpoint);

        return `${endpoint}?${ApiUrlParser.encodeData(getParameters)}`;
    }

    static encodeData(data) {
        if ('undefined' === typeof data) {
            throw new ValidationError('No data provided to encode');
        }

        return Object.keys(data).map(function(key) {
            return [key, data[key]].map(encodeURIComponent).join("=");
        }).join("&");
    }

    static pushValidations(argument, protocol, toValidate = []) {
        Validator.validateString(argument, 0);
        Validator.validateString(protocol, 3);

        if (argument.len > 0) {
            toValidate.push(argument);
        }

        if (protocol.length < 0) {
            toValidate.push(protocol);
        }
    }

    static createFromWindowLocation() {
        if (
            'undefined' === typeof window
            || 'undefined' === typeof window.location.hostname
            || 'undefined' === typeof window.location.protocol
        ) {
            throw new ValidationError('Error with your browser. Please consider using firefox');
        }

        if ('8080' === window.location.port) {
            return `api.${window.location.hostname}:${window.location.port}`;
        }

        return `api.${window.location.hostname}`;
    }
}
