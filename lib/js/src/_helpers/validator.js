import ValidationError from "../error/validationError";

export default class Validator {
    static validateString(string, len = 1) {
        if ('number' !== typeof len) {
            throw new ValidationError('len is not a number');
        }

        if (null === string || 'string' != typeof string || string.length < len) {
            throw new ValidationError('Invalid string');
        }
    }

    static validateStrings(len, ...strings) {
        for (let string of strings) {
            Validator.validateString(string, len);
        }
    }

    static validateArray(arr, len = 1) {
        if (!Array.isArray(arr) || arr.length < len) {
            throw new ValidationError('Invalid array');
        }
    }
}
