import validator from './_helpers/validator'
import ApiUrlParser from './_helpers/urlParser'
import TranslationStruct from './struct/translationStruct'
import TranslationClientError from './error/translationClientError'
import TranslationCollectionStruct from "./struct/translationCollectionStruct";

export default class Translator {
    constructor (apiUrl) {
        validator.validateString(apiUrl);
        this.apiUrl = apiUrl;
    }

    /**
     * You should not use this method. Use TranslationCollection instead.
     * @Deprecated
     * @param transKey
     * @param locale
     * @param protocol
     * @returns {Promise<TranslationStruct>}
     */
    async trans (transKey, locale = 'en', protocol = 'https') {
        validator.validateString(transKey);

        const translationUrl = ApiUrlParser.parseGetUrl(
            this.apiUrl,
            `i18n/${locale}`,
            transKey,
            protocol
        );

        const response = await fetch(translationUrl);

        if (response.status === 200) {
            return TranslationStruct.fromJson(response.body);
        }

        throw new TranslationClientError(response);
    }

    async transCollection (transCollection, locale = 'en', protocol = 'https') {
        protocol = protocol.replace(':', '');

        const endpoint = ApiUrlParser.parseGetUrl(
            this.apiUrl,
            `i18n/${locale}`,
            '',
            protocol
        );

        const fullRequestUrl = ApiUrlParser.parseGetParameters(
            endpoint,
            transCollection
        );

        let response = await fetch(fullRequestUrl, { mode: "cors" });

        if (response.status === 200) {
            response = await response.json();
            const body = await JSON.stringify(response);

            return new TranslationCollectionStruct(body);
        }

        throw new TranslationClientError(response.body);
    }
}
