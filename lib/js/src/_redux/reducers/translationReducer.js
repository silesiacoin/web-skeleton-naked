import { translationConstants } from '../constants/translationConstants';
import ValidationError from '../../error/validationError';
import { translationActions } from '../actions/translationActions';

export function translation(state = {}, action) {
    if ('undefined' === typeof action || 'undefined' === typeof action.type) {
        throw new ValidationError('action or actionType cannot be undefined');
    }

    switch (action.type) {
        case translationConstants.INITIAL: {
            const newState = Object.assign({}, state);

            if (!Array.isArray(newState.toTranslate)) {
                newState.toTranslate = [];
            }

            newState.toTranslate.push(action.translationKey);
            newState.toTranslate = [...new Set(newState.toTranslate)];

            return Object.assign({ type: action.type }, newState);
        }
        case translationConstants.FETCHING_COLLECTION:
            return Object.assign({ type: action.type }, state);
        case translationConstants.SUCCESS_COLLECTION:
            return Object.assign({ type: action.type }, action.payload);
        case translationConstants.EMIT_TRANSLATION:
            return Object.assign({ type: action.type }, action.translation);
        case translationConstants.END_EMISSION:
            return { type: action.type };
        case translationConstants.ERROR:
            return translationActions.error(action.error);
        default:
            return state;
    }
}
