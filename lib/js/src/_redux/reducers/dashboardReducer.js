import { dashboardConstants } from '../constants/dashboardConstants';

export function dashboard(state = {}, action) {
    switch(action.type) {
        case dashboardConstants.SHOW_PROFILE:
            return Object.assign(state, { type: action.type });
        case dashboardConstants.SHOW_MAIN_SECTIONS:
            return Object.assign(state, { type: action.type });
        case dashboardConstants.SHOW_YOUR_WALLETS:
            return Object.assign(state, { type: action.type });
        case dashboardConstants.SHOW_WALLET:
            return Object.assign(state, { type: action.type, walletSelector: action.walletSelector });
        case dashboardConstants.WITH_WALLETS:
            return Object.assign(state, { type: action.type });
        case dashboardConstants.SHOW_CREATE_WALLET_MODAL:
            return Object.assign(state, { type: action.type });
        case dashboardConstants.CREATE_WALLET:
            return Object.assign(state, { type: action.type });
        default:
            return state;
    }
}
