import { combineReducers } from 'redux';
import { translation } from './translationReducer';
import { reducer as reduxFormReducer } from 'redux-form';

const rootReducer = combineReducers({
  form: reduxFormReducer, // mounted under "form"
  translation
});

export default rootReducer;
