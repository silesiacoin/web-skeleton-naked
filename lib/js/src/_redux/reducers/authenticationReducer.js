import {authenticationConstants, authenticationResponseConstants} from '../constants/authenticationConstants';
import ValidationError from '../../error/validationError';
import RegistrationUser from '../../struct/registrationUser';
import AuthenticatorError from '../../error/authenticatorError';

export function authentication(state = {}, action) {
    if ('undefined' === typeof action || 'undefined' === typeof action.type) {
        throw new ValidationError('action or actionType cannot be undefined');
    }

    switch (action.type) {
        case authenticationConstants.WALLET_OBJECT: {
            return Object.assign(state, { type: action.type, wallet: action.wallet });
        }
        case authenticationConstants.AUTHENTICATION_MODAL: {
            return Object.assign(state, { type: action.type });
        }
        case authenticationConstants.INITIAL: {
            if ('undefined' === typeof window.csrf) {
                throw new ValidationError('CSRF token not found');
            }

            return Object.assign({}, state, { csrf: window.csrf, type: authenticationConstants.INITIAL });
        }
        case authenticationConstants.FETCHED_AUTH_LINKS: {
            if ('undefined' === typeof action.loginUrls) {
                throw new ValidationError('login urls are not present in response');
            }

            return Object.assign({}, state, {
                type: authenticationConstants.FETCHED_AUTH_LINKS,
                loginUrls: action.loginUrls
            });
        }
        case authenticationConstants.REGISTER_USER_VALIDATE_FRONT: {
            if ('undefined' === typeof action.form) {
                throw new ValidationError('Form not found within action');
            }

            if ('undefined' === typeof state.csrf) {
                throw new ValidationError('CSRF not found within state');
            }

            const form = action.form;
            const { email, password1, password2, checked } = form;
            const user = new RegistrationUser(email, password1, password2, checked);
            const newState = {
                user: user,
                type: user.errorsOccured ?
                    authenticationConstants.REGISTER_USER_VALIDATE_FRONT_INVVALID
                    : authenticationConstants.REGISTER_USER_VALIDATE_FRONT_VALID
            };

            return Object.assign({}, state, newState);
        }
        case authenticationConstants.REGISTER_USER_SEND: {
            if ('undefined' === typeof state.user || false === state.user instanceof RegistrationUser) {
                throw new ValidationError('User in state not found');
            }

            if ('undefined' === typeof state.csrf) {
                throw new ValidationError('CSRF not found within state');
            }

            return Object.assign({}, state, {
                user: state.user,
                type: authenticationConstants.REGISTER_USER_SEND
            });
        }
        case authenticationConstants.REGISTER_USER_RESPONSE: {
            if ('undefined' === typeof action.response) {
                throw new AuthenticatorError('Could not reduce action without response');
            }

            const { status } = action.response;
            let authenticationType = authenticationConstants.REGISTER_USER_BACKEND_ERROR;

            if (status === authenticationResponseConstants.REGISTRATION_SUCCESS_CODE) {
                authenticationType = authenticationConstants.REGISTER_USER_SUCCESS
            }

            return Object.assign({}, state, {
                type: authenticationType
            });
        }
    }
}
