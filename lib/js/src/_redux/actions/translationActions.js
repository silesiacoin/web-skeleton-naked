import { translationConstants } from '../constants/translationConstants';

export const translationActions = {
    error,
    fetchCollection,
    handleTranslations,
    emitTranslation,
    endEmission,
    addToTranslationQueue
};

function addToTranslationQueue(translationKey) {
    return {
        type: translationConstants.INITIAL,
        translationKey: translationKey
    }
}

function error(error) {
    // Add error handling for RollBar
    if (error instanceof Error) {
        return { type: translationConstants.ERROR, error: error };
    }

    throw new Error(error);
}

function fetchCollection() {
    return {
        type: translationConstants.FETCHING_COLLECTION
    }
}

function emitTranslation(translation) {
    return {
        type: translationConstants.EMIT_TRANSLATION,
        translation: translation
    }
}

function endEmission() {
    return {
        type: translationConstants.END_EMISSION
    }
}

function handleTranslations(translationResponse) {
    return {
        type: translationConstants.SUCCESS_COLLECTION,
        payload: translationResponse
    };
}
