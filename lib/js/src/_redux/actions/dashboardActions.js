import { dashboardConstants } from '../constants/dashboardConstants';

export const dashboardActions = {
    showProfile,
    showMainSections,
    showYourWallets,
    showWallet,
    showCreateWalletModal,
    createWallet
};

function showProfile() {
    return {
        type: dashboardConstants.SHOW_PROFILE
    }
}

function showMainSections() {
    return {
        type: dashboardConstants.SHOW_MAIN_SECTIONS
    }
}

function showYourWallets() {
    return {
        type: dashboardConstants.SHOW_YOUR_WALLETS
    }
}

function showWallet(walletSelector) {
    return {
        type: dashboardConstants.SHOW_WALLET,
        walletSelector: walletSelector,
    }
}

function showCreateWalletModal() {
    return {
        type: dashboardConstants.SHOW_CREATE_WALLET_MODAL
    }
}

function createWallet() {
    return {
        type: dashboardConstants.CREATE_WALLET
    }
}
