import { authenticationConstants } from '../constants/authenticationConstants';

export const authenticationActions = {
    initial,
    validateRegisterForm,
    handleRegisterResponse,
    walletObject,
    registerLoginUrlsInStore,
    authenticationModal
};

function authenticationModal() {
    return {
        type: authenticationConstants.AUTHENTICATION_MODAL
    }
}

function initial() {
    return {
        type: authenticationConstants.INITIAL,
        csrf: window.csrf
    }
}

function validateRegisterForm(form) {
    return {
        type: authenticationConstants.REGISTER_USER_VALIDATE_FRONT,
        form: form
    }
}

function handleRegisterResponse(response) {
    return {
        type: authenticationConstants.REGISTER_USER_RESPONSE,
        response: response
    }
}

function registerLoginUrlsInStore(loginUrls) {
    return {
        type: authenticationConstants.FETCHED_AUTH_LINKS,
        loginUrls: loginUrls
    }
}

function walletObject(wallet) {
    return {
        type: authenticationConstants.WALLET_OBJECT,
        wallet: wallet,
    }
}
