import { createStore } from 'redux';
import rootReducer from './../reducers/index';
import ValidationError from "../../error/validationError";

export default class defaultStore {
    constructor(toRegister = rootReducer) {
        if ('function' !== typeof toRegister) {
            throw new ValidationError('cannot register non-function type');
        }

        this.store = toRegister;
    }

    registerStore(initialState = {}) {
        return createStore(this.store, initialState);
    }
}
