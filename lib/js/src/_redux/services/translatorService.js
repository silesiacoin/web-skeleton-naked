import Translator from '../../translator';
import ApiUrlParser from './../../_helpers/urlParser';
import TranslationClientError from '../../error/translationClientError';
import ValidationError from '../../error/validationError';
import TransObject from '../../components/generic/transObject';
import TranslationStruct from '../../struct/translationStruct';
import CookieService from './cookieService';
import { cookieConstrants } from '../constants/cookieConstants';
import defaultStore from '../store/defaultStore';
import { translationActions } from '../actions/translationActions';
import { translation } from '../reducers/translationReducer';
import { translationConstants } from '../constants/translationConstants';
import CookieStruct from "../../struct/cookieStruct";

export default class TranslatorService extends Translator {
    constructor() {
        super(ApiUrlParser.createFromWindowLocation());

        let cookieLocale = '';

        this.protocol = window.location.protocol;
        this.locale = 'en';

        try {
            cookieLocale = CookieService.getCookie(cookieConstrants.LOCALE_LANG);
        } catch (error) {
            this.locale = 'en';
            CookieService.setCookie(new CookieStruct(cookieConstrants.LOCALE_LANG, this.locale));
        }

        if (cookieLocale.length > 1) {
            this.locale = cookieLocale;
        }

        this.handleSubscription();
    }

    handleSubscription() {
        this.store = TranslatorService.getTranslationStore();
        this.unsubscribe = this.store.subscribe(async () => {
            const state = this.store.getState();

            switch (state.type) {
                case translationConstants.SUCCESS_COLLECTION:
                    return await this.emitTranslations();
            }
        });
    }

    async translateWholeWindow() {
        this.store.dispatch(translationActions.fetchCollection());

        const translationCollectionStructResponse = await this.transCollection(
            TranslatorService.parseSimpleTransactionArray(),
            this.locale,
            this.protocol
        );

        this.store.dispatch(translationActions.handleTranslations(translationCollectionStructResponse));
    }

    async emitTranslations() {
        const state = this.store.getState();

        if (Object.keys(state.translations).length < 1) {
            throw new ValidationError('there are no translations');
        }

        for (const translationStruct of Object.values(state.translations)) {
            this.store.dispatch(translationActions.emitTranslation(translationStruct));
        }

        this.store.dispatch(translationActions.endEmission());
    }

    static parseSimpleTransactionArray() {
        const store = TranslatorService.getTranslationStore();
        const state = store.getState();

        if (!Array.isArray(state.toTranslate)) {
            throw new TranslationClientError('There are no elements to translate');
        }

        return state.toTranslate;
    }

    static validateTranslationObjects(toTranslate, translatedStruct) {
        if (false === toTranslate instanceof TransObject) {
            throw new ValidationError('Object provided for translation is not instance of TransObj');
        }

        if (false === translatedStruct instanceof TranslationStruct) {
            throw new ValidationError('Translated object is not instance of translatedStruct');
        }

        if (toTranslate.state.translationKey !== translatedStruct.translationKey) {
            const toTranslateKey = toTranslate.state.translationKey;
            const translationKeyResponse = translatedStruct.translationKey;

            throw new ValidationError(
                `Keys does not match in translation: ${toTranslateKey} !== ${translationKeyResponse}`
            );
        }
    }

    static getTranslationStore() {
        if ('undefined' === typeof window) {
            throw new ValidationError('Check your browser. Window is undefined');
        }

        if ('undefined' === typeof window.translationStore) {
            window.translationStore = (new defaultStore(translation)).registerStore();
        }

        return window.translationStore;
    }
}
