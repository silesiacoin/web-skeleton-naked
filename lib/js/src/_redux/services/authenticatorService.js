import ApiUrlParser from './../../_helpers/urlParser';
import ValidationError from '../../error/validationError';
import defaultStore from '../store/defaultStore';
import { authentication } from '../reducers/authenticationReducer';
import { authenticationConstants } from '../constants/authenticationConstants';
import { authenticationActions } from '../actions/authenticationActions';
import authenticatorError from "../../error/authenticatorError";
import Validator from '../../_helpers/validator';
import RegistrationUser from '../../struct/registrationUser';

export default class AuthenticatorService {
    constructor() {
        this.url = ApiUrlParser.createFromWindowLocation();
        this.handleSubscription();
    }

    handleSubscription() {
        this.store = AuthenticatorService.getAuthenticatorStore();
        this.store.dispatch(authenticationActions.initial());
        this.unsubscribe = this.store.subscribe(async () => {
            const state = this.store.getState();

            if (authenticationConstants.REGISTER_USER_SEND === state.type) {
                return await this.registerUser();
            }

            if (authenticationConstants.INITIAL === state.type) {
                return await this.fetchLoginUrls();
            }
        });
    }

    componentWillUnmount() {
        if ('function' === typeof this.unsubscribe) {
            this.unsubscribe();
        }
    }

    async fetchLoginUrls() {
        const url = ApiUrlParser.parseApiUrlWithDefaultProtocol(this.url, 'v1/auth');
        const response = await fetch(url);
        const jsonResponse = await response.json();
        this.store.dispatch(authenticationActions.registerLoginUrlsInStore(jsonResponse));
    }

    async registerUser() {
        const state = this.store.getState();
        const { user, csrf, type } = state;

        if (authenticationConstants.REGISTER_USER_SEND !== type) {
            throw new authenticatorError('Invalid state, could not register user');
        }

        Validator.validateString(csrf);

        if (!(user instanceof RegistrationUser)) {
            throw new authenticatorError('User passed to payload not instance of RegistrationUser');
        }

        const response = await this.sendRegisterPayload(user, csrf);
        this.store.dispatch(authenticationActions.handleRegisterResponse(response));
    }

    async sendRegisterPayload(user, csrf) {
        const registerApiUrl = ApiUrlParser.parseApiUrl(this.url, 'v1/users');
        const { email, password, errorsOccured } = user;
        const { password1 } = password;
        return await fetch(
            registerApiUrl,
            {
                method: 'PUT',
                headers: {
                    'Content-Type': 'application/json',
                    'Access-Control-Allow-Origin': `${this.url}`
                },
                mode: 'cors',
                body: JSON.stringify({
                    email: email.email,
                    plainPassword: password1,
                    userName: email.email,
                    errorsOccured: errorsOccured,
                    authenticate: csrf
                })
            }
        );
    }

    static getAuthenticatorStore() {
        if ('undefined' === typeof window) {
            throw new ValidationError('Check your browser. Window is undefined');
        }

        if ('undefined' === typeof window.authenticatorStore) {
            window.authenticatorStore = (new defaultStore(authentication)).registerStore();
        }

        return window.authenticatorStore;
    }
}
