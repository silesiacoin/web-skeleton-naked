import CookieStruct from '../../struct/cookieStruct';
import ValidationError from '../../error/validationError';
import Validator from '../../_helpers/validator';

export default class CookieService {
    static setCookie(cookieStruct) {
        if (false === cookieStruct instanceof CookieStruct) {
            throw new ValidationError('argument provided is not instance of CookieStruct');
        }

        document.cookie = `${cookieStruct.key}=${cookieStruct.value};path=/`;
    }

    static getCookie(cname) {
        Validator.validateString(cname);

        const name = `${cname}=`;
        const ca = document.cookie.split(';');

        for(let i = 0; i < ca.length; i++) {
            let c = ca[i];
            while (c.charAt(0) === ' ') {
                c = c.substring(1);
            }
            if (c.indexOf(name) === 0) {
                return c.substring(name.length, c.length);
            }
        }

        return '';
    }
}
