import Translator from '../../translator';
import ApiUrlParser from '../../_helpers/urlParser';

export default class Web3Service extends Translator {
    static async getBalanceSsc(address) {
        const apiUrl = ApiUrlParser.createFromWindowLocation();

        const balanceResponse = await fetch(ApiUrlParser.parseApiUrlWithDefaultProtocol(apiUrl, 'v1/ssc/balance/' + address), {
            method: 'GET'
        });

        const { balance } = await balanceResponse.json();

        return balance;
    }
}
