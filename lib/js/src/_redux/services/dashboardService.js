import Translator from '../../translator';
import ApiUrlParser from './../../_helpers/urlParser';
import ValidationError from '../../error/validationError';
import defaultStore from '../store/defaultStore';
import { dashboard } from '../reducers/dashboardReducer';

export default class DashboardService extends Translator {
    constructor() {
        super(ApiUrlParser.createFromWindowLocation());
    }

    static getDashboardStore() {
        if ('undefined' === typeof window) {
            throw new ValidationError('Check your browser. Window is undefined');
        }

        if ('undefined' === typeof window.dashboardStore) {
            window.dashboardStore = (new defaultStore(dashboard)).registerStore();
        }

        return window.dashboardStore;
    }
}
