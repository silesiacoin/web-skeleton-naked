export const translationConstants = {
    INITIAL: 'initial',
    SUCCESS_SINGLE: 'translation-single-success',
    SUCCESS_COLLECTION: 'translation-collection-success',
    EMIT_TRANSLATION: 'single-emitted-translation',
    END_EMISSION: 'emission-ended',
    FETCHING_SINGLE: 'translation-fetching-single',
    FETCHING_COLLECTION: 'translation-fetching-collection',
    ERROR: 'translation-error'
};
