export const authenticationConstants = {
    INITIAL: 'initial',
    FETCHED_AUTH_LINKS: 'fetch-auth-links',
    REGISTER_USER_VALIDATE_FRONT: 'register-user-validate-front',
    REGISTER_USER_VALIDATE_FRONT_VALID: 'register-user-validate-front-valid',
    REGISTER_USER_VALIDATE_FRONT_INVVALID: 'register-user-validate-front-invalid',
    REGISTER_USER_SEND: 'register-user-send',
    REGISTER_USER_RESPONSE: 'register-user-response',
    REGISTER_USER_SUCCESS: 'register-user-success',
    REGISTER_USER_BACKEND_ERROR: 'register-user-backend-error',
    WALLET_OBJECT: 'wallet-object',
    AUTHENTICATION_MODAL: 'authentication_modal'
};

export const authenticationResponseConstants = {
    REGISTRATION_SUCCESS_CODE: 201,
    REGISTRATION_ERROR_CODE: 500,
};
