export const dashboardConstants = {
    SHOW_PROFILE: 'show-profile',
    SHOW_MAIN_SECTIONS: 'show-main-sections',
    SHOW_YOUR_WALLETS: 'show-your-wallets',
    SHOW_WALLET: 'show-wallet',
    WITH_WALLETS: 'with-wallets',
    SHOW_CREATE_WALLET_MODAL: 'show-create-wallet-modal',
    CREATE_WALLET: 'create-wallet'
};
