import Validator from '../_helpers/validator';

export default class CookieStruct {
    constructor(key, value) {
        Validator.validateStrings(1, key, value);
        this.key = key;
        this.value = value;
    }
}
