import Email from './email';
import RegistrationPassword from './registrationPassword';
import EmailError from '../error/emailError';
import PasswordError from '../error/passwordError';

export default class RegistrationUser {
    constructor(email, password1, password2, checked) {
        this.guard(email, password1, password2, checked)
    }

    guard(email, password1, password2, checked) {
        this.errors = {};
        this.errorsOccured = false;

        try {
            this.email = new Email(email);
        } catch (error) {
            if (error instanceof EmailError) {
                this.errors.email = error.message;
            }
        }

        try {
            this.password = new RegistrationPassword(password1, password2);
        } catch (error) {
            if (error instanceof PasswordError) {
                this.errors.password = error.message;
            }
        }

        if ('on' !== checked) {
            this.errors.checked = true;
        }

        if (Object.keys(this.errors).length > 0) {
            this.errorsOccured = true;
        }
    }

    toJson() {
        return JSON.stringify({
            firstName: this.firstName,
            lastName: this.lastName,
            email: this.email.toString(),
            password: this.password.toString()
        });
    }
}