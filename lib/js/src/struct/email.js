import Validator from '../_helpers/validator';
import EmailError, { emailErrors } from '../error/emailError';

export default class Email {
    constructor(email) {
        try {
            Validator.validateString(email, 5);
        } catch (error) {
            throw new EmailError(emailErrors.INVALID_EMAIL);
        }

        if (!/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(email)) {
            throw new EmailError(emailErrors.INVALID_EMAIL);
        }

        this.email = email;
    }

    toString() {
        return this.email;
    }
}