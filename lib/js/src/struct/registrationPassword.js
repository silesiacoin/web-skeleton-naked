import Validator from '../_helpers/validator';
import PasswordError, { passwordErrors } from '../error/passwordError';

export default class RegistrationPassword {
    constructor(password1, password2) {
        try {
            Validator.validateStrings(1, password1, password2);
        } catch (error) {
            throw new PasswordError(passwordErrors.INVALID_PASSWORD);
        }

        this.password1 = password1;
        this.password2 = password2;
        this.validate();
    }

    validate() {
        if (this.password1 !== this.password2) {
            throw new PasswordError(passwordErrors.DOES_NOT_MATCH);
        }

        const regex = /^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$%^&*-]).{9,}$/;
        const valid = regex.test(this.password1);

        if (valid) {
            return;
        }

        if (this.password1.length < 8) {
            throw new PasswordError(passwordErrors.INVALID_LENGTH);
        }

        if (!/^(?=.*?[A-Z])/.test(this.password1)) {
            throw new PasswordError(passwordErrors.NO_CAPITAL_LETTER);
        }

        if (!/(?=.*?[a-z])/.test(this.password1)) {
            throw new PasswordError(passwordErrors.NO_LOWERCASE_LETTER);
        }

        if (!/(?=.*?[0-9])/.test(this.password1)) {
            throw new PasswordError(passwordErrors.NO_DIGIT);
        }

        if (!/(?=.*?[#?!@$%^&*-])/.test(this.password1)) {
            throw new PasswordError(passwordErrors.NO_SPECIAL_SIGN);
        }
    }

    toString() {
        return this.password1;
    }
}