import Validator from "../_helpers/validator";
import ValidationError from "../error/validationError";

export default class TranslationStruct {
    constructor(locale, translationKey, translation) {
        Validator.validateStrings(1, locale, translationKey, translation);
        this.locale = locale;
        this.translationKey = translationKey;
        this.translation = translation;
    }

    static fromJson(jsonString) {
        Validator.validateString(jsonString);
        const decodedObj = JSON.parse(jsonString);

        return TranslationStruct.fromObj(decodedObj);
    }

    static fromObj(obj) {
        TranslationStruct.validateCreation(obj);

        return new TranslationStruct(
            obj.locale,
            obj.translationKey,
            obj.translation
        );
    }

    static validateCreation(obj) {
        if ('undefined' === typeof obj
            || 'undefined' === typeof obj.locale
            || 'undefined' === typeof obj.translationKey
            || 'undefined' === typeof obj.translation
        ) {
            throw new ValidationError('Invalid obj');
        }

        Validator.validateStrings(
            2,
            obj.locale,
            obj.translationKey,
            obj.translation
        );
    }
}
