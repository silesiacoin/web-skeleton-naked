import Validator from "../_helpers/validator";
import ValidationError from "../error/validationError";
import TranslationStruct from "./translationStruct";

export default class TranslationCollectionStruct {
    constructor(jsonTranslations) {
        this.createTranslationCollection(jsonTranslations);
    }

    createTranslationCollection(jsonTranslations) {
        Validator.validateStrings(2, jsonTranslations);
        const decodedTranslations = JSON.parse(jsonTranslations);
        Validator.validateArray(Object.keys(decodedTranslations));
        this.translations = {};

        Object.keys(decodedTranslations).map((key) => {
            if ('undefined' !== typeof this.translations[key]) {
                throw new ValidationError('Duplicated object translation key');
            }

            this.translations[key] = TranslationStruct.fromObj(decodedTranslations[key]);
        });
    }

    toSimpleArray() {
        const simpleArray = [];

        Object.keys(this.translations).map((key) => {
            simpleArray.push(this.translations[key].translationKey);
        });

        return simpleArray;
    }
}
