import * as React from 'react';
import TranslatorService from '../_redux/services/translatorService';

export default class TranslatedComponent extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            loading: true
        };

        this.translationService = new TranslatorService();
        TranslatedComponent.clearWindowSwitchParams();
    }

    static clearWindowSwitchParams() {
        if ('undefined' === typeof window.location.search || 'undefined' === typeof window.history) {
            return;
        }

        let params = new URLSearchParams(window.location.search);
        params.delete('ssc_language');
        window.history.replaceState(null, '', '?' + params + window.location.hash);
    }

    componentDidMount() {
        this.translationService.translateWholeWindow()
            .then(() => {
                this.setState({ loading: false })
            }).catch((e) => {
                throw e;
        });
    }
}
