import React from 'react';
import { Container , Col, Row } from 'reactstrap';
import Wallets from '../components/dashboard/wallets';
import Profile from '../components/dashboard/profile';
import FullProfile from '../components/dashboard/fullProfile';
import YourWallets from '../components/dashboard/yourWallets';
import Ssc from '../components/dashboard/ssc';
import Mining from '../components/dashboard/mining';
import Wallet from '../components/dashboard/wallet';
import Navbar from '../components/dashboard/navbar';
import Account from '../components/dashboard/account';
import CreateWallet from '../components/modals/dashboard/createWallet';
import Search from '../components/dashboard/search';
import TranslatedComponent from './translatedComponent';
import DashboardService from '../_redux/services/dashboardService';
import { dashboardConstants } from '../_redux/constants/dashboardConstants';
import CookieService from '../_redux/services/cookieService';
import Authenticator from '../components/modals/authenticator';
import AuthenticatorService from '../_redux/services/authenticatorService';
import { authenticationConstants } from '../_redux/constants/authenticationConstants';

export default class Dashboard extends TranslatedComponent {
    constructor(props) {
        super(props);
        this.props = props;

        if ('undefined' === typeof props) {
            this.props = {};
        }

        const enabledFeature = CookieService.getCookie('ssc_not_finished');

        this.state = {
            enabledFeature: enabledFeature.length > 0,
            mainPageActive: !!this.props.mainPageActive,
            profilePanelActive: !!this.props.profilePanelActive,
            yourWalletsActive: !!this.props.yourWalletsActive,
            walletActive: !!this.props.walletActive
        };

        this.autStore = AuthenticatorService.getAuthenticatorStore();
        this.unsubscribe = this.autStore.subscribe(() => {
            const state = this.autStore.getState();

            if (state.type === authenticationConstants.WALLET_OBJECT) {
                this.setState({
                    wallet: state.wallet
                })
            }
        });

        this.store = DashboardService.getDashboardStore();
        this.unsubscribe = this.store.subscribe(() => {
            const state = this.store.getState();
            switch (state.type) {
                case dashboardConstants.SHOW_PROFILE:
                    return(
                        this.setState({
                            mainPageActive: false,
                            profilePanelActive: true,
                            yourWalletsActive: false,
                            walletActive: false,
                        })
                    );
                case dashboardConstants.SHOW_MAIN_SECTIONS:
                    return(
                        this.setState({
                            mainPageActive: true,
                            profilePanelActive: false,
                            yourWalletsActive: false,
                            walletActive: false,
                        })
                    );
                case dashboardConstants.SHOW_YOUR_WALLETS:
                    return(
                        this.setState({
                            mainPageActive: false,
                            profilePanelActive: false,
                            yourWalletsActive: true,
                            walletActive: false,
                        })
                    );
                case dashboardConstants.SHOW_WALLET:
                    return(
                        this.setState({
                            mainPageActive: false,
                            profilePanelActive: false,
                            yourWalletsActive: false,
                            walletActive: true,
                            walletSelector: state.walletSelector,
                        })
                    );
                default:
                    return state
            }
        });
    }

    componentWillUnmount() {
        if ('function' === typeof this.unsubscribe) {
            this.unsubscribe();
        }
    }

    render() {
        return (
            <div>
                <Navbar mainPageActive={true}/>
                <Account/>
                <CreateWallet wallet={this.state.wallet}/>
                <Authenticator modal={true}/>
                <Container fluid={true} className="bgLightBackground container-bonus-left-padding pb-md-5 pt-0 pt-lg-5">
                    {this.state.enabledFeature &&
                        <Search/>
                    }
                    {this.state.mainPageActive === true &&
                        <Row className="m-0 container-pb container-padding">
                            <Col xs={'12'} sm={'12'} md={'12'} xl={'6'} lg={'6'} className={'pt-0 pt-sm-0 pt-md-0 pt-xl-0 pt-lg-0 pb-0 pb-sm-0 pb-md-3 pb-xl-0 pb-lg-0 pl-0 pl-sm-0 pl-md-0 pl-xl-3 pl-lg-3 pr-0 pr-sm-0 pr-md-0 pr-xl-3 pr-lg-3 '}>
                                <Row className={'m-0 p-0'}>
                                    <Col xs={'12'} sm={'12'} md={'12'} xl={'12'} lg={'12'} className={'p-0'}>
                                        <div className={'component-mobile'}>
                                            <Wallets wallet={this.state.wallet}/>
                                        </div>
                                        <Profile mainPageActive={this.state.mainPageActive}/>
                                    </Col>
                                </Row>
                                <Row className={'p-0 m-0'}>
                                    <Col xs={'12'} sm={'12'} md={'6'} xl={'6'} lg={'6'}>
                                        <Ssc/>
                                    </Col>
                                    <Col xs={'12'} sm={'12'} md={'6'} xl={'6'} lg={'6'}>
                                        <Mining/>
                                    </Col>
                                </Row>
                            </Col>
                            <Col xs={'12'} sm={'12'} md={'12'} xl={'6'} lg={'6'} className={'p-0'}>
                                <div className={'component-desktop'}>
                                    <Wallets wallet={this.state.wallet}/>
                                </div>
                            </Col>
                        </Row>
                    }
                    {this.state.profilePanelActive === true &&
                        <FullProfile mainPageActive={this.state.mainPageActive}/>
                    }
                    {this.state.yourWalletsActive === true &&
                        <YourWallets wallet={this.state.wallet}/>
                    }
                    {this.state.walletActive === true &&
                        <Wallet walletSelector={this.state.walletSelector}/>
                    }
                </Container>
            </div>
        );
    }
}
