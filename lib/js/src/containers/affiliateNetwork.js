import React from 'react';
import { MainSection } from '../components/affiliatenetwork/mainSection';
import { TimeRevolution } from '../components/affiliatenetwork/timeRevolution';
import { Timer } from '../components/affiliatenetwork/timer';
import { People } from '../components/affiliatenetwork/people';
import { Team } from '../components/affiliatenetwork/team';
import { Join } from '../components/affiliatenetwork/join';
import Header from '../components/header/header';
import Footer from '../components/footer/footer';
import { Container } from 'reactstrap';
import LargeInfoSection from '../components/affiliatenetwork/largeInfoSection';
import TranslatedComponent from './translatedComponent';

export default class AffiliateNetwork extends TranslatedComponent {
    render() {
        return (
            <div>
                <Header/>
                <Container fluid={true} className='bg-white container--pt'>
                    <MainSection/>
                    <TimeRevolution/>
                    <LargeInfoSection/>
                    <Timer currentDate={Date.now()}/>
                    <Team/>
                    <People/>
                    <Join/>
                </Container>
                <Footer/>
             </div>
        );
    }
}



