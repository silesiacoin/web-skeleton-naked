import React from 'react';
import Header from '../components/header/header';
import Tree from '../components/home/tree';
import Footer from '../components/footer/footer';
import { Container } from 'reactstrap';
import MainSection from '../components/home/mainSection';
import SmallInfoSection from '../components/home/smalInfoSection';
import LargeInfoSection from '../components/home/largeInfoSection';
import TranslatedComponent from './translatedComponent';
import RodoInfo from '../components/generic/rodoInfo';

export default class Home extends TranslatedComponent {
    render() {
        return (
            <div>
                <Header/>
                <Container fluid={true} className="bgBlue container--pt">
                    <MainSection/>
                    <SmallInfoSection/>
                    <LargeInfoSection/>
                    <Tree/>
                </Container>
                <Footer/>
                <RodoInfo/>
            </div>
        );
    }
}
