import React from 'react';
import Header from '../components/header/header';
import Footer from '../components/footer/footer';
import { Container } from 'reactstrap';
import LargeInfoSection from '../components/mining/LargeInfoSection';
import MiningSystemSection from '../components/mining/miningSystemSection';
import ProofOfWorkSection from '../components/mining/proofOfWorkSection';
import BlocksAreValidatedSection from '../components/mining/blocksAreValidatedSection';
import ProcessSection from '../components/mining/processSection';
import SmallInfoSection from '../components/mining/smalInfoSection';
import ValuableSection from '../components/mining/valuableSection';
import { Join } from '../components/mining/join';
import { MainSection } from '../components/mining/mainSection';
import TranslatedComponent from './translatedComponent';

export default class MiningCrew extends TranslatedComponent {
    render() {
        return (
            <div>
                <Header/>
                <Container fluid={true} className='pr-0 pl-0 container--pt'>
                    <MainSection/>
                    <MiningSystemSection/>
                    <LargeInfoSection/>
                    <ProofOfWorkSection/>
                    <BlocksAreValidatedSection/>
                    <SmallInfoSection/>
                    <ProcessSection/>
                    <ValuableSection/>
                    <Join/>
                </Container>
                <Footer/>
            </div>
        );
    }
}
