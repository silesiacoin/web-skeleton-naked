import React from 'react';
import MainSection from '../components/businessEarning/mainSection';
import HighChance from '../components/businessEarning/highChance';
import LargeInfoSection from '../components/businessEarning/largeInfoSection';
import WhyTheFeatureIsSSC from '../components/businessEarning/whyTheFeatureIsSSC';
import LookWhyCrypto from '../components/businessEarning/lookWhyCrypto';
import EthereumAddressGrowth from '../components/businessEarning/views/ethereumAdressGrowth';
import EthereumAddressGrowthChart from '../components/businessEarning/views/charts/ethereumAddressGrowthChart';
import Footer from '../components/footer/footer';
import EthereumActiveAddressesChart from '../components/businessEarning/views/charts/ethereumActiveAddressesChart';
import CryptoUserGrowthChart from '../components/businessEarning/views/charts/cryptoUserGrowthChart';
import CryptoExchangeUserGrowthChart from '../components/businessEarning/views/charts/cryptoExchangeUserGrowthChart';
import BitcoinActiveAddressesChart from '../components/businessEarning/views/charts/bitcoinActiveAddressesChart';
import CryptoExchangeVolumeChart from '../components/businessEarning/views/charts/cryptoExchangeVolumeChart';
import WebsiteGrowthChart from '../components/businessEarning/views/charts/websiteGrowthChart';
import BitcoinWalletAddressGrowthChart from '../components/businessEarning/views/charts/bitcoinWalletAddressGrowthChart';
import TranslatedComponent from './translatedComponent';
import Header from '../components/header/header';
import { Container } from 'reactstrap';
import ValuableSection from '../components/businessEarning/valuableSection';
import Join from '../components/businessEarning/join';

export default class BusinessEarning extends TranslatedComponent {
    render() {
        return (
            <div>
                <Header />
                <Container fluid={true} className='bg-white pl-0 pr-0 container--pt'>
                    <MainSection/>
                    <HighChance/>
                    <LargeInfoSection/>
                    <WhyTheFeatureIsSSC/>
                    <LookWhyCrypto/>
                    <EthereumAddressGrowth/>
                    <EthereumAddressGrowthChart/>
                    <ValuableSection/>
                    <Join/>
                    <EthereumActiveAddressesChart/>
                    <CryptoUserGrowthChart/>
                    <CryptoExchangeUserGrowthChart/>
                    <BitcoinActiveAddressesChart/>
                    <CryptoExchangeVolumeChart/>
                    <WebsiteGrowthChart/>
                    <BitcoinWalletAddressGrowthChart/>
                </Container>
				<Footer/>
            </div>
        );
    }
}
