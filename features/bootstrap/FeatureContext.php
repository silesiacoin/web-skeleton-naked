<?php
declare(strict_types=1);

use App\Subscribers\CorrelationIdSubscriber;
use Behat\Behat\Context\Context;
use Behat\Gherkin\Node\PyStringNode;
use Symfony\Component\HttpKernel\Kernel;
use Ubirak\RestApiBehatExtension\Rest\RestApiBrowser;
use Ubirak\RestApiBehatExtension\Json\JsonInspector;
use Symfony\Component\Security\Csrf\CsrfTokenManager;

class FeatureContext implements Context
{
    private $restApiBrowser;
    private $jsonInspector;
    private $kernel;

    public function __construct(
        RestApiBrowser $restApiBrowser,
        JsonInspector $jsonInspector,
        Kernel $kernel
    ) {
        $this->restApiBrowser = $restApiBrowser;
        $this->jsonInspector = $jsonInspector;
        $this->kernel = $kernel;
    }

    /**
     * @When /^(?:I )?send a ([A-Z]+) request to "([^"]+)" with json:$/
     */
    public function iSendARequestToWithJson(
        string $method,
        string $url,
        PyStringNode $json
    ) {
        $this->restApiBrowser->setRequestHeader('Content-Type', 'application/json');
        $decodedJson = json_decode((string )$json, true);
        $json = new PyStringNode([json_encode($decodedJson)], 0);

        $this->restApiBrowser->sendRequest(
            $method,
            $url,
            $json
        );
    }
}
