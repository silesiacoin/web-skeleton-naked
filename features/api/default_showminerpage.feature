@api @default_showminerpage @apiV1
Feature: Loads miner subpage
  As an api user I want to be able to load miner subpage

  @default_showminerpage @load
  Scenario: miner subpage is loaded correctly
    When I send a GET request to "/miners"
    Then the response status code should be 200


