@api @app_translation_gettranslations @apiV1
Feature: Translates components
  As an api user I want to be able to get all components translated

  @app_translation_gettranslations @correctTranslationToEnglish
  Scenario: Attempt at translating component with mining.have.experience translationKey to english
    When I send a GET request to "/i18n/en/mining.have.experience"
    Then the response status code should be 200
    And the JSON path expression locale should be equal to:
    """
      "en"
    """
    And the JSON path expression translationKey should be equal to:
    """
      "mining.have.experience"
    """
    And the JSON path expression translation should be equal to:
    """
      "Do you have experience in digging?"
    """

  @app_translation_gettranslations @correctTranslationToPolish
  Scenario: Attempt at translating component with mining.have.experience translationKey to polish
    When I send a GET request to "/i18n/pl/mining.have.experience"
    Then the response status code should be 200
    And the JSON path expression locale should be equal to:
    """
      "pl"
    """
    And the JSON path expression translationKey should be equal to:
    """
      "mining.have.experience"
    """
    And the JSON path expression translation should be equal to:
    """
      "Czy masz doświadczenie w wydobywaniu?"
    """
