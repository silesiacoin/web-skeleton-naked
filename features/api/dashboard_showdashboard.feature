@api @dashboard_showdashboard @apiV1
Feature: Loads dashboard subpage
  As an api user I want to be able to load dashboard subpage

  @dashboard_showdashboard @load
  Scenario: dashboard subpage is loaded correctly
    When I send a GET request to "/dashboard"
    Then the response status code should be 200


