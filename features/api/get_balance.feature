@api @apiGetBalance @apiV1
Feature: Gets account balance
  As an api dev user
  I want to get balance of an account
  So we should implement endpoint that can Get balance

  @api @apiGetBalance @invalidAddress
  Scenario: Attempt to run endpoint with invalid address
    When I send a GET request to "/v1/ssc/balance/dfgkjndfg" with json:
    """
    {
    }
    """
    Then the response status code should be 400
    And the response should be in JSON
    And the JSON node "error" should exist
    And the JSON node "message" should exist

  @api @apiGetBalance @validAccount
  Scenario: Attempt to run endpoint without required parameter specified in the request
    When I send a GET request to "/v1/ssc/balance/0x84Dec2f0F58aE672C84d470E430b257cc8Ddea20" with json:
    """
    {
    }
    """
    Then the response status code should be 200
    And the response should be in JSON
    And the JSON node "balance" should exist
