@api @pkgList @apiV1
Feature: Shows list of content inside package
  As an api user I want to be able to see contents of package

  @pkgList @invalidCredentials
  Scenario: Show list failure when user credentials are invalid
    When I send a GET request to "/pkg/ssc/list" with body:
    """
    {}
    """
    Then the response status code should be 400
    And the JSON node "error" should be equal to "something went wrong"
    And the JSON node "message" should exist


