@api @pkgDownload @apiV1
Feature: Downloads packages from storage
  As an api user I want to download packages

  @pkgDownload @invalidCredentials
  Scenario: Package download failure when user credentials are invalid
    When I send a GET request to "/pkg/linuxamd64/download/ssc" with body:
    """
    {}
    """
    Then the response status code should be 400
    And the JSON node "error" should be equal to "something went wrong"
    And the JSON node "message" should exist


