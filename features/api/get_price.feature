@api @getPrices @apiV1
Feature: Get rates from pro coinbase for cryptocurrencies
  As an api user I want to get rates for cryptocurrencies

  @getPrices @Success
  Scenario: Attempt to get rates from pro coinbase API.
    When I send a GET request to "/v1/get-price"
    Then the response status code should be 200
    And the response should be in JSON
    And the JSON node "response" should exist