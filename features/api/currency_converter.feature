@api @currencyConverter @apiV1
Feature: Get exchange rates for EUR-PLN from exchangeratesapi.io
  As an api user I want to get exchange rates for EUR currency to PLN currency

  @getExchangeRate @Success
  Scenario: Attempt to get rates from exchangeratesapi.io api
    When I send a GET request to "/v1/currency-converter"
    Then the response status code should be 200
    And the response should be in JSON
    And the JSON node "response" should exist
