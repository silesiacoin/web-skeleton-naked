@api @app_create_transaction @apiV1
Feature: Creates transaction
  As an api user I want to create transaction with given parameters, by calling endpoint

  @app_create_transaction @fail @uuid_unspecified
  Scenario: Attempt to run endpoint without required parameter specified in the request
    When I send a POST request to "/v1/coinpayments/basic-transaction/ltc/5"
    Then the response status code should be 500
    And the response should be in JSON
    And the JSON should be equal to:
    """
    {
      "error": "you must provide uuid"
    }
    """

  @app_create_transaction @fail @empty_json_body
  Scenario: Attempt to run endpoint, request is json but it's body is empty
    When I send a POST request to "/v1/coinpayments/basic-transaction/ltc/5" with json:
    """
    {}
    """
    Then the response status code should be 400
    And the response should be in JSON
    And the JSON should be equal to:
    """
    {
      "error": "something went wrong",
      "message": "#0 \/var\/www\/app\/vendor\/symfony\/http-kernel\/HttpKernel.php(146): App\\Controller\\Api\\CoinpaymentsController->createTransaction('ltc', '5', Object(Symfony\\Component\\HttpFoundation\\Request))\n#1 \/var\/www\/app\/vendor\/symfony\/http-kernel\/HttpKernel.php(68): Symfony\\Component\\HttpKernel\\HttpKernel->handleRaw(Object(Symfony\\Component\\HttpFoundation\\Request), 1)\n#2 \/var\/www\/app\/vendor\/symfony\/http-kernel\/Kernel.php(201): Symfony\\Component\\HttpKernel\\HttpKernel->handle(Object(Symfony\\Component\\HttpFoundation\\Request), 1, true)\n#3 \/var\/www\/app\/public\/index.php(34): Symfony\\Component\\HttpKernel\\Kernel->handle(Object(Symfony\\Component\\HttpFoundation\\Request))\n#4 {main}"
    }
    """

  @app_create_transaction @fail @no_uuid_value
  Scenario: Attempt to run endpoint with parameter key, but with value unspecified
    When I send a POST request to "/v1/coinpayments/basic-transaction/ltc/5" with json:
    """
    {
      "uuid": ""
    }
    """
    Then the response status code should be 500
    And the response should be in JSON
    And the JSON should be equal to:
    """
    {
      "error": "you must provide uuid"
    }
    """

  @app_create_transaction @fail @no_uuid_key
  Scenario: Attempt to run endpoint with parameter key unspecified and specified value
    When I send a POST request to "/v1/coinpayments/basic-transaction/ltc/5" with json:
    """
    {
      "": "dummyUuid1234"
    }
    """
    Then the response status code should be 400
    And the response should be in JSON
    And the JSON should be equal to:
    """
   {
      "error": "something went wrong",
      "message": "#0 \/var\/www\/app\/vendor\/symfony\/http-kernel\/HttpKernel.php(146): App\\Controller\\Api\\CoinpaymentsController->createTransaction('ltc', '5', Object(Symfony\\Component\\HttpFoundation\\Request))\n#1 \/var\/www\/app\/vendor\/symfony\/http-kernel\/HttpKernel.php(68): Symfony\\Component\\HttpKernel\\HttpKernel->handleRaw(Object(Symfony\\Component\\HttpFoundation\\Request), 1)\n#2 \/var\/www\/app\/vendor\/symfony\/http-kernel\/Kernel.php(201): Symfony\\Component\\HttpKernel\\HttpKernel->handle(Object(Symfony\\Component\\HttpFoundation\\Request), 1, true)\n#3 \/var\/www\/app\/public\/index.php(34): Symfony\\Component\\HttpKernel\\Kernel->handle(Object(Symfony\\Component\\HttpFoundation\\Request))\n#4 {main}"
   }
    """

  @app_create_transaction @fail @invalid_uuid_key
  Scenario: Attempt to run endpoint with parameter specified, but parameters key name is not valid
    When I send a POST request to "/v1/coinpayments/basic-transaction/ltc/5" with json:
    """
    {
      "uudi": "dummyUuid1234"
    }
    """
    Then the response status code should be 400
    And the response should be in JSON
    And the JSON should be equal to:
    """
   {
      "error": "something went wrong",
      "message": "#0 \/var\/www\/app\/vendor\/symfony\/http-kernel\/HttpKernel.php(146): App\\Controller\\Api\\CoinpaymentsController->createTransaction('ltc', '5', Object(Symfony\\Component\\HttpFoundation\\Request))\n#1 \/var\/www\/app\/vendor\/symfony\/http-kernel\/HttpKernel.php(68): Symfony\\Component\\HttpKernel\\HttpKernel->handleRaw(Object(Symfony\\Component\\HttpFoundation\\Request), 1)\n#2 \/var\/www\/app\/vendor\/symfony\/http-kernel\/Kernel.php(201): Symfony\\Component\\HttpKernel\\HttpKernel->handle(Object(Symfony\\Component\\HttpFoundation\\Request), 1, true)\n#3 \/var\/www\/app\/public\/index.php(34): Symfony\\Component\\HttpKernel\\Kernel->handle(Object(Symfony\\Component\\HttpFoundation\\Request))\n#4 {main}"
   }
    """

  @app_create_transaction @fail @uuid_key_and_value_not_provided
  Scenario: Attempt to run endpoint with parameter's key and value as empty strings
    When I send a POST request to "/v1/coinpayments/basic-transaction/ltc/5" with json:
    """
    {
      "": ""
    }
    """
    Then the response status code should be 400
    And the response should be in JSON
    And the JSON should be equal to:
    """
   {
      "error": "something went wrong",
      "message": "#0 \/var\/www\/app\/vendor\/symfony\/http-kernel\/HttpKernel.php(146): App\\Controller\\Api\\CoinpaymentsController->createTransaction('ltc', '5', Object(Symfony\\Component\\HttpFoundation\\Request))\n#1 \/var\/www\/app\/vendor\/symfony\/http-kernel\/HttpKernel.php(68): Symfony\\Component\\HttpKernel\\HttpKernel->handleRaw(Object(Symfony\\Component\\HttpFoundation\\Request), 1)\n#2 \/var\/www\/app\/vendor\/symfony\/http-kernel\/Kernel.php(201): Symfony\\Component\\HttpKernel\\HttpKernel->handle(Object(Symfony\\Component\\HttpFoundation\\Request), 1, true)\n#3 \/var\/www\/app\/public\/index.php(34): Symfony\\Component\\HttpKernel\\Kernel->handle(Object(Symfony\\Component\\HttpFoundation\\Request))\n#4 {main}"
   }
    """

  @app_create_transaction @success
  Scenario: Attempt to run endpoint with correct parameter
    When I send a POST request to "/v1/coinpayments/basic-transaction/ltc/5" with json:
    """
    {
      "uuid": "dummyUuid1234"
    }
    """
    Then the response status code should be 200
    And the response should be in JSON
    And the JSON node "response" should exist