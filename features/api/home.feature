@api @home @apiV1
Feature: Loads main page
  As an api user I want to be able to load main page

  @home @load
  Scenario: Main page is loaded correctly
    When I send a GET request to "/"
    Then the response status code should be 200


