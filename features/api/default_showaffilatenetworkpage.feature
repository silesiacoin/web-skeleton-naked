@api @default_showaffilatenetworkpage @apiV1
Feature: Loads affilatentwork subpage
  As an api user I want to be able to load affilatentwork subpage

  @default_showaffilatenetworkpage @load
  Scenario: affilatentwork subpage is loaded correctly
    When I send a GET request to "/affiliatenetwork"
    Then the response status code should be 200


