<?php
declare(strict_types=1);

namespace App\Tests\src\Subscribers;

use App\Subscribers\CookieSubscriber;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\HttpFoundation\ParameterBag;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Event\GetResponseEvent;

class CookieSubscriberTest extends WebTestCase
{
    public function testCookieDispatchCookie(): void
    {
        $newVal = 'uc3guo6n9h3a7trcr0ti2p0qv9';
        /** @noinspection PhpParamsInspection */
        $cookieSubscriber = new CookieSubscriber();
        $event = $this->createMock(GetResponseEvent::class);
        $request = $this->getMockBuilder(Request::class)->disableOriginalConstructor()->getMock();
        $headers = $this->createMock(ParameterBag::class);
        $cookies = $this->createMock(ParameterBag::class);
        $cookies->method('set')->willReturn(null);
        $request->headers = $headers;
        $request->cookies = $cookies;
        $cookies->expects($this->once())
            ->method('set');
        $event->method('isMasterRequest')->willReturn(true);
        $event->method('getRequest')->willReturn($request);
        $headers->method('get')
            ->withConsecutive(['Set-Cookie'])
            ->willReturnOnConsecutiveCalls("cookieVal=$newVal");

        /** @noinspection PhpParamsInspection */
        $cookieSubscriber->onKernelRequest($event);
    }

    public function testDispatchCookieNoMasterRequest(): void
    {
        $masterRequestMethod = 'isMasterRequest';
        $cookieSubscriber = new CookieSubscriber();
        $event = $this->createMock(GetResponseEvent::class);
        $event->method($masterRequestMethod)->willReturn(false);
        $event->expects($this->once())->method($masterRequestMethod);
        /** @noinspection PhpParamsInspection */
        $cookieSubscriber->onKernelRequest($event);
    }

    public function testDispatchCookieNoSetCookieHeader(): void
    {
        $cookieSubscriber = new CookieSubscriber();
        $event = $this->createMock(GetResponseEvent::class);
        $request = $this->getMockBuilder(Request::class)->disableOriginalConstructor()->getMock();
        $headers = $this->createMock(ParameterBag::class);
        $cookies = $this->createMock(ParameterBag::class);
        $cookies->method('set')->willReturn(null);
        $request->headers = $headers;
        $request->cookies = $cookies;
        $event->method('isMasterRequest')->willReturn(true);
        $event->method('getRequest')->willReturn($request);
        $headers->method('get')
            ->withConsecutive(['Set-Cookie'])
            ->willReturnOnConsecutiveCalls(null);
        $cookies->expects($this->never())->method('set');
        /** @noinspection PhpParamsInspection */
        $cookieSubscriber->onKernelRequest($event);
    }
}
