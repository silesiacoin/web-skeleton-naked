<?php
declare(strict_types=1);
namespace App\Tests\src\Entity;

use App\Entity\User;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class ApplicationUserTest extends WebTestCase
{
    /** @var User */
    private $user;

    public function setUp() : void
    {
        $this->user = new User();
    }

    public function testProperRoleSet()
    {
        $user = $this->user;
        $user->addNewRole('ROLE_TEST');
        $user->addNewRole('ROLE_TEST');
        $user->addNewRole('ROLE_USER');
        $roles = $user->getRoles();
        $this->assertCount(2, $roles);
        $this->assertEquals($roles[0], 'ROLE_USER');
        $this->assertEquals($roles[1], 'ROLE_TEST');
    }

    public function testUnsetRole()
    {
        $user = $this->user;
        $user->removeRole('ROLE_USER');
        $user->removeRole('ROLE_THAT_NOT_EXIST');
        $this->assertCount(0, $user->getRoles());
    }
}
