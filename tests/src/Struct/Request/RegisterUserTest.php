<?php
declare(strict_types=1);

namespace App\Tests\src\Struct\Request;

use App\Struct\Request\RegisterUser;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Csrf\CsrfTokenManagerInterface;

class RegisterUserTest extends WebTestCase
{
    /** @var \PHPUnit\Framework\MockObject\MockObject */
    private $request;

    /** @var \PHPUnit\Framework\MockObject\MockObject */
    private $csrfTokenManager;

    public function setUp(): void
    {
        $this->request = $this->createMock(Request::class);
        $this->csrfTokenManager = $this->createMock(CsrfTokenManagerInterface::class);
    }

    public function testRegisterUserInstantiate(): void
    {
        $this->setRequestMockedGetMethod(
            ['dummy@email.com', 'dummyUserEmail', 'plainPassword']
        );
        $this->csrfTokenManager->method('isTokenValid')->willReturn(true);
        /** @noinspection PhpParamsInspection */
        $registerUser = new RegisterUser($this->request, $this->csrfTokenManager);
        $this->assertInstanceOf(RegisterUser::class, $registerUser);
    }

    public function testRegisterUserInvalidEmail(): void
    {
        $this->setRequestMockedGetMethod(
            ['invalidEmail', 'dummyUserEmail', 'plainPassword']
        );
        $this->expectExceptionMessage("Email 'invalidEmail' is invalid");
        /** @noinspection PhpParamsInspection */
        new RegisterUser($this->request, $this->csrfTokenManager);
    }

    public function testRegisterUserInvalidUserEmailStruct(): void
    {
        $this->setRequestMockedGetMethod(
            [[], 'dummy', 'plainPassword']
        );
        $this->expectExceptionMessage("Email is not a string");
        /** @noinspection PhpParamsInspection */
        new RegisterUser($this->request, $this->csrfTokenManager);
    }

    public function testRegisterUserInvalidUserName(): void
    {
        $this->setRequestMockedGetMethod(
            ['valid@email.com', [], 'plainPassword']
        );
        $this->expectExceptionMessage("UserName is not a string");
        /** @noinspection PhpParamsInspection */
        new RegisterUser($this->request, $this->csrfTokenManager);
    }

    public function testRegisterUserInvalidUserNameLength(): void
    {
        $this->setRequestMockedGetMethod(
            ['valid@email.com', "ak", 'plainPassword']
        );
        $expectedLen = RegisterUser::USER_NAME_MIN_LENGTH;
        $this->expectExceptionMessage("UserName is too short, expected len: {$expectedLen}");
        /** @noinspection PhpParamsInspection */
        new RegisterUser($this->request, $this->csrfTokenManager);
    }

    public function testRegisterUserInvalidPassword(): void
    {
        $this->setRequestMockedGetMethod(
            ['valid@email.com', "dummyNickName", []]
        );
        $this->expectExceptionMessage("PlainPassword is not a string");
        /** @noinspection PhpParamsInspection */
        new RegisterUser($this->request, $this->csrfTokenManager);
    }

    public function testRegisterUserInvalidPasswordTooShort(): void
    {
        $this->setRequestMockedGetMethod(
            ['valid@email.com', "dummyNickName", 'abcdefg']
        );
        $expectedLen = RegisterUser::PASSWORD_MIN_LENGTH;
        $this->expectExceptionMessage("Plain password is too short. Expected minimum {$expectedLen}");
        /** @noinspection PhpParamsInspection */
        new RegisterUser($this->request, $this->csrfTokenManager);
    }

    public function testRegisterUserInvalidCsrfToken(): void
    {
        $this->setRequestMockedGetMethod(
            ['valid@email.com', "dummyNickName", 'abcdefghij']
        );

        $this->csrfTokenManager->method('isTokenValid')->willReturn(false);

        $this->expectExceptionMessage("Csrf token is invalid");
        /** @noinspection PhpParamsInspection */
        new RegisterUser($this->request, $this->csrfTokenManager);
    }

    private function setRequestMockedGetMethod(array $response): void
    {
        $this->assertEquals(3, count($response));
        $this->request->method('get')
            ->withConsecutive(
                [RegisterUser::EMAIL_SELECTOR],
                [RegisterUser::USER_NAME_SELECTOR],
                [RegisterUser::PASSWORD_SELECTOR]
            )
            ->willReturnOnConsecutiveCalls(...$response);
    }
}
