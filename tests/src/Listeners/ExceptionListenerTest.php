<?php
declare(strict_types=1);

namespace App\Tests\src\Listeners;

use App\Handlers\ExceptionHandler;
use App\Listeners\ExceptionListener;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Event\ExceptionEvent;

class ExceptionListenerTest extends WebTestCase
{
    /**
     * @var ExceptionHandler
     */
    private $exceptionHandlerMock;

    public function setUp(): void
    {
        $exceptionHandlerMock = $this->createMock(ExceptionHandler::class);
        $exceptionHandlerMock->method('storeException');
        $this->exceptionHandlerMock = $exceptionHandlerMock;
    }

    public function testOnKernelExceptionChangeToJson(): void
    {
        $exceptionListener = new ExceptionListener($this->exceptionHandlerMock);
        $event = $this->createMock(ExceptionEvent::class);
        $this->exceptionHandlerMock->expects($this->once())->method('storeException');
        $event->expects($this->once())->method('setResponse');
        $exception = $this->createMock(\Exception::class);
        $request = $this->createMock(Request::class);
        $request->method('getHost')->willReturn('api.silesiacoin.local');
        $event->method('getException')->willReturn($exception);
        $event->method('getRequest')->willReturn($request);
        $event->method('getException')->willReturn($exception);
        $exceptionListener->onKernelException($event);

        return;
    }

    public function testOnKernelExceptionWithoutJson(): void
    {
        $exceptionListener = new ExceptionListener($this->exceptionHandlerMock);
        $event = $this->createMock(ExceptionEvent::class);
        $this->exceptionHandlerMock->expects($this->once())->method('storeException');
        $event->expects($this->never())->method('setResponse');
        $exception = $this->createMock(\Exception::class);
        $request = $this->createMock(Request::class);
        $request->method('getHost')->willReturn('something.silesiacoin.local');
        $event->method('getException')->willReturn($exception);
        $event->method('getRequest')->willReturn($request);
        $event->method('getException')->willReturn($exception);
        $exceptionListener->onKernelException($event);
    }
}
