<?php
declare(strict_types=1);

namespace App\Handlers;

use Monolog\Handler\StreamHandler;
use Monolog\Logger;
use Rollbar\Payload\Level;
use Rollbar\Rollbar;

class ExceptionHandler
{
    protected $logger;

    protected function getLogger(): Logger
    {
        if (null === $this->logger) {
            Rollbar::init([
                'access_token' => getenv('ROLLBAR_ACCESS_TOKEN'),
                'environment' => getenv('APP_ENV')
            ]);
            $this->logger = new Logger('ExceptionHandler');
        }

        return $this->logger;
    }

    /**
     * @param $context
     * @throws \Exception
     */
    public function storeException(\Exception $context): void
    {
        $log = $this->getLogger();
        $log->pushHandler(
            new StreamHandler(
                '../var/log/exception-' . date('Y-F-j-H:i:s'),
                Logger::ERROR
            )
        );
        $log->addError($context);

        if ('prod' === getenv('APP_ENV')) {
            $this->sendRollbar($context);
        }
    }

    private function sendRollbar(\Exception $exception) : void
    {
        Rollbar::log(
            Level::ERROR,
            $exception->getMessage(),
            [
                'code' => $exception->getCode(),
                'trace' => $exception->getTraceAsString()
            ]
        );
    }
}
