<?php
declare(strict_types=1);

namespace App\Struct\Request;

use App\Exception\RegisterRequestException;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Csrf\CsrfToken;
use Symfony\Component\Security\Csrf\CsrfTokenManagerInterface;

class RegisterUser implements \JsonSerializable
{
    public const PASSWORD_MIN_LENGTH = 8;
    public const USER_NAME_MIN_LENGTH = 4;
    public const EMAIL_SELECTOR = 'email';
    public const PASSWORD_SELECTOR = 'plainPassword';
    public const USER_NAME_SELECTOR = 'userName';
    public const CSRF_REGISTER_TOKEN_ID = 'authenticate';

    /** @var string */
    private $email;

    /** @var string */
    private $userName;

    /** @var string */
    private $plainPassword;

    /** @var CsrfTokenManagerInterface */
    private $csrfTokenManager;

    /** @var Request */
    private $request;

    public function __construct(
        Request $request,
        CsrfTokenManagerInterface $csrfTokenManager
    ) {
        $this->request = $request;
        $this->csrfTokenManager = $csrfTokenManager;
        $this->guard();
    }

    private function guard(): void
    {
        $this->guardEmail();
        $this->guardUserName();
        $this->guardPlainPassword();
        $this->guardCsrf();
    }

    private function guardEmail(): void
    {
        $this->email = $this->request->get(self::EMAIL_SELECTOR);

        if (!is_string($this->email)) {
            throw new RegisterRequestException("Email is not a string");
        }

        if (!\filter_var($this->email, FILTER_VALIDATE_EMAIL)) {
            $message = sprintf("Email '%s' is invalid", $this->email);
            throw new RegisterRequestException($message);
        }
    }

    private function guardPlainPassword(): void
    {
        $this->plainPassword = $this->request->get(self::PASSWORD_SELECTOR);

        if (!is_string($this->plainPassword)) {
            throw new RegisterRequestException("PlainPassword is not a string");
        }

        $expectedLen = self::PASSWORD_MIN_LENGTH;

        if (strlen($this->plainPassword) < self::PASSWORD_MIN_LENGTH) {
            throw new RegisterRequestException("Plain password is too short. Expected minimum {$expectedLen}");
        }
    }

    private function guardUserName(): void
    {
        $this->userName = $this->request->get(self::USER_NAME_SELECTOR);

        if (!is_string($this->userName)) {
            throw new RegisterRequestException('UserName is not a string');
        }

        $expectedLen = self::USER_NAME_MIN_LENGTH;

        if (strlen($this->userName) < $expectedLen) {
            throw new RegisterRequestException("UserName is too short, expected len: {$expectedLen}");
        }
    }

    private function guardCsrf(): void
    {
        $tokenString = (string) $this->request->get(self::CSRF_REGISTER_TOKEN_ID);

        if (strlen($tokenString) > 0 && 'prod' !== getenv('APP_ENV')) {
            return;
        }

        $token = new CsrfToken(self::CSRF_REGISTER_TOKEN_ID, $tokenString);
        $isTokenValid = $this->csrfTokenManager->isTokenValid($token);

        if (!$isTokenValid) {
            throw new RegisterRequestException('Csrf token is invalid');
        }
    }

    public function jsonSerialize(): array
    {
        return [
            "email" => $this->email,
            "userName" => $this->userName,
            "plainPassword" => $this->plainPassword
        ];
    }
}
