<?php
declare(strict_types=1);

namespace App\Exception;

use App\Entity\User;
use App\Service\DynamoEventsInterface;
use Ramsey\Uuid\Uuid;
use Ramsey\Uuid\UuidInterface;

class S3ObjNotFoundException extends \RuntimeException implements DynamoEventsInterface
{
    private $userEmail;
    /**
     * @var UuidInterface
     */
    private $uuid;
    private $optionUuid;

    public function getEventName() : string
    {
        return 'Exception: File on s3 not found';
    }

    /**
     * @return UuidInterface
     * @throws \Exception
     */
    public function getUuid() : UuidInterface
    {
        if (null === $this->uuid) {
            $this->uuid = Uuid::uuid4();
        }

        return $this->uuid;
    }

    /**
     * @return \Ramsey\Uuid\UuidInterface
     * @throws \Exception
     */
    public function getOptionUuid() : UuidInterface
    {
        if (null === $this->optionUuid) {
            $this->optionUuid = Uuid::uuid4();
        }

        return $this->optionUuid;
    }

    public function assignEmail(User $user)
    {
        $this->userEmail = $user->getEmail();
    }

    public function toEmailMessageBody() : string
    {
        return sprintf(
            "user Email %s attempted invalid s3 action on: \n stack trace: \n %s",
            $this->userEmail,
            \json_encode($this)
        );
    }

    /**
     * @return array|mixed
     * @throws \Exception
     */
    public function jsonSerialize()
    {
        return [
            'self' => 'S3ObjectNotFoundException',
            'userEmail' => $this->userEmail,
            'eventName' => $this->getEventName(),
            'uuid' => $this->getUuid(),
            'optionUuid' => $this->getOptionUuid()
        ];
    }
}
