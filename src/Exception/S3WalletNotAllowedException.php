<?php
declare(strict_types=1);

namespace App\Exception;

use App\Entity\User;
use App\Service\DynamoEventsInterface;
use Ramsey\Uuid\Uuid;

class S3WalletNotAllowedException extends \RuntimeException implements DynamoEventsInterface
{
    private $userName;
    private $sscAccountAddr;
    private $uuid;
    private $optionUuid;

    public static function fromStorageAttempt(
        string $message,
        User $user,
        $sscAccountAddr
    ) : S3WalletNotAllowedException {
        $exception = new self($message);
        $exception->userName = $user->getUsername();
        $exception->sscAccountAddr = $sscAccountAddr;

        return $exception;
    }

    public function getEventName() : string
    {
        return 'Exception: Wallet is not allowed';
    }

    public function getUuid()
    {
        if (null === $this->uuid) {
            $this->uuid = Uuid::uuid4();
        }

        return $this->uuid;
    }

    public function getOptionUuid()
    {
        if (null === $this->optionUuid) {
            $this->optionUuid = Uuid::uuid4();
        }

        return $this->optionUuid;
    }

    public function toEmailMessageBody() : string
    {
        return sprintf(
            "user Name %s attempted unauthorized s3 ssc wallet download action on: \n stack trace: \n %s",
            $this->userName,
            \json_encode($this)
        );
    }

    public function jsonSerialize()
    {
        return [
            'self' => 'S3WalletNotAllowedException',
            'userName' => $this->userName,
            'sscAccountAddr' => $this->sscAccountAddr,
            'eventName' => $this->getEventName(),
            'uuid' => $this->getUuid(),
            'optionUuid' => $this->getOptionUuid()
        ];
    }
}
