<?php
declare(strict_types=1);

namespace App\Security;

use App\Entity\FbUser;
use App\Entity\GUser;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Security\Core\User\User;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\Exception\UnsupportedUserException;

class SocialMediaProvider implements SocialMediaProviderInterface
{
    private $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    public function loadUserByUsername($username)
    {
        $entityManager = $this->entityManager;
        $repository = $entityManager->getRepository('App:User');

        return $repository->findOneBy(['username' => $username]);
    }

    /**
     * @param UserInterface $user
     * @return SocialMediaProviderInterface|void
     * @throws \Symfony\Component\Security\Core\Exception\UnsupportedUserException
     */
    public function refreshUser(UserInterface $user)
    {
        throw new UnsupportedUserException('Stateless Exception');
    }

    public function supportsClass($class): bool
    {
        return User::class === $class;
    }

    /**
     * @param $accessToken string
     * @return FbUser
     */
    public function getFacebookUser($accessToken)
    {
        $entityManager = $this->entityManager;
        $repository = $entityManager->getRepository('App:FbUser');
        /** @var FbUser $user */
        $user = $repository->findOneBy(['accessToken' => $accessToken]);

        return $user;
    }

    /**
     * @param $accessToken string
     * @return GUser
     */
    public function getGoogleUser($accessToken)
    {
        $entityManager = $this->entityManager;
        $repository = $entityManager->getRepository('App:GUser');
        /** @var GUser $user */
        $user = $repository->findOneBy(['accessToken' => $accessToken]);

        return $user;
    }
}
