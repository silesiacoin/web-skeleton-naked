<?php
declare(strict_types=1);

namespace App\Security;

use App\Entity\FbUser;
use App\Entity\GUser;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Authentication\Token\AnonymousToken;
use Symfony\Component\Security\Core\Authentication\Token\PreAuthenticatedToken;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Exception\CustomUserMessageAuthenticationException;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\User\UserProviderInterface;
use Symfony\Component\Security\Http\Authentication\SimplePreAuthenticatorInterface;

class SocialMediaAuthenticator implements SimplePreAuthenticatorInterface
{
    /**
     * @param TokenInterface $token
     * @param UserProviderInterface $userProvider
     * @param $providerKey
     * @return PreAuthenticatedToken | AnonymousToken
     * @throws \Symfony\Component\Security\Core\Exception\CustomUserMessageAuthenticationException
     * @throws \InvalidArgumentException
     */
    public function authenticateToken(TokenInterface $token, UserProviderInterface $userProvider, $providerKey)
    {
        if (!$token->getCredentials()) {
            return new AnonymousToken(getenv('APP_SECRET'), 'anon.');
        }
        $userProvider = $this->checkInstance($userProvider);
        $fbUser = $userProvider->getFacebookUser($token->getCredentials());
        $gUser = $userProvider->getGoogleUser($token->getCredentials());
        $this->guardUser($fbUser, $gUser, $token);
        $user = $this->getUserChoice($fbUser, $gUser, $userProvider);

        return new PreAuthenticatedToken($user, $token->getCredentials(), $providerKey, $user->getRoles());
    }

    public function supportsToken(TokenInterface $token, $providerKey)
    {
        return $token instanceof PreAuthenticatedToken && $token->getProviderKey() === $providerKey;
    }

    /**
     * @param Request $request
     * @param $providerKey
     * @return PreAuthenticatedToken
     * @throws \Symfony\Component\Security\Core\Exception\AccessDeniedException
     * @throws \Symfony\Component\Security\Core\Exception\BadCredentialsException
     */
    public function createToken(Request $request, $providerKey)
    {
        $session = $request->getSession();
        $fbToken = $session->get('fb_token');
        $token = $session->get('google_token');

        if ($fbToken) {
            $token = $fbToken;
        }

        return new PreAuthenticatedToken(
            'anon.',
            $token,
            $providerKey
        );
    }

    private function checkInstance($userProvider): SocialMediaProvider
    {
        if (!$userProvider instanceof SocialMediaProvider) {
            throw new \InvalidArgumentException(
                sprintf(
                    'The user provider must be an instance of SocialMediaProvider (%s was given).',
                    \get_class($userProvider)
                )
            );
        }

        return $userProvider;
    }

    private function guardUser($fbUser, $gUser, TokenInterface $token): void
    {
        if (!$fbUser && !$gUser) {
            throw new CustomUserMessageAuthenticationException(
                sprintf('Facebook access token "%s" does not exist.', $token->getCredentials())
            );
        }
    }

    private function getUserChoice($fbUser, $gUser, UserProviderInterface $userProvider): UserInterface
    {
        /** @var GUser $userChoice */
        $userChoice = $gUser;

        if ($fbUser) {
            /** @var FbUser $userChoice */
            $userChoice = $fbUser;
        }

        $user = $userProvider->loadUserByUsername($userChoice->getMainUserName());

        if (!$user) {
            throw new CustomUserMessageAuthenticationException(
                sprintf('User not found "%s".', $userChoice->getName())
            );
        }

        return $user;
    }
}
