<?php
declare(strict_types=1);

namespace App\Entity;

use App\Service\DynamoEventsInterface;
use Ramsey\Uuid\Uuid;

class CoinPaymentsOrder implements DynamoEventsInterface
{
    private $amount;
    private $txnId;
    private $adress;
    private $confirmsNeeded;
    private $timeOut;
    private $statusUrl;
    private $qrCodeUrl;
    private $uuid;
    private $optionUuid;

    /**
     * @return mixed
     */
    public function getAmount()
    {
        return $this->amount;
    }

    /**
     * @return mixed
     */
    public function getTxnId()
    {
        return $this->txnId;
    }

    /**
     * @return mixed
     */
    public function getAdress()
    {
        return $this->adress;
    }

    /**
     * @return mixed
     */
    public function getConfirmsNeeded()
    {
        return $this->confirmsNeeded;
    }

    /**
     * @return mixed
     */
    public function getTimeOut()
    {
        return $this->timeOut;
    }

    /**
     * @return mixed
     */
    public function getStatusUrl()
    {
        return $this->statusUrl;
    }

    /**
     * @return mixed
     */
    public function getQrCodeUrl()
    {
        return $this->qrCodeUrl;
    }

    /**
     * @return mixed
     */
    public function getUuid()
    {
        return $this->uuid;
    }

    /**
     * @param $transaction
     * @param $uuid
     * @return CoinPaymentsOrder
     * @throws \Exception
     */
    public static function fromCoinpaymentsTransaction($transaction, $uuid) : CoinPaymentsOrder
    {
        $order = new self;
        $order->amount = $transaction['amount'];
        $order->txnId = $transaction['txn_id'];
        $order->adress = $transaction['address'];
        $order->confirmsNeeded = $transaction['confirms_needed'];
        $order->timeOut = $transaction['timeout'];
        $order->statusUrl = $transaction['status_url'];
        $order->qrCodeUrl = $transaction['qrcode_url'];
        $order->optionUuid = $uuid;
        $order->uuid = Uuid::uuid4()->toString();

        return $order;
    }

    public static function fromDynamoFetch(string $jsonEvent)
    {
        $cpOrder = new self;
        $decoded = json_decode($jsonEvent);
        $keys = get_object_vars($cpOrder);

        foreach ($decoded as $key => $value) {
            if (array_key_exists($key, $keys)) {
                $cpOrder->$key = $value;
            }
        }

        return $cpOrder;
    }

    /**
     * Specify data which should be serialized to JSON
     * @link http://php.net/manual/en/jsonserializable.jsonserialize.php
     * @return mixed data which can be serialized by <b>json_encode</b>,
     * which is a value of any type other than a resource.
     * @since 5.4.0
     */
    public function jsonSerialize()
    {
        return [
            'self' => 'coinPaymentsOrder',
            'amount' => $this->amount,
            'txnId' => $this->txnId,
            'adress' => $this->adress,
            'confirmsNeeded' => $this->confirmsNeeded,
            'timeOut' => $this->timeOut,
            'statusUrl' => $this->statusUrl,
            'qrCodeUrl' => $this->qrCodeUrl,
            'uuid' => $this->uuid,
            'optionUuid' => $this->optionUuid
        ];
    }

    public function toEmailMessageBody() :string
    {
        return "Yo, <br/> We had recieved new Coinpayments order. <br/> UUID: $this->uuid. <br/> Amount: $this->amount 
        <br/>txnId: $this->txnId <br/>ConfirmsNeeded $this->confirmsNeeded <br/>TimeOut: $this->timeOut <br/>
        StatusUrl $this->statusUrl <br/>QrCodeUrl $this->qrCodeUrl <br/> OptionUuid $this->optionUuid";
    }

    public function getEventName(): string
    {
        return 'New coinpayments order';
    }

    public function getOptionUuid()
    {
        return $this->optionUuid;
    }
}
