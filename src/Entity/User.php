<?php
declare(strict_types=1);

namespace App\Entity;

use App\Entity\SilesiaCoin\RefCode;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Table(name="app_users")
 * @ORM\Entity()
 * @UniqueEntity(fields="email", message="Email already taken")
 * @UniqueEntity(fields="username", message="Username already taken")
 * @UniqueEntity(fields="apiToken", message="Exact api token does not match")
 */
class User extends AbstractApplicationUser implements UserInterface, \Serializable
{
    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(type="string", length=255, unique=true)
     * @Assert\NotBlank()
     */
    protected $username;

    /**
     * @ORM\Column(type="string", length=255, unique=true)
     *@Assert\NotBlank()
     * @Assert\Email()
     */
    protected $email;

    /**
     * @ORM\ManyToOne(targetEntity="User")
     * @ORM\JoinColumn(onDelete="CASCADE")
     */
    protected $belongsTo;

    /**
     * @ORM\Column(name="api_token", type="string", length=30, unique=true, nullable=true)
     */
    protected $apiToken;

    /**
     * @ORM\Column(type="array", nullable=true)
     */
    protected $roles = array();

    /**
     * @var RefCode
     * @ORM\Column(type="string", nullable=true)
     */
    private $refCode;

    /**
     *@Assert\NotBlank()
     * @Assert\Length(max=4096)
     */
    private $plainPassword;
    /**
     * @ORM\Column(type="string", length=64)
     */
    private $password;

    /**
     * @throws \Exception
     */
    public function getRefCode()
    {
        if (null === $this->refCode) {
            $this->refCode = new RefCode();
        }

        return $this->refCode;
    }

    /**
     * User constructor.
     * @throws \Exception
     */
    public function __construct()
    {
        parent::__construct();
        $this->getRefCode();
    }

    public static function withFbRoles(User $user)
    {
        $user->addNewRole('ROLE_FACEBOOK');
        $user->addNewRole('ROLE_CONFIRMED');
        $user->addNewRole('ROLE_USER');

        return $user;
    }

    public function getSalt()
    {
        return null;
    }

    public function getPassword()
    {
        return $this->password;
    }

    public function eraseCredentials()
    {
    }

    /** @see \Serializable::serialize() */
    public function serialize()
    {
        return serialize(array(
            $this->id,
            $this->username,
            $this->password,
        ));
    }

    /** @see \Serializable::unserialize() */
    public function unserialize($serialized)
    {
        list (
            $this->id,
            $this->username,
            $this->password,
            ) = unserialize($serialized);
    }

    /**
     * @return mixed
     */
    public function getPlainPassword()
    {
        return $this->plainPassword;
    }

    /**
     * @param mixed $plainPassword
     */
    public function changePlainPassword($plainPassword)
    {
        $this->plainPassword = $plainPassword;
    }

    /**
     * @param string $password
     */
    public function changePassword(string $password)
    {
        $this->password = $password;
    }
}
