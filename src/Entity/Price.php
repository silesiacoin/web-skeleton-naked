<?php
declare(strict_types=1);
namespace App\Entity;

class Price extends AbstractPrice
{
    public static function pln(float $amount) : Price
    {
        return new self($amount);
    }

    public static function usd(float $amount) : Price
    {
        return new self($amount, 'USD');
    }

    public static function eur(float $amount) : Price
    {
        return new self($amount, 'EUR');
    }

    public static function eth(float $amount) : Price
    {
        return new self($amount, 'ETH');
    }

    public static function btc(float $amount) : Price
    {
        return new self($amount, 'BTC');
    }

    public static function ltc(float $amount) : Price
    {
        return new self($amount, 'LTC');
    }

    public static function ssc(float $amount) : Price
    {
        return new self($amount, 'SSC');
    }
}
