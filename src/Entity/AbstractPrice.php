<?php
declare(strict_types=1);
namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Ramsey\Uuid\Uuid;
use Ramsey\Uuid\UuidInterface;

/** @ORM\Embeddable() */
abstract class AbstractPrice implements \JsonSerializable
{
    /**
     * @var UuidInterface
     */
    private $uuid;

    /**
     * @ORM\Column(type="float")
     * @var float
     */
    private $amount;
    /**
     * @ORM\Column(type="string")
     * @var string
     */
    private $symbol;

    /**
     * AbstractCurrency constructor.
     * @param float $amount
     * @param string $symbol
     * @throws \Exception
     */
    public function __construct(float $amount, string $symbol = 'PLN')
    {
        $this->uuid = Uuid::uuid4();
        $this->amount = $amount;
        $this->symbol = $symbol;
    }

    public function __toString()
    {
        return strtolower(sprintf('%s %s', $this->getAmount(), $this->getSymbol()));
    }

    public function getAmount() : float
    {
        return $this->amount;
    }

    public function getSymbol() : string
    {
        return strtolower($this->symbol);
    }

    public function addPrice(AbstractPrice $price) : void
    {
        if ($this->getSymbol() !== $price->getSymbol()) {
            throw new \RuntimeException('These are different currencies, could not add');
        }

        $this->amount += $price->getAmount();
    }

    /**
     * @return UuidInterface
     */
    public function getUuid(): UuidInterface
    {
        return $this->uuid;
    }

    public function jsonSerialize()
    {
        return [
            'amount' => $this->getAmount(),
            'symbol' => $this->getSymbol(),
            'stringPrice' => $this->__toString(),
            'uuid' => $this->uuid
        ];
    }
}
