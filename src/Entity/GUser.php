<?php
declare(strict_types=1);

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * Class Guser
 * @package App\Entity
 * @ORM\Table("google_user")
 * @ORM\Entity(repositoryClass="App\Repository\GoogleUserRepository")
 * @UniqueEntity(fields="email", message="Email already taken")
 * @UniqueEntity(fields="id", message="Id from facebook already present")
 * @UniqueEntity(fields="mainUserName", message="Main user is already integrated with facebook")
 */
class GUser
{
    /**
     * @ORM\Column(type="string")
     * @var $name string
     */
    private $name;

    /**
     * @ORM\Column(type="string")
     * @var $email string
     */
    private $email;

    /**
     * @ORM\Column(type="integer", unique=true)
     * @ORM\Id()
     * @var $id string
     */
    private $id;

    /**
     * @ORM\Column(type="string", unique=true)
     * @var $accessToken string
     */
    private $accessToken;

    /**
     * @ORM\Column(type="string", nullable=true)
     * @var $mainUserName string
     */
    private $mainUserName;

    /**
     * @return string
     */
    public function getEmail(): string
    {
        return $this->email;
    }

    /**
     * @param string $email
     */
    public function changeEmail(string $email): void
    {
        $this->email = $email;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function changeName(string $name): void
    {
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getId(): string
    {
        return $this->id;
    }

    /**
     * @param string $id
     */
    public function changeId(string $id): void
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getAccessToken(): string
    {
        return $this->accessToken;
    }

    /**
     * @param string $accessToken
     */
    public function changeAccessToken(string $accessToken): void
    {
        $this->accessToken = $accessToken;
    }

    /**
     * @return string
     */
    public function getMainUserName(): string
    {
        return $this->mainUserName;
    }

    public function changeMainUserName($mainUserName): void
    {
        $this->mainUserName = $mainUserName;
    }
}
