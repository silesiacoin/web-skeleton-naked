<?php
declare(strict_types=1);
namespace App\Entity;

abstract class AbstractApplicationUser
{
    protected $id;
    protected $username;
    protected $email;
    protected $belongsTo;
    protected $apiToken;
    protected $roles = array();

    /**
     * User constructor.
     * @throws \Exception
     */
    public function __construct()
    {
        $this->addNewRole('ROLE_USER');
    }

    public function getUsername() : string
    {
        if (!\is_string($this->username)) {
            $this->username = '';
        }

        return (string) $this->username;
    }

    /**
     * @return mixed
     */
    public function getRoles() : array
    {
        $roles = $this->roles;
        $unique = array_unique($roles);

        return array_values($unique);
    }

    public function addNewRole($role)
    {
        $roles = $this->roles;

        if (!\in_array($role, $roles, true)) {
            $this->roles[] = $role;
        }
    }

    public function removeRole($role)
    {
        $roles = $this->roles;
        $key = array_search($role, $roles, true);

        if ($key !== false) {
            unset($roles[$key]);
        }

        $this->roles = array_values($roles);
    }

    public function getApiToken() : string
    {
        return (string) $this->apiToken;
    }

    public function changeApiToken(string $apiToken)
    {
        $this->apiToken = $apiToken;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    public function getEmail() : string
    {
        return (string) $this->email;
    }

    public function changeEmail(string $email)
    {
        $this->email = $email;
    }

    /**
     * @param mixed $username
     */
    public function changeUsername(string $username)
    {
        $this->username = $username;
    }

    public function changeBelongsTo($belongsTo)
    {
        if ($belongsTo instanceof AbstractApplicationUser) {
            $this->belongsTo = $belongsTo;
        }
    }
}
