<?php
declare(strict_types=1);

namespace App\Entity\SilesiaCoin;

use App\Exception\PackageNotAllowed;

class PackageKey
{
    private const ALLOWED_PACKAGES = ['ssc', 'puppeth', 'mkalloc', 'abomination'];
    private const ALLOWED_BUILDS = ['linuxAmd64', 'macosx', 'windows64', 'windows32', 'alpine'];

    private $pkgName;
    private $pkgBuild;
    private $pkgVersion;

    public function __construct(string $key)
    {
        $this->verifyKey($key);
    }

    public function sayPkgName() : string
    {
        return (string) $this->pkgName;
    }

    public function sayPkgBuild() : string
    {
        return (string) $this->pkgBuild;
    }

    public function sayPkgVersion() : string
    {
        return (string) $this->pkgVersion;
    }

    public function sayIntPkgVersion() : int
    {
        return (int) (10000 * str_replace('v', '', $this->sayPkgVersion()));
    }

    public function sayPkgUrl() : string
    {
        return sprintf(
            '%s/%s/%s/%s',
            $this->sayPkgName(),
            $this->sayPkgVersion(),
            $this->sayPkgBuild(),
            $this->sayPkgName()
        );
    }

    public function __toString()
    {
        return $this->sayPkgUrl();
    }

    private function verifyKey(string $key) : void
    {
        $keyArr = explode('/', $key, 4);

        if (count($keyArr) < 4) {
            throw new \RuntimeException(sprintf("PKG %s is invalid \n", $key));
        }

        if (!\in_array($keyArr[0], self::ALLOWED_PACKAGES, true)) {
            throw new PackageNotAllowed(sprintf("PKG %s not supported \n", $keyArr[0]));
        }

        if (!\in_array($keyArr[2], self::ALLOWED_BUILDS, true)) {
            throw new \RuntimeException(sprintf("PKG BUILD %s not supported \n", $keyArr[2]));
        }

        $this->pkgName = $keyArr[0];
        $this->pkgVersion = $keyArr[1];
        $this->pkgBuild = $keyArr[2];
    }
}
