<?php
declare(strict_types=1);

namespace App\Entity\SilesiaCoin;

class BinaryPackage implements \JsonSerializable
{
    private const S3_PREFIX_URL = 'https://s3-eu-west-1.amazonaws.com/silesiacoin/';
    private const ICONS = [
        'linux' => 'fab fa-linux',
        'alpine' => 'fab fa-linux',
        'mac' => 'fab fa-apple',
        'windows' => 'fab fa-windows'
    ];

    /** @var PackageKey */
    private $key;
    private $lastModified;
    private $icon;

    public function __construct(PackageKey $key, string $lastModified)
    {
        $this->key = $key;
        $this->lastModified = $lastModified;
        $this->resolveIcon();
    }

    public static function fromS3List(array $objList): BinaryPackage
    {
        if (!isset($objList['Key'], $objList['LastModified'])) {
            throw new \RuntimeException('Invalid objList');
        }

        return new self(new PackageKey($objList['Key']), (string) $objList['LastModified']);
    }

    public function sayKey() : PackageKey
    {
        return $this->key;
    }

    public function sayLastModified() : string
    {
        return $this->lastModified;
    }

    public function sayDownloadUrl() : string
    {
        return self::S3_PREFIX_URL . $this->sayKey()->sayPkgUrl();
    }

    public function sayIcon() : string
    {
        return $this->icon;
    }

    public function sayIntPkgVersion() : int
    {
        return $this->sayKey()->sayIntPkgVersion();
    }

    public function sayPkgBuild() : string
    {
        $key = $this->sayKey();

        return $key->sayPkgBuild();
    }

    private function resolveIcon() : void
    {
        $this->icon = 'fas fa-download';

        foreach (self::ICONS as $key => $value) {
            if (strpos($this->key->sayPkgBuild(), $key) > -1) {
                $this->icon = $value;
            }
        }
    }

    public function jsonSerialize()
    {
        return [
           'downloadUrl' => $this->sayDownloadUrl(),
           'modified' => $this->sayLastModified(),
           'version' => $this->sayIntPkgVersion(),
           'icon' => $this->sayIcon()
        ];
    }
}
