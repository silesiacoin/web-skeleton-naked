<?php
declare(strict_types=1);
namespace App\Entity\SilesiaCoin;

use App\Entity\User;
use Ramsey\Uuid\Uuid;
use Ramsey\Uuid\UuidFactory;

class RefCode
{
    private $code;

    /**
     * RefCode constructor.
     * @throws \Exception
     */
    public function __construct()
    {
        $this->code = $this->generateCode();
    }

    public function __toString()
    {
        return $this->getCode();
    }

    public function getCode() : string
    {
        return $this->code;
    }

    /**
     * @return bool|string
     * @throws \Exception
     */
    private function generateCode() : string
    {
        $userSalt = Uuid::uuid4()->toString();

        return substr($userSalt, 0, 12);
    }
}
