<?php
declare(strict_types=1);

namespace App\Entity\SilesiaCoin;

use App\Entity\PresaleOrder;

class PreWallet implements \JsonSerializable
{
    private $uuid;
    private $amount;
    private $crypto;
    private $serviceProvider;
    private $jsonString;

    public function __construct(PresaleOrder $order)
    {
        $this->amount = $order->getAmount();
        $this->uuid = $order->getUuid();
        $this->crypto = $order->getCrypto();
        $this->serviceProvider = $order->getServiceProvider();
        $this->jsonString = json_encode($this);
    }

    public static function fromPresaleOrder(PresaleOrder $order)
    {
        $wallet = new self($order);

        $wallet->amount = $order->getAmount();
        $wallet->uuid = $wallet->getUuid();
        $wallet->crypto = $order->getCrypto();
        $wallet->serviceProvider = $order->getServiceProvider();
        $wallet->jsonString = json_encode($wallet);
    }

    /**
     * @return string
     */
    public function getJsonString(): string
    {
        return $this->jsonString;
    }

    /**
     * @param string $jsonString
     */
    public function changeJsonString(string $jsonString): void
    {
        $this->jsonString = $jsonString;
    }

    /**
     * @return mixed
     */
    public function getUuid()
    {
        return $this->uuid;
    }

    /**
     * @param mixed $uuid
     */
    public function changeUuid($uuid): void
    {
        $this->uuid = $uuid;
    }

    /**
     * @return mixed
     */
    public function getAmount()
    {
        return $this->amount;
    }

    /**
     * @param mixed $amount
     */
    public function changeAmount($amount): void
    {
        $this->amount = $amount;
    }

    /**
     * @return mixed
     */
    public function getCrypto()
    {
        return $this->crypto;
    }

    /**
     * @param mixed $crypto
     */
    public function changeCrypto($crypto): void
    {
        $this->crypto = $crypto;
    }

    /**
     * @return mixed
     */
    public function getServiceProvider()
    {
        return $this->serviceProvider;
    }

    /**
     * @param mixed $serviceProvider
     */
    public function changeServiceProvider($serviceProvider): void
    {
        $this->serviceProvider = $serviceProvider;
    }

    public function jsonSerialize()
    {
        return [
            'uuid' => $this->uuid,
            'amount' => $this->amount,
            'crypto' => $this->crypto,
            'serviceProvider' => $this->serviceProvider,
        ];
    }
}
