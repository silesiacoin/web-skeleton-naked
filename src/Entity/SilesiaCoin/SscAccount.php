<?php
declare(strict_types=1);

namespace App\Entity\SilesiaCoin;

class SscAccount
{
    private $address;

    /**
     * @var float
     */
    private $amount;

    /**
     * @var string
     */
    private $crypto;

    public function __construct($adress)
    {
        $this->address = $adress;
        $this->crypto = 'ssc';
    }

    public static function withRandomAddress() : SscAccount
    {
        $hex = '0x';
        $hex .= implode(
            array_map(
                function () {
                    return dechex(random_int(0, 15));
                },
                array_fill(0, 40, null)
            )
        );

        return new self($hex);
    }

    /**
     * @return mixed
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * @param mixed $adress
     */
    public function changeAddress($adress) : void
    {
        $this->address = (string) $adress;
    }

    /**
     * @return mixed
     */
    public function getAmount() : float
    {
        return $this->amount;
    }

    /**
     * @param mixed $amount
     */
    public function changeAmount($amount) : void
    {
        $this->amount = (float) $amount;
    }

    public function getHexAddr()
    {
        $hexAddr = $this->getAddress();

        if (strpos($hexAddr, 'UTC') > -1 && \strlen($hexAddr) === 77) {
            $hexAddr = substr($hexAddr, 37, 40);
        }

        if (strpos($hexAddr, '0x') === false) {
            $hexAddr = sprintf('0x%s', $hexAddr);
        }

        if (\strlen($hexAddr) !== 42) {
            throw new \RuntimeException('Invalid account address');
        }

        return $hexAddr;
    }

    public function getCrypto() : string
    {
        return $this->crypto;
    }
}
