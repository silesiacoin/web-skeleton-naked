<?php

namespace App\Service;

use Ramsey\Uuid\Uuid;

class LeadVerifier
{
    private $dynamoClient;
    private const TABLE_NAME = 'dummy-table';

    /**
     * LeadVerifier constructor.
     * @throws \InvalidArgumentException
     */
    public function __construct()
    {
        $this->dynamoClient = CryptoBotDynamoIrelandClient::defaultDynamoClient();
    }

    /**
     * @param $email
     * @param $ipAddr
     * @return string
     * @throws \RuntimeException
     */
    public function leadSubscriber($email, $ipAddr): string
    {
        $this->storeEventTemp($email, $ipAddr);

        if ($this->emailVerifier($email) && $this->putEmailOntoDynamo($email)) {
            return "$email was stored into database";
        }
        throw new \RuntimeException('Invalid email');
    }

    /**
     * @param $email
     * @return \Aws\Result
     * @throws \RuntimeException
     */
    public function putEmailOntoDynamo($email) : ?\Aws\Result
    {
        $uuid = Uuid::uuid5(Uuid::NAMESPACE_DNS, $email);
        $date = date('Y-m-d H:i:s');
        try {
            return $this->dynamoClient->putItem([
                'TableName' => self::TABLE_NAME,
                'Item' => [
                    'uuid' => ['S' => $uuid],
                    'name' => ['S' => $email],
                    'date' => ['S' => $date],
                    'host' => ['S' => gethostname()]
                ],
            ]);
        } catch (\Exception $exception) {
            throw new \RuntimeException($exception->getMessage());
        }
    }

    /**
     * @param $event
     * @param $ipAddr
     * @return \Aws\Result
     * @throws \RuntimeException
     */
    public function storeEventTemp($event, $ipAddr): \Aws\Result
    {
        try {
            return $this->dynamoClient->putItem([
                'TableName' => 'temp',
                'Item' => [
                    'uuid' => ['S' => Uuid::uuid1(Uuid::UUID_TYPE_HASH_SHA1)],
                    'date' => ['S' => date('Y-m-d H:i:s')],
                    'host' => ['S' => gethostname()],
                    'event' => ['S' => $event],
                    'client' => ['S' => $ipAddr]
                ]
            ]);
        } catch (\Exception $exception) {
            throw new \RuntimeException($exception->getMessage());
        }
    }

    public function fetchEmailFromDynamo($email) : \Aws\Result
    {
        return $this->dynamoClient->scan([
            'TableName' => self::TABLE_NAME,
            'ProjectionExpression' => '#uuid, #email, #interacion',
            'FilterExpression' => 'attribute_exists(#email)',
            'ExpressionAttributeNames'=> [
                '#uuid' => 'uuid',
                '#email' => $email,
                '#interaction' => 'interaction'
            ],
        ]);
    }

    public function emailVerifier($email): bool
    {
        if (filter_var($email, FILTER_VALIDATE_EMAIL)) {
            return $email;
        }

        return false;
    }
}
