<?php

namespace App\Service;

use Blockchain\Blockchain;

class BlockchainSdkClient
{
    /**
     * @var Blockchain
     */
    private $blockchain;

    private function configure()
    {
        return [
            'xpub' => getenv('BLOCKCHAIN_XPUB'),
            'key' => getenv('BLOCKCHAIN_KEY')
        ];
    }

    private function getBlockchain()
    {
        if (!isset($this->blokchain)) {
            $this->blockchain = new Blockchain();
        }

        return $this->blockchain;
    }

    public function testME()
    {
        return $this->blockchain->get('/q/getdifficulty');
    }
}
