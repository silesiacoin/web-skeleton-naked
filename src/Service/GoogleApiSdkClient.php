<?php
declare(strict_types=1);

namespace App\Service;

use Doctrine\ORM\EntityManagerInterface;

class GoogleApiSdkClient
{
    /**
     * @var \Google_Client
     */
    private $google;

    public function __construct()
    {
        $this->google = $this->configure($this->getGoogleSdk());
    }

    public function getLoginUrl() : string
    {
        return $this->google->createAuthUrl();
    }

    /**
     * @param $code string
     * @return array|false
     * @throws \InvalidArgumentException
     * @throws \RuntimeException
     * @throws \Symfony\Component\Security\Core\Exception\AuthenticationException
     * @throws \LogicException
     */
    public function getUserFromCode($code)
    {
        return $this->getVerifiedUserToken($code);
    }

    protected function __clone()
    {
    }

    private function getGoogleSdk() : \Google_Client
    {
        $domain = getenv('DOMAIN');
        $port = $_SERVER['SERVER_PORT'];

        if ("80" !== $port) {
            $domain = $domain . ":$port";
        }

        $serverProtocol = isset($_SERVER['HTTPS']) ? 'https://' : 'http://';

        if (null === $this->google) {
            $this->google = new \Google_Client([
                'client_id' => getenv('GOOGLE_CLIENT_ID'),
                'client_secret' => getenv('GOOGLE_CLIENT_SECRET'),
                'redirect_uri' => $serverProtocol . 'secure.' . $domain . '/google/login',
                'application_name' => 'Silesiacoin'
            ]);
        }

        return $this->google;
    }

    private function configure(\Google_Client $googleClient) : \Google_Client
    {
        $googleClient->setAccessType('offline');
        $googleClient->setIncludeGrantedScopes(true);
        $googleClient->addScope(\Google_Service_Plus::USERINFO_EMAIL);
        $googleClient->addScope(\Google_Service_Plus::USERINFO_PROFILE);

        return $googleClient;
    }

    /**
     * @param $code
     * @return array|false
     * @throws \LogicException
     * @throws \InvalidArgumentException
     * @throws \RuntimeException
     */
    private function getVerifiedUserToken($code)
    {
        $token = $this->getPreparedToken();

        if ('silesiacoin.local' !== getenv('DOMAIN')) {
            $token = $this->google->fetchAccessTokenWithAuthCode($code);

            if (isset($token['error'])) {
                throw new \RuntimeException('Google Exception: ' . $token['error_description']);
            }
        }

        $client = clone $this->google;
        $client->setAccessToken($token);
        $verified = $client->verifyIdToken();
        $verified['access_token'] = $token['access_token'];

        return $verified;
    }

    private function getPreparedToken(): array
    {
        return [
            'access_token' => 'ya29.Glt5BR4TC71QAUIkfmC7b6xWtKlejLETB7SCO_imbyRImqbamWk5Y6D837bC_hOiySmPp95S9NcrgKzTMYCXuny6lHen1_tJsUKKl2oSKgxceWp5Cav3KwZNeLiM',
            'token_type' => 'Bearer',
            'expires_in' => 3600,
            'id_token' => 'eyJhbGciOiJSUzI1NiIsImtpZCI6ImFjMmI2M2ZhZWZjZjgzNjJmNGM1MjhlN2M3ODQzMzg3OTM4NzAxNmIifQ.eyJhenAiOiI4NTg5NzEzNzg2MC1wdWo5aGg2ZW5iY3U1NDNqcDYybmh2aWwydTZoZmtmZS5hcHBzLmdvb2dsZXVzZXJjb250ZW50LmNvbSIsImF1ZCI6Ijg1ODk3MTM3ODYwLXB1ajloaDZlbmJjdTU0M2pwNjJuaHZpbDJ1Nmhma2ZlLmFwcHMuZ29vZ2xldXNlcmNvbnRlbnQuY29tIiwic3ViIjoiMTAxNjUwODk5Nzc4MzAyNDA2MTc2IiwiZW1haWwiOiJ0dWJic3llZEBnbWFpbC5jb20iLCJlbWFpbF92ZXJpZmllZCI6dHJ1ZSwiYXRfaGFzaCI6ImpVM2hnUEJCeW9DX2ZoUnltS1RGWXciLCJleHAiOjE1MjA2MDQ2NzAsImlzcyI6Imh0dHBzOi8vYWNjb3VudHMuZ29vZ2xlLmNvbSIsImlhdCI6MTUyMDYwMTA3MCwibmFtZSI6IkVkIFR1YmJzIiwicGljdHVyZSI6Imh0dHBzOi8vbGg0Lmdvb2dsZXVzZXJjb250ZW50LmNvbS8tN0NFSXg1YWpydlkvQUFBQUFBQUFBQUkvQUFBQUFBQUFBQUEvQUdpNGdmd0dXc0xBbVAtQkJ5b3ZaSGJFOExfM05uV2hSZy9zOTYtYy9waG90by5qcGciLCJnaXZlbl9uYW1lIjoiRWQiLCJmYW1pbHlfbmFtZSI6IlR1YmJzIiwibG9jYWxlIjoicGwifQ.kel36DizU9bh3l1vLU42tCDuE6w_7ycJ5PTBbIXd77WGHJTkIbfpYOrbr681MV4tl6-Snz0qpKEgd4bNAZxRo9GRVBgljzC6Y1nDJSDCPDEVfdBVNRCxqLa_VOvLbAsMs7DzJPad0UjzpIuc86lMsvyRkKid1PTTGQFYZXpE4kcoKCLKXeCp910A1TlFiN4Ch9-83GongcMXVGXPtugaVyKDSsE-YdglOJuSgkH_oewdHuXl8B5ImGru4pCro8TKeKYiegvxt66eAraMGIaxQnVd7f6XICx_o0yv07FaK1QcKZn2CY4mHw-L5V6J7nNizDy8BvjizoATYpwmgL1GKA',
            'created' => 1520601070,
        ];
    }

    private function getPreparedClientId()
    {
        return '85897137860-puj9hh6enbcu543jp62nhvil2u6hfkfe.apps.googleusercontent.com';
    }
}
