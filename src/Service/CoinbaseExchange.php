<?php

namespace App\Service;

class CoinbaseExchange
{
    public function __construct()
    {
        $this->key = getenv('GDAX_KEY');
        $this->secret = getenv('GDAX_SECRET');
        $this->passphrase = getenv('GDAX_PASSPHRASE');
    }

    public function signature($request_path = '', $body = '', $timestamp = false, $method = 'GET')
    {
        $body = is_array($body) ? json_encode($body) : $body;
        $timestamp = $timestamp ? $timestamp : time();
        $what = $timestamp.$method.$request_path.$body;
        return base64_encode(hash_hmac("sha256", $what, base64_decode($this->secret), true));
    }
}
