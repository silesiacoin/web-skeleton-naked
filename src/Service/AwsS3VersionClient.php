<?php
declare(strict_types=1);
namespace App\Service;

use App\Entity\SilesiaCoin\BinaryPackage;
use App\Entity\User;
use App\Exception\PackageNotAllowed;
use App\Exception\S3ObjNotFoundException;

class AwsS3VersionClient
{
    private const S3BUCKET = 'silesiacoin';

    /** @var AwsSdkClient */
    private $sdk;

    public function __construct(AwsSdkClient $awsSdkClient)
    {
        $this->sdk = $awsSdkClient;
    }

    /**
     * @param string $packageName
     * @param User $user
     * @param string $acl
     * @return array
     * @throws \Exception
     */
    public function getCurrentPackageVersion(string $packageName, User $user, $acl = 'public-read') : array
    {
        try {
            $objList = $this->sdk->myListObjects(self::S3BUCKET, $packageName, $acl);
        } catch (S3ObjNotFoundException $exception) {
            throw new PackageNotAllowed(
                sprintf(
                    "%s \n userEmail: %s \n",
                    $exception->getEventName(),
                    $user->getEmail()
                )
            );
        }

        return $this->buildLatestPackageCollection($objList);
    }

    public function getCurrentPackageVersionByOs(string $packageName, string $system, User $user) : string
    {
        $version = $this->getCurrentPackageVersion($packageName, $user);
        $list = &$version['list'];
        $uniquePkg = $this->getUniquePackage($system, $version);
        $this->getLatestOs($system, $list);

        if (isset($list['latestUrl'])) {
            return $list['latestUrl'];
        }

        return $uniquePkg->sayDownloadUrl();
    }

    private function getUniquePackage(string $system, array $version) : BinaryPackage
    {
        if (isset($version['unique'][$system]) && $version['unique'][$system] instanceof BinaryPackage) {
            return $version['unique'][$system];
        }

        throw new S3ObjNotFoundException("$system not found in packages");
    }

    private function getLatestOs(string $system, array &$list) : void
    {
        $max = max(array_keys($list));
        $pkgAssoc = $list[$max];

        if (isset($pkgAssoc[$system]) && $pkgAssoc[$system] instanceof BinaryPackage) {
            $package = $pkgAssoc[$system];
            $list['latestUrl'] = $package->sayDownloadUrl();
        }

        unset($list[$max]);

        if (isset($list['latestUrl'])) {
            return;
        }

        if (!empty($list)) {
            $this->getLatestOs($system, $list);
        }
    }

    private function buildLatestPackageCollection(array $objList) : array
    {
        $pkgArr = [];

        if (empty($objList)) {
            return $pkgArr;
        }

        foreach ($objList as $pkg) {
            $pkg = BinaryPackage::fromS3List($pkg);
            $intVersion = $pkg->sayIntPkgVersion();
            $pkgArr['list'][$intVersion][$pkg->sayPkgBuild()] = $pkg;
            $pkgArr['unique'][$pkg->sayPkgBuild()] = $pkg;
        }

        return $this->prepareProperBuildOrder($pkgArr);
    }

    private function prepareProperBuildOrder(array $pkgArr): array
    {
        if (!empty($pkgArr['list'])) {
            $maxKey = max(array_keys($pkgArr['list']));
            $pkgArr['latest'][$maxKey] =  $pkgArr['list'][$maxKey];
        }

        if (!empty($pkgArr['list'])) {
            $maxKey = max(array_keys($pkgArr['list']));
            $pkgArr['latest'][$maxKey] =  $pkgArr['list'][$maxKey];
        }

        return $pkgArr;
    }
}
