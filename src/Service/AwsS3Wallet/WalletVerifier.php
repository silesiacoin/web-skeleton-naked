<?php
declare(strict_types=1);

namespace App\Service\AwsS3Wallet;

use App\Entity\User;
use App\Exception\S3WalletNotAllowedException;
use App\Service\CryptoBotDynamoIrelandClient;

class WalletVerifier
{
    private $dynamoClient;
    private const TABLE_NAME = 'dummy-table-wallet';

    /**
     * LeadVerifier constructor.
     * @throws \InvalidArgumentException
     */
    public function __construct()
    {
        $this->dynamoClient = CryptoBotDynamoIrelandClient::defaultDynamoIrelandClient();
    }

    public function verifyAndProvideAcc(User $user, string $sscAccountAddr)
    {
        $result = $this->fetchSscAccountFromDynamo($user, $sscAccountAddr);

        if (!isset($result[0]['s3Url'])) {
            throw S3WalletNotAllowedException::fromStorageAttempt('Unauthorized', $user, $sscAccountAddr);
        }

        return $result[0]['s3Url'];
    }

    private function fetchSscAccountFromDynamo(User $user, string $sscAccount)
    {
        $dynamoClient = $this->dynamoClient;
        $result = $dynamoClient->scan([
            'TableName' => self::TABLE_NAME,
            'ScanFilter' => [
                'adress' => [
                    'AttributeValueList' => [['S' => $sscAccount]],
                    'ComparisonOperator' => 'EQ'
                ],
                'userName' => [
                    'AttributeValueList' => [['S' => $user->getUsername()]],
                    'ComparisonOperator' => 'EQ'
                ]
            ]
        ]);

        return $result->get('Items');
    }
}
