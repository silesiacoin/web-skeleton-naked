<?php
declare(strict_types=1);

namespace App\Service;

use App\Entity\SilesiaCoin\SscAccount;
use App\Exception\FetchWalletFileException;

class Web3Client
{
    public function createAccount(string $passphrase)
    {
        return $this->executeCurl(
            $this->prepareStandardJsonBody(
                'personal_newAccount',
                [$passphrase]
            )
        );
    }

    public function getAccountBalance(SscAccount $account) : void
    {
        $json = $this->executeCurl(
            $this->prepareStandardJsonBody(
                'eth_getBalance',
                [$account->getHexAddr(), 'latest']
            )
        );

        $decode = json_decode($json, true);

        if (isset($decode['result'])) {
            $account->changeAmount(hexdec($decode['result']));
        }
    }

    private function prepareStandardJsonBody(string $method, array $params = []) : string
    {
        $data = [];
        $data['method'] = $method;

        if (!empty($params)) {
            $data['params'] = $params;
        }

        $data['id'] = '1';
        $data['jsonrpc'] = '2.0';

        return json_encode($data);
    }

    private function chooseWhichNode()
    {
        $node = getenv('SC_LOCAL_NODE');

        if ('prod' === getenv('APP_ENV')) {
            $node = getenv('SC_NODE_SYNC');
        }

        return $node;
    }

    /**
     * @param string $jsonBody
     * @return bool|mixed
     */
    private function executeCurl(string $jsonBody)
    {
        return $this->handleCurl($this->initCurl($jsonBody));
    }

    private function handleCurl($curl)
    {
        $data = curl_exec($curl);
        $httpcode = curl_getinfo($curl, CURLINFO_HTTP_CODE);
        $curlErrorCode = curl_errno($curl);
        $curlErrorMsg = curl_error($curl);
        curl_close($curl);

        if ($curlErrorCode || $curlErrorMsg) {
            $this->throwCurlException($curlErrorCode, $curlErrorMsg);
        }

        if (302 === $httpcode && \is_string($data) && $data = json_decode($data)) {
            return $data;
        }

        if ((404 === $httpcode && \is_string($data)) || ($httpcode >= 200 && $httpcode < 300)) {
            return $data;
        }

        return $data;
    }

    private function throwCurlException($curlErrorCode, $curlErrorMsg) : void
    {
        throw new FetchWalletFileException(
            sprintf(
                'check curl settings: %s %s',
                $curlErrorCode,
                $curlErrorMsg
            )
        );
    }

    private function initCurl(string $jsonBody)
    {
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $this->chooseWhichNode());
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($curl, CURLOPT_CONNECTTIMEOUT, 5);
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 'dev' === getenv('APP_ENV') ? 0 : 2);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, 'dev' === getenv('APP_ENV') ? 0 : true);
        curl_setopt($curl, CURLOPT_CUSTOMREQUEST, 'POST');
        curl_setopt($curl, CURLOPT_POSTFIELDS, $jsonBody);
        curl_setopt($curl, CURLOPT_HTTPHEADER, array(
            'Content-Type: application/json',
            'Content-Length: ' . \strlen($jsonBody)));

        curl_setopt($curl, CURLOPT_TIMEOUT, 5);

        return $curl;
    }
}
