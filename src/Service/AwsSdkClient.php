<?php
declare(strict_types=1);

namespace App\Service;

use App\Exception\S3ObjNotFoundException;
use Aws\DynamoDb\DynamoDbClient;
use Aws\DynamoDb\Marshaler;
use Aws\Result;
use Aws\S3\S3Client;
use Aws\Sdk;

class AwsSdkClient
{
    /**
     * @var Sdk
     */
    private $sdk;

    private $sdks = [];

    public function __construct(Sdk $sdk)
    {
        $this->sdk = $sdk;
    }

    /**
     * @return DynamoDbClient
     * @throws \InvalidArgumentException
     */
    public function getAwsDynamoClient() : DynamoDbClient
    {
        if (!isset($this->sdks['dynamodb'])) {
            $this->sdks['dynamodb'] = new DynamoDbClient([
                'version'=> 'latest',
                'region'=> getenv('AWS_REGION'),
                'credentials' => [
                    'key' => getenv('AWS_KEY'),
                    'secret' => getenv('AWS_SECRET')
                ],
            ]);
        }

        return $this->sdks['dynamodb'];
    }

    public function getS3Client() : S3Client
    {
        if (!isset($this->sdks['s3'])) {
            $this->sdks['s3'] = $this->sdk->createS3([
                'region' => getenv('AWS_REGION'),
                'version' => 'latest',
                'credentials' => [
                    'key' => getenv('AWS_KEY'),
                    'secret' => getenv('AWS_SECRET')
                ]
            ]);
        }
        return $this->sdks['s3'];
    }

    public function createDynamoMarshaller() : Marshaler
    {
        return new Marshaler();
    }

    /**
     * @param $bucket
     * @param $prefix
     * @param string $acl
     * @return Result|null
     * @throws \Exception
     */
    public function myListObjects($bucket, $prefix, $acl = 'public-read') : array
    {
        $client = $this->getS3Client();
        $rClient = $client->listObjects(['Bucket' => $bucket, 'Prefix' => $prefix, 'ACL' => $acl]);
        $iterator = $rClient->getIterator();

        if (!isset($iterator->getArrayCopy()['Contents'])) {
            throw new S3ObjNotFoundException(sprintf('Object %s not found', $prefix));
        }

        return $iterator->getArrayCopy()['Contents'];
    }

    /**
     * @param $bucket
     * @param $oKey
     * @return Result|null
     * @throws \RuntimeException
     */
    public function getMyObject($bucket, $oKey) : ?Result
    {
        try {
            return $this->getS3Client()->getObject([
                'Bucket' => $bucket,
                'Key' => $oKey,
            ]);
        } catch (\Exception $exception) {
            throw new \RuntimeException($exception->getMessage());
        }
    }

    /**
     * @param $item
     * @return array|null
     * @throws \RuntimeException
     */
    public function getMarshallerItem($item) : ?array
    {
        try {
            return $this->createDynamoMarshaller()->marshalItem($item);
        } catch (\Exception $exception) {
            throw new \RuntimeException('Item that you have provided is invalid'.$exception->getMessage());
        }
    }
}
