<?php
declare(strict_types=1);

namespace App\Service;

class CoinpaymentsClient
{
    protected $credentials;

    protected function getCredentials(): array
    {
        if (null === $this->credentials) {
            $this->credentials = [
                'public_key' => getenv('COINPAYMENTS_PUB_KEY'),
                'private_key' => getenv('COINPAYMENTS_PRIV_KEY'),
                'version' => 1,
                'format' => 'json'
            ];
        }

        return $this->credentials;
    }

    private function optionResolver(array $options = null)
    {
        $req = [
            'key' => $this->getCredentials()['public_key'],
            'format' => $this->getCredentials()['format'],
            'version' => $this->getCredentials()['version'],
        ];

        if ($options) {
            foreach ($options as $key => $value) {
                $req[$key] = $value;
            }
        }

        return $req;
    }

    /**
     * @param $cmd
     * @param array $req
     * @return array|mixed
     * @throws \RuntimeException
     */
    public function callApi($cmd, array $req = null)
    {
        $curl = $this->prepCurlParams($cmd, $req);
        $data = curl_exec($curl);

        if ($data !== false) {
            $dec = json_decode($data, true);

            if (PHP_INT_SIZE < 8 && version_compare(PHP_VERSION, '5.4.0') >= 0) {
                $dec = json_decode($data, true, 512, JSON_BIGINT_AS_STRING);
            }

            if ($dec !== null && count($dec)) {
                return $dec;
            }

            return array('error' => 'Unable to parse JSON result ('.json_last_error().')');
        }

        throw new \RuntimeException('cURL error:' . curl_error($curl));
    }

    /**
     * @return array|mixed
     * @throws \RuntimeException
     */
    public function getRates()
    {
        return $this->callApi('rates');
    }

    /**
     * @param string $currency
     * @param string $amount
     * @param array|null $options
     * @return array|mixed
     * @throws \RuntimeException
     */
    public function createBasicTransaction(string $currency, string $amount, array $options = null)
    {
        $data = [
            'amount' => $amount,
            'currency1' => $currency,
            'currency2' => $currency
        ];

        if ($options) {
            foreach ($options as $key => $value) {
                $req[$key] = $value;
            }
        }

        return $this->callApi('create_transaction', $data);
    }

    private function prepCurlParams($cmd, array $req = null)
    {
        $req = $this->optionResolver($req);
        $req['cmd'] = $cmd;

        $postData = http_build_query($req, '', '&');
        $hmac = hash_hmac('sha512', $postData, $this->getCredentials()['private_key']);

        $curl = curl_init('https://www.coinpayments.net/api.php');
        curl_setopt($curl, CURLOPT_FAILONERROR, true);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, 0);

        curl_setopt($curl, CURLOPT_HTTPHEADER, array('HMAC: '.$hmac));
        curl_setopt($curl, CURLOPT_POSTFIELDS, $postData);

        return $curl;
    }
}
