<?php

namespace App\Service;

use App\Entity\Price;

class CurrencyConvertClient
{
    private const API_URL = "https://api.exchangeratesapi.io/latest?base=EUR&symbols=PLN";

    public function getCurrencyExchangePrice()
    {
        $url = self::API_URL;
        $curl = $this->getCurl($url);

        $result = curl_exec($curl);
        curl_close($curl);

        return json_decode($result);
    }

    private function getCurl(string $url)
    {
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);

        return $curl;
    }
}
