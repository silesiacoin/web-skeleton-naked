<?php
declare(strict_types=1);

namespace App\Service;

use Aws\DynamoDb\DynamoDbClient;

class CryptoBotDynamoIrelandClient
{
    /**
     * @return DynamoDbClient
     * @throws \InvalidArgumentException
     */
    public static function defaultDynamoClient(): DynamoDbClient
    {
        return new DynamoDbClient([
            'region' => getenv('AWS_REGION'),
            'version' => getenv('AWS_VERSION'),
            'credentials' => [
                'key' => getenv('AWS_KEY'),
                'secret' => getenv('AWS_SECRET')
            ]
        ]);
    }
}
