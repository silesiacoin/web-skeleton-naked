<?php

declare(strict_types=1);

namespace App\Service;

use App\Entity\Price;

class GdaxSdkClient
{
    private $credentials;
    private $coinbaseExchange;

    public function __construct()
    {
        $this->coinbaseExchange = new CoinbaseExchange();
        $this->credentials = [
            'CB-ACCESS-KEY' => getenv('GDAX_KEY'),
            'CB-ACCESS-SECRET' => getenv('GDAX_SECRET'),
            'CB-ACCESS-PASSPHRASE' => getenv('GDAX_PASSPHRASE'),
        ];
    }

    public function callApi($method, $url, $data = false)
    {
        $curl = curl_init();
        $timestamp = time();
        $signature = $this->coinbaseExchange->signature($url, false, $timestamp, $method);
        curl_setopt($curl, CURLOPT_HTTPHEADER, $this->getSignedHeaderOptions($signature));
        $url = 'https://api.pro.coinbase.com' . $url;
        $url = $this->prepareHeadersByMethod($curl, $method, $url, $data);
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        $result = curl_exec($curl);
        curl_close($curl);

        return json_decode($result);
    }

    public function getAccounts()
    {
        return $this->callApi('GET', '/accounts');
    }

    public function getProducts()
    {
        return $this->callApi('GET', '/products');
    }

    public function getProductBook($id, $level = 1)
    {
        return $this->callApi('GET', "/products/$id/book", ['level' => $level]);
    }

    /**
     * @param $id
     * @param int $level
     * @return mixed
     * @throws \Symfony\Component\DependencyInjection\Exception\ServiceNotFoundException
     * @throws \Symfony\Component\DependencyInjection\Exception\ServiceCircularReferenceException
     */
    private function getProduct($id, $level = 1)
    {
        return $this->getProductBook($id, $level);
    }

    /**
     * @return array
     * @throws \Symfony\Component\DependencyInjection\Exception\ServiceNotFoundException
     * @throws \Symfony\Component\DependencyInjection\Exception\ServiceCircularReferenceException
     */
    private function getEurPrices()
    {
        return [
            'eth' => $this->getProduct('ETH-EUR')->bids[0][0],
            'btc' => $this->getProduct('BTC-EUR')->bids[0][0],
            'ltc' => $this->getProduct('LTC-EUR')->bids[0][0],
            'ssc' => 1
        ];
    }

    /**
     * @return array
     * @throws \Symfony\Component\DependencyInjection\Exception\ServiceNotFoundException
     * @throws \Symfony\Component\DependencyInjection\Exception\ServiceCircularReferenceException
     */
    public function calcPrice(): array
    {
        $gdaxPrices = $this->getEurPrices();

        return $gdaxPrices;
    }

    private function getSignedHeaderOptions(string $signature) : array
    {
        $timestamp = time();

        return [
            'CB-ACCESS-KEY: ' .$this->credentials['CB-ACCESS-KEY'],
            'CB-ACCESS-TIMESTAMP: ' . $timestamp,
            'CB-ACCESS-PASSPHRASE: ' . $this->credentials['CB-ACCESS-PASSPHRASE'],
            'CB-ACCESS-SIGN: ' . $signature,
            'User-Agent: mciav5'
        ];
    }

    private function prepareHeadersByMethod($curl, ...$arg) : string
    {
        [$method, $url, $data] = $arg;
        $method = strtolower($method);

        if ('post' === $method) {
            curl_setopt($curl, CURLOPT_POST, 1);
            $url = $data ? $url = sprintf('%s?%s', $url, http_build_query($data)) : $url;
        }

        if ('put' === $method) {
            curl_setopt($curl, CURLOPT_PUT, 1);
        }

        if ($data && 'put' !== $method && 'post' !== $method) {
            $url = sprintf('%s?%s', $url, http_build_query($data));
        }

        return $url;
    }
}
