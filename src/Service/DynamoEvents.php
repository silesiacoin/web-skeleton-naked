<?php
declare(strict_types=1);

namespace App\Service;

use Aws\DynamoDb\Marshaler;
use Ramsey\Uuid\Uuid;
use function MongoDB\BSON\toJSON;

class DynamoEvents
{
    private $dynamoClient;
    private $marshaler;
    private $mailer;
    private const TABLE_NAME = 'dummy-table';

    /**
     * LeadVerifier constructor.
     * @param \Swift_Mailer $mailer
     * @throws \InvalidArgumentException
     */
    public function __construct(\Swift_Mailer $mailer)
    {
        $this->mailer = $mailer;
        $this->dynamoClient = CryptoBotDynamoIrelandClient::defaultDynamoIrelandClient();
    }

    private function getMarshaler(): Marshaler
    {
        if (null === $this->marshaler) {
            $this->marshaler = new Marshaler();
        }

        return $this->marshaler;
    }

    /**
     * Provide JSON as Event
     * @param string $name
     * @param $event
     * @throws \Exception
     */
    public function storeEvent(string $name, DynamoEventsInterface $event): void
    {
        $dateTime = new \DateTime();
        $uuid = $event->getUuid();
        $this->dynamoClient->putItem([
            'TableName' => self::TABLE_NAME,
            'Item' => [
                'uuid' => ['S' => $uuid],
                'name' => ['S' => $name],
                'event' => ['S' => json_encode($event)],
                'time' => ['S' => $dateTime->format('d-m-Y H:i:s')],
                'host' => ['S' => gethostname()]
            ],
        ]);

        $this->sentEventEmail($event);
    }

    private function sentEventEmail(DynamoEventsInterface $event): void
    {
        $message = (new \Swift_Message($event->getEventName()))
            ->setFrom('kontakt@silesiacoin.org')
            ->setTo('ed@silesiacoin.org')
            ->setBody(
                $event->toEmailMessageBody(),
                'text/html'
            );

        $this->mailer->send($message);
    }

    /**
     * @param string $uuid
     * @return array|\stdClass
     * @throws \RuntimeException
     * @throws \InvalidArgumentException
     * @throws \UnexpectedValueException
     */
    public function getEvent(string $uuid)
    {
        $result = $this->dynamoClient->query([
            'TableName' => 'events-silesiacoin',
            'KeyConditionExpression' => '#uuid = :filter',
            'ExpressionAttributeNames'=> [ '#uuid' => 'uuid' ],
            'ExpressionAttributeValues' => [':filter' => ['S' => $uuid]],
        ]);

        $items = $result->get('Items');

        if (!empty($items) && (null !== [$items[0], $items[0]['event']])) {
            return $this->getMarshaler()->unmarshalItem($items[0]);
        }

        throw new \RuntimeException('Event does not exists');
    }

    public function getEventByOptionUuid(string $optionUuid): array
    {
        $params = $this->prepareScanParams($optionUuid);
        $result = $this->dynamoClient->scan($params);
        $items = $result->get('Items');

        if (\is_array($items) && !empty($items)) {
            return $this->getMarshaler()->unmarshalItem($items[0]);
        }

        return [];
    }

    private function prepareScanParams(string $optionUuid) : array
    {
        return [
            'TableName' => 'events-silesiacoin',
            'ScanFilter' => [
                'event' => [
                    'ComparisonOperator' => 'CONTAINS',
                    'AttributeValueList' => [
                        ['S' => sprintf('"optionUuid":"%s"', $optionUuid)]
                    ]
                ]
            ],
        ];
    }
}
