<?php
declare(strict_types=1);

namespace App\Service;

use Facebook\Exceptions\FacebookSDKException;
use Facebook\Facebook;
use Facebook\Facebook as FacebookSdk;

class FacebookSdkClient
{
    private $fbclient;

    /**
     * @return Facebook
     * @throws FacebookSDKException
     */
    public function getFb(): Facebook
    {
        if (null === $this->fbclient) {
            $this->fbclient = new FacebookSdk([
                'app_id' => getenv('FACEBOOK_APP_ID'),
                'app_secret' => getenv('FACEBOOK_APP_SECRET'),
                'default_graph_version' => 'v2.10',
                'client_token' => getenv('FACEBOOK_CLIENT_TOKEN'),
            ]);
        }

        return $this->fbclient;
    }

    /**
     * @return \Facebook\Authentication\AccessToken|null
     * @throws FacebookSDKException
     */
    public function getAccessToken(): ?\Facebook\Authentication\AccessToken
    {
        return $this->getFb()->getDefaultAccessToken();
    }

    /**
     * @return string
     * @throws FacebookSDKException
     */
    public function getLoginUrl() : string
    {
        $helper = $this->getFb()->getRedirectLoginHelper();
        $permissions = ['email'];
        $port = $_SERVER['SERVER_PORT'];
        $domain = getenv('DOMAIN');

        if ("80" !== $port) {
            $domain = $domain . ":$port";
        }

        return $helper
            ->getLoginUrl('https://api.' .$domain . '/v1/facebook/login', $permissions);
    }

    /**
     * @throws \RuntimeException
     * @throws FacebookSDKException
     */
    public function handleCallback() : \stdClass
    {
        $facebookClient = $this->getFb();
        $helper = $facebookClient->getRedirectLoginHelper();
        $accessToken = $helper->getAccessToken();
        $oauth = $facebookClient->getOAuth2Client();
        $tokenMetadata = $oauth->debugToken($accessToken);
        $tokenMetadata->validateExpiration();

        if ($accessToken && $accessToken->isLongLived()) {
            $accessToken = $oauth->getLongLivedAccessToken($accessToken);
        }

        $fbUser = json_decode($this->getEmailFromToken($accessToken)->getBody());
        $fbUser->access_token = $accessToken->getValue();

        return $fbUser;
    }

    /**
     * @param $accessToken
     * @return \Facebook\FacebookResponse
     * @throws \RuntimeException
     * @throws FacebookSDKException
     */
    private function getEmailFromToken($accessToken): ?\Facebook\FacebookResponse
    {
        return $this->getFb()->get('/me?fields=name,email', $accessToken);
    }
}
