<?php
declare(strict_types=1);

namespace App\Service;

interface DynamoEventsInterface extends \JsonSerializable
{
    public function getEventName() : string;

    public function getUuid();

    public function getOptionUuid();

    public function toEmailMessageBody() : string;
}
