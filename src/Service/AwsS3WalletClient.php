<?php
declare(strict_types=1);

namespace App\Service;

use App\Entity\SilesiaCoin\SscAccount;
use App\Entity\User;
use App\Event\S3FileUploaded;
use App\Event\SscAccountCreated;
use App\Query\DynamoDb\Wallet\ListWalletsQuery;
use App\Query\DynamoDb\Wallet\StoreS3FileIntoDeleteQueueQuery;
use App\Query\DynamoDb\Wallet\StoreWalletIntoDynamo;
use App\Service\AwsS3Wallet\WalletVerifier;
use Aws\Result;
use Ramsey\Uuid\Uuid;

class AwsS3WalletClient
{
    private const SECURES3BUCKET = 'dummyStorage';
    private const OPENS3BUCKET = 'dummyCache';

    /**
     * @var AwsSdkClient
     */
    private $sdk;

    /**
     * @var DynamoEvents
     */
    private $dynamoEvents;

    public function __construct(AwsSdkClient $awsSdkClient, DynamoEvents $dynamoEvents)
    {
        $this->sdk = $awsSdkClient;
        $this->dynamoEvents = $dynamoEvents;
    }

    /**
     * @param  string $walletAdress
     * @param  $body
     * @param  User   $user
     * @return SscAccount
     * @throws \Exception
     */
    public function storeWallet(string $walletAdress, $body, User $user) : SscAccount
    {
        $sscAccount = $this->produceWallet($walletAdress);
        $s3Array = $this->storeFileIntoS3($user, $sscAccount, $body);
        $this->storeWalletIntoDynamo($sscAccount, $user, $s3Array['ObjectURL']);

        return $sscAccount;
    }

    public function listWallets(User $user) : array
    {
        return $this->fetchListByEnvironment($user);
    }

    private function fetchListByEnvironment(User $user) : array
    {
        if ('prod' === getenv('APP_ENV')) {
            return $this->fetchWalletListInProd($user);
        }

        return $this->getMockedWalletList($user);
    }

    private function fetchWalletListInProd(User $user) : array
    {
        $sdk = $this->sdk;
        $dynamoClient = $sdk->getAwsDynamoClient();
        $params = (new ListWalletsQuery($user))->getQuery();
        /*** @var Result $result */
        $result = $dynamoClient->scan($params);
        $wallets = $result->get('Items');
        $sscAccounts = [];

        if (\is_array($wallets) && !empty($wallets)) {
            foreach ($wallets as $wallet) {
                $sscAccounts[] = new SscAccount($wallet['adress']['S']);
            }
        }

        return $sscAccounts;
    }

    private function getMockedWalletList(User $user) : array
    {
        return [SscAccount::withRandomAddress(), SscAccount::withRandomAddress()];
    }

    /**
     * @param  User   $user
     * @param  string $walletAdress
     * @return mixed
     * @throws \Exception
     */
    public function getWallet(User $user, string $walletAdress)
    {
        $walletVerifier = new WalletVerifier();
        $sdk = $this->sdk;
        $s3Client = $sdk->getS3Client();
        $bucketRoute = 'https://ssc-wallet.s3.eu-west-1.amazonaws.com/';
        $s3Url = $walletVerifier->verifyAndProvideAcc($user, $walletAdress);
        $args = explode('/', str_replace($bucketRoute, '', $s3Url['S']));
        $lockedUrl = sprintf('%s/%s', $args[0], $args[1]);
        $params = $this->prepareAclForPublicStore($s3Url['S'], $lockedUrl);
        $accountFile = $s3Client->copyObject($params);

        if (!isset($accountFile['ObjectURL'])) {
            throw new \RuntimeException('problem within storage of copy');
        }

        $fullUrl = $accountFile['ObjectURL'];
        $this->storePublicS3FileIntoDeleteQueue($fullUrl);

        return $fullUrl;
    }

    /**
     * @throws \Exception
     */
    private function storePublicS3FileIntoDeleteQueue(string $fullUrl) : void
    {
        $query = new StoreS3FileIntoDeleteQueueQuery($fullUrl);

        CryptoBotDynamoIrelandClient::defaultDynamoIrelandClient()->putItem($query->getQuery());
    }

    /**
     * @throws \Exception
     */
    private function storeWalletIntoDynamo(SscAccount $sscAccount, User $user, string $s3Url) : void
    {
        $query = new StoreWalletIntoDynamo($sscAccount, $user, $s3Url);

        CryptoBotDynamoIrelandClient::defaultDynamoIrelandClient()->putItem($query->getQuery());
    }

    /**
     * @throws \Exception
     */
    private function produceWallet(string $walletAdress) : SscAccount
    {
        $sscAccount = new SscAccount($walletAdress);
        $createdEvent = new SscAccountCreated($sscAccount);
        $this->dynamoEvents->storeEvent((string) $createdEvent, $createdEvent);

        return $sscAccount;
    }

    /**
     * @throws \Exception
     */
    private function storeFileIntoS3(User $user, SscAccount $sscAccount, $body) : array
    {
        $sdk = $this->sdk;
        $s3Client = $sdk->getS3Client();
        $s3result = $s3Client->putObject($this->prepareAclForSecureStore($sscAccount, $body));
        $s3Array = $s3result->getIterator();
        $copy = $s3Array->getArrayCopy();

        if (!isset($copy['ObjectURL'])) {
            throw new \RuntimeException('Something went wrong within storing wallet onto s3 action');
        }

        $this->dynamoEvents->storeEvent('SscAccountStore', new S3FileUploaded($user, $copy['ObjectURL']));

        return $copy;
    }

    private function prepareAclForSecureStore(SscAccount $sscAccount, $body) : array
    {
        return [
            'ACL' => 'private',
            'Bucket' => self::SECURES3BUCKET,
            'Key' => sprintf('%s/%s', Uuid::uuid4()->toString(), $sscAccount->getAddress()),
            'Body' => $body
        ];
    }

    private function prepareAclForPublicStore(string $url, string $lockedUrl) : array
    {
        return [
            'ACL' => 'public-read',
            'Bucket' => self::OPENS3BUCKET,
            'CopySource' => $url,
            'Key' => $lockedUrl,
        ];
    }
}
