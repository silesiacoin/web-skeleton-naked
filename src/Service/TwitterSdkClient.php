<?php
declare(strict_types=1);

namespace App\Service;

class TwitterSdkClient
{
    private $twitter;

    /**
     * @return \TwitterAPIExchange
     * @throws \RuntimeException
     * @throws \InvalidArgumentException
     */
    private function getTwitter() : \TwitterAPIExchange
    {
        if (null === $this->twitter) {
            $this->twitter = new \TwitterAPIExchange([
                'oauth_access_token' => getenv('TWITTER_ACCESS_TOKEN'),
                'oauth_access_token_secret' => getenv('TWITTER_ACCESS_TOKEN_SECRET'),
                'consumer_key' => getenv('TWITTER_CONSUMER_KEY'),
                'consumer_secret' => getenv('TWITTER_CONSUMER_SECRET')
            ]);
        }

        return $this->twitter;
    }

    /**
     * @throws \RuntimeException
     * @throws \Exception
     */
    public function getUserTimeline(): ?string
    {
        $url = 'https://api.twitter.com/1.1/statuses/user_timeline.json';
        $getfield = '?screen_name=Silesiacoin';
        $requestMethod = 'GET';
        $twitter = $this->getTwitter();
        $tgf = $twitter->setGetfield($getfield);
        $oAuth = $tgf->buildOauth($url, $requestMethod);

        return $oAuth->performRequest();
    }
}
