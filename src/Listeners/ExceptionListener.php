<?php
declare(strict_types=1);

namespace App\Listeners;

use App\Handlers\ExceptionHandler;
use http\Exception\RuntimeException;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpKernel\Event\ExceptionEvent;

class ExceptionListener
{
    private $exceptionHandler;

    public function __construct(ExceptionHandler $exceptionHandler)
    {
        $this->exceptionHandler = $exceptionHandler;
    }

    /**
     * @param ExceptionEvent $event
     * @throws \Exception
     */
    public function onKernelException(ExceptionEvent $event)
    {
        $throwable = $event->getThrowable();
        $exception = new \RuntimeException($throwable->getTraceAsString());
        $exceptionHandler = $this->exceptionHandler;
        $exceptionHandler->storeException($exception);

        $request = $event->getRequest();
        $host = $request->getHost();
        $jsonMessage = ['error' => 'something went wrong'];

        if ('prod' !== getenv('APP_ENV')) {
            $jsonMessage['message'] = $exception->getMessage();
        }

        if (false !== strpos($host, 'api.')) {
            $event->setResponse(
                new JsonResponse($jsonMessage, 400)
            );
        }
    }
}
