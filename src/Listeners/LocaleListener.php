<?php
declare(strict_types = 1);

namespace App\Listeners;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Event\GetResponseEvent;
use Symfony\Component\HttpKernel\KernelEvents;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

class LocaleListener implements EventSubscriberInterface
{
    public const SSC_LANGUAGE_FEATURE = 'ssc_language';
    private const SUPPORTED_LANGUAGES = ['pl', 'en'];
    private $language;

    public function __construct($defaultLanguage)
    {
        $this->language = $defaultLanguage;
    }

    public function onKernelRequest(GetResponseEvent $event)
    {
        $request = $event->getRequest();
        $this->parseLanguageFromGetParams($request);
        $this->prepareLanguage($request);

        if ($request->getLocale() !== $this->language) {
            $request->setLocale($this->language);
        }
    }

    public static function getSubscribedEvents()
    {
        return [
            KernelEvents::REQUEST => [['onKernelRequest', 20]],
        ];
    }

    private function prepareLanguage(Request $request): void
    {
        $cookies = $request->cookies;
        $sscLanguageFromCookies = $cookies->get(self::SSC_LANGUAGE_FEATURE);
        $userLang = $request->getPreferredLanguage();

        if ($sscLanguageFromCookies) {
            $this->language = $sscLanguageFromCookies;

            return;
        }

        if (\is_string($userLang) && null === $this->language) {
            $this->language = $userLang;
        }

        $attributes = &$request->attributes;
        $attributes->set('ssc_language_dispatch', $this->language);
    }

    private function parseLanguageFromGetParams(Request &$request): void
    {
        $languageFromParams = $request->get(self::SSC_LANGUAGE_FEATURE);

        if (null === $languageFromParams) {
            return;
        }

        if (!\in_array($languageFromParams, self::SUPPORTED_LANGUAGES)) {
            throw new \RuntimeException(sprintf('%s is not supported locale', $languageFromParams));
        }

        $cookies = $request->cookies;
        $cookies->remove(self::SSC_LANGUAGE_FEATURE);
        $this->language = $languageFromParams;
        $query = $request->query;
        $query->remove(self::SSC_LANGUAGE_FEATURE);
    }
}
