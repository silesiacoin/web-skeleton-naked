<?php
declare(strict_types=1);

namespace App\Subscribers;

use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Event\GetResponseEvent;
use Symfony\Component\HttpKernel\KernelEvents;

class CookieSubscriber implements EventSubscriberInterface
{
    public static function getSubscribedEvents(): array
    {
        // Higher for request
        // than symfony/http-kernel/EventListener/AbstractSessionListener.php
        return [
            KernelEvents::REQUEST => ['onKernelRequest', 256]
        ];
    }

    /**
     * @throws \Exception
     */
    public function onKernelRequest(GetResponseEvent $event): void
    {
        if (!$event->isMasterRequest()) {
            return;
        }

        $request = $event->getRequest();
        $headers = $request->headers;
        $cookiesSetter = $headers->get('Set-Cookie');

        if (!is_string($cookiesSetter)) {
            return;
        }

        $this->handleCookie($cookiesSetter, $request);
    }

    private function handleCookie(string $cookiesSetter, Request $request): void
    {
        $cookiesArr = explode('=', $cookiesSetter);

        if (count($cookiesArr) < 2) {
            return;
        }

        $cookieKey = $cookiesArr[0];
        $cookieValue = $cookiesArr[1];
        $cookies = $request->cookies;
        $cookies->set($cookieKey, $cookieValue);
    }
}
