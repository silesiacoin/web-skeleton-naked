<?php
declare(strict_types=1);

namespace App\Event;

use App\Entity\SilesiaCoin\SscAccount;
use App\Service\DynamoEventsInterface;
use Ramsey\Uuid\Uuid;

class SscAccountCreated implements DynamoEventsInterface
{
    /** @var SscAccount */
    private $account;

    /** @var \Ramsey\Uuid\UuidInterface */
    private $uuid;

    /** @var \Ramsey\Uuid\UuidInterface */
    private $optionUuid;

    public function __construct(SscAccount $account)
    {
        $this->uuid = Uuid::uuid4();
        $this->optionUuid = Uuid::uuid4();
        $this->account = $account;
    }


    public function __toString()
    {
        return 'sscAccountCreated';
    }

    public function getEventName(): string
    {
        return (string) $this;
    }

    public function getUuid()
    {
        return $this->uuid;
    }

    public function getOptionUuid()
    {
        return $this->optionUuid;
    }

    public function toEmailMessageBody(): string
    {
        $hostname = gethostname();
        $env = getenv('APP_ENV');

        return <<<HTML
<p>
    Hello,
</p>
<p>
    <b>
        $this
    </b>
</p>
<p>
    Uuid: {$this->getUuid()} <br/>
    Account: {$this->account->getAddress()}<br/>
    Hostname: $hostname <br/> 
    Env: $env
</p>
HTML;
    }

    public function jsonSerialize()
    {
        return [
            'self' => (string) $this,
            'account' => $this->account->getAddress(),
            'optionUuid' => $this->optionUuid,
            'uuid' => $this->uuid
        ];
    }
}
