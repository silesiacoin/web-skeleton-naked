<?php
declare(strict_types=1);
namespace App\Event;

use App\Entity\User;
use App\Service\DynamoEventsInterface;
use Ramsey\Uuid\Uuid;
use Ramsey\Uuid\UuidInterface;

class S3FileUploaded implements DynamoEventsInterface
{
    /**
     *
     *
     * @var User
     */
    private $user;
    private $fileLink;

    public function __construct(User $user, string $fileLink)
    {
        $this->user = $user;
        $this->fileLink = $fileLink;
    }

    public function __toString()
    {
        return 's3FileUploaded';
    }

    public function getEventName(): string
    {
        return (string) $this;
    }

    /**
     * @return \Ramsey\Uuid\UuidInterface
     * @throws \Exception
     */
    public function getUuid() : UuidInterface
    {
        return Uuid::uuid4();
    }

    /**
     * @return UuidInterface
     * @throws \Exception
     */
    public function getOptionUuid() : UuidInterface
    {
        return Uuid::uuid4();
    }

    public function toEmailMessageBody(): string
    {
        return <<<HTML
<p>
    File was stored to S3: <br/>
    <a href="{$this->fileLink}">Link</a><br/>
    By user: <br/>
    {$this->user->getUsername()} 
</p>
HTML;
    }

    /**
     * @return array|mixed
     * @throws \Exception
     */
    public function jsonSerialize()
    {
        return [
            'self' => (string) $this,
            'uuid' => $this->getUuid(),
            'optionUuid' => $this->getOptionUuid(),
            'eventName' => (string) $this,
            'userName' => $this->user->getUsername()
        ];
    }
}
