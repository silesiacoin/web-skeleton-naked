<?php
declare(strict_types=1);
namespace App\Query\DynamoDb\Wallet;

use App\Entity\User;
use App\Query\DynamoDb\AbstractDynamoQuery;

class ListWalletsQuery extends AbstractDynamoQuery
{
    /**
     * @var User
     */
    private $user;

    public function __construct(User $user)
    {
        $this->user = $user;

        parent::__construct('wallet');
    }

    public function getQuery(): array
    {
        return [
            'TableName' => $this->tableName,
            'ScanFilter' => [
                'userName' => [
                    'ComparisonOperator' => 'EQ',
                    'AttributeValueList' => [
                        ['S' => $this->user->getUsername()]
                    ]
                ]
            ],
        ];
    }
}
