<?php
declare(strict_types=1);
namespace App\Query\DynamoDb\Wallet;

use App\Entity\User;
use App\Query\DynamoDb\AbstractDynamoQuery;
use Ramsey\Uuid\Uuid;

class StoreS3FileIntoDeleteQueueQuery extends AbstractDynamoQuery
{
    /**
     * @var string
     */
    private $fullUrl;

    public function __construct(string $fullUrl)
    {
        $this->fullUrl = $fullUrl;

        parent::__construct('ssc-s3-cache');
    }

    /**
     * @return array
     * @throws \Exception
     */
    public function getQuery() : array
    {
        $expire = (new \DateTime())->add(new \DateInterval('PT5M'))->format('d-m-Y H:i:s');

        return [
            'TableName' => $this->tableName,
            'Item' => [
                'uuid' => ['S' => Uuid::uuid4()],
                'path' => ['S' => $this->fullUrl],
                'expire' => ['S' => $expire],
                'action' => ['S' => 'delete']
            ],
        ];
    }
}
