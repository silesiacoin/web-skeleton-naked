<?php
declare(strict_types=1);
namespace App\Query\DynamoDb\Wallet;

use App\Entity\SilesiaCoin\SscAccount;
use App\Entity\User;
use App\Query\DynamoDb\AbstractDynamoQuery;
use Ramsey\Uuid\Uuid;

class StoreWalletIntoDynamo extends AbstractDynamoQuery
{
    /**
     * @var SscAccount
     */
    private $sscAccount;
    /**
     * @var User
     */
    private $user;
    /**
     * @var string
     */
    private $s3Url;

    public function __construct(SscAccount $sscAccount, User $user, string $s3Url)
    {
        $this->sscAccount = $sscAccount;
        $this->user = $user;
        $this->s3Url = $s3Url;

        parent::__construct('wallet');
    }

    /**
     * @return array
     * @throws \Exception
     */
    public function getQuery() : array
    {
        return [
            'TableName' => $this->tableName,
            'Item' => [
                'uuid' => ['S' => Uuid::uuid4()],
                'adress' => ['S' => $this->sscAccount->getAddress()],
                's3Url' => ['S' => $this->s3Url],
                'crypto' => ['S' => 'ssc'],
                'userName' => ['S' => $this->user->getUsername()],
                'time' => ['S' => (new \DateTime())->format('d-m-Y H:i:s')],
                'host' => ['S' => gethostname()]
            ],
        ];
    }
}
