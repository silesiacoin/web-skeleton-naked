<?php
declare(strict_types=1);
namespace App\Query\DynamoDb;

abstract class AbstractDynamoQuery
{
    /**
     * @var string
     */
    protected $tableName;

    public function __construct(string $tableName)
    {
        $this->tableName = $tableName;
    }

    public function getQuery() : array
    {
        return [];
    }
}
