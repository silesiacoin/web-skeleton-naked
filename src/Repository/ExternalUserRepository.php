<?php
declare(strict_types=1);

namespace App\Repository;

use App\Entity\FbUser;
use App\Entity\GUser;
use App\Entity\User;
use App\Factory\UserFactory;
use Doctrine\ORM\EntityManagerInterface;
use App\Factory\FbUserFactory;
use Ramsey\Uuid\Uuid;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class ExternalUserRepository
{
    private $entityManager;
    private $fbUserRepository;
    private $userRepository;
    private $encoder;
    /** @var GoogleUserRepository */
    private $gUserRepository;

    public function __construct(
        EntityManagerInterface $entityManager,
        UserPasswordEncoderInterface $encoder
    ) {
        $this->entityManager = $entityManager;
        $this->fbUserRepository = $entityManager->getRepository(FbUser::class);
        $this->gUserRepository = $entityManager->getRepository(GUser::class);
        $this->userRepository = $entityManager->getRepository(User::class);
        $this->encoder = $encoder;
    }

    public function registerUser(User $user, $seek)
    {
        $encoder = $this->encoder;
        $password = $encoder->encodePassword($user, $user->getPlainPassword());
        $user->changePassword($password);

        if ($seek instanceof User) {
            $user->changeBelongsTo($seek);
        }

        $this->entityManager->persist($user);
        $this->entityManager->flush();
    }

    public function fetchDbRefUserFromSession(Request $request)
    {
        $session = $request->getSession();
        $ref = $session->get('ssc_ref');

        return $this->userRepository->findOneBy(['id' => $ref]);
    }

    public function registerFbUser($handledCallback, Request $request): string
    {
        $entityManager = $this->entityManager;
        $fbUser = $this->fbUserRepository->findOneBy(['email' => $handledCallback->email]);
        $fbUser = FbUserFactory::fromHandledCallback($handledCallback, $fbUser);
        $mainUser = $this->fetchMainUserByFBUser($fbUser);
        $encoder = $this->encoder;
        $mainUser = UserFactory::fromFbUser($fbUser, $encoder, $mainUser);
        $mainUser->changeBelongsTo($this->fetchDbRefUserFromSession($request));
        $entityManager->persist(User::withFbRoles($mainUser));
        $entityManager->persist($fbUser);
        $entityManager->flush();

        return $fbUser->getAccessToken();
    }

    public function authenticateGoogleUser(Request $request)
    {
        $code = $request->get('code');
        $gUser = $this->gUserRepository->fetchUserFromCode($code);
        $mainUser = $this->userRepository->findOneBy(['email' => $gUser->getEmail()]);

        if ($mainUser instanceof User) {
            $gUser->changeMainUserName($mainUser->getUserName());
        }

        if (null === $mainUser) {
            $mainUser = $this->prepareNewUserIfNoMain($request, $gUser);
        }

        $this->gUserRepository->activateGoogleUser($mainUser, $gUser);

        return $gUser;
    }

    public function fetchRefByRequest(Request $request)
    {
        $session = $request->getSession();
        $session->set('ssc_ref', $request->get('ref'));

        return $this->userRepository->findOneBy(['refCode' => $request->get('ref')]);
    }

    private function fetchMainUserByFBUser(FbUser $fbUser): User
    {
        $mainUser = $this->userRepository->findOneBy(['email' => $fbUser->getEmail()]);

        if ($mainUser instanceof User) {
            return $mainUser;
        }

        return new User();
    }

    private function prepareNewUserIfNoMain(Request $request, GUser $gUser): User
    {
        $session = $request->getSession();
        $ref = $session->get('ssc_ref');
        $mainUser = new User();
        $uuid = Uuid::uuid4();
        $gUser->changeMainUserName($uuid->toString());
        $mainUser->changeUsername($uuid->toString());
        $mainUser->changeEmail($gUser->getEmail());
        $mainUser->changePassword(Uuid::uuid4()->toString());
        $mainUser->changePassword($this->encoder->encodePassword($mainUser, Uuid::uuid4()));
        $ref = $this->userRepository->findOneBy(['id' => $ref]);

        if ($ref instanceof User) {
            $mainUser->changeBelongsTo($ref);
        }

        return $mainUser;
    }
}
