<?php
declare(strict_types=1);

namespace App\Repository;

use App\Entity\GUser;
use App\Entity\User;
use App\Service\GoogleApiSdkClient;
use Doctrine\Common\Persistence\ManagerRegistry;
use Ramsey\Uuid\Uuid;
use Symfony\Component\HttpFoundation\File\Exception\AccessDeniedException;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;

class GoogleUserRepository extends ServiceEntityRepository
{
    private $apiSdkClient;

    public function __construct(ManagerRegistry $registry, GoogleApiSdkClient $googleApiSdkClient)
    {
        parent::__construct($registry, GUser::class);
        $this->apiSdkClient = $googleApiSdkClient;
    }

    public function fetchUserFromCode(string $code)
    {
        $verified = $this->apiSdkClient->getUserFromCode($code);

        if (!isset($verified['email'])) {
            throw new AccessDeniedException('Verification failed');
        }

        return $this->parseGUser($verified);
    }

    public function activateGoogleUser(User $mainUser, GUser $gUser)
    {
        $entityManager = $this->getEntityManager();
        $mainUser->addNewRole('ROLE_GOOGLE');
        $mainUser->addNewRole('ROLE_USER');
        $mainUser->addNewRole('ROLE_CONFIRMED');
        $entityManager->persist($mainUser);
        $entityManager->persist($gUser);
        $entityManager->flush();
    }

    private function parseGUser(array $verified): GUser
    {
        $user = $this->findOneBy(['email' => $verified['email']]);

        if (null === $user) {
            $user = new GUser();
            $user->changeId(Uuid::uuid4()->toString());
        }

        $user->changeAccessToken($verified['access_token']);
        $user->changeEmail($verified['email']);
        $user->changeName($verified['given_name'] . $verified['family_name']);

        return $user;
    }
}
