<?php
declare(strict_types=1);

namespace App\Repository;

use Endroid\QrCode\Factory\QrCodeFactory;

class NodeAddressessRepository
{
    public const ADDRESSES = [
        'eth' => '0x63C4516A46F7D99587D19E18fBD83661c14C3ADF',
        'btc' => '33TVPQFuHHt5LerabUUcW57ZnGspEmAUY5',
        'ltc' => 'MSothoQ1ZGNy1eNSGdmMDqSBWt6REFXuVV',
        'bch' => 'qqmja8d279humnz9fd9ay9zg0rcqeda23ydy4vf7v5'
    ];

    /** @var QrCodeFactory */
    private $qrCodeFactory;

    public function __construct()
    {
        $this->initializeQrCodeFactory();
    }

    public function getNodeWalletAdresses($crypto) : array
    {
//        TODO: Api integration with blockchain
        $adresses = [
            'eth' => '0x63C4516A46F7D99587D19E18fBD83661c14C3ADF',
            'btc' => '33TVPQFuHHt5LerabUUcW57ZnGspEmAUY5',
            'ltc' => 'MSothoQ1ZGNy1eNSGdmMDqSBWt6REFXuVV',
            'bch' => 'qqmja8d279humnz9fd9ay9zg0rcqeda23ydy4vf7v5'
        ];

        $qrCodeFactory = $this->initializeQrCodeFactory();
        $qrCode = $qrCodeFactory->create($adresses[$crypto], ['size' => 100]);

        return [
            'adress' => $adresses[$crypto],
            'type' => $crypto,
            'qr' => $qrCode->writeDataUri()
        ];
    }

    private function initializeQrCodeFactory() : QrCodeFactory
    {
        if (null === $this->qrCodeFactory) {
            $this->qrCodeFactory = new QrCodeFactory();
        }

        return $this->qrCodeFactory;
    }
}
