<?php
declare(strict_types=1);

namespace App\Command\GdaxCommands;

use App\Service\GdaxSdkClient;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class GetProducts extends Command
{
    private $sdk;

    /**
     * GetProducts constructor.
     * @param GdaxSdkClient $sdk
     * @throws \Symfony\Component\Console\Exception\LogicException
     */
    public function __construct(GdaxSdkClient $sdk)
    {
        $this->sdk = $sdk;
        parent::__construct('gdax:get:products');
    }

    protected function configure() : void
    {
        $this
            ->setDescription('Get current GDAX Products')
            ->setHelp('Get current gdax products');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $products = json_encode($this->sdk->getProducts());
        $output->writeln("List of GDAX products: \n $products");
    }
}
