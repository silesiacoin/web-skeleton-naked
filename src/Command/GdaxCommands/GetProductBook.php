<?php
declare(strict_types=1);

namespace App\Command\GdaxCommands;

use App\Service\GdaxSdkClient;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class GetProductBook extends Command
{
    private $sdk;

    /**
     * GetProductBook constructor.
     * @param GdaxSdkClient $sdk
     * @throws \Symfony\Component\Console\Exception\LogicException
     */
    public function __construct(GdaxSdkClient $sdk)
    {
        $this->sdk = $sdk;

        parent::__construct('gdax:get:product');
    }

    protected function configure() : void
    {
        $this->setDescription('Get current GDAX Products');
        $this->addArgument('id', InputArgument::OPTIONAL, 'Provide id', 'BTC-EUR');
        $this->addArgument('level', InputArgument::OPTIONAL, 'Provide level', 1);
        $this->setHelp('Get current gdax products');
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return int|null|void
     * @throws \Symfony\Component\Console\Exception\InvalidArgumentException
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $product = json_encode(
            $this->sdk->getProductBook($input->getArgument('id'), $input->getArgument('level'))
        );
        $output->writeln("List of GDAX products: \n $product");
    }
}
