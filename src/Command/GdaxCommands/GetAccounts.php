<?php
declare(strict_types=1);

namespace App\Command\GdaxCommands;

use App\Service\GdaxSdkClient;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class GetAccounts extends Command
{
    private $sdk;

    /**
     * GetAccounts constructor.
     * @param GdaxSdkClient $sdk
     * @throws \Symfony\Component\Console\Exception\LogicException
     */
    public function __construct(GdaxSdkClient $sdk)
    {
        $this->sdk = $sdk;
        parent::__construct('gdax:get:accounts');
    }

    protected function configure() : void
    {
        $this
            ->setDescription('Get current time from GDAX')
            ->setHelp('Get current time for api request. Authentication not required');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $accounts = json_encode($this->sdk->getAccounts());
        $output->writeln("List of GDAX accounts: \n $accounts");
    }
}
