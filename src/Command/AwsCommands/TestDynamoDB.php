<?php
declare(strict_types=1);

namespace App\Command\AwsCommands;

use App\Service\AwsSdkClient;
use Ramsey\Uuid\Uuid;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class TestDynamoDB extends Command
{
    private $sdk;

    public function __construct(AwsSdkClient $sdk)
    {
        $this->sdk = $sdk;
        parent::__construct('aws:dynamo:test');
    }

    protected function configure() : void
    {
        $this->addArgument(
            'Data',
            InputArgument::OPTIONAL,
            'Specify data string',
            'I like silesiaCoin'
        );
        $this->addArgument(
            'Table',
            InputArgument::OPTIONAL,
            'Add prefix',
            'temp'
        );
        $this->setDescription('Test connection with Dynamo DB');
        $this->setHelp('Use Table to define other table, data to store data. Undefined runs default');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $dynamoClient = $this->sdk->getAwsDynamoClient();
        $uuid = Uuid::uuid5(Uuid::NAMESPACE_DNS, $input->getArgument('Data'));
        $dynamoClient->putItem([
            'TableName' => $input->getArgument('Table'),
            'Item' => [
                'uuid' => ['S' => $uuid],
                'name' => ['S' => $input->getArgument('Data')],
            ],
        ]);
        $output->writeln('Record was stored into db');
    }
}
