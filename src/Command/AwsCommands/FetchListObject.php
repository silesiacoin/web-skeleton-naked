<?php
declare(strict_types=1);

namespace App\Command\AwsCommands;

use App\Service\AwsSdkClient;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Filesystem\Filesystem;

class FetchListObject extends Command
{
    private $sdk;

    public function __construct(AwsSdkClient $sdk)
    {
        $this->sdk = $sdk;
        parent::__construct('aws:fetch');
    }

    protected function configure() : void
    {
        $this->addArgument(
            'Prefix',
            InputArgument::OPTIONAL,
            'provide prefix to s3 instance',
            'free-images/Architecture/'
        );
        $this->addArgument(
            'Marker',
            InputArgument::OPTIONAL,
            'provide where to start listObjects',
            ''
        );
        $this->setDescription('Fetch all images from S3');
        $this->setHelp('This command allows you To fetch all images from S3');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $list = $this->sdk->myListObjects(
            'images.assets-landingi.com',
            $input->getArgument('Prefix'),
            $input->getArgument('Marker')
        );
        $properList = json_encode($list['Contents']);
        $filesystem = new Filesystem();
        $filesystem->dumpFile('test.json', $properList);
        $output->writeln('List of files was found. Check test.json in Your project root dir.');
    }
}
