<?php
declare(strict_types=1);

namespace App\Command\AccountCommands;

use App\Command\AbstractCommand;
use App\Entity\User;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class GrantRole extends AbstractCommand
{
    /**
     * GrantRole constructor.
     *
     * @throws \Exception
     */
    public function __construct(EntityManagerInterface $entityManager)
    {
        parent::__construct($entityManager, 'acc:role:grant');
    }

    protected function configure() : void
    {
        $this->addArgument(
            'AccountName',
            InputArgument::REQUIRED,
            'provide the account name for grant role'
        );
        $this->addArgument(
            'Role',
            InputArgument::REQUIRED,
            'provide the role to grant'
        );
        $this->setDescription('Grant user role');
        $this->setHelp('This command allows you grant user new roles');
    }

    protected function execute(InputInterface $input, OutputInterface $output): void
    {
        $entityManager = $this->getEntityManager();
        $repository = $this->getUserRepository();
        $user = $repository->findOneBy(['username' => $input->getArgument('AccountName')]);

        if ($user instanceof User) {
            $user->addNewRole($input->getArgument('Role'));
            $entityManager->persist($user);
            $entityManager->flush();
            $output->writeln($input->getArgument('AccountName') .' was granted '. $input->getArgument('Role'));
            die();
        }

        $output->writeln($input->getArgument('AccountName') . ' not found');
    }
}
