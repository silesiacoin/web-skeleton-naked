<?php
declare(strict_types=1);

namespace App\Command\AccountCommands;

use App\Command\AbstractCommand;
use App\Entity\User;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class ShowUsers extends AbstractCommand
{
    /**
     * @throws \Exception
     */
    public function __construct(EntityManagerInterface $entityManager)
    {
        parent::__construct($entityManager, 'acc:users');
    }

    protected function configure() : void
    {
        $this->addArgument(
            'AccountName',
            InputArgument::OPTIONAL,
            'provide the account name'
        );
        $this->setDescription('Show user(s)');
        $this->setHelp('This command allows you to look upon users');
    }

    protected function execute(InputInterface $input, OutputInterface $output): void
    {
        $repository = $this->getUserRepository();
        $user = $repository->findOneBy(['username' => $input->getArgument('AccountName')]);

        if ($user instanceof User) {
            $this->printSingleUserRole($user, $output);
            die();
        }

        $users = $repository->findAll();

        foreach ($users as $user) {
            $this->printSingleUserRole($user, $output);
        }
    }

    private function printSingleUserRole(User $user, OutputInterface $output): void
    {
        $output->writeln($user->getUsername());
        $output->writeln($user->getEmail());

        foreach ($user->getRoles() as $role) {
            $output->write($role . " \n ");
        }

        $output->write("\n");
    }
}
