<?php
declare(strict_types=1);

namespace App\Command\AccountCommands;

use App\Command\AbstractCommand;
use App\Entity\User;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class ShowRoles extends AbstractCommand
{
    /**
     * @throws \Exception
     */
    public function __construct(EntityManagerInterface $entityManager)
    {
        parent::__construct($entityManager, 'acc:roles:show');
    }

    protected function configure() : void
    {
        $this->addArgument(
            'AccountName',
            InputArgument::REQUIRED,
            'provide the account name to remove role'
        );
        $this->setDescription('Remove user role');
        $this->setHelp('This command allows you remove user roles');
    }


    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $repository = $this->getUserRepository();
        $user = $repository->findOneBy(['username' => $input->getArgument('AccountName')]);

        if ($user instanceof User) {
            /** @var User $user */
            foreach ($user->getRoles() as $role) {
                $output->writeln($role);
            }

            die();
        }

        $output->writeln($input->getArgument('AccountName') . ' not found');
    }
}
