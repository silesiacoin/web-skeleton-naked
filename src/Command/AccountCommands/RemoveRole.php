<?php
declare(strict_types=1);

namespace App\Command\AccountCommands;

use App\Command\AbstractCommand;
use App\Entity\User;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class RemoveRole extends AbstractCommand
{
    public function __construct(EntityManagerInterface $entityManager)
    {
        parent::__construct($entityManager, 'acc:role:remove');
    }

    protected function configure() : void
    {
        $this->configureRequiredOption('AccountName', 'provide the account name to remove role');
        $this->configureRequiredOption('Role', 'provide the role to grant');
        $this->setDescription('Remove user role');
        $this->setHelp('This command allows you remove user role');
    }

    protected function execute(InputInterface $input, OutputInterface $output): void
    {
        $entityManager = $this->getEntityManager();
        $repository = $entityManager->getRepository(User::class);
        $user = $repository->findOneBy(['username' => $input->getOption('AccountName')]);

        if ($user) {
            $user->removeRole($input->getOption('Role'));
            $entityManager->persist($user);
            $entityManager->flush();
            $output->writeln($input->getOption('AccountName') .' was unset role '. $input->getOption('Role'));
        }

        if (!$user) {
            $output->writeln($input->getOption('AccountName') . ' not found');
        }
    }
}
