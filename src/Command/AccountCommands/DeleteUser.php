<?php
declare(strict_types=1);
namespace App\Command\AccountCommands;

use App\Command\AbstractCommand;
use App\Entity\User;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class DeleteUser extends AbstractCommand
{
    /**
     * @throws \Exception
     */
    public function __construct(EntityManagerInterface $entityManager)
    {
        parent::__construct($entityManager, 'acc:user:delete');
    }

    protected function configure(): void
    {
        $this->addArgument(
            'AccountName',
            InputArgument::REQUIRED,
            'provide the account name for user delete'
        );
        $this->setDescription('Delete user');
        $this->setHelp('This command allows you to delete user');
    }

    protected function execute(InputInterface $input, OutputInterface $output): void
    {
        $entityManager = $this->getEntityManager();
        $repository = $this->getUserRepository();
        $user = $repository->findOneBy(['name' => $input->getArgument('AccountName')]);

        if ($user instanceof User) {
            $entityManager->remove($user);
            $entityManager->flush();
            $output->writeln('User' . $input->getFirstArgument() . ' deleted');
            die();
        }

        $output->writeln('User ' . $input->getFirstArgument() . ' not found');
    }
}
