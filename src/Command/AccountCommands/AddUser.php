<?php
declare(strict_types=1);
namespace App\Command\AccountCommands;

use App\Command\AbstractCommand;
use App\Entity\User;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class AddUser extends AbstractCommand
{
    /**
     * @var UserPasswordEncoderInterface
     */
    private $passwordEncoder;

    /**
     * @throws \Exception
     */
    public function __construct(
        EntityManagerInterface $entityManager,
        UserPasswordEncoderInterface $passwordEncoder
    ) {
        parent::__construct($entityManager, 'acc:user:add');
        $this->passwordEncoder = $passwordEncoder;
    }

    protected function configure() : void
    {
        $this->configureRequiredOption('AccountName', 'provide the acount name for user to add');
        $this->configureRequiredOption('AccountPassword', 'provide password');
        $this->configureRequiredOption('AccountPassword', 'provide password');
        $this->configureRequiredOption('RepeatPassword', 'repeat password');
        $this->configureRequiredOption('Email', 'provide email');
        $this->configureRequiredOption('Role', 'provide user main role');
        $this->setDescription('Add new user');
        $this->setHelp('This command allows you to add new user');
    }

    /**
     * @param  InputInterface  $input
     * @param  OutputInterface $output
     * @return int|null|void
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        if ($input->getOption('AccountPassword') !== $input->getOption('RepeatPassword')) {
            $output->writeln('Passwords does not match');
            die();
        }

        if (!filter_var($input->getOption('Email'), FILTER_VALIDATE_EMAIL)) {
            $output->writeln('Email is invalid');
            die();
        }

        $this->createUser($input);
        $output->writeln('User' . $input->getOption('AccountName') . ' created');
    }

    /**
     * @param  InputInterface $input
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     * @throws \Exception
     */
    private function createUser(InputInterface $input) : void
    {
        $user = new User();
        $entityManager = $this->getEntityManager();
        $encoder = $this->passwordEncoder;
        $user->changeUsername($input->getOption('AccountName'));
        $password = $encoder->encodePassword($user, $input->getOption('AccountPassword'));
        $user->changePassword($password);
        $user->changeEmail($input->getOption('Email'));
        $user->addNewRole($input->getOption('Role'));
        $entityManager->persist($user);
        $entityManager->flush();
    }
}
