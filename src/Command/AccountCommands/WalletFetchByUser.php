<?php
declare(strict_types=1);

namespace App\Command\AccountCommands;

use App\Command\AbstractCommand;
use App\Entity\SilesiaCoin\SscAccount;
use App\Entity\User;
use App\Service\AwsS3WalletClient;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class WalletFetchByUser extends AbstractCommand
{
    /**
     * @var AwsS3WalletClient
     */
    private $awsS3WalletClient;

    public function __construct(AwsS3WalletClient $awsS3WalletClient, EntityManagerInterface $entityManager)
    {
        parent::__construct($entityManager, 'wallet:list');
        $this->awsS3WalletClient = $awsS3WalletClient;
    }

    protected function configure(): void
    {
        $this->addArgument(
            'UserName',
            InputArgument::REQUIRED,
            'provide the account name to fetch wallet hashes'
        );
        $this->setDescription('Fetch user wallet');
        $this->setHelp('This command allows you to fetch users wallets');
    }

    protected function execute(InputInterface $input, OutputInterface $output): void
    {
        $s3Client = $this->awsS3WalletClient;
        $entityManager = $this->getEntityManager();
        $repository = $entityManager->getRepository('App:User');
        $userName = $input->getArgument('UserName');
        $user = $repository->findOneBy(['username' => $userName]);

        if (!$user instanceof User) {
            $output->writeln("User: $userName not found");
            die();
        }

        $list = $s3Client->listWallets($user);

        foreach ($list as $item) {
            if ($item instanceof SscAccount) {
                $output->writeln($item->getHexAddr());
            }
        }
    }
}
