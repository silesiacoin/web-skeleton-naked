<?php
declare(strict_types=1);
namespace App\Command;

use App\Entity\User;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

/**
 * @property SymfonyStyle outputInitializer
 */
abstract class AbstractCommand extends Command
{
    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    public function __construct(EntityManagerInterface $entityManager, $name = null)
    {
        parent::__construct($name);
        $this->entityManager = $entityManager;
    }

    /**
     * Initialize for input
     */
    protected function initialize(InputInterface $input, OutputInterface $output)
    {
        $this->outputInitializer = new SymfonyStyle($input, $output);
        $this->outputInitializer->title($this->getName());
    }

    /**
     * Interact with user
     */
    protected function interact(InputInterface $input, OutputInterface $output)
    {
        foreach ($input->getOptions() as $option => $value) {
            if (null === $value) {
                $input->setOption($option, $this->outputInitializer->ask(sprintf('%s', ucfirst($option))));
            }
        }
    }

    protected function configureRequiredOption(string $name, string $description) : void
    {
        $this->addOption(
            $name,
            null,
            InputOption::VALUE_REQUIRED,
            $description
        );
    }

    protected function getEntityManager() : EntityManagerInterface
    {
        return $this->entityManager;
    }

    protected function getUserRepository()
    {
        $entityManager = $this->getEntityManager();

        return $entityManager->getRepository(User::class);
    }
}
