<?php
declare(strict_types=1);

namespace App\Form\Account;

use App\Entity\Crypto;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class BuybyForm extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->setMethod('POST');

        $builder->add('symbol', HiddenType::class, ['label'=>'Crypto']);
        $builder->add('amount', IntegerType::class, [
                'label'=>'Ilość SC',
                'data' => 10,
                'attr'=>['class'=>'form-control']
        ]);
        $builder->add('adress', HiddenType::class, ['label'=>'Adres']);
        $builder->add('uuid', HiddenType::class, ['label'=>'Uuid']);
        $builder->add('submit', SubmitType::class, [
                'label' => 'zamów',
                'attr' => ['class' => 'btn btn-sc-blue color-yellow right-submit order-submit ajaxForm']
        ]);
    }

    /**
     * @param OptionsResolver $resolver
     * @throws \Symfony\Component\OptionsResolver\Exception\AccessException
     */
    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Crypto::class,
        ]);
    }
}
