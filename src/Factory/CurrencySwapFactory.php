<?php
declare(strict_types=1);
namespace App\Factory;

use App\Entity\CurrencySwap;
use App\Entity\Price;

/**
 * This class is register of supported currencies swap
 * If You want to add new swap, You should also add currency to Price entity factory
 */
class CurrencySwapFactory extends AbstractFactory
{
    public static function eurPln(float $value) : CurrencySwap
    {
        return new CurrencySwap(Price::eur(1), Price::pln(1), $value);
    }

    public static function usdPln(float $value) : CurrencySwap
    {
        return new CurrencySwap(Price::usd(1), Price::pln(1), $value);
    }

    public static function eurUsd(float $value) : CurrencySwap
    {
        return new CurrencySwap(Price::eur(1), Price::usd(1), $value);
    }

    public static function ethEur(float $value) : CurrencySwap
    {
        return new CurrencySwap(Price::eth(1), Price::eur(1), $value);
    }

    public static function btcEur(float $value) : CurrencySwap
    {
        return new CurrencySwap(Price::btc(1), Price::eur(1), $value);
    }

    public static function ltcEur(float $value) : CurrencySwap
    {
        return new CurrencySwap(Price::ltc(1), Price::eur(1), $value);
    }

    public static function btcPln(float $value) : CurrencySwap
    {
        return new CurrencySwap(Price::btc(1), Price::pln(1), $value);
    }

    public static function ethPln(float $value) : CurrencySwap
    {
        return new CurrencySwap(Price::eth(1), Price::pln(1), $value);
    }

    public static function ltcPln(float $value) : CurrencySwap
    {
        return new CurrencySwap(Price::ltc(1), Price::pln(1), $value);
    }

    public static function sscPln(float $value): CurrencySwap
    {
        return new CurrencySwap(Price::ssc(1), Price::pln(1), $value);
    }

    public static function sscEur(float $value): CurrencySwap
    {
        return new CurrencySwap(Price::ssc(1), Price::eur(1), $value);
    }

    public static function sscUsd(float $value): CurrencySwap
    {
        return new CurrencySwap(Price::ssc(1), Price::usd(1), $value);
    }

    public static function sscEth(float $value) : CurrencySwap
    {
        return new CurrencySwap(Price::ssc(1), Price::eth(1), $value);
    }

    public static function sscBtc(float $value) : CurrencySwap
    {
        return new CurrencySwap(Price::ssc(1), Price::btc(1), $value);
    }

    public static function sscLtc(float $value) : CurrencySwap
    {
        return new CurrencySwap(Price::ssc(1), Price::ltc(1), $value);
    }
}
