<?php
declare(strict_types=1);
namespace App\Factory;

use App\Entity\FbUser;
use App\Entity\User;
use Ramsey\Uuid\Uuid;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class UserFactory
{
    public static function apiClientUser() : User
    {
        $user = new User();
        $user->changeEmail('apiClient@silesiacoin.org');
        $user->changeUsername('api');

        return $user;
    }

    public static function fromFbUser(
        FbUser $fbUser,
        UserPasswordEncoderInterface $encoder,
        $user = null
    ) : User {
        if (!$user instanceof $user) {
            $user = new User();
        }

        $user->changeEmail($fbUser->getEmail());
        $user->changeUsername(Uuid::uuid4()->toString());
        $user->changePassword($encoder->encodePassword($user, Uuid::uuid4()));
        $fbUser->changeMainUserName($user->getUsername());

        return User::withFbRoles($user);
    }
}
