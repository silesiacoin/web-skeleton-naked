<?php
declare(strict_types=1);
namespace App\Factory;

class AbstractFactory
{
    /**
     * @param string $name
     * @param mixed ...$args
     * @return mixed
     * @throws \ReflectionException
     */
    public function create(string $name, ...$args)
    {
        $name = $this->camelize($name);
        $method = new \ReflectionMethod($this, $name);

        if (\count($method->getParameters()) !== \count($args)) {
            throw new \RuntimeException('Invalid number of args');
        }

        return $this->$name(...$args);
    }

    private function camelize($input) : string
    {
        return lcfirst(str_replace('_', '', ucwords($input, '_')));
    }
}
