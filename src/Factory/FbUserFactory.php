<?php
namespace App\Factory;

use App\Entity\FbUser;

class FbUserFactory
{
    public static function fromHandledCallback(\stdClass $callback, $fbUser = null) : FbUser
    {
        if (!$fbUser instanceof FbUser) {
            $fbUser = new FbUser();
        }

        $fbUser->changeName($callback->name);
        $fbUser->changeEmail($callback->email);
        $fbUser->changeAccessToken($callback->access_token);

        return $fbUser;
    }
}
