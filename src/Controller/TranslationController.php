<?php
declare(strict_types=1);

namespace App\Controller;

use App\Handlers\ExceptionHandler;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Translation\Exception\InvalidResourceException;
use Symfony\Contracts\Translation\TranslatorInterface;

class TranslationController extends AbstractController
{
    private $translator;

    public function __construct(TranslatorInterface $translator)
    {
        $this->translator = $translator;
    }

    /**
     * @Route("/i18n/{locale}", methods={"GET"})
     * @param string $locale
     * @param Request $request
     * @return JsonResponse
     * @throws \Exception
     */
    public function getTranslations(string $locale, Request $request): JsonResponse
    {
        $query = $request->query;
        /** @var \ArrayIterator $iterator */
        $iterator = $query->getIterator();
        $exceptionHandler = new ExceptionHandler();
        $collection = [];

        if ($iterator->count() < 1) {
            throw new \RuntimeException('No params were found in request', 400);
        }

        foreach ($iterator->getArrayCopy() as $key => $value) {
            try {
                $collection[$key] = $this->prepareTransObj($locale, $value);
            } catch (InvalidResourceException $exception) {
                $exceptionHandler->storeException($exception);
            }
        }

        return new JsonResponse($collection);
    }

    /**
     * @Route("/i18n/{locale}/{slug}", methods={"GET"})
     * @param string $locale
     * @param string $slug
     * @return JsonResponse
     */
    public function getTranslation(string $locale, string $slug): JsonResponse
    {
        return new JsonResponse($this->prepareTransObj($locale, $slug));
    }

    private function prepareTransObj(string $locale, string $slug)
    {
        $translator = $this->translator;

        return [
            'locale' => $locale,
            'translationKey' => $slug,
            'translation' => $translator->trans($slug, [], null, $locale)
        ];
    }
}
