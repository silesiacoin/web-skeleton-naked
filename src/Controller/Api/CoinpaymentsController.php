<?php
declare(strict_types=1);

namespace App\Controller\Api;

use App\Entity\CoinPaymentsOrder;
use App\Service\CoinpaymentsClient;
use App\Service\DynamoEvents;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

class CoinpaymentsController extends AbstractController
{
    private $cpClient;
    /** @var DynamoEvents */
    private $dynamoEvents;
    private $mailer;

    public function __construct(\Swift_Mailer $mailer)
    {
        $this->mailer = $mailer;
    }

    private function getCpClient()
    {
        if (null === $this->cpClient) {
            $this->cpClient = new CoinpaymentsClient();
        }

        return $this->cpClient;
    }

    /**
     * @return DynamoEvents
     * @throws \InvalidArgumentException
     */
    private function getDynamoEvent(): DynamoEvents
    {
        if (null === $this->dynamoEvents) {
            $this->dynamoEvents = new DynamoEvents($this->mailer);
        }

        return $this->dynamoEvents;
    }

    /**
     * @Route(
     *     "/v1/coinpayments/get-rates",
     *     name="coinpayments_get_rates",
     *     host="api.{DOMAIN}",
     *     defaults={"DOMAIN"="%DOMAIN%"},
     *     requirements={"DOMAIN"="%DOMAIN%"}
     *     )
     * )
     * @return JsonResponse
     * @throws \Symfony\Component\Security\Core\Exception\AccessDeniedException
     * @throws \RuntimeException
     */
    public function getRates(): JsonResponse
    {
        $this->denyAccessUnlessGranted('ROLE_DEVELOPER');

        return new JsonResponse(['rates' => $this->getCpClient()->getRates()]);
    }

    /**
     * @Route(
     *     "/v1/coinpayments/basic-transaction/{currency}/{amount}",
     *     name="coinpayments_create_transaction",
     *     host="api.{DOMAIN}",
     *     defaults={"DOMAIN"="%DOMAIN%"},
     *     requirements={"DOMAIN"="%DOMAIN%"}
     *     )
     * )
     * @param $currency
     * @param $amount
     * @param Request $request
     * @return JsonResponse
     * @throws \UnexpectedValueException
     * @throws \Symfony\Component\Security\Core\Exception\AccessDeniedException
     * @throws \InvalidArgumentException
     * @throws \RuntimeException
     */
    public function createTransaction($currency, $amount, Request $request): JsonResponse
    {
        $content = $request->getContent();
        $decodedContent = json_decode($content, true);
        $cpClient = $this->getCpClient();

        if (!$decodedContent['uuid']) {
            return new JsonResponse(['error' => 'you must provide uuid'], 500);
        }

        try {
            $this->handleDynamoEvent($decodedContent, $currency, $amount);
        } catch (\Exception $exception) {
            return new JsonResponse(['error' => $exception->getMessage()], 404);
        }

        return new JsonResponse(['response' => $cpClient->createBasicTransaction($currency, $amount)]);
    }

    /**
     * @Route(
     *     "/v1/coinpayments/get-callback-adress/{currency}",
     *     name="coinpayments_get_callback_adress",
     *     host="api.{DOMAIN}",
     *     defaults={"DOMAIN"="%DOMAIN%"},
     *     requirements={"DOMAIN"="%DOMAIN%"}
     *     )
     * )
     * @param $currency
     * @return JsonResponse
     * @throws \RuntimeException
     * @throws \Symfony\Component\Security\Core\Exception\AccessDeniedException
     */
    public function getCallbackAdress($currency): JsonResponse
    {
        $cpClient = $this->getCpClient();
        $responseBody = $cpClient->callApi('get_callback_address', ['currency' => $currency]);

        return new JsonResponse(['response' => $responseBody]);
    }

    /**
     * @param $decodedContent
     * @param $currency
     * @param $amount
     * @throws \Exception
     */
    private function handleDynamoEvent($decodedContent, $currency, $amount)
    {
        if ('dev' === getenv('APP_ENV')) {
            return;
        }

        $cpClient = $this->getCpClient();
        $dynamoEvent = $this->getDynamoEvent();

        $event = CoinPaymentsOrder::fromCoinpaymentsTransaction(
            $cpClient->createBasicTransaction($currency, $amount)['result'],
            $decodedContent['uuid']
        );

        //TODO: search for event where optionUuid same as Uuid

        $dynamoEvent->storeEvent('coinPaymentsOrder', $event);
    }
}
