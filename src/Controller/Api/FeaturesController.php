<?php
declare(strict_types=1);

namespace App\Controller\Api;

use App\Controller\DashboardController;
use App\Listeners\LocaleListener;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Cookie;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class FeaturesController extends AbstractController
{
    private $cookieFeatures = [
        'ssc_authentication_feature'                               => 'trigger authentication path',
        'ssc_rodo_agreement'                                       => 'trigger to toggle visibility of rodo info',
        'ssc_payment'                                              => 'trigger to toggle visibility of payment gateway',
        LocaleListener::SSC_LANGUAGE_FEATURE                       => 'trigger to change language'
    ];
//    Get method is also allowed, so any mobile can add cookie to global domain
//    Feature gherkin implementation will be done after merge of register branch
    /**
     * @Route(
     *     "/v1/cookies/{name}/{value}",
     *     name="cookies_add_global",
     *     host="api.{DOMAIN}",
     *     defaults={"DOMAIN"="%DOMAIN%"},
     *     requirements={"DOMAIN"="%DOMAIN%"},
     *     methods={"GET", "PUT"}
     *     )
     * )
     */
    public function addCookie(
        string $name,
        string $value
    ): JsonResponse {
        if (false === $this->validateCookie($name)) {
            return new JsonResponse(['status' => 'dispatched none'], Response::HTTP_BAD_REQUEST);
        }

        $cookie = new Cookie(
            $name,
            $value,
            0,
            "/",
            getenv('DOMAIN'),
            false,
            false
        );
        $response = new JsonResponse(['status' => 'added set-cookie header']);
        $headers = $response->headers;
        $headers->setCookie($cookie);

        return $response;
    }

    private function validateCookie(string $cookieName): bool
    {
        foreach ($this->cookieFeatures as $cookieFeature => $description) {
            if ($cookieName === $cookieFeature) {
                return true;
            }
        }

        return false;
    }
}
