<?php
declare(strict_types=1);

namespace Appz\Controller\Api;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;

class AbstractApiController extends AbstractController
{
    protected function serializeJsonRequest(Request $request)
    {
        $headers = $request->headers;
        $contentType = $headers->get('Content-Type');

        if ('application/json' !== $contentType) {
            return;
        }

        $jsonContent = $request->getContent();
        $content = json_decode($jsonContent, true);

        if (empty($content)) {
            return;
        }

        foreach ($content as $key => $value) {
            $requestObj = $request->request;
            $requestObj->set($key, $value);
        }
    }
}
