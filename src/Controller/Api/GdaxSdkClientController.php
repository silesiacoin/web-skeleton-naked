<?php
declare(strict_types=1);

namespace App\Controller\Api;

use App\Service\GdaxSdkClient;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

class GdaxSdkClientController extends AbstractController
{
    private $gdaxSdkClient;

    private function getGdaxSdkClient()
    {
        if (null === $this->gdaxSdkClient) {
            $this->gdaxSdkClient = new GdaxSdkClient();
        }

        return $this->gdaxSdkClient;
    }

    /**
     * @Route(
     *     "/v1/get-price",
     *     name="get_price",
     *     host="api.{DOMAIN}",
     *     defaults={"DOMAIN"="%DOMAIN%"},
     *     requirements={"DOMAIN"="%DOMAIN%"},
     *     methods={"GET"}
     *     )
     * )
     * @return JsonResponse
     * @throws \UnexpectedValueException
     * @throws \Symfony\Component\Security\Core\Exception\AccessDeniedException
     * @throws \InvalidArgumentException
     * @throws \RuntimeException
     */
    public function getPrice(): JsonResponse
    {
        $myPrice = $this->getGdaxSdkClient()->calcPrice();

        return new JsonResponse(['response' => $myPrice]);
    }
}
