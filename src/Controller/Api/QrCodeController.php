<?php
declare(strict_types=1);

namespace App\Controller\Api;

use Endroid\QrCode\Factory\QrCodeFactory;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

class QrCodeController extends AbstractController
{
    private $qrCode;

    /**
     * @return QrCodeFactory
     */
    private function getQr()
    {
        if (null === $this->qrCode) {
            $this->qrCode = new QrCodeFactory();
        }

        return $this->qrCode;
    }

    /**
     * @Route(
     *     "/v1/qr",
     *     name="qrgen",
     *     host="api.{DOMAIN}",
     *     defaults={"DOMAIN"="%DOMAIN%"},
     *     requirements={"DOMAIN"="%DOMAIN%"},
     *     methods={"POST"}
     *     )
     * )
     * @return JsonResponse
     */
    public function generateQr(Request $request): JsonResponse
    {
        $data = $request->get('content');
        $size = $request->get('size');
        $qrCode = $this->getQr();
        $createdQr = $qrCode->create($data, ['size' => $size]);

        return new JsonResponse($createdQr->writeDataUri());
    }
}
