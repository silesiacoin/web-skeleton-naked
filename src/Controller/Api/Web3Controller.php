<?php
declare(strict_types=1);

namespace App\Controller\Api;

use App\Entity\SilesiaCoin\SscAccount;
use App\Service\Web3Client;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;

class Web3Controller extends AbstractApiController
{
    /**
     * @Route(
     *     "/v1/ssc/balance/{address}",
     *     name="fetchAccountBalance",
     *     host="api.{DOMAIN}",
     *     defaults={"DOMAIN"="%DOMAIN%"},
     *     requirements={"DOMAIN"="%DOMAIN%"},
     *     methods={"GET"}
     *     )
     * )
     */
    public function fetchAccountBalance(string $address): JsonResponse
    {
        $web3Client = new Web3Client();
        $sscAccount = new SscAccount($address);
        $web3Client->getAccountBalance($sscAccount);

        return new JsonResponse([
            'address' => $sscAccount->getHexAddr(),
            'balance' => $sscAccount->getAmount() * pow(0.1, 18),
            'unit' => 'SSC'
        ]);
    }
}
