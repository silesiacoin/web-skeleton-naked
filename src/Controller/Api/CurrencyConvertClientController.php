<?php
declare(strict_types=1);

namespace App\Controller\Api;

use App\Service\CurrencyConvertClient;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

class CurrencyConvertClientController extends AbstractController
{
    private $currencyClient;

    private function getCurrencyConvertClient()
    {
        if (null === $this->currencyClient) {
            $this->currencyClient = new CurrencyConvertClient();
        }

        return $this->currencyClient;
    }

    /**
     * @Route(
     *     "/v1/currency-converter",
     *     name="currency_converter",
     *     host="api.{DOMAIN}",
     *     defaults={"DOMAIN"="%DOMAIN%"},
     *     requirements={"DOMAIN"="%DOMAIN%"},
     *     methods={"GET"}
     *     )
     * )
     * @return JsonResponse
     * @throws \UnexpectedValueException
     * @throws \Symfony\Component\Security\Core\Exception\AccessDeniedException
     * @throws \InvalidArgumentException
     * @throws \RuntimeException
     */
    public function getCurrencyConverter(): JsonResponse
    {
        $myConversion = $this->getCurrencyConvertClient()->getCurrencyExchangePrice();

        return new JsonResponse(['response' => $myConversion]);
    }
}
