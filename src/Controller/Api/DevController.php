<?php
declare(strict_types=1);
namespace App\Controller\Api;

use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Security\Csrf\CsrfTokenManagerInterface;

class DevController extends AbstractController
{
    /**
     * @var CsrfTokenManagerInterface
     */
    private $csrfTokenManager;

    public function __construct(CsrfTokenManagerInterface $csrfTokenManager)
    {
        $this->csrfTokenManager = $csrfTokenManager;
    }

    /**
     * @Route(
     *     "/v1/csrf/{tokenName}",
     *     name="csrf_dev_gen",
     *     host="api.{DOMAIN}",
     *     defaults={"DOMAIN"="%DOMAIN%"},
     *     requirements={"DOMAIN"="%DOMAIN%"},
     *     methods={"GET"}
     *     )
     * )
     */
    public function getCsrfToken(Request $request): JsonResponse
    {
        if ('prod' === getenv('APP_ENV')) {
            return new JsonResponse(['status' => 'access denied'], Response::HTTP_FORBIDDEN);
        }

        $token = $this->csrfTokenManager->refreshToken($request->get('tokenName'));

        return new JsonResponse([
            'tokenId' => $token->getId(),
            'tokenValue' => $token->getValue()
        ]);
    }
}
