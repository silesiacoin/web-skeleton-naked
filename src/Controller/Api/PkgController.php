<?php
declare(strict_types=1);
namespace App\Controller\Api;

use App\Factory\UserFactory;
use App\Service\AwsS3VersionClient;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RedirectResponse;

class PkgController extends AbstractController
{
    /** @var AwsS3VersionClient */
    private $versionClient;

    public function __construct(AwsS3VersionClient $awsS3VersionClient)
    {
        $this->versionClient = $awsS3VersionClient;
    }

    /**
     * @Route(
     *     "/pkg/{osVersion}/download/{pkgName}",
     *     name="pkg_download",
     *     host="api.{DOMAIN}",
     *     defaults={"DOMAIN"="%DOMAIN%"},
     *     requirements={"DOMAIN"="%DOMAIN%"}
     *     )
     * )
     */
    public function getPackage(string $pkgName, string $osVersion) : RedirectResponse
    {
        $pkgUrl = $this->versionClient->getCurrentPackageVersionByOs(
            $pkgName,
            $osVersion,
            UserFactory::apiClientUser()
        );

        return new RedirectResponse($pkgUrl);
    }

    /**
     * @Route(
     *     "/pkg/{pkgName}/list",
     *     name="api_pkg_list",
     *     host="api.{DOMAIN}",
     *     defaults={"DOMAIN"="%DOMAIN%"},
     *     requirements={"DOMAIN"="%DOMAIN%"}
     *     )
     * )
     */
    public function getPackageList(string $pkgName) : JsonResponse
    {
        return new JsonResponse($this->versionClient->getCurrentPackageVersion(
            $pkgName,
            UserFactory::apiClientUser()
        ));
    }
}
