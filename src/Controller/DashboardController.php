<?php
declare(strict_types=1);

namespace App\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class DashboardController extends AbstractTemplateController
{
    /**
     * @Route("/dashboard")
     */
    public function showDashboard(Request $request)
    {
        return $this->render('Front/react.html.twig');
    }
}
