<?php
declare(strict_types=1);

namespace App\Controller;

use App\Service\AwsS3VersionClient;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;

class DownloadControler extends AbstractController
{
    /** @var AwsS3VersionClient */
    private $versionClient;

    public function __construct(AwsS3VersionClient $awsS3VersionClient)
    {
        $this->versionClient = $awsS3VersionClient;
    }

    /**
     * @Route(
     *     "/client/{pkgName}",
     *     name="client_downloads",
     *     host="download.{DOMAIN}",
     *     defaults={"DOMAIN"="%DOMAIN%"},
     *     requirements={"DOMAIN"="%DOMAIN%"}
     *     )
     * )
     * @param string $pkgName
     * @return Response
     * @throws \Exception
     */
    public function showClientDownloads(string $pkgName) : Response
    {
        $currentVersion = $this->getCurrentVersion($pkgName);

        return $this->render(
            'Download/download.html.twig',
            [
                'currentVersion' => $currentVersion
            ]
        );
    }

    /**
     * @param string $pkgName
     * @return array
     * @throws \Exception
     */
    private function getCurrentVersion(string $pkgName) : array
    {
        $currentVersion = $this->versionClient->getCurrentPackageVersion($pkgName, $this->getUser());
        $currentVersion['title'] = $pkgName;
        $currentVersion['body'] = $this->getCurrentVersionBody();

        return $currentVersion;
    }

    private function getCurrentVersionBody() : string
    {
        return <<<HTML
<p>        
    Proof of concept version with full functionalities as blockchain client.
</p>
<p>
    Able to: <br/>
    mine, start bootstrap, send transactions
</p>
HTML;
    }
}
