<?php
declare(strict_types=1);

namespace App\Controller;

use Psr\Container\ContainerExceptionInterface;
use Psr\Container\NotFoundExceptionInterface;
use Symfony\Component\HttpFoundation\Cookie;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use App\Service\LeadVerifier;
use App\Service\TwitterSdkClient;

class DefaultController extends AbstractTemplateController
{
    private $leadVerifier;

    /**
     * @Route("/", name="home")
     */
    public function indexAction()
    {
        return $this->render('Front/react.html.twig');
    }

    /**
     * @Route("/miners")
     */
    public function showMinerPage()
    {
        return $this->render('Front/react.html.twig');
    }

    /**
     * @Route("/business")
     */
    public function showBusinessEarningPage()
    {
        return $this->render('Front/react.html.twig');
    }

    /**
     * @Route("/affiliatenetwork")
     */
    public function showAffiliateNetwork()
    {
        return $this->render('Front/react.html.twig', [
            'REV_DATE' => getenv('REV_DATE')
        ]);
    }

    /**
     * @Route("/contact", name="post_store_contact", methods={"POST"})
     * @param Request $request
     * @return JsonResponse
     * @throws \RuntimeException
     * @throws \Psr\Container\ContainerExceptionInterface
     */
    public function subscribeContact(Request $request): ?JsonResponse
    {
        try {
            return new JsonResponse(
                $this->getLeadVerifier()
                ->leadSubscriber($request->get('email'), $request->getClientIp())
            );
        } catch (NotFoundExceptionInterface $exception) {
            throw new \RuntimeException($exception->getMessage());
        } catch (ContainerExceptionInterface $exception) {
            throw new \RuntimeException($exception->getMessage());
        }
    }

    public function changeUserLanguage()
    {
        $response = new Response();
        $headers = &$response->headers;
        $headers->setCookie(new Cookie('_ssc_language', 'de'));

        return $this->render('', [], $response);
    }

    /**
     * @return LeadVerifier
     * @throws \InvalidArgumentException
     */
    private function getLeadVerifier()
    {
        if (null === $this->leadVerifier) {
            $this->leadVerifier = new LeadVerifier();
        }

        return $this->leadVerifier;
    }
}
