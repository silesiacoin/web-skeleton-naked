<?php
declare(strict_types=1);

namespace Axd\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Cookie;
use Symfony\Component\HttpFoundation\Response;

abstract class AbstractTemplateController extends AbstractController
{
    protected function render(string $view, array $parameters = array(), Response $response = null): Response
    {
        if (null === $response) {
            $response = new Response();
        }

        $this->dispatchCookies($response);

        return parent::render($view, $parameters, $response);
    }

    private function dispatchCookies(Response &$response): void
    {
        $container = $this->container;
        $requestStack = $container->get('request_stack');
        $request = $requestStack->getCurrentRequest();
        $requestAttributes = $request->attributes;
        $languageFromCookie = $requestAttributes->get('ssc_language_dispatch');

        if (\is_string($languageFromCookie)) {
            $this->prepareSscLanguage30DaysCookie($languageFromCookie, $response);
        }
    }

    private function prepareSscLanguage30DaysCookie(string $languageFromCookie, Response &$response): void
    {
        $headers = &$response->headers;
        $headers->add(['Set-Cookie', "ssc_language=$languageFromCookie; path=/;"]);
        $headers->setCookie(new Cookie(
            'ssc_language',
            $languageFromCookie,
            time() + (30 * 24 * 60 * 60),
            '/',
            null,
            false,
            false
        ));
    }
}
