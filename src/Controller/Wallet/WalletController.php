<?php
declare(strict_types=1);

namespace App\Controller\Wallet;

use App\Entity\SilesiaCoin\SscAccount;
use App\Exception\FetchWalletFileException;
use App\Exception\S3WalletNotAllowedException;
use App\Exception\ToManyWalletException;
use App\Service\AwsS3WalletClient;
use App\Service\Web3Client;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\Finder\Finder;
use Symfony\Component\Finder\SplFileInfo;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class WalletController extends AbstractController
{
    /** @var Web3Client */
    private $web3Client;

    /** @var AwsS3WalletClient */
    private $s3Client;

    public function __construct(Web3Client $web3, AwsS3WalletClient $s3Client)
    {
        $this->web3Client = $web3;
        $this->s3Client = $s3Client;
    }

    /**
     * @Route(
     *     "/list",
     *     name="sscShowWallets",
     *     host="wallet.{DOMAIN}",
     *     defaults={"DOMAIN"="%DOMAIN%"},
     *     requirements={"DOMAIN"="%DOMAIN%"}
     *     )
     * )
     * @return Response
     * @throws \Exception
     */
    public function showWalletPage() : Response
    {
        $wallets = $this->getWallets();
        $canAddWallet = count($wallets) < 10;

        foreach ($wallets as $wallet) {
            $this->web3Client->getAccountBalance($wallet);
        }

        return $this->render('/Account/dashboard/wallet/wallet.html.twig', [
            'title' => 'Twój Portfel',
            'wallets' => $wallets,
            'canAddWallet' => $canAddWallet,
        ]);
    }

    /**
     * @Route(
     *     "/acc/create",
     *     name="sscCreateAcc",
     *     host="wallet.{DOMAIN}",
     *     defaults={"DOMAIN"="%DOMAIN%"},
     *     requirements={"DOMAIN"="%DOMAIN%"}
     *     )
     * )
     * @return Response
     * @throws \Exception
     */
    public function createAccount(Request $request) : Response
    {
        $pass = $request->get('ssc_pass');

        if (null !== $pass && \is_string($pass)) {
            $this->checkWalletsLimits();
            $response = $this->web3Client->createAccount($pass);
            $sscAccount = $this->storeKeyFromTemp((string) $response);

            return $this->render('/Account/dashboard/standalone/standaloneWindow.html.twig', [
                'title' => 'pomyślnie utworzono konto dla SSC',
                'content' => sprintf('Numer konta: %s', $sscAccount->getHexAddr())
            ]);
        }

        return $this->render('/Account/dashboard/standalone/createWallet.html.twig', [
            'title' => 'Stwórz konto SSC'
        ]);
    }

    /**
     * @Route(
     *     "/acc/fetch/{accountAddr}",
     *     name="sscFetchAcc",
     *     host="wallet.{DOMAIN}",
     *     defaults={"DOMAIN"="%DOMAIN%"},
     *     requirements={"DOMAIN"="%DOMAIN%"}
     *     )
     * )
     * @return Response
     * @throws \Exception
     */
    public function getSscAccount($accountAddr) : Response
    {
        try {
            $url = $this->s3Client->getWallet($this->getUser(), $accountAddr);
        } catch (S3WalletNotAllowedException $exception) {
            return $this->render('/Account/dashboard/standalone/standaloneWindow.html.twig', [
                'title' => 'Nie znaleziono',
                'content' => 'Nie masz dostępu do portfela, lub nie posiadamy danego portfela u Nas. Sprawdź adres.'
            ]);
        }

        return $this->render('/Account/dashboard/standalone/standaloneWindow.html.twig', [
            'title' => 'Pobierz portfel',
            'content' => $this->openWalletHtml($url),
        ]);
    }

    private function openWalletHtml(string $url) : string
    {
        return <<<HTML
     <h2>Pobierasz portfel Silesiacoin</h2>
     <a href="{$url}" target="_blank" style="text-decoration: none;">POBIERZ</a>
     <h3>Pamiętaj!</h3>
     <p>Nie udostępniaj nikomu tego linku. Link jest ważny przez 24h.</p>
     <p>Odblokowanie portfela jest możliwe jedynie za pomocą hasła, ustalonego przy jego kreacji.</p>
     <p>Nie jest możliwe odzyskanie hasła do portfela. Tracąc hasło <strong>tracisz środki.</strong></p>
     <p>Zalecamy zrobić backup w minimum trzech miejscach.</p>
     <h4>Nie bierzemy odpowiedzialności za portfele przetrzymywane w serwisie silesiacoin.org.</h4>
     <p>Jest to jedynie udogodnienie, które ma pomóc w korzystaniu z konta.</p>
    <p>W razie problemów: <a href="mailto:kontakt@silesiacoin.org">kontakt@silesiacoin.org</a></p>
HTML;
    }

    /**
     * @param string $rpcResponse
     * @return SscAccount
     * @throws \Exception
     */
    private function storeKeyFromTemp(string $rpcResponse) : SscAccount
    {
        $data = json_decode($rpcResponse, true);

        if (!isset($data['result'])) {
            throw new \RuntimeException('Invalid response from JSONRPC');
        }

        if ('dev' === getenv('APP_ENV')) {
            return new SscAccount($data['result']);
        }

        return $this->getWalletFile($data['result']);
    }

    /**
     * @param string $walletAdress
     * @return SscAccount
     * @throws \Exception
     */
    private function getWalletFile(string $walletAdress) : SscAccount
    {
        $sscAccount = new SscAccount($walletAdress);
        $listInKeystore = $this->fetchWalletFileList($walletAdress);

        foreach ($listInKeystore as $file) {
            $sscAccount = $this->handleWalletFile($file);
        }

        return $sscAccount;
    }

    private function fetchWalletFileList(string $walletAddress) : Finder
    {
        $finder = new Finder();
        $serviceContainer = $this->get('service_container');
        $kernel = $serviceContainer->get('kernel');
        $rootDir = $kernel->getRootDir();
        $keyStoreDir = sprintf('%s/../temp/keystore', $rootDir);
        $walletAddress = str_replace('0x', '', $walletAddress);
        $files = $finder->files();
        $fileList = $files->name(sprintf('*%s*', $walletAddress));
        $listInKeystore = $fileList->in($keyStoreDir);

        if ($listInKeystore->count() < 1) {
            throw new FetchWalletFileException("$keyStoreDir is empty");
        }

        return $listInKeystore;
    }

    private function handleWalletFile(SplFileInfo $file) : SscAccount
    {
        $filesystem = new Filesystem();
        $fileContent = file_get_contents($file->getRealPath());
        $sscAccount = $this->s3Client->storeWallet($file->getRelativePathname(), $fileContent, $this->getUser());
        $filesystem->remove($file->getRealPath());

        return $sscAccount;
    }

    private function getWallets() : array
    {
        $user = $this->getUser();

        return $this->s3Client->listWallets($user);
    }

    private function checkWalletsLimits(int $limit = 10) : void
    {
        $wallets = $this->getWallets();

        if (count($wallets) >= $limit) {
            throw new ToManyWalletException(sprintf('One user cannot have more wallets then %s.', $limit));
        }
    }
}
