$(document).ready(function() {
    $('.link').on('click', function() {
        $('.link').removeClass('active');
        $(this).toggleClass('active');
    });
});

var c1 = document.getElementById("c1");
var parent = document.getElementById("p1");
c1.width = parent.offsetWidth;
c1.height = parent.offsetHeight;

var data1 = {
    labels : ["00","10","20","30","40","50","60"],
    datasets : [
        {
            fillColor : "rgba(200,150,150,0.5)",
            strokeColor : "rgba(100,100,100,0.8)",
            pointColor : "#0af",
            pointStrokeColor : "rgba(255,255,255,1)",
            data : [150,200,235,290,300,350, 450]
        }
    ]
}

var options1 = {
    scaleFontColor : "rgba(100,100,100,1)",
    scaleLineColor : "rgba(120,120,120,1)",
    scaleGridLineColor : "transparent",
    bezierCurve : false,
    scaleOverride : true,
    scaleSteps : 5,
    scaleStepWidth : 100,
    scaleStartValue : 0
}

new Chart(c1.getContext("2d")).Line(data1,options1)


var pieData = [
    {
        value: 300,
        color:"#F7464A",
        highlight: "#FF5A5E",
        label: "Red"
    },
    {
        value: 50,
        color: "#46BFBD",
        highlight: "#5AD3D1",
        label: "Green"
    },
    {
        value: 100,
        color: "#FDB45C",
        highlight: "#FFC870",
        label: "Yellow"
    },
    {
        value: 40,
        color: "#949FB1",
        highlight: "#A8B3C5",
        label: "Grey"
    },
    {
        value: 120,
        color: "#4D5360",
        highlight: "#616774",
        label: "Dark Grey"
    }

];



var radarChartData = {
    labels: ["Eating", "Drinking", "Sleeping", "Designing", "Coding", "Cycling", "Running"],
    datasets: [
        {
            label: "My First dataset",
            fillColor: "rgba(220,220,220,0.2)",
            strokeColor: "rgba(220,220,220,1)",
            pointColor: "rgba(220,220,220,1)",
            pointStrokeColor: "#fff",
            pointHighlightFill: "#fff",
            pointHighlightStroke: "rgba(220,220,220,1)",
            data: [65,59,90,81,56,55,40]
        },
        {
            label: "My Second dataset",
            fillColor: "rgba(151,187,205,0.2)",
            strokeColor: "rgba(151,187,205,1)",
            pointColor: "rgba(151,187,205,1)",
            pointStrokeColor: "#fff",
            pointHighlightFill: "#fff",
            pointHighlightStroke: "rgba(151,187,205,1)",
            data: [28,48,40,19,96,27,100]
        }
    ]
};
var polarData = [
    {
        value: 300,
        color:"#F7464A",
        highlight: "#FF5A5E",
        label: "Red"
    },
    {
        value: 50,
        color: "#46BFBD",
        highlight: "#5AD3D1",
        label: "Green"
    },
    {
        value: 100,
        color: "#FDB45C",
        highlight: "#FFC870",
        label: "Yellow"
    },
    {
        value: 40,
        color: "#949FB1",
        highlight: "#A8B3C5",
        label: "Grey"
    },
    {
        value: 120,
        color: "#4D5360",
        highlight: "#616774",
        label: "Dark Grey"
    }

];
// Colour variables
var red = "#bf616a",
    blue = "#5B90BF",
    orange = "#d08770",
    yellow = "#ebcb8b",
    green = "#a3be8c",
    teal = "#96b5b4",
    pale_blue = "#8fa1b3",
    purple = "#b48ead",
    brown = "#ab7967";
var colours = {
    "Core": blue,
    "Line": orange,
    "Bar": teal,
    "Polar Area": purple,
    "Radar": brown,
    "Doughnut": green
};
function Colour(col, amt) {

    var usePound = false;

    if (col[0] == "#") {
        col = col.slice(1);
        usePound = true;
    }

    var num = parseInt(col,16);

    var r = (num >> 16) + amt;

    if (r > 255) r = 255;
    else if  (r < 0) r = 0;

    var b = ((num >> 8) & 0x00FF) + amt;

    if (b > 255) b = 255;
    else if  (b < 0) b = 0;

    var g = (num & 0x0000FF) + amt;

    if (g > 255) g = 255;
    else if (g < 0) g = 0;

    return (usePound?"#":"") + (g | (b << 8) | (r << 16)).toString(16);

}
var doughnutData = [
    {
        value: 7.57,
        color: colours["Core"],
        highlight: Colour(colours["Core"], 10),
        label: "Core"
    },

    {
        value: 1.63,
        color: colours["Bar"],
        highlight: Colour(colours["Bar"], 10),
        label: "Bar"
    },

    {
        value: 1.09,
        color: colours["Doughnut"],
        highlight: Colour(colours["Doughnut"], 10),
        label: "Doughnut"
    },

    {
        value: 1.71,
        color: colours["Radar"],
        highlight: Colour(colours["Radar"], 10),
        label: "Radar"
    },

    {
        value: 1.64,
        color: colours["Line"],
        highlight: Colour(colours["Line"], 10),
        label: "Line"
    },

    {
        value: 1.37,
        color: colours["Polar Area"],
        highlight: Colour(colours["Polar Area"], 10),
        label: "Polar Area"
    }
];
var randomScalingFactor = function(){ return Math.round(Math.random()*100)};

var barChartData = {
    labels : ["January","February","March","April","May","June","July"],
    datasets : [
        {
            fillColor : "rgba(220,220,220,0.5)",
            strokeColor : "rgba(220,220,220,0.8)",
            highlightFill: "rgba(220,220,220,0.75)",
            highlightStroke: "rgba(220,220,220,1)",
            data : [randomScalingFactor(),randomScalingFactor(),randomScalingFactor(),randomScalingFactor(),randomScalingFactor(),randomScalingFactor(),randomScalingFactor()]
        },
        {
            fillColor : "rgba(151,187,205,0.5)",
            strokeColor : "rgba(151,187,205,0.8)",
            highlightFill : "rgba(151,187,205,0.75)",
            highlightStroke : "rgba(151,187,205,1)",
            data : [randomScalingFactor(),randomScalingFactor(),randomScalingFactor(),randomScalingFactor(),randomScalingFactor(),randomScalingFactor(),randomScalingFactor()]
        }
    ]

}
var lineChartData = {
    labels : ["January","February","March","April","May","June","July"],
    datasets : [
        {
            label: "My First dataset",
            fillColor : "rgba(220,220,220,0.2)",
            strokeColor : "rgba(220,220,220,1)",
            pointColor : "rgba(220,220,220,1)",
            pointStrokeColor : "#fff",
            pointHighlightFill : "#fff",
            pointHighlightStroke : "rgba(220,220,220,1)",
            data : [randomScalingFactor(),randomScalingFactor(),randomScalingFactor(),randomScalingFactor(),randomScalingFactor(),randomScalingFactor(),randomScalingFactor()]
        },
        {
            label: "My Second dataset",
            fillColor : "rgba(151,187,205,0.2)",
            strokeColor : "rgba(151,187,205,1)",
            pointColor : "rgba(151,187,205,1)",
            pointStrokeColor : "#fff",
            pointHighlightFill : "#fff",
            pointHighlightStroke : "rgba(151,187,205,1)",
            data : [randomScalingFactor(),randomScalingFactor(),randomScalingFactor(),randomScalingFactor(),randomScalingFactor(),randomScalingFactor(),randomScalingFactor()]
        }
    ]

}

window.onload = function(){
    var ctx = document.getElementById("canvas-line").getContext("2d");
    window.myLine = new Chart(ctx).Line(lineChartData, {
        responsive: true
    });

    var ctx = document.getElementById("canvas-bar").getContext("2d");
    window.myBar = new Chart(ctx).Bar(barChartData, {
        responsive : true
    });

    var ctx = document.getElementById("chart-area-nut").getContext("2d");
    window.myDoughnut = new Chart(ctx).Doughnut(doughnutData, {responsive : true});

    var ctx = document.getElementById("chart-area-nut1").getContext("2d");
    window.myDoughnut = new Chart(ctx).Doughnut(doughnutData, {responsive : true});

    var ctx = document.getElementById("chart-area-nut2").getContext("2d");
    window.myDoughnut = new Chart(ctx).Doughnut(doughnutData, {responsive : true});

    var ctx = document.getElementById("chart-area-polar").getContext("2d");
    window.myPolarArea = new Chart(ctx).PolarArea(polarData, {
        responsive:true
    });

    window.myRadar = new Chart(document.getElementById("canvas").getContext("2d")).Radar(radarChartData, {
        responsive: true
    });
    var ctx = document.getElementById("chart-area2").getContext("2d");
    window.myPie = new Chart(ctx).Pie(pieData);
    var ctx = document.getElementById("chart-area").getContext("2d");
    window.myPie = new Chart(ctx).Pie(pieData);
};